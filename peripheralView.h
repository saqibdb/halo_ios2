//
//  peripheralView.h
//  LookUp
//
//  Created by Divey Punj on 17/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "SERVICES.h"
#import "ServerQueryBridge.h"
#import <SAMKeychain/SAMKeychain.h>

@interface peripheralView : NSObject <CBPeripheralManagerDelegate>


@property (strong, nonatomic)CBPeripheralManager *peripheralManager;
@property (strong, nonatomic) CBMutableCharacteristic *transferCharacteristic, *recieverCharacteristicOtherId, *recieverCharacteristicOtherAnswer;
@property (strong, nonatomic) NSData *dataToSend;
@property (nonatomic, readwrite) NSInteger sendDataIndex;

- (void)initiliase;
-(void)stopAdvertising;
-(void)startAdvertising;

@end
