//
//  beaconReceiverClass.m
//  LookUp
//
//  Created by Divey Punj on 15/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "beaconReceiverClass.h"
#import "centralView.h"


centralView *centralViewClass;

@implementation beaconReceiverClass
@synthesize beaconRegion, locationManager;

-(void)initialise{
    NSLog(@"Looking for beacons");
    locationManager= [[CLLocationManager alloc]init];
    locationManager.delegate= self;
    
    //Create proximity uuid
    NSUUID *proximityUUID= [[NSUUID alloc]initWithUUIDString:TRANSFER_SERVICE_UUID];
    
    //Create a region
    beaconRegion= [[CLBeaconRegion alloc]initWithProximityUUID:proximityUUID identifier:@"com.mpdp.LookUp"];
    
    //Start monitoring a region
    if([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]){
        [locationManager requestAlwaysAuthorization];
    }
    [locationManager startMonitoringForRegion:beaconRegion];
    
    [locationManager startRangingBeaconsInRegion:beaconRegion];
    
    centralViewClass= [[centralView alloc]init];
}

//This method is called when device enters a region
-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(nonnull CLRegion *)region{
    
    //NSLog(@"Region entered");
    
    //Start ranging for iBeacons
    //[locationManager startRangingBeaconsInRegion:beaconRegion];
    
}

-(void)locationManager: (CLLocationManager *)manager didRangeBeacons:(nonnull NSArray<CLBeacon *> *)beacons inRegion:(nonnull CLBeaconRegion *)region{
    
    CLBeacon *beacon= [[CLBeacon alloc]init];
    beacon = [beacons lastObject];
    
    //NSLog(@"UUID: %@",beacon.proximityUUID.UUIDString);
    //NSLog(@"Beacon accuracy: %f",beacon.accuracy);
    
    if(beacon.accuracy>0 && beacon.accuracy < .1){
        NSLog(@"I have been tapped");
        
        [centralViewClass initialise];
        //[bluetoothClassForBeaconRecog initialiseBluetooth];
        //[bluetoothClassForBeaconRecog searchDevicesUsingBluetooth];
    }
    
    /*switch (beacon.proximity) {
        case CLProximityUnknown:
            NSLog(@"Unknown Proximity");
            break;
        case CLProximityImmediate:
            NSLog(@"Immediate");
            break;
        case CLProximityNear:
            NSLog(@"Near");
            break;
        case CLProximityFar:
            NSLog(@"Far");
            break;
            
        default:
            break;
    }*/
    
    //NSLog(@"Beacon RSSI: %li decibels", (long)beacon.rssi);
    
}













@end
