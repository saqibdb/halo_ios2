//
//  emailVerificationViewController.m
//  LookUp
//
//  Created by Divey Punj on 17/05/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "emailVerificationViewController.h"

ServerQueryBridge *serverForEV;
NSString* entityId_EV, *randomCodeStr;
NSManagedObjectContext *context_EV;
NSManagedObject *managedDataObject_EV;
AppDelegate *delegate_EV;
NSFetchRequest *userDataRequest_EV;
NSArray *userDataArray_EV;
BOOL profileSaved;

@interface emailVerificationViewController ()

@end

@implementation emailVerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    serverForEV=[[ServerQueryBridge alloc]init];
    [serverForEV initialiseBackend];
    
    
    //Get entityId and username from key chain
    entityId_EV = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    //Initialise Core data object
    NSError *error;
    context_EV = [self managedObjectContext];
    userDataRequest_EV= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_EV.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_EV];
    userDataArray_EV = [context_EV executeFetchRequest:userDataRequest_EV error:&error];
    managedDataObject_EV= [userDataArray_EV objectAtIndex:0];
    
    [serverForEV initialiseCoreData:entityId_EV];
    
    //Tap gesture to remove keyboard
    UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeKeyboard:)];
    tap.cancelsTouchesInView= NO;
    [self.view addGestureRecognizer:tap];
    
    profileSaved= NO;
    
    if(_userEmail){
        [self generateRandomCode:_userEmail];
    }
    else{
        [self generateRandomCode:[managedDataObject_EV valueForKey:@"email"]];
    }
    
    if([[managedDataObject_EV valueForKey:@"interest"]isEqualToString:@""] || [managedDataObject_EV valueForKey:@"interest"]==nil){
        _skipBut.hidden= NO;
    }
    else{
        _skipBut.hidden= YES;
    }
    
    [self hideChangeEmailView];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    if(profileSaved){
        profileSaved= NO;
        //Show tutorial from here
        [self performSegueWithIdentifier:@"EVToTutSegue" sender:self];
    }
}

-(void)showChangeEmailView{
    _emailChangeView.hidden= NO;
    _emailChangeText.hidden= NO;
    _emailChangeText.text= @"";
}

-(void)hideChangeEmailView{
    _emailChangeView.hidden= YES;
    _emailChangeText.hidden= YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)generateRandomCode: (NSString*)email{
    
    int randomCode = arc4random_uniform(10000) + 10000;
    randomCodeStr= [NSString stringWithFormat:@"%d",randomCode];
    
    //Save the random code to device
    [[NSUserDefaults standardUserDefaults] setObject:randomCodeStr forKey:@"verifiedUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *message= [NSString stringWithFormat:@"Your Halo access code is %d",randomCode];
    message= [message stringByAppendingString:@". Please enter this code in the Halo app to proceed."];
    
    [serverForEV sendEmailVerification:email emailBody:message];
    
}


- (IBAction)doneAction:(id)sender {
    //Check if user is now active, then dismiss view controller
    
    if([_accessCodeText.text isEqualToString:@""] || _accessCodeText.text==nil){
        [self popMessage:@"The access code cannot be empty" title:@"Error"];
    }
    else{
        if([_accessCodeText.text isEqualToString:randomCodeStr]){
            //Access code matches, email verified
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(signUpCompleted:) name:@"signUpCompleted" object:nil];
            [self performSegueWithIdentifier:@"EVToProfSetupSegue" sender:self];
            [managedDataObject_EV setValue:@"Yes" forKey:@"verifiedUser"];
            [self saveData];
            
            //Set the randomCode on device to non
            [[NSUserDefaults standardUserDefaults] setObject:@"none" forKey:@"verifiedUser"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else{
            //Access not matching
            [self popMessage:@"This access code is not correct. Please try again, or request a new access code to be sent." title:@"Error"];
        }
    }
}


-(void)popMessage:(NSString*)message title:(NSString*)title{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)resendCodeAction:(id)sender {
    
    if(_userEmail){
        [self generateRandomCode:_userEmail];
    }
    else{
        [self generateRandomCode:[managedDataObject_EV valueForKey:@"email"]];
    }
    [self popMessage:@"A new access code has been sent to your email." title:@"Note"];
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void)signUpCompleted:(NSNotification*)notification{
    profileSaved= YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"EVToProfSetupSegue"]){
        userSettings *setView= [segue destinationViewController];
        setView.fromView= @"signUp";
    }
}

-(void) closeKeyboard: (UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
}

-(void)saveData{
    NSError *error;
    if(![context_EV save:&error]){
        NSLog(@"Failed Saving:%@",error);
    }
    else{
        NSLog(@"Saved");
        [serverForEV saveData:@"" entityId:entityId_EV];
    }
}


- (IBAction)emailChangeAct:(id)sender {
    
    
    if([_emailChangeText.text isEqualToString:@""]||_emailChangeText.text== nil){
        [self popMessage:@"Email cannot be empty!" title:@"Error"];
    }
    else{
        //Update email on device
        [managedDataObject_EV setValue:_emailChangeText.text forKey:@"email"];
        [managedDataObject_EV setValue:@"No" forKey:@"verifiedUser"];
        //Save data
        [self saveData];
        //Save to server
        [serverForEV saveData:@"" entityId:entityId_EV];
        [serverForEV updateUsernameAndEmail:entityId_EV email:_emailChangeText.text];
        //Resend email
        [self generateRandomCode:_emailChangeText.text];
        [self popMessage:@"A new code has been sent to your email." title:@"Note"];
        [self hideChangeEmailView];
    }
    
}

- (IBAction)changeEmailActBut:(id)sender {
    [self showChangeEmailView];
}

- (IBAction)closeChangeEmailViewAct:(id)sender {
    [self hideChangeEmailView];
}

- (IBAction)skipAction:(id)sender {
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Note"
                                 message:@"Enjoy Halo! We will remind you after 3 days to verify your email."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(signUpCompleted:) name:@"signUpCompleted" object:nil];
                                   [self performSegueWithIdentifier:@"EVToProfSetupSegue" sender:self];
                                   [managedDataObject_EV setValue:@"Maybe" forKey:@"verifiedUser"];
                                   [self saveData];
                                   
                                   //Set the randomCode on device to non
                                   [[NSUserDefaults standardUserDefaults] setObject:@"none" forKey:@"verifiedUser"];
                                   [[NSUserDefaults standardUserDefaults] synchronize];


                               }];
    
    UIAlertAction* cancelBut = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    [alert addAction:okButton];
    [alert addAction:cancelBut];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
