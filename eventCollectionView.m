//
//  eventCollectionView.m
//  LookUp
//
//  Created by Divey Punj on 30/01/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "eventCollectionView.h"

@implementation eventCollectionView


-(void)setCell:(NSString*)name venue:(NSString*)venue date:(NSString*)date month:(NSString*)month imageURL:(NSString*)imageUrl{
    
    _event_name.text= name;
    _event_venue.text= venue;
    _event_date.text= date;
    _event_month.text=  month;
    
    NSURL *url= [NSURL URLWithString:imageUrl];
    
    if(imageUrl){
        [_event_image sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    }
    
    /*if(image){
        _event_image.image= image;
    }
    else{
        //Nothing
    }*/
}

- (IBAction)eventCheckAction:(id)sender {
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://www.facebook.com/events/242796666161263/"]];
    
    
}





@end
