//
//  camOverlayController.h
//  LookUp
//
//  Created by Divey Punj on 12/05/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface camOverlayController : UIViewController <UIImagePickerControllerDelegate>

#define photoViewXPos 110
#define photoViewYPos 102
#define photoViewWidLarge 155
#define photoViewHigLarge 145



@property (strong, nonatomic) UIImagePickerController *barCodePhotoRef;

@property (strong, nonatomic) IBOutlet UIView *photoView;
@property (strong, nonatomic) IBOutlet UIButton *takePhotoBut;


- (IBAction)takePhotoAction:(id)sender;



@end
