//
//  promotionViewController.h
//  LookUp
//
//  Created by Divey Punj on 22/5/17.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface promotionViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *percentLab;
@property (strong, nonatomic) IBOutlet UILabel *additionalInfoLab;

- (IBAction)doneAction:(id)sender;




@end
