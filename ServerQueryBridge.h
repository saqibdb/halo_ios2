//
//  ServerQueryBridge.h
//  LookUp
//
//  Created by Divey Punj on 14/02/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "Kinvey-Swift.h"
#import <Foundation/Foundation.h>

//#import "signUp.h"

@class serverBridgeClass;

@interface ServerQueryBridge : NSObject


-(void)connectServerToUserClass;
-(void)serverPing;
-(void)createUser: (NSString*)username password:(NSString*)password;
-(BOOL)checkActiveUser;
-(void)initialiseBackend;
-(void)initialiseCoreData:(NSString*)entityId;
-(void)saveData:(NSString*)notiName entityId:(NSString*)entityId;
-(void)getOtherUsers: (int) distance gender:(NSString*)gender age_from:(NSString*)age_from age_to:(NSString*)age_to ort:(NSString*)ortin;


-(void)addUserPlaceHolder: (NSString*)username;
-(void)logOutUser;
-(void)loginUser: (NSString*)username password: (NSString*)password;
-(void)getChatMatesInfo: (NSMutableArray*)mateId;
-(void)getChatMatesArray: (NSString*)eID;
-(void)saveMessagesToServer: (NSString*)messageId reciptId:(NSString*)recipientId sendrIds: (NSString*)senderIds text:(NSString*)messageText date:(NSDate*)timeStamp;
-(void)getMessagesFromServerAsSender: (NSString*)senderId recipient:(NSString*)recipientId skipCount:(int)skipCount notiName:(NSString*)notiName;

-(void)loadMyProfile:(NSString*)entityId notiName:(NSString*)notiName;

-(void)fetchNearByEvents: (double)distance notiName:(NSString*)notiName keyName:(NSString*)keyName;
-(void)writeToEvent:(NSString*)eventId userId:(NSString*)userId attendees: (NSString*)attendees;
-(void)getMyEventsInFull:(NSMutableArray*)myEvents;
-(void)getUserWithId: (NSString*)entityId notiName:(NSString*)notiName keyName:(NSString*)keyName;
-(void)createUserUsingFacebook: (NSDictionary*)facebookDict accessToken:(NSString*)accessToken;
-(void)uploadSmallImage:(UIImage*)image entityId: (NSString*)eID notiName:(NSString*)notiName name:(NSString*)name;
-(void)uploadLargeImage:(UIImage*)image entityId: (NSString*)eID notiName:(NSString*)notiName name:(NSString*)name;
-(void)downloadImage:(NSString*)eID;
-(void)downloadImageAsCachedByName: (NSString*)fileName;
-(void)downloadLargeImageByName:(NSString*)filename notificationName: (NSString*)notiName keyName:(NSString*)keyName;
-(void)downloadSmallImageByNames:(NSMutableArray*)fileIds notiName: (NSString*)notiName keyName:(NSString*)keyName;
-(void)fetchNearByEventsForRegionMon: (double)distance notiName:(NSString*)notiName keyName:(NSString*)keyName;
-(void)connectUserToHaloMessenger:(NSString*)email userId: (NSString*)userId;
-(void)downloadLargeImageFromEvents:(NSString*)filename notificationName: (NSString*)notiName keyName:(NSString*)keyName;
-(void)downloadProfilePicture:(NSString*)filename;
-(void)downloadLargeImageByNames:(NSMutableArray*)filenames notificationName: (NSString*)notiName keyName:(NSString*)keyName;
-(void)downloadImagesForEvents:(NSArray*)fileIds notificationName: (NSString*)notiName keyName:(NSString*)keyName;
-(void)getOtherUserProfiles:(int) skipCount gender:(NSString*)gender age_from:(NSString*)age_from age_to:(NSString*)age_to ort:(NSString*)ortin status:(NSString*)status;
-(void)resetPassword:(NSString*)email;
-(void)addAdditionalUserInfo:(NSString*)entityId email:(NSString*)email;
-(void)addVenueAnalyticsData:(NSString*)entId checkedInTime:(NSDate*)checkedInTime checkedOutTime:(NSDate*)checkedOutTime venueName:(NSString*)venueName eventName:(NSString*)eventName userLocLat:(NSString*)userLocLat userLocLon:(NSString*)userLocLon;
-(void)connectUsers:(NSString*)friendID myId:(NSString*)myId myName:(NSString*)myName friendName:(NSString*)friendName friendStatus:(NSString*)friendStatus venueName:(NSString*)venueName timeStamp:(NSString*)timeStamp isHalo:(NSString*)isHalo myStatus:(NSString*)myStatus notiName: (NSString*)notiName;
-(void)friendRequestResponse:(NSString*)myId friendId:(NSString*)friendId myResponse:(NSString*)myResponse;
-(void)sendEmailVerification:(NSString*)email emailBody:(NSString*)emailBody;
-(void)updateUsernameAndEmail:(NSString*)entityId email:(NSString*)email;
-(void)getMyFriendsList:(NSString*)entityId notiName:(NSString*)notiName;
-(void)removeExistingFriend:(NSString*)friendId myId:(NSString*)myId;
-(void)canCheckIn:(double)lat lon:(double)lon isCheckedIn:(NSString*)isCheckedIn isActive:(NSString*)isActive notiName:(NSString*)notiName;
-(void)checkForExistingMessages:(NSString*)recipient senderId:(NSString*)senderId text:(NSString*)text timestamp:(NSDate*)timestamp notiName:(NSString*)notiName;
-(void)verifiedAs:(NSString*)entityId notiName:(NSString*)notiName;

@end







#ifndef ServerQueryBridge_h
#define ServerQueryBridge_h


#endif /* ServerQueryBridge_h */
