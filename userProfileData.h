//
//  userProfileData.h
//  LookUp
//
//  Created by Divey Punj on 22/02/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerQueryBridge.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>


@interface userProfileData : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    UIImagePickerController *imagePicker;
    UIImagePickerController *takePhotoPicker;}

@property (strong, nonatomic) IBOutlet UIImageView *profilePicture;
@property (strong, nonatomic) IBOutlet UIView *cropView;
@property (strong, nonatomic) IBOutlet UIImageView *smallImage;
@property (strong, nonatomic) IBOutlet UIScrollView *imageScrollview;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;


//https://www.iconfinder.com/arunxthomas
//https://www.iconfinder.com/paomedia
//https://www.iconfinder.com/dinosoftlabs




- (IBAction)imagePickLibrary:(id)sender;
- (IBAction)imagePickCamera:(id)sender;
- (IBAction)profileSave:(id)sender;
- (IBAction)cropAction:(id)sender;
- (IBAction)cancelAction:(id)sender;












@end
