//
//  location.m
//  LookUp
//
//  Created by Divey Punj on 21/02/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "location.h"


ServerQueryBridge *serverForLocation;
NSString *entityId_loc;
NSManagedObjectContext *context_loc;
NSManagedObject *managedDataObject_loc;
AppDelegate *delegate_loc;
NSFetchRequest *userDataRequest_loc;
NSArray *userDataArray_loc;

//NSTimer *locationTimer;
NSUserDefaults *deviceSaveds;

@implementation location

@synthesize userLatitude, userLongitude;

-(void) initialiseLocation{
    
    entityId_loc = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    //Initialise Core data object
    NSError *error;
    context_loc = [self managedObjectContext];
    userDataRequest_loc= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_loc.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_loc];
    userDataArray_loc = [context_loc executeFetchRequest:userDataRequest_loc error:&error];
    managedDataObject_loc= [userDataArray_loc objectAtIndex:0];
    
    locationManager= [[CLLocationManager alloc] init];
    userLatitude= @"";
    userLongitude= @"";
    serverForLocation= [[ServerQueryBridge alloc]init];
    [serverForLocation initialiseBackend];
    [serverForLocation initialiseCoreData:entityId_loc];

}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    NSLog(@"LocationError: %@", error);
    
}
-(void) locationManager: (CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *currentLoc= [locations lastObject];
    userLatitude= [NSString stringWithFormat:@"%.8f", currentLoc.coordinate.latitude];
    userLongitude= [NSString stringWithFormat:@"%.8f", currentLoc.coordinate.longitude];
    
    [managedDataObject_loc setValue:userLatitude forKey:@"latitude"];
    [managedDataObject_loc setValue:userLongitude forKey:@"longitude"];
    [self saveData];
    
}

-(void)fetchLocation{

    [self createRegionMonitoringForEventsNear];
}

-(void)getLocation{
    [NSThread detachNewThreadSelector:@selector(locationCalculator) toTarget:self withObject:nil];
}

-(void) locationCalculator{
    //NSLog(@"Location Calculator");
    locationManager.delegate= self;
    locationManager.desiredAccuracy= kCLLocationAccuracyBest;
    locationManager.allowsBackgroundLocationUpdates= YES;
    if([[[UIDevice currentDevice] systemVersion] floatValue]>=8){
        //[locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
    
    [locationManager startUpdatingLocation];
    sleep(5);
    [locationManager stopUpdatingLocation];
    [NSThread exit];
    
}

-(NSString*) getLocationLatitude{
    return userLatitude;
    
}
-(NSString*) getLocationLongitude{
    
    return userLongitude;
    
}

//********************Region Monitoring Methods**********************
-(void)createRegionMonitoringForEventsNear{
    NSLog(@"CREATING REGION");
    //Get events based on location
    //Create a notification for getting region information from location
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(nearByEventsRecievedForRegMon:) name:@"nearByEventsRecievedForRegMon" object:nil];
    
    [serverForLocation fetchNearByEvents:500 notiName:@"nearByEventsRecievedForRegMon" keyName:@"eventArray"];
    
}

-(void)nearByEventsRecievedForRegMon:(NSNotification*)notification{
    //Remove this notification
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"nearByEventsRecievedForRegMon" object:nil];
   //Loop through events and register region monitoring, less than 20 only
    if ([[[notification userInfo]objectForKey:@"eventArray"] isKindOfClass:[NSMutableArray class]]) {
        NSMutableArray *eventsArray= [[NSMutableArray alloc]initWithArray:[[notification userInfo]objectForKey:@"eventArray"]];
        if([eventsArray count]==0){
            //Nothing
        }
        else{
            NSInteger limit= 20;
            if([eventsArray count]>20){
                limit= 20;
            }
            else{
                limit= [eventsArray count];
            }
            
            //Delete any existing event information
            [self deleteCoreDataEventObjects];
            NSManagedObject *newEventObject= [NSEntityDescription insertNewObjectForEntityForName:@"Events" inManagedObjectContext:context_loc];
            NSData *eventLocs;
            NSError *error;
            
            for(int i=0; i<limit; i++){
                
                double d1= [[[eventsArray objectAtIndex:i]objectForKey:@"eventCoordLat"]doubleValue];
                double d2= [[[eventsArray objectAtIndex:i]objectForKey:@"eventCoordLon"]doubleValue];
                CLLocationCoordinate2D coord= {d1, d2};
                
                [self registerRegionMonitoring:coord venueIdentifier:[[eventsArray objectAtIndex:i]objectForKey:@"venueName"]];
                
                //Save the inside location to device
                //Add new
                [newEventObject setValue:[[eventsArray objectAtIndex:i]objectForKey:@"venueName"] forKey:@"venueName"];
                [newEventObject setValue:[[eventsArray objectAtIndex:i]objectForKey:@"eventId"] forKey:@"eventId"];
                eventLocs = [NSKeyedArchiver archivedDataWithRootObject: [[eventsArray objectAtIndex:i]objectForKey:@"insideLocations"]];
                [newEventObject setValue:eventLocs forKey:@"venueLocations"];
                NSString *promoCode= [[eventsArray objectAtIndex:i]objectForKey:@"promoCode"];
                [newEventObject setValue:[[eventsArray objectAtIndex:i]objectForKey:@"promoCode"] forKey:@"promoCode"];
                eventLocs= nil;
                
            }
            if(![context_loc save:&error]){
                NSLog(@"Save failed:%@",error);
            }
        }
        
    }
    
}

-(void)registerRegionMonitoring:(CLLocationCoordinate2D)coord venueIdentifier:(NSString*)venueId{
    
    double radius= 200;
    
    if(radius > locationManager.maximumRegionMonitoringDistance){
        radius= locationManager.maximumRegionMonitoringDistance;
    }

    
    //Create geographic region to be monitored
    CLCircularRegion *geoRegion= [[CLCircularRegion alloc]initWithCenter:coord radius:radius identifier:venueId];
    geoRegion.notifyOnEntry= YES;
    geoRegion.notifyOnExit= YES;
    
    
    [locationManager startMonitoringForRegion:geoRegion];
    [locationManager requestStateForRegion:geoRegion];
}

- (void)locationManager:(CLLocationManager *)manager
monitoringDidFailForRegion:(CLRegion *)region
              withError:(NSError *)error{
    
    NSLog(@"Error in monitoring the region: %@", error);
    //Just create the region again and monitor
    [self createRegionMonitoringForEventsNear];
    
}


-(void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region{
    //Check if user is already within the boundary
    
    
    if(state == CLRegionStateInside){
        //Is inside
        [managedDataObject_loc setValue:@"Yes" forKey:@"regionFlag"];
        [self saveData];
        NSLog(@"User is inside the region: %@", region.identifier);
        //Set check in to yes
        NSString *message= @"You are near or in: ";
        message= [message stringByAppendingString:region.identifier];
        message= [message stringByAppendingString:@" . Would you like to check-in?"];
        NSMutableDictionary *regionDict= [[NSMutableDictionary alloc]init];
        [regionDict setValue:message forKey:@"regionMessage"];
        [regionDict setValue:region.identifier forKey:@"venueName"];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"regionInfoRecieved" object:self userInfo:@{@"regionDict": regionDict}];
        
    }
    else{
        //Start monitoring this region
        
    }
}

-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region{
     //Entered
    
    [managedDataObject_loc setValue:@"Yes" forKey:@"regionFlag"];
    [self saveData];
    
    NSLog(@"User entered region: %@", region.identifier);
    //Set check in to yes
    NSString *message= @"You are near or at ";
    message= [message stringByAppendingString:region.identifier];
    message= [message stringByAppendingString:@" . Check-in!"];

    //Check if in background then raise a notification
    if([UIApplication sharedApplication].applicationState== UIApplicationStateBackground){
        UILocalNotification *notification= [[UILocalNotification alloc]init];
        notification.alertBody= message;
        notification.soundName= @"Glass.aiff";
        [[UIApplication sharedApplication]presentLocalNotificationNow:notification];
    }
    else{
        //Send alert to userProfile
        NSMutableDictionary *regionDict= [[NSMutableDictionary alloc]init];
        [regionDict setValue:message forKey:@"regionMessage"];
        [regionDict setValue:region.identifier forKey:@"venueName"];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"regionInfoRecieved" object:self userInfo:@{@"regionDict": regionDict}];
    }
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region{
    //Set checkin to No
    
    [managedDataObject_loc setValue:@"No" forKey:@"checkedIn"];
    [managedDataObject_loc setValue:@"No" forKey:@"regionFlag"];
    [managedDataObject_loc setValue:@"None" forKey:@"venueName"];
    [self saveData];
    
    //Let baseview know that user has left so turn on check in alert
    [[NSNotificationCenter defaultCenter]postNotificationName:@"turnOnCheckinAlert" object:self];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"backToForeChkIn" object:nil];
}

-(void)saveData{
    NSError *error;
    if(![context_loc save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
       NSLog(@"SAVED TO DEVICE for location");
       //Save to Server
        [serverForLocation saveData:@"" entityId:entityId_loc];
    }
}

-(void)deleteCoreDataEventObjects{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Events"];
    NSArray *tempArray = [context_loc executeFetchRequest:fetchRequest error:&error];
    
    if([tempArray count]==0 || tempArray==nil){
        //Nothing
    }
    else{
        for(int i=0; i<[tempArray count]; i++){
            [context_loc deleteObject:[tempArray objectAtIndex:i]];
        }
    }
    
    
    if(![context_loc save:&error]){
        NSLog(@"Save failed:%@",error);
    }
}



@end

