//
//  facebookTableCell.m
//  LookUp
//
//  Created by Divey Punj on 29/01/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "facebookTableCell.h"

@implementation facebookTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCell:(UIImage*)image imageUrl:(NSString*)imageUrl photoName:(NSString*)photoName{
    
    if([imageUrl isEqualToString:@""]){
        //Show a textview only
        //get size of post
        [_postTitle setText:photoName];
        [_postImage setImage:nil];
    }
    else{
        //Create Label
        _postTitle.text= photoName;
        //Create image view
        [_postImage setImage:image];
    }

    
}

-(CGSize)getSizeOfPost:(NSString*)message{
    CGSize textSize= {300.0, 10000.0};
    
    NSAttributedString *attributedText= [[NSAttributedString alloc]initWithString:message attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:14]}];
    CGRect rect= [attributedText boundingRectWithSize:(CGSize){textSize.width, textSize.height} options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return rect.size;
}

@end
