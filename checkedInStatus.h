//
//  checkedInStatus.h
//  LookUp
//
//  Created by Divey Punj on 9/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface checkedInStatus : UIViewController

#define redButYPosLar 223
#define amberButYPosLar 350
#define greenButYPosLar 472
#define redButYPosSm 170
#define amberButYPosSm 250
#define greenButYPosSm 330
#define butSizSmall 70
#define butSizLarge 100

@property (strong, nonatomic) IBOutlet UILabel *titleLab;
@property (strong, nonatomic) IBOutlet UIButton *redStatBut;
@property (strong, nonatomic) IBOutlet UIButton *amberStatBut;
@property (strong, nonatomic) IBOutlet UIButton *greenStatBut;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (strong, nonatomic) IBOutlet UILabel *redStatusInfo;
@property (strong, nonatomic) IBOutlet UILabel *amberStatusInfo;
@property (strong, nonatomic) IBOutlet UILabel *greenStatusInfo;




- (IBAction)redStatAct:(id)sender;
- (IBAction)amberStatAct:(id)sender;
- (IBAction)greenStatAct:(id)sender;
- (IBAction)statusInfoAction:(id)sender;






@end
