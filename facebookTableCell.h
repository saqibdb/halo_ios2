//
//  facebookTableCell.h
//  LookUp
//
//  Created by Divey Punj on 29/01/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface facebookTableCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *postTitle;
@property (strong, nonatomic) IBOutlet UIImageView *postImage;


-(void)setCell:(UIImage*)image imageUrl:(NSString*)imageUrl photoName:(NSString*)photoName;




@end
