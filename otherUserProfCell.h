//
//  otherUserProfCell.h
//  LookUp
//
//  Created by Divey Punj on 13/02/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface otherUserProfCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *subtitleLab;
@property (strong, nonatomic) IBOutlet UILabel *subInfoLab;
@property (strong, nonatomic) IBOutlet UITextView *interestTextview;




-(void)setCell:(NSString*)info location:(NSString*)loc subtitle:(NSString*)subTitle;




@end
