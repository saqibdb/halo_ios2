//
//  myStatCell.m
//  LookUp
//
//  Created by Divey Punj on 30/5/17.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "myStatCell.h"

@implementation myStatCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setCell: (NSString*)name status:(NSString*)status venueName:(NSString*)venueName timeStamp:(NSString*)timeStamp{
    
    _friendNameLab.text= name;
    _venueLab.text= venueName;
    _connectDateLab.text= timeStamp;
    
    if([status isEqualToString:@"Green"]){
        _friendStatusImg.image= [UIImage imageNamed:@"light_green_on.png"];
    }
    else if([status isEqualToString:@"Yellow"]){
        _friendStatusImg.image= [UIImage imageNamed:@"light_orange_on.png"];
    }
    else{
        _friendStatusImg.image= [UIImage imageNamed:@"light_red_on.png"];
    }
    
}



@end
