//
//  promotionViewController.m
//  LookUp
//
//  Created by Divey Punj on 22/5/17.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "promotionViewController.h"
NSManagedObjectContext *context_prm;
NSManagedObject *managedDataObject_prm;
AppDelegate *delegate_prm;
NSFetchRequest *userDataRequest_prm;
NSArray *userDataArray_prm;
NSString *entityId_prm;
NSMutableArray *locationArray_prm;

@interface promotionViewController ()

@end

@implementation promotionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Get entityId and username from key chain
    entityId_prm = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    //Initialise Core data object
    NSError *error;
    context_prm = [self managedObjectContext];
    userDataRequest_prm= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_prm.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_prm];
    userDataArray_prm = [context_prm executeFetchRequest:userDataRequest_prm error:&error];
    managedDataObject_prm= [userDataArray_prm objectAtIndex:0];
    
    //Get promotion info
    [self getPromoInfo];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doneAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)getPromoInfo{
    
    //Load event locations from device
    NSError *error;
    NSFetchRequest *eventRequest= [[NSFetchRequest alloc]initWithEntityName:@"Events"];
    NSString *str= [managedDataObject_prm valueForKey:@"venueName"];
    eventRequest.predicate= [NSPredicate predicateWithFormat:@"venueName == %@",[managedDataObject_prm valueForKey:@"venueName"]];
    NSArray *eventsArray = [context_prm executeFetchRequest:eventRequest error:&error];
    
    if([eventsArray count]==0){
        _additionalInfoLab.text = @"No promotional discounts available at this venue";
    }
    else{
        NSManagedObject *eventObj= [eventsArray objectAtIndex:0];
        
        if([[eventObj valueForKey:@"promoCode"]isEqualToString:@"0"]||[eventObj valueForKey:@"promoCode"]==nil){
            _additionalInfoLab.text= @"No promotional discounts available at this venue";
        }
        else{
            _percentLab.text= [NSString stringWithFormat:@"%@%%",[eventObj valueForKey:@"promoCode"]];
            _additionalInfoLab.text= @"Off all food and drinks. Flash this page at the bar!";
        }
        
    }
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}


@end
