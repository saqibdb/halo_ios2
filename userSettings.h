//
//  userSettings.h
//  LookUp
//
//  Created by Divey Punj on 16/03/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerQueryBridge.h"
#import "config.h"
#import "AppDelegate.h"

@interface userSettings : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    UIImagePickerController *imagePicker;
    UIImagePickerController *takePhotoPicker;
    UIImage *userImage;
    __weak IBOutlet UIImageView *profilePicture;
}

@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *gender;
@property (weak, nonatomic) IBOutlet UIScrollView *userProfileScroll;
@property (strong, nonatomic) IBOutlet UITextView *interests;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;

@property (strong, nonatomic) IBOutlet UIButton *myGenderButton_male;
@property (strong, nonatomic) IBOutlet UIButton *myGenderButton_female;
@property (strong, nonatomic) IBOutlet UILabel *myAgeLab;
@property (strong, nonatomic) IBOutlet UISlider *myAgeSlider;
@property (strong, nonatomic) IBOutlet UIButton *myOrientButton_straight;
@property (strong, nonatomic) IBOutlet UIButton *myOrientButton_gay;
@property (strong, nonatomic) IBOutlet UIButton *myOrientButton_lesbian;
@property (strong, nonatomic) IBOutlet UIButton *myOrientButton_trans;
@property (strong, nonatomic) IBOutlet UIButton *otherUserGender_male;
@property (strong, nonatomic) IBOutlet UIButton *otherUserGender_female;
@property (strong, nonatomic) IBOutlet UIButton *otherUserOrient_straight;
@property (strong, nonatomic) IBOutlet UIButton *otherUserOrient_gay;
@property (strong, nonatomic) IBOutlet UIButton *otherUserOrient_lesbian;
@property (strong, nonatomic) IBOutlet UIButton *otherUserOrient_trans;
@property (strong, nonatomic) IBOutlet UILabel *otherUserAgeLab_from;
@property (strong, nonatomic) IBOutlet UISlider *otherUserAgeSlider_from;
@property (strong, nonatomic) IBOutlet UILabel *otherUserAgeLab_to;
@property (strong, nonatomic) IBOutlet UISlider *otherUserAgeSlider_to;

@property (strong, nonatomic) IBOutlet UISwitch *orientationDiscloseSwitch;
@property (strong, nonatomic) IBOutlet UILabel *myOrientationTitle;
@property (strong, nonatomic) IBOutlet UISwitch *otherOritAnySwitch;







- (IBAction)otherUserAgeSliderAction_from:(id)sender;
- (IBAction)otherUserAgeSliderAction_to:(id)sender;
- (IBAction)myAgeSliderAction:(id)sender;
- (IBAction)myGenderAction_female:(id)sender;
- (IBAction)myGenderAction_male:(id)sender;
- (IBAction)myOrientAction_straight:(id)sender;
- (IBAction)myOrientAction_gay:(id)sender;
- (IBAction)myOrientAction_lesbian:(id)sender;
- (IBAction)myOrientAction_trans:(id)sender;
- (IBAction)otherUserGenderAction_male:(id)sender;
- (IBAction)otherUserGenderAction_female:(id)sender;
- (IBAction)otherUserOrientAction_straight:(id)sender;
- (IBAction)otherUserOrientAction_gay:(id)sender;
- (IBAction)otherUserOrientAction_lesbian:(id)sender;
- (IBAction)otherUserOrientAction_trans:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet NSString *fromView;


- (IBAction)saveProfile:(id)sender;
- (IBAction)userImageAction:(id)sender;
- (IBAction)logoutAction:(id)sender;
- (IBAction)takePhotoAction:(id)sender;
-(UIImage *)resizeImage:(UIImage *)image scaleToSize: (CGSize)imageSize;
-(void)initialise;
-(UIImage *)rotateImage:(UIImage *)image;
- (IBAction)cancelMyProfileAction:(id)sender;
- (IBAction)orientationDiscloseAction:(id)sender;
- (IBAction)otherOrientationSwitch:(id)sender;



@end
