//
//  bluetooth.h
//  LookUp
//
//  Created by Divey Punj on 18/03/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#ifndef bluetooth_h
#define bluetooth_h


#endif /* bluetooth_h */
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface bluetooth : NSObject

@property(retain, readonly) NSString *name;


-(void)startAdvertising;
- (void)searchDevicesUsingBluetooth;
-(void)initialiseBluetooth;
-(void)setCurrentStatusUUID: (NSString*)status;
-(void) stopScanningForDevices;
-(void)stopAdvertising;
-(NSMutableArray*)getDetectedDevices;

@end