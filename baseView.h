//
//  baseView.h
//  LookUp
//
//  Created by Divey Punj on 29/01/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerQueryBridge.h"
#import "location.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Reachability.h"
#import <SAMKeychain/SAMKeychain.h>
#import "checkedInStatus.h"
#import <MapKit/MapKit.h>
#import "venueEventCell.h"
#import "baseMapAnnotations.h"

#define baseEventViewCollCellWidth5 330
#define baseEventViewCollCellWidth6 388
#define baseEventViewCollCellWidth6P 410


@interface baseView : UIViewController <UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate, MKMapViewDelegate>

@property (strong, nonatomic)MMDrawerController *drawerContainer;


//**********MAP VIEW********************
@property (strong, nonatomic) IBOutlet MKMapView *baseMapView;
@property (strong, nonatomic) IBOutlet UIButton *allVenueBut;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *venueInfoViewTopConst;
@property (strong, nonatomic) IBOutlet UITableView *allVenueTable;
@property (strong, nonatomic) IBOutlet UILabel *mainLoaderLab;
@property (strong, nonatomic) IBOutlet UIView *mainLoaderView;


- (IBAction)allVenueAction:(id)sender;

//******************************EVENTS**********************************
@property (strong, nonatomic) IBOutlet UIImageView *messageInd_Baseview;

@property (strong, nonatomic) IBOutlet UIView *nextEventView;
@property (strong, nonatomic) IBOutlet UILabel *nextEventLab;

- (IBAction)nextEventCloseAct:(id)sender;









@end
