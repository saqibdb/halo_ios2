//
//  chatMessageHistoryLocal.m
//  LookUp
//
//  Created by Divey Punj on 21/08/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "chatMessageHistoryLocal.h"

@implementation chatMessageHistoryLocal

-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self= [super init]){
        self.messageId= [aDecoder decodeObjectForKey:@"messageId"];
        self.recipientIds= [aDecoder decodeObjectForKey:@"recipientIds"];
        self.senderId= [aDecoder decodeObjectForKey:@"senderId"];
        self.text= [aDecoder decodeObjectForKey:@"text"];
        self.headers= [aDecoder decodeObjectForKey:@"headers"];
        self.timestamp= [aDecoder decodeObjectForKey:@"timestamp"];
    }
    
    return self;
}


-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.messageId forKey:@"messageId"];
    [aCoder encodeObject:self.recipientIds forKey:@"recipientIds"];
    [aCoder encodeObject:self.senderId forKey:@"senderId"];
    [aCoder encodeObject:self.text forKey:@"text"];
    [aCoder encodeObject:self.headers forKey:@"headers"];
    [aCoder encodeObject:self.timestamp forKey:@"timestamp"];
    
}




@end
