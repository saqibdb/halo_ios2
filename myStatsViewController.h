//
//  myStatsViewController.h
//  LookUp
//
//  Created by Divey Punj on 30/5/17.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ServerQueryBridge.h"
#import "myStatCell.h"

@interface myStatsViewController : UIViewController


@property (strong, nonatomic) IBOutlet UILabel *myConnectionsLab;

@property (strong, nonatomic) IBOutlet UITableView *myStatTable;

@property (strong, nonatomic) IBOutlet UIView *progressIndView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progressInd;
@property (strong, nonatomic) IBOutlet UILabel *progressLab;
- (IBAction)closeAction:(id)sender;




@end
