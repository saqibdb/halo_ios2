//
//  rightSideDrawerTabCell.h
//  LookUp
//
//  Created by Divey Punj on 10/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface rightSideDrawerTabCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *locLab2;

-(void)setCell:(NSString*)loc;

@end
