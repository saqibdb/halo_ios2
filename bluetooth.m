//
//  bluetooth.m
//  LookUp
//
//  Created by Divey Punj on 18/03/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "bluetooth.h"
#import "userClass.h"

CBUUID *deviceDetectionUUID;
CBUUID *myServiceUUID;
CBUUID *myCharacteristicUUID;
CBUUID *receivedUUID;
CBCentralManager *myCentralManager;
CBPeripheralManager *myPeripheralManager;
CBPeripheral *discoveredPeripheral;
NSMutableData *data;
NSArray *UUIDArray;
NSMutableArray *detectedDevices;
userClass *userClassForBLE;
CBMutableCharacteristic *myCharacteristic;

@implementation bluetooth
@synthesize name;

-(void)initialiseBluetooth{

    //Initialise variables
    data= [[NSMutableData alloc]init];
    detectedDevices= [[NSMutableArray alloc]init];

    
    /*****************BLUETOOTH INITIALISATION**********************/
    //deviceDetectionUUID= [CBUUID UUIDWithString:@"6125EB0F-C0F8-42E0-8632-162F8A833602"];
    myServiceUUID= [CBUUID UUIDWithString:@"D8A38098-519A-4674-90B6-0855022CFEDB"];
    myCharacteristicUUID= [CBUUID UUIDWithString:@"C0AA60E0-BC1A-4DA5-A72E-F4B76948D715"];
    UUIDArray= [NSArray arrayWithObjects:myServiceUUID, myCharacteristicUUID, nil];
    
    name= @"Hello Device";
    
    userClassForBLE= [[userClass alloc]init];
    [userClassForBLE userInitialise];
}

-(NSMutableArray*)getDetectedDevices{
    return detectedDevices;
}

//**********************************CENTRAL CODE********************************
- (void)searchDevicesUsingBluetooth {
    myCentralManager= [[CBCentralManager alloc] initWithDelegate:self queue:nil options:nil];
}
-(void) stopScanningForDevices{
    [myCentralManager stopScan];
}

-(void) centralManagerDidUpdateState:(CBCentralManager *)central{
    
    if(central.state== CBCentralManagerStatePoweredOn){
        //Scan for devices
        //NSLog(@"My Service UUID: %@",[UUIDArray objectAtIndex:0]);
        [myCentralManager scanForPeripheralsWithServices: [UUIDArray objectAtIndex:0] options:nil];
        NSLog(@"Scanning for devices");
    }
    else if(central.state== CBCentralManagerStatePoweredOff){
        [myCentralManager stopScan];
        NSLog(@"Bluetooth is off");
    }
    else if(central.state== CBCentralManagerStateUnsupported){
        NSLog(@"Bluetooth unsupported");
    }
    
}

-(void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI{
    
    
    NSLog(@"Discovered: %@", peripheral.identifier.UUIDString);
    //NSLog(@"Ad data: %@",[advertisementData valueForKey:@"CBAdvertisementDataServiceUUIDsKey"]);
    //Connect to peripheral
    [myCentralManager connectPeripheral:peripheral options:nil];
    
   /* //Connect to the peripheral
    NSLog(@"Connecting to peripheral %@", peripheral);
    [myCentralManager connectPeripheral:peripheral options:nil];*/
}

-(void)centralManager: (CBCentralManager *)central didConnectPeripheral:(nonnull CBPeripheral *)peripheral{
    NSLog(@"Connected to Peripheral");
    peripheral.delegate= self;
    
    //Look for peripheral services
    [peripheral discoverServices:[UUIDArray objectAtIndex:0]];
}
-(void)peripheral: (CBPeripheral *)peripheral didDiscoverServices:(nullable NSError *)error{
    for(CBService *service in peripheral.services){
        NSLog(@"Discovered service %@", service);
        [peripheral discoverCharacteristics:[UUIDArray objectAtIndex:1] forService:service];
    }
}
-(void)peripheral: (CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(nonnull CBService *)service error:(nullable NSError *)error{
    for(CBCharacteristic *characteristic in service.characteristics){
        NSLog(@"Discovered characteristic %@", characteristic);
        [peripheral readValueForCharacteristic:characteristic];
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(nonnull CBCharacteristic *)characteristic error:(nullable NSError *)error{
    
    NSData *data= characteristic.value;
    NSString *otherUserId= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Other user ID: %@",otherUserId);
    
}

//*****************************PERIPHERAL CODE**********************************

-(void)startAdvertising{
    myPeripheralManager= [[CBPeripheralManager alloc]initWithDelegate:self queue:nil options:nil];
}
-(void)stopAdvertising{
    [myPeripheralManager stopAdvertising];
    NSLog(@"Stopped Broadcasting.....");
}
-(void) peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral{
    
    if(peripheral.state==CBPeripheralManagerStatePoweredOn){
        //Bluetooth is on
        //Update the bluetooth statuslabel.
        myCharacteristic= [[CBMutableCharacteristic alloc] initWithType:[UUIDArray objectAtIndex:1] properties:CBCharacteristicPropertyRead value:[[userClassForBLE getEntityId] dataUsingEncoding:NSUTF8StringEncoding] permissions:CBAttributePermissionsReadable];
        CBMutableService *myService= [[CBMutableService alloc] initWithType:[UUIDArray objectAtIndex:0] primary:YES];
        myService.characteristics= @[myCharacteristic];
        [myPeripheralManager addService:myService];
        
        //[myPeripheralManager startAdvertising:@{CBAdvertisementDataServiceUUIDsKey: @[myService.UUID]}];
        [myPeripheralManager startAdvertising:@{CBAdvertisementDataServiceUUIDsKey: @[[UUIDArray objectAtIndex:0]]}];

    }
    else if(peripheral.state==CBPeripheralManagerStatePoweredOff){
        
        [myPeripheralManager stopAdvertising];
    }
    else if(peripheral.state== CBPeripheralManagerStateUnsupported){
        NSLog(@"Broadcasting unsupported");
    }
  
}

-(void) peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error{
    if(error){
        NSLog(@"Error in Advertising: %@", error);
    }
    else{
        NSLog(@"Started Advertising");
    }
}

-(void) peripheralManager:(CBPeripheralManager *)peripheral didAddService:(CBService *)service error:(NSError *)error{
    
    if(error){
        NSLog(@"Error publishing service: %@", [error localizedDescription]);
    }
    else{
        NSLog(@"Did add service: %@",service.UUID);
    }
}

-(void) peripheralManager:(CBPeripheralManager *)peripheral didReceiveReadRequest:(nonnull CBATTRequest *)request{
    
    if([request.characteristic.UUID isEqual:myCharacteristicUUID]){
        request.value= [myCharacteristic.value subdataWithRange:NSMakeRange(request.offset, myCharacteristic.value.length- request.offset)];
        [myPeripheralManager respondToRequest:request withResult:CBATTErrorSuccess];
    }
    
}




@end
