//
//  haloUsersProfileCells.h
//  LookUp
//
//  Created by Divey Punj on 11/02/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface haloUsersProfileCells : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UILabel *userName;


-(void)setCell:(NSString*)imageUrl name:(NSString*)name;



@end
