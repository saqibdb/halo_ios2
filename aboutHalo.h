//
//  aboutHalo.h
//  LookUp
//
//  Created by Divey Punj on 17/09/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface aboutHalo : UIViewController


@property (strong, nonatomic) IBOutlet UIView *infoView;
@property (strong, nonatomic) IBOutlet UILabel *infoLabel;
@property (strong, nonatomic) IBOutlet UITextView *infoTextView;









- (IBAction)closeViewAction:(id)sender;

- (IBAction)aboutHaloAction:(id)sender;

- (IBAction)userAgreementAction:(id)sender;

- (IBAction)privacyPolicyAction:(id)sender;

- (IBAction)reportAction:(id)sender;
- (IBAction)closeAboutAction:(id)sender;



@end
