//
//  checkedInLocCell.m
//  LookUp
//
//  Created by Divey Punj on 10/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "checkedInLocCell.h"

@implementation checkedInLocCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCell:(NSString*)loc{
    _locLab.text= loc;
}

@end
