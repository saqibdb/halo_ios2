//
//  messageCell.m
//  LookUp
//
//  Created by Divey Punj on 17/04/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "messageCell.h"

@implementation messageCell
@synthesize messageContentView, bubbleImageView;

/*-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self= [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        
        bubbleImageView= [[UIImageView alloc]initWithFrame:CGRectZero];
        [self.contentView addSubview:bubbleImageView];
        
        messageContentView= [[UITextView alloc]init];
        messageContentView.backgroundColor= [UIColor clearColor];
        messageContentView.editable= NO;
        messageContentView.scrollEnabled= NO;
        [messageContentView sizeToFit];
        [self.contentView addSubview:messageContentView];
    }
    return self;
}*/

-(id)initWithCoder:(NSCoder *)aDecoder{
    if(!(self=[super initWithCoder:aDecoder]))return nil;
    
    bubbleImageView= [[UIImageView alloc]initWithFrame:CGRectZero];
    [self.contentView addSubview:bubbleImageView];
    
    messageContentView= [[UITextView alloc]init];
    messageContentView.backgroundColor= [UIColor clearColor];
    messageContentView.editable= NO;
    messageContentView.scrollEnabled= NO;
    [messageContentView sizeToFit];
    [self.contentView addSubview:messageContentView];
    
    return self;
}













@end
