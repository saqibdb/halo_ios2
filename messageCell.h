//
//  messageCell.h
//  LookUp
//
//  Created by Divey Punj on 17/04/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface messageCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *otherUserMessageLabel;
@property (strong, nonatomic) IBOutlet UILabel *myMessageLabel;

@property (strong, nonatomic)UITextView *messageContentView;
@property (strong, nonatomic)UIImageView *bubbleImageView;


@end
