//
//  eventDialog.m
//  LookUp
//
//  Created by Divey Punj on 7/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "eventDialog.h"


ServerQueryBridge *serverBridgeForEventDialog;
userClass *userClassForEventDialog;
userSettings *userSettingsForEventDialog;
messageClass *messageClassForEvents;
NSDictionary *eventNotiDict;
eventTimeAndNotification *eventNoti;

@interface eventDialog ()

@end

@implementation eventDialog
@synthesize eventAttendees, eventAddress, eventDate, eventName, eventAttendeesLab, eventDateLab, eventAddressLab, eventNameLab, eventMaybeButton, eventYesButton, eventNoButton, eventId, eventImageView, eventImageLoadingLab, eventImageLoadingIndicator, goingStatusLabel, eventDialogScrollview, eventImage, eventDescription, eventViewHeightConstraint, eventDescriptionTextview;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    eventNameLab.text= eventName;
    eventAddressLab.text= eventAddress;
    eventDateLab.text= eventDate;
    eventAttendeesLab.text= [NSString stringWithFormat:@"%d",[eventAttendees intValue]];
    
    if(eventDescription){
        [eventDescriptionTextview setEditable:NO];
        eventDescriptionTextview.text= eventDescription;
    }
    else{
        //Nothing
    }
    
    serverBridgeForEventDialog= [[ServerQueryBridge alloc]init];
    [serverBridgeForEventDialog initialise];
    
    userClassForEventDialog= [[userClass alloc]init];
    
    userSettingsForEventDialog= [[userSettings alloc]init];
    [userSettingsForEventDialog initialise];
    
    messageClassForEvents= [[messageClass alloc]init];
    
    eventNotiDict= [[NSDictionary alloc]init];
    eventNoti= [[eventTimeAndNotification alloc]init];
    
    //Initialise variables that may not have a value
    if([eventId isEqualToString:@""]){
        eventId= @"EMPTY";
    }
    
    //Clear my event label
    goingStatusLabel.text= @"";
    
    //Load event image
    //Check if the image data is empty
    CGImageRef cgref= [eventImage CGImage];
    CIImage *cim= [eventImage CIImage];

        
    if(cim==nil && cgref == NULL){
        [self loadEventImage];
    }
    else{
        eventImageLoadingLab.hidden= YES;
        eventImageLoadingIndicator.hidden= YES;
        [eventImageLoadingIndicator stopAnimating];
        eventImageView.image= eventImage;
    }
    

    //Check if this event is my event
    [self checkIfMyEvent];
    
    //Notification for event alarm notifications
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(raiseLocalEventNotification:) name:@"raiseLocalEventNotification" object:nil];
    
    //[eventDialogScrollview setScrollEnabled:YES];
    //[eventDialogScrollview setContentSize:CGSizeMake(300, 1300)];
    eventViewHeightConstraint.constant= 1200;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)checkIfMyEvent{
    //Get my events from device
    NSMutableArray* tempArray= [[NSMutableArray alloc]initWithArray:[userClassForEventDialog getMyEvents]];
    if(tempArray.count==0){
        //Display empty events label
    }
    else{
        if([tempArray containsObject:eventId]){
            eventYesButton.hidden= YES;
            eventNoButton.hidden= YES;
            eventMaybeButton.hidden= YES;
        }
        else{
            eventYesButton.hidden= NO;
            eventNoButton.hidden= NO;
            eventMaybeButton.hidden= NO;
        }
    }

}

- (IBAction)eventYesAction:(id)sender {
    
    NSLog(@"Pressed Yes");
    
    //Save notification
    [eventNoti setAlarm:eventDate eventId:eventId eventName:eventName];
    
    //Add to the attendees
    int temp= [eventAttendees intValue] + 1;
    //Send updated attendees and my userId to server, with event id
    [serverBridgeForEventDialog writeToEvent:eventName userId:[userClassForEventDialog getEntityId] attendees:[NSString stringWithFormat:@"%d",temp]];
    //Get events array from device
    NSMutableArray *tempArray= [[NSMutableArray alloc]initWithArray:[userClassForEventDialog getMyEvents]];
    //Create a new dictionary to save into the events array
    NSMutableDictionary *tempDict= [[NSMutableDictionary alloc]init];
    [tempDict setValue:eventId forKey:@"eventId"];
    [tempDict setValue:eventName forKey:@"eventName"];
    [tempDict setValue:eventDate forKey:@"eventDate"];
    [tempDict setValue:eventAddress forKey:@"eventAddress"];
    [tempDict setValue:eventAttendees forKey:@"eventAttendees"];
    //Add this dictionary to the array
    [tempArray addObject:tempDict];
    //Save array back to device
    [userClassForEventDialog setMyEvents:tempArray];
    //Save profile
    [userSettingsForEventDialog saveProfile:0];
    
    //Hide Yes button
    eventYesButton.hidden= YES;
    //Show event going status label
    goingStatusLabel.text= @"You are going";

    
}

- (IBAction)eventNoAction:(id)sender {
}

- (IBAction)eventMaybeAction:(id)sender {
}

/*- (IBAction)addEventAsChatMate:(id)sender {
    //Read chat mate array
    NSMutableArray *tempArray= [[NSMutableArray alloc]initWithArray:[messageClassForEvents getChatMateArray]];
    
    if(tempArray.count==0){
        
    }
    else{
        //Check if event owner already exists as mate
        if([tempArray containsObject:eventId]){
            //Then display already added
            NSLog(@"This event owner is already in Chat Mates");
        }
        else{
            //Add new event owner to mates array
            [tempArray addObject:eventId];
            //Save new array to device
            [userClassForEventDialog setChatMates:tempArray];
            //Save profile
            [userSettingsForEventDialog saveProfile:0];
        }
    }
}*/

-(void)loadEventImage{
    //Show indicator and label
    eventImageLoadingIndicator.hidden= NO;
    [eventImageLoadingIndicator startAnimating];
    eventImageLoadingLab.text= @"Loading image..";
    eventImageLoadingLab.hidden= NO;
    //Listen out for image
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(eventImageLoaded:) name:@"eventImageLoaded" object:nil];
    //Call server method to download image
    
    dispatch_queue_t queue= dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        
        
        [serverBridgeForEventDialog downloadLargeImageFromEvents:[eventId stringByAppendingString:@"Large.png"] notificationName:@"eventImageLoaded" keyName:@"eventImage"];
        
    });
    //[serverForUserProfDialog downloadLargeImageByName:[userEntityId stringByAppendingString:@"Large.png"] notificationName:@"otherUserImageLoaded"];
}

-(void)eventImageLoaded:(NSNotification*)notification{
    
    //Hide indicator and label
    eventImageLoadingIndicator.hidden= YES;
    [eventImageLoadingIndicator stopAnimating];
    eventImageLoadingLab.text= @"";
    
    UIImage* image= [[notification userInfo]objectForKey:@"eventImage"];
    if(image != nil){
        eventImageView.image= image;
    }
    else{
        eventImageView.image= [UIImage imageNamed:@"no_image_profile.png"];
    }
}



-(void)raiseLocalEventNotification: (NSNotification*)notification{
    
    //Read the dict in the notification
    NSDictionary* tempDict= [[NSDictionary alloc]initWithDictionary:[[notification userInfo]objectForKey:@"eventNotiInfo"]];
    
    UILocalNotification *aNotification = [[UILocalNotification alloc] init];
    aNotification.fireDate = [tempDict objectForKey:@"date"];
    aNotification.soundName = @"Glass.aiff";
    aNotification.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    aNotification.alertBody = [tempDict objectForKey:@"message"];
    aNotification.alertTitle= @"Event Notification";
    aNotification.userInfo= tempDict;
    //aNotification.alertAction = @"Event Notification";
    
    //Add to the eventNotiDict
    [eventNotiDict setValue:aNotification forKey:[tempDict objectForKey:@"eventId"]];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:aNotification];
    
}














@end
