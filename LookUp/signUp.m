//
//  signUp.m
//  LookUp
//
//  Created by Divey Punj on 14/02/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "signUp.h"


ServerQueryBridge *serverAccess; //Create instance of server bridge
UITextField *activeField;
NSString *username;
NSString *password;
NSString *entityId_sgnup, *nameOfUser_sgnup, *userEmail_sgnup;
NSManagedObjectContext *context_sgnup;
NSManagedObject *newUserDataObj_sgnup;
AppDelegate *delegate_sgnup;
NSFetchRequest *userDataRequest_sgnup;
NSArray *userDataArray_sgnup;
UIImage *userDP_sgnup;


@interface signUp ()

@end

@implementation signUp

@synthesize usernameText, passwordText, signUpScrollView, passwordConfirmationText, submitButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //GENERAL
    submitButton.layer.cornerRadius= 20;
    submitButton.clipsToBounds= YES;

    _signupView.hidden= YES;
    [self hideLoadView];
    [self.view setUserInteractionEnabled:YES];
    //Animate entrance
    double delay= 0.5;
    dispatch_time_t popTime= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self animateViewEntrance];
    });
    
    //Initialise
    serverAccess= [[ServerQueryBridge alloc]init];
    [serverAccess initialiseBackend];

    username= @"";
    password= @"";
    nameOfUser_sgnup= @"";
    userEmail_sgnup= @"";
    
    //Initalise other functions
    //Tap gesture to remove keyboard
    UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeKeyboard:)];
    tap.cancelsTouchesInView= NO;
    [self.view addGestureRecognizer:tap];
    //[self registerForKeyboardNotifications];

    //Create a notifcation for showing error alerts
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(errorAlerts:) name:@"errorAlerts" object:nil];
    

  
}

-(void)animateViewEntrance{
    [_signupView setAlpha:0];
    _signupView.hidden= NO;
    [UIView animateWithDuration:0.3 animations:^{
        [_signupView setAlpha:1];
        [self.view layoutIfNeeded];
    }];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    [submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)signupSubmit:(id)sender {
 
    if([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable){
        //Connection unavailable
        [self errorPopUp:@"No internet connection available. Please check your connection."];
    }
    else{
        username= usernameText.text;
        password= passwordText.text;
        
        //Check if the text enterred is relevant
        if(![username isEqualToString:@""] && ![password isEqualToString:@""]){
            //Check if email is in valid form
            if((!([username rangeOfString:@"@"].location== NSNotFound))){
                //Check if password and confirmation password match
                if([password isEqualToString:passwordConfirmationText.text]){
                    //Stop interaction with screen
                    [self.view setUserInteractionEnabled:NO];
                    //Show load view
                    [self showLoadView];
                    submitButton.enabled= NO;
                    //Save user to Server
                    //Create a notifcation for dismissing signup view when signed up
                    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(newUserCreated:) name:@"newUserCreated" object:nil];
                    [serverAccess createUser:usernameText.text password:passwordText.text];
                }
                else{
                    [self errorPopUp:@"Your passwords don't match, please check again!"];
                    usernameText.text= @"";
                    passwordText.text= @"";
                    passwordConfirmationText.text= @"";
                }
            }
            else{
                [self errorPopUp:@"Email is of an invalid form, please try again."];
                usernameText.text= @"";
                passwordText.text= @"";
                passwordConfirmationText.text= @"";
            }
        }
        else{
            [self errorPopUp:@"Fields cannot be left empty, please enter your details."];
            usernameText.text= @"";
            passwordText.text= @"";
            passwordConfirmationText.text= @"";
        }

    }
    
}

-(void) newUserCreated:(NSNotification*)notification{
    NSString* isDone= [[notification userInfo]objectForKey:@"message"];
    
    if([isDone isEqualToString:@"isDone"]){

        //Save username and entity id in KeyChain
        [SAMKeychain setPassword:usernameText.text forService:@"username" account:@"haloStatus-username"];
        [SAMKeychain setPassword:[[notification userInfo]objectForKey:@"entityId"] forService:@"entityId" account:@"haloStatus-entityId"];
        
        entityId_sgnup = [[notification userInfo]objectForKey:@"entityId"];
        
        //Using this entity id, create a new user in core data
        context_sgnup = [self managedObjectContext];
        //Add new
        newUserDataObj_sgnup= [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context_sgnup];
        [newUserDataObj_sgnup setValue:[[notification userInfo]objectForKey:@"entityId"] forKey:@"entityId"];

        [newUserDataObj_sgnup setValue:usernameText.text forKey:@"email"];
        [newUserDataObj_sgnup setValue:@"No" forKey:@"verifiedUser"];
        [self saveData];
        
        //Save user email to User
        [serverAccess addAdditionalUserInfo:entityId_sgnup email:username];
        
        //Connect user with Halo
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSString* dateString = [dateFormatter stringFromDate:[NSDate date]];
        //Create a notification to wait for Halo connect
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(haloConnectedWithSignUp:) name:@"haloConnectedWithSignUp" object:nil];
        [serverAccess connectUsers:HALO_FEEDBACK_ID myId:entityId_sgnup myName:usernameText.text friendName:@"Halo Status" friendStatus:@"" venueName:@"" timeStamp:dateString isHalo:@"YES" myStatus:@"" notiName:@"haloConnectedWithSignUp"];


    }
    else{
        //Dont Segue
        //Clear data and show error
        usernameText.text= @"";
        passwordText.text= @"";
        passwordConfirmationText.text= @"";
        [self errorPopUp:[[notification userInfo]objectForKey:@"errorMessage"]];
        [self hideLoadView];
        [self.view setUserInteractionEnabled:YES];
        submitButton.enabled= YES;
    }
}

-(void)haloConnectedWithSignUp:(NSNotification*)notification{
    //Verify email
    [self performSegueWithIdentifier:@"signupToEVSegue" sender:self];
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}



- (IBAction)cancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)errorAlerts:(NSNotification*)notification{
    
    NSString* errorStr = [[notification userInfo]objectForKey:@"signupError"];
    
    NSString* message;
    if([errorStr isEqualToString:@"UserAlreadyExists"]){
        message= @"The user already exists. Please enter a new email address and try again!";
    }
    else{
        message= @"Something went wrong :(. Please try again. If this fails, please contact us on our Facebook page at Halo status app!";
    }
    
    UIAlertView *signUpRestrictAlert= [[UIAlertView alloc]initWithTitle:@"Sorry!" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [signUpRestrictAlert show];
    
    [self hideLoadView];
    submitButton.enabled= YES;
    [submitButton setTitle:@"Submit" forState:UIControlStateNormal];
}

-(void)saveHaloToUserPhone{
    /*NSMutableArray *tempArray= [[NSMutableArray alloc]init];
    //Add new contact to this array
    NSMutableDictionary *tempDict= [[NSMutableDictionary alloc]init];
    [tempDict setValue:@"Halo" forKey:@"chatMateName"];
    [tempDict setValue:HALO_FEEDBACK_ID forKey:@"chatMateId"];
    [tempArray addObject:tempDict];
    [user setChatMates:tempArray];
    [signUpSettings saveProfile:0];
    [user saveUserData:@"" notiNeed:NO];*/
}

-(void)errorPopUp: (NSString*)message{
    
    UIAlertController *alert= [UIAlertController alertControllerWithTitle:@"ERROR" message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* okButton= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


//The next 4 methods are responsible to allow text view to be seen when keyboard is open
//*************************KEYBOARD METHODS******************************//

-(void) registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:self.view.window];
}

-(void) keyboardWasShown: (NSNotification *) aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGRect keyPadFrame=[[UIApplication sharedApplication].keyWindow convertRect:[[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue] fromView:self.view];
    CGSize kbSize =keyPadFrame.size;
    CGRect activeRect=[self.view convertRect:activeField.frame fromView:activeField.superview];
    CGRect aRect = self.view.bounds;
    aRect.size.height -= ((kbSize.height)+30);
    
    CGPoint origin =  activeRect.origin;
    origin.y -= signUpScrollView.contentOffset.y;
    if (!CGRectContainsPoint(aRect, origin)) {
        CGPoint scrollPoint = CGPointMake(0.0,CGRectGetMaxY(activeRect)-(aRect.size.height));
        [signUpScrollView setContentOffset:scrollPoint animated:YES];
    }
}

-(void) keyboardWillBeHidden :(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGRect keyPadFrame=[[UIApplication sharedApplication].keyWindow convertRect:[[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue] fromView:self.view];
    CGSize kbSize =keyPadFrame.size;
    CGRect activeRect=[self.view convertRect:activeField.frame fromView:activeField.superview];
    CGRect aRect = self.view.bounds;
    aRect.size.height += ((kbSize.height)+30);
    
    CGPoint origin =  activeRect.origin;
    origin.y += signUpScrollView.contentOffset.y;
    CGPoint scrollPoint = CGPointMake(0.0,0.0);
    [signUpScrollView setContentOffset:scrollPoint animated:YES];
}

-(void) textFieldDidBeginEditing: (UITextField *) textField{
    activeField= textField;
}

-(void) textFieldDidEndEditing: (UITextField *) textField{
    activeField= nil;
}
-(void) closeKeyboard: (UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
}

-(void)showLoadView{
    _loadIndView.hidden= NO;
    _loadingText.hidden= NO;
    [_loadIndicator startAnimating];
}

-(void)hideLoadView{
    _loadIndView.hidden= YES;
    [_loadIndicator stopAnimating];
    _loadingText.hidden= YES;
}



-(void)saveData{
    NSError *error;
    if(![context_sgnup save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
    }
}



@end


