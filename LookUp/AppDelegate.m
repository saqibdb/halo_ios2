 //
//  AppDelegate.m
//  LookUp
//
//  Created by Divey Punj on 14/02/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "AppDelegate.h"
#import "messageClass.h"
#import "LookUp-Swift.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "messageClass.h"
#import "messageDialogClass.h"

NSTimer *internetCheckTimer;

@interface AppDelegate ()

@end

ServerQueryBridge *accessServer;
messageDialogClass *appDelegateMessageDialog;
NSString *entityId_AD, *sendMessage_senderId, *sendMessage_recipientId, *sendMessage_text, *sendMessage_messageId;
NSDate* sendMessage_timestamp;
NSDictionary* sendMessage_header;

@implementation AppDelegate
@synthesize sinchMessageClient, managedObjectContext, managedObjectModel, persistentStoreCoordinator, drawerContainer;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [Fabric with:@[[Crashlytics class]]];

    //Create notifications
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loggedOutWork:) name:@"loggedOutWork" object:nil];
    //Create a notification to catch sign up and login completion
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(signUpToAddDel:) name:@"signUpToAddDel" object:nil];
    
    //Change status bar colour to white colour
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //NOTIFICATIONS
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        /*[center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [application registerForRemoteNotifications];
            }
        }];*/
        
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        
        [application registerForRemoteNotifications];
    }
    else {
        // Code for old versions
        if([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]){
            
            [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [application registerForRemoteNotifications];
        }
        else{
            [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
            
        }
        
        if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
            
            [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
        }
    }

    
    //Check for internet connection
    if([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable){
        //Connection unavailable
        
    }
    else{
        
        //Setup Kinvey
        serverBridgeClass *serInst= [[serverBridgeClass alloc]init];
        [serInst setupClient];
        //Initialise Server
        accessServer= [[ServerQueryBridge alloc]init];
        [accessServer initialiseBackend];
        //Check if user is active
        if([accessServer checkActiveUser]){
            
            //Check if verification code exists, if so, then show verification screen, if not, then stay on this page
            NSString *randomCode = [[NSUserDefaults standardUserDefaults]stringForKey:@"verifiedUser"];
            
            if(randomCode){
                if([randomCode isEqualToString:@"none"]){
                    //Nothing
                    //Active User
                    [self setupSliderMenu];
                    
                    //Initialise FACEBOOK
                    [FBSDKProfilePictureView class];
                    
                }
                else{
                    self.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"emailVerificationCont"];
                }
               
            }
            else{
                //Nothing
                //Active User
                [self setupSliderMenu];
                
                //Initialise FACEBOOK
                [FBSDKProfilePictureView class];
                
                //SINCH REMOTE NOTIFICATIONS
                /*self.push   = [Sinch managedPushWithAPSEnvironment:SINAPSEnvironmentDevelopment];
                self.push.delegate= self;
                [self.push setDesiredPushTypeAutomatically];
                [self.push registerUserNotificationSettings];*/
            }
            
        }
        else{
            //Not active user
            self.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"initialLoginView"];
        }
        

    }


    
    
    
    //Setup how often we want the background fetch to happen
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    
    
    
    UILocalNotification *notification = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notification){
        // do something with notification userinfo
        UIViewController *rootController = self.window.rootViewController;
        UIViewController *currentController = rootController;
        
        if ([currentController isKindOfClass:[UITabBarController class]]) {
            currentController = [(UITabBarController *)currentController selectedViewController];
            UITabBarController *tabController = (UITabBarController *)currentController;
            [tabController setSelectedIndex:1];
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Title"
                                                                          message:@"Message"
                                                                   preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes, please"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action)
            {
                /** What we write here???????? **/
                NSLog(@"you pressed Yes, please button");
                
                // call method whatever u need
            }];
            
            UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"No, thanks"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action)
            {
                /** What we write here???????? **/
                NSLog(@"you pressed No, thanks button");
                // call method whatever u need
            }];
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            
            [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
            NSLog(@"Selected controller is tabbbar");
        }
        
        /*
        
        if ([currentController isKindOfClass:[UINavigationController class]]){
            [(UINavigationController *)currentController pushViewController:newVC animated:YES];
        }else if ([currentController isKindOfClass:[UIViewController class]]){
            [currentController.navigationController pushViewController:newVC animated:YES];
        }
         */
    }
    
    
    
    

    
    return YES;
}



-(void)showTestAlert {
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Title"
                                                                  message:@"Message"
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes, please"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    /** What we write here???????? **/
                                    NSLog(@"you pressed Yes, please button");
                                    
                                    // call method whatever u need
                                }];
    
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"No, thanks"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   /** What we write here???????? **/
                                   NSLog(@"you pressed No, thanks button");
                                   // call method whatever u need
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
}

-(void)signUpToAddDel:(NSNotification*)notification{

    [self setupSliderMenu];
    
}

//Delegate method that manages the background fetching code
-(void)application:(UIApplication*)application performFetchWithCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler{
    
    NSLog(@"Im in background");
    
    //userProfile *viewController= (userProfile *)self.window.rootViewController;
    //[viewController fetchNewDataWithCompletionHandler:^(UIBackgroundFetchResult result) {
    //    completionHandler(result);
    //}];

}

-(void)setupSliderMenu{
    
    UIStoryboard *mainStoryboard= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //UITabBarController *tabBarController= (UITabBarController*)self.window.rootViewController;
    
    //UINavigationController *navController= (UINavigationController*)self.window.rootViewController;
    
    UITabBarController *tabBarController = [mainStoryboard instantiateViewControllerWithIdentifier:@"mainTabCon"];
    
    UIViewController *sliderViewController= [mainStoryboard instantiateViewControllerWithIdentifier:@"leftSideSliderMenu"];

    UIViewController *rightSliderViewController= [mainStoryboard instantiateViewControllerWithIdentifier:@"rightSideSliderMenu"];
    
    drawerContainer= [[MMDrawerController alloc]initWithCenterViewController:tabBarController leftDrawerViewController:sliderViewController rightDrawerViewController:rightSliderViewController];
    //[drawerContainer setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [drawerContainer setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    self.window.rootViewController= drawerContainer;
}

-(void)saveContext{
    NSError *error = nil;
    
    NSManagedObjectContext *managedObjectContext= self.managedObjectContext;
    if(managedObjectContext != nil){
        if([managedObjectContext hasChanges] && ![managedObjectContext save:&error]){
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

-(NSManagedObjectContext*) managedObjectContext{
    if(managedObjectContext != nil){
        return managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator= [self persistentStoreCoordinator];
    if(coordinator != nil){
        managedObjectContext= [[NSManagedObjectContext alloc]initWithConcurrencyType:NSMainQueueConcurrencyType];
        [managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext;
}

-(NSManagedObjectModel*)managedObjectModel{
    if(managedObjectModel != nil){
        return managedObjectModel;
    }
    
    NSURL *modelUrl= [[NSBundle mainBundle]URLForResource:@"userDatabase" withExtension:@"momd"];
    managedObjectModel= [[NSManagedObjectModel alloc]initWithContentsOfURL:modelUrl];
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"userDatabase.sqlite"];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return persistentStoreCoordinator;
}

-(void)deleteAndRecreateStore{
    NSPersistentStore * store = [[self.persistentStoreCoordinator persistentStores] lastObject];
    NSError * error;
    [self.persistentStoreCoordinator removePersistentStore:store error:&error];
    [[NSFileManager defaultManager] removeItemAtURL:[store URL] error:&error];
    managedObjectContext = nil;
    persistentStoreCoordinator = nil;
    [self managedObjectContext];//Rebuild The CoreData Stack
}



#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}



-(void) showSignUpScreen: (BOOL)animated{
    
    //Show Sign Up screen
    UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    signUp *signUpController= (signUp *)[storyBoard instantiateViewControllerWithIdentifier:@"signUpController"];
    [self.window makeKeyAndVisible];
    [self.window.rootViewController presentViewController:signUpController animated:animated completion:nil];
}

-(void)showLoginScreen:(BOOL)animated{
    //Show Login screen
    UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    signUp *loginController= (signUp *)[storyBoard instantiateViewControllerWithIdentifier:@"loginController"];
    [self.window makeKeyAndVisible];
    [self.window.rootViewController presentViewController:loginController animated:animated completion:nil];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
    //This notification will cause all the work happening in any view controller to stop
    [[NSNotificationCenter defaultCenter] postNotificationName:@"appClosingProfileWork" object:self];
    
    
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    //Create a notification for messages to come here.
    //Send notification assuming the screen is not being viewed
    [self saveData];

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"backToForeChkIn" object:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  ){
        NSLog(@"applicationDidBecomeActive FROM NOTIFICATIONS");
        [self showTestAlert];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    //Synchronize NsUserDefaults to ensure data does not get lost
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self saveData];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

-(void)application:(UIApplication*)application didRegisterUserNotificationSettings:(nonnull UIUserNotificationSettings *)notificationSettings{
    
}

-(void) application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    NSLog(@"Received Notification");

    //Set up CoreData
    //Get entityId and username from key chain
    entityId_AD = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    //Initialise Core data object
    NSError *error;
    managedObjectContext = [self managedObjectContext];
    NSFetchRequest *userDataRequest_AD= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_AD.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_AD];
    NSArray *userDataArray_AD = [managedObjectContext executeFetchRequest:userDataRequest_AD error:&error];
    NSManagedObject *managedDataObject_AD = [userDataArray_AD objectAtIndex:0];
    
    //Check if notification is has a region
    CLRegion *region= notification.region;
    if(region){
        //Entered a region, let the otherUserManager know.
        
        //Read checkedIn flag from device
        NSString* flag= [managedDataObject_AD valueForKey:@"checkedIn"];
        //If not checkedIn, and need to checkIn, set flag to Yes
        if([flag isEqualToString:@"No"]){
            flag= @"Yes";
            [managedDataObject_AD setValue:flag forKey:@"checkedIn"];
        }
        
        //If checkedIn, and need to check out, set flag to No
        if([flag isEqualToString:@"Yes"]){
            flag= @"No";
            [managedDataObject_AD setValue:flag forKey:@"checkedIn"];
        }
        
    }
    
    [self saveData];

}

-(void)saveData{
    NSError *error;
    if(![managedObjectContext save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
    }
}

-(void) application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(nonnull NSData *)deviceToken{
    
    
    NSLog(@"Device Token: %@", deviceToken);
    NSString *device = [deviceToken description];
    device = [device stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    device = [device stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"DEVICE TOKEN: %@", device);

    
    
    [self.push application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}
-(void)application: (UIApplication*)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo{
    [self.push application: application didReceiveRemoteNotification:userInfo];

    [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
    
    
    if(application.applicationState == UIApplicationStateActive) {
        
        //app is currently active, can update badges count here
        NSLog(@"UIApplicationStateActive");
        
    } else if(application.applicationState == UIApplicationStateBackground){
        
        //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
        NSLog(@"UIApplicationStateBackground");

    } else if(application.applicationState == UIApplicationStateInactive){
        
        //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
        NSLog(@"UIApplicationStateInactive");

        /*
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        
        UIViewController *viewController = // determine the initial view controller here and instantiate it with [storyboard instantiateViewControllerWithIdentifier:<storyboard id>];
        
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];
        
         */
    }
    
    

}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler{
    if(application.applicationState == UIApplicationStateActive) {
        
        //app is currently active, can update badges count here
        NSLog(@"UIApplicationStateActive");
        
    } else if(application.applicationState == UIApplicationStateBackground){
        
        //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
        NSLog(@"UIApplicationStateBackground");
        
    } else if(application.applicationState == UIApplicationStateInactive){
        
        //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
        NSLog(@"UIApplicationStateInactive");
        //[self showTestAlert];
        [self gotoChatScreenFromPushClick];
        /*
         self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
         
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
         
         UIViewController *viewController = // determine the initial view controller here and instantiate it with [storyboard instantiateViewControllerWithIdentifier:<storyboard id>];
         
         self.window.rootViewController = viewController;
         [self.window makeKeyAndVisible];
         
         */
    }
    
    
    [self.push application: application didReceiveRemoteNotification:userInfo];
}

    //PUSH NOTIFICATIONS FOR iOS 10
    //Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}
    
    //Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
}
    


//*********************************Sinch Code**************************************************
//Initiliase the Sinch Client
-(void)initSinchClient: (NSString*)userId{
    
    if(userId == nil){
        NSLog(@"APP DELEGATE ERROR: Unable to create Sinch client, userId is empty");
        //Tell user to restart app or login again
    }
    else{
        self.sinchClient= [Sinch clientWithApplicationKey:SINCH_APPLICATION_KEY applicationSecret:SINCH_APPLICATION_SECRET environmentHost:SINCH_ENVIRONMENT_HOST userId:userId];
    
        NSLog(@"Sinch version: %@, userId: %@", [Sinch version], [self.sinchClient userId]);
    
        [self.sinchClient setSupportMessaging:YES];
        //Allow for background listening
        [self.sinchClient setSupportActiveConnectionInBackground:YES];
        [self.sinchClient enableManagedPushNotifications];
        [self.sinchClient start];
        [self.sinchClient startListeningOnActiveConnection];
        self.sinchClient.delegate= self;
        
        //SINCH REMOTE NOTIFICATIONS
        self.push   = [Sinch managedPushWithAPSEnvironment:SINAPSEnvironmentAutomatic];
        self.push.delegate= self;
        [self.push setDesiredPushTypeAutomatically];
        [self.push registerUserNotificationSettings];
    }
}

-(void)loggedOutWork:(NSNotification*)notification{
    [self.sinchClient unregisterPushNotificationDeviceToken];
}

#pragma mark SINCH Push Notification methods
-(void)managedPush:(id<SINManagedPush>)managedPush didReceiveIncomingPushWithPayload:(NSDictionary *)payload forType:(NSString *)pushType{
    //[self.sinchClient relayRemotePushNotification:payload];
    id<SINClient> client;
    
    NSDictionary *aps = [payload objectForKey:@"aps"];
    NSLog(@"APS = %@",aps);
    
    
    NSDictionary *newPushDictAps = [[NSDictionary alloc] initWithObjectsAndKeys:[aps objectForKey:@"alert"], @"alert", @"Glass.aiff", @"sound", nil];

    
    NSDictionary *newPushDict = [[NSDictionary alloc] initWithObjectsAndKeys:newPushDictAps, @"aps", [payload objectForKey:@"sin"], @"sin", nil];
    
    NSLog(@"OLD = %@",payload);
    NSLog(@"NEW = %@",newPushDict);

    [client relayRemotePushNotification:newPushDict];

    NSLog(@"managedPush********");
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)call {
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [call remoteUserId]];
    return notification;
}

#pragma mark SINClientDelegate methods
-(void)clientDidStart:(id<SINClient>)client{
    NSLog(@"Start SINClient successfull");
    self.sinchMessageClient= [self.sinchClient messageClient];
    self.sinchMessageClient.delegate= self;
}
-(void)clientDidFail:(id<SINClient>)client error:(NSError *)error{
    NSLog(@"Start SINClient failed. Description %@. Reason: %@.", error.localizedDescription, error.localizedFailureReason);
    //Get user to log in again?
}

#pragma mark SINMessageClientDelegate methods
//Receiving an incoming message
-(void)messageClient:(id<SINMessageClient>)messageClient didReceiveIncomingMessage:(id<SINMessage>)message{

    chatMessageHistory *chatMessage= message;
    //Get name of sender
    NSString *senderName= [self getNameOfSenderWithId:chatMessage.senderId];
    //NSString* messageText= chatMessage.text;

    NSString *messageText= [senderName stringByAppendingString:@": "];
    messageText= [messageText stringByAppendingString:chatMessage.text];
    
    //Save message to server
    //[accessServer saveMessagesToServer:chatMessage.messageId reciptId:entityId_AD sendrIds:chatMessage.senderId text:chatMessage.text date:chatMessage.timestamp];
    
    //Save message to device
    [self saveIncomingMessageToDev:chatMessage];
    
    
    if([UIApplication sharedApplication].applicationState== UIApplicationStateBackground){
        UILocalNotification *notification= [[UILocalNotification alloc]init];
        notification.alertBody= messageText;
        notification.soundName= @"Glass.aiff";
        [[UIApplication sharedApplication]presentLocalNotificationNow:notification];
        
        
        //TODO Something here
        
        //Post notifications
        [[NSNotificationCenter defaultCenter]postNotificationName:@"newMessageAlertInBase" object:self];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"newMessageAlertInCheckedin" object:self];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"newMessageNoti" object:self];

    }
    else{
        //Nothing
        //Call all the notifications
        [[NSNotificationCenter defaultCenter]postNotificationName:@"newMessageAlertInBase" object:self];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"newMessageAlertInCheckedin" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:SINCH_MESSAGE_RECIEVED object:self userInfo:@{@"message": message}];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"newMessageNoti" object:self];
    }

    
}
//Finish sending a message
-(void)messageSent:(id<SINMessage>)message recipientId:(NSString *)recipientId{
    [self saveMessagesOnDataBase:message];
    [[NSNotificationCenter defaultCenter]postNotificationName:SINCH_MESSAGE_SENT object:self userInfo:@{@"message": message}];
}

//Failed to send a message
-(void)messageFailed:(id<SINMessage>)message info:(id<SINMessageFailureInfo>)messageFailureInfo{
    
    [[NSNotificationCenter defaultCenter]postNotificationName:SINCH_MESSAGE_FAILED object:self userInfo:@{@"message": message, @"failed":@"YES", @"error":messageFailureInfo}];
    
    NSLog(@"MessageBoard: message to %@ failed. Description: %@. Reason: %@.", messageFailureInfo.recipientId, messageFailureInfo.error.localizedDescription, messageFailureInfo.error.localizedFailureReason);
}
//Message delivered
-(void)messageDelivered:(id<SINMessageDeliveryInfo>)info{
    //[[NSNotificationCenter defaultCenter]postNotificationName:SINCH_MESSAGE_DELIVERED object:info];
    //Send confirmation to another method and let user know that message is delivered.
}

#pragma mark Functional methods
//Send a text message
-(void)sendTextMessage:(NSString *)messageText toRecipient:(NSString *)recipientID name:(NSString*)name{
    NSLog(@"Sending Message");
    //RANDOM
    NSString *tempMessage2= [name stringByAppendingString:@": "];
    tempMessage2= [tempMessage2 stringByAppendingString:messageText];
    [self.push setDisplayName: tempMessage2];
    
    SINOutgoingMessage *outgoingMessage= [SINOutgoingMessage messageWithRecipient:recipientID text:messageText];

    
    [self.sinchClient.messageClient sendMessage:outgoingMessage];
}

-(void)saveMessagesOnDataBase: (id<SINMessage>)message{
    
    sendMessage_messageId= [message messageId];
    sendMessage_senderId= [message senderId];
    sendMessage_recipientId= [message recipientIds ][0];
    sendMessage_text = [message text];
    sendMessage_timestamp = [message timestamp];
    sendMessage_header = [message headers];
    
    //Check if this message exists on the server, check for same text and timestamp
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(existingMessageResponse:) name:@"existingMessageResponse" object:nil];
    [accessServer checkForExistingMessages:sendMessage_recipientId senderId:sendMessage_senderId text:sendMessage_text timestamp:sendMessage_timestamp notiName:@"existingMessageResponse"];
    
}

-(void)existingMessageResponse:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"existingMessageResponse" object:nil];
    NSString *response = [[notification userInfo]objectForKey:@"response"];
    
    if([response isEqualToString:@"YES"]){
        //Check the timestamp first
        NSMutableArray *tempArray= [[NSMutableArray alloc]initWithArray:[[notification userInfo]objectForKey:@"messageData"]];
        BOOL messageExists= NO;
        for(int i=0; i<[tempArray count]; i++){
            NSMutableDictionary *tempDict= [[NSMutableDictionary alloc]initWithDictionary:[tempArray objectAtIndex:i]];
            if([sendMessage_text isEqualToString:[tempDict objectForKey:@"text"]]){
                if([self isSameDay:sendMessage_timestamp otherDay:[tempDict objectForKey:@"timestamp"]]){
                    messageExists= YES;
                    break;
                }
                else{
                    messageExists= NO;
                }
            }
            else{
                messageExists= NO;
            }

        }
        
        if(messageExists){
            
        }
        else{
            [accessServer saveMessagesToServer:sendMessage_messageId reciptId:sendMessage_recipientId sendrIds:sendMessage_senderId text:sendMessage_text date:sendMessage_timestamp];
        }
    }
    else if([response isEqualToString:@"error"]){
        //Nothing
    }
    else{
        [accessServer saveMessagesToServer:sendMessage_messageId reciptId:sendMessage_recipientId sendrIds:sendMessage_senderId text:sendMessage_text date:sendMessage_timestamp];
    }
}

-(NSString*)getNameOfSenderWithId:(NSString*)eId{
    
    //Set up CoreData
    //Get entityId and username from key chain
    entityId_AD = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    //Initialise Core data object
    NSError *error;
    managedObjectContext = [self managedObjectContext];
    NSFetchRequest *userDataRequest_AD= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_AD.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_AD];
    NSArray *userDataArray_AD = [managedObjectContext executeFetchRequest:userDataRequest_AD error:&error];
    
    if (userDataArray_AD.count == 0) {
        return @"none";
    }
    NSManagedObject *managedDataObject_AD = [userDataArray_AD objectAtIndex:0];

    
    NSMutableArray *tempArray= [[NSMutableArray alloc]initWithArray: [NSKeyedUnarchiver unarchiveObjectWithData:[managedDataObject_AD valueForKey:@"messageMates"]]];
    if([tempArray count]==0){
        
    }
    else{
        for(int i=0; i<tempArray.count; i++){
            NSMutableDictionary* tempDict= [[NSMutableDictionary alloc]initWithDictionary:[tempArray objectAtIndex:i]];
            if([[tempDict objectForKey:@"chatMateId"] isEqualToString:eId]){
                return [tempDict objectForKey:@"chatMateName"];
            }
        }
    }
    return @"none";
}

-(void)saveIncomingMessageToDev:(chatMessageHistory*)messageDict{
    
    //Add this message to device array
    NSMutableDictionary *message = [[NSMutableDictionary alloc]init];
    [message setObject:messageDict.messageId forKey:@"messageId"];
    [message setObject:messageDict.senderId forKey:@"senderId"];
    [message setObject:[messageDict.recipientIds objectAtIndex:0] forKey:@"recipientId"];
    [message setObject:messageDict.text forKey:@"text"];
    [message setObject:messageDict.timestamp forKey:@"timestamp"];
    [message setObject:@"NO" forKey:@"messageFailed"];
    
    NSError *error;
    NSFetchRequest *request= [[NSFetchRequest alloc]initWithEntityName:@"Friends"];
    request.predicate = [NSPredicate predicateWithFormat:@"chatMateId == %@", [message valueForKey:@"senderId"]];
    
    request.sortDescriptors= @[[NSSortDescriptor sortDescriptorWithKey:@"chatMateId" ascending:YES]];
    NSArray *results= [managedObjectContext executeFetchRequest:request error:&error];
    
    if([results count]==0){
        //Create new
        NSMutableArray *tempNewArray= [[NSMutableArray alloc]init];
        [tempNewArray addObject:message];
        NSData *messageData= [NSKeyedArchiver archivedDataWithRootObject: tempNewArray];
        NSManagedObject *newMessageObject= [NSEntityDescription insertNewObjectForEntityForName:@"Friends" inManagedObjectContext:managedObjectContext];
        [newMessageObject setValue:messageData forKey:@"messageDict"];
        [newMessageObject setValue:[message valueForKey:@"senderId"] forKey:@"chatMateId"];
        [newMessageObject setValue:@"Yes" forKey:@"newMessage"];
    }
    else{
        NSManagedObject *messageObj= [results objectAtIndex:0];
        NSMutableArray *tempArray= [[NSMutableArray alloc]initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:[messageObj valueForKey:@"messageDict"]]];
        
        
        //Check if the message is potentially existing
        BOOL messageExists= NO;
        for(int i=0; i<[tempArray count];i++){
            NSMutableDictionary *tempDict= [[NSMutableDictionary alloc]initWithDictionary:[tempArray objectAtIndex:i]];
            //Check if the message text matches exactly
            if([[tempDict objectForKey:@"text"]isEqualToString:[message objectForKey:@"text"]]){
                //Check if the time stamps match
                BOOL isSameTimestamp= [self isSameDay:[tempDict objectForKey:@"timestamp"] otherDay:[message objectForKey:@"timestamp"]];
                
                if(isSameTimestamp){
                    //Dont do anything
                    messageExists= YES;
                    break;
                }
                else{
                    messageExists= NO;
                }
            }
            else{
                messageExists= NO;
            }
        }
        
        if(messageExists){
            
        }
        else{
            if([tempArray count]<50){
                [tempArray addObject:message];
            }
            else{
                //Remove the top message
                [tempArray removeObjectAtIndex:0];
                //Add the new message
                [tempArray addObject:message];
            }
        }
        
        //Archive again and save to device
        NSData *messageData= [NSKeyedArchiver archivedDataWithRootObject: tempArray];
        [messageObj setValue:messageData forKey:@"messageDict"];
        [messageObj setValue:@"Yes" forKey:@"newMessage"];
    }
    if(![managedObjectContext save:&error]){
        NSLog(@"FAILED TO SAVE MESSAGES:%@",error);
    }
    else{
        NSLog(@"MESSAGES SAVED TO DEVICE");
    }
}

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year] && [comp1 hour] == [comp2 hour] && [comp1 minute] == [comp2 minute] && [comp1 second] == [comp2 second];

}

-(void)gotoChatScreenFromPushClick {
    if ([self.window.rootViewController isKindOfClass:[MMDrawerController class]]) {
        MMDrawerController *rootController = (MMDrawerController*)self.window.rootViewController;

        
        
        UIViewController *currentController = rootController.centerViewController;
        
        if ([currentController isKindOfClass:[UITabBarController class]]) {
            
            UITabBarController *tabController = (UITabBarController *)currentController;
            [tabController setSelectedIndex:1];
            
            if ([[tabController selectedViewController] isKindOfClass:[UINavigationController class]]) {
                
                
                UINavigationController *friendControllerNavigation = [tabController selectedViewController];
                
                
                
                UIViewController *friendController = [[friendControllerNavigation viewControllers] lastObject];
                if ([friendController isKindOfClass:[messageDialogClass class]]) {
                    [friendControllerNavigation dismissViewControllerAnimated:NO completion:^{
                        [friendController performSegueWithIdentifier:@"openMessageDialog" sender:nil];
                    }];
                }
                else{
                    [friendController performSegueWithIdentifier:@"openMessageDialog" sender:nil];
                }
                

            }
            else{
                
                UIViewController *friendController = [tabController selectedViewController];
                
                [friendController performSegueWithIdentifier:@"openMessageDialog" sender:nil];
            }
            
            
            
            /*
            currentController = [(UITabBarController *)currentController selectedViewController];
            UITabBarController *tabController = (UITabBarController *)currentController;
            [tabController setSelectedIndex:1];
             */
        }
        
        
        
        
        
        
    }
    
    
    
    
    
}

@end
