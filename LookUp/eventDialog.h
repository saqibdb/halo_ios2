//
//  eventDialog.h
//  LookUp
//
//  Created by Divey Punj on 7/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerQueryBridge.h"
#import "userClass.h"
#import "userSettings.h"
#import "messageClass.h"

@interface eventDialog : UIViewController


@property(strong, nonatomic)NSString* eventName;
@property(strong, nonatomic)NSString* eventDate;
@property(strong, nonatomic)NSString* eventAddress;
@property(strong, nonatomic)NSString* eventAttendees;
@property(strong, nonatomic)NSString* eventId;
@property(strong, nonatomic)UIImage* eventImage;
@property(strong, nonatomic)NSString* eventDescription;

@property (strong, nonatomic) IBOutlet UILabel *eventNameLab;
@property (strong, nonatomic) IBOutlet UIImageView *eventImageView;
@property (strong, nonatomic) IBOutlet UILabel *eventAddressLab;
@property (strong, nonatomic) IBOutlet UILabel *eventDateLab;
@property (strong, nonatomic) IBOutlet UILabel *eventAttendeesLab;
@property (strong, nonatomic) IBOutlet UIButton *eventYesButton;
@property (strong, nonatomic) IBOutlet UIButton *eventNoButton;
@property (strong, nonatomic) IBOutlet UIButton *eventMaybeButton;
@property (strong, nonatomic) IBOutlet UILabel *eventImageLoadingLab;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *eventImageLoadingIndicator;
@property (strong, nonatomic) IBOutlet UILabel *goingStatusLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *eventDialogScrollview;
@property (strong, nonatomic) IBOutlet UITextView *eventDescriptionTextview;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *eventViewHeightConstraint;


- (IBAction)eventYesAction:(id)sender;
- (IBAction)eventNoAction:(id)sender;
- (IBAction)eventMaybeAction:(id)sender;




@end
