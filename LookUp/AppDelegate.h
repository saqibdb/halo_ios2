//
//  AppDelegate.h
//  LookUp
//
//  Created by Divey Punj on 14/02/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import "config.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "signUp.h"
#import "messageDialogClass.h"
#import "chatMessageHistory.h"
#import <CoreData/CoreData.h>
#import "MMDrawerController.h"
#import <SAMKeychain/SAMKeychain.h>
#import "Reachability.h"
#import <UserNotifications/UserNotifications.h>


@class serverBridgeClass;

@interface AppDelegate : UIResponder <UIApplicationDelegate, SINClientDelegate, SINMessageClientDelegate, SINManagedPushDelegate, UNUserNotificationCenterDelegate>
    
    #define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic)id<SINClient> sinchClient;
@property (strong, nonatomic)id<SINMessageClient> sinchMessageClient;
@property (nonatomic, readwrite, strong) id<SINManagedPush> push;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic)MMDrawerController *drawerContainer;



-(void)initSinchClient: (NSString*)userId;
-(void)sendTextMessage:(NSString *)messageText toRecipient:(NSString *)recipientID name:(NSString*)name;
-(void)saveContext;
-(NSURL *)applicationDocumentsDirectory;
-(void)deleteAndRecreateStore;
-(void)setupSliderMenu;















@end

