//
//  events.h
//  LookUp
//
//  Created by Divey Punj on 7/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "eventDialog.h"
#import "ServerQueryBridge.h"
#import "eventCustomCell.h"
#import "userClass.h"
#import "config.h"

@interface events : UIViewController<UITableViewDataSource, UITableViewDelegate>


@property (strong, nonatomic) IBOutlet UITableView *eventsListTable;
@property (strong, nonatomic) eventDialog *activeEventDialog;
@property (strong, nonatomic) IBOutlet UIButton *mapViewButton;
@property (strong, nonatomic) IBOutlet UIButton *myEventsButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *eventsLoadIndicator;
@property (strong, nonatomic) IBOutlet UILabel *eventsLoadingLab;
@property (strong, nonatomic) IBOutlet UIView *eventsView;
@property (strong, nonatomic) IBOutlet UIView *eventsViewHolder;
@property (strong, nonatomic) IBOutlet UIButton *allEventsBut;

-(void)decodeEventsArray:(NSMutableArray*)array;
-(void)initialise;
-(NSMutableArray*)getEventsArray;
- (IBAction)myEventsAction:(id)sender;
- (IBAction)allEventsAction:(id)sender;
-(void)fetchEvents;

@end
