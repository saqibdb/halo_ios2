//
//  messageDialogClass.h
//  LookUp
//
//  Created by Divey Punj on 17/04/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "messageCell.h"
#import "chatMessageHistory.h"
#import "AppDelegate.h"
#import "ServerQueryBridge.h"
#import "chatMessageHistoryLocal.h"
#import <SAMKeychain/SAMKeychain.h>
#import "ChatTableViewCell.h"
#import "ChatCellSettings.h"

@interface messageDialogClass : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate>


@property (strong, nonatomic) IBOutlet UILabel *otherUserNameTitleLabel;
@property(strong, nonatomic) NSMutableArray *messageArray;
@property(strong, nonatomic)NSString* otherUserId;
@property(strong, nonatomic)NSString* otherUserName;
@property(strong, nonatomic)UIImage* otherUserImage;
@property(strong, nonatomic)NSMutableArray* previousMessages;
@property (strong, nonatomic) IBOutlet UITableView *messageDialogTable;
@property (strong, nonatomic) IBOutlet UITextView *messageTextBox;
@property (strong, nonatomic) IBOutlet UIButton *sendMessageButton;
@property (strong, nonatomic) IBOutlet UIScrollView *messagesScrollView;
@property (strong, nonatomic) IBOutlet UILabel *sendingMessageLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *messageDialogLoadIndicator;
@property (strong, nonatomic) IBOutlet UILabel *messageDialogLoadLab;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *messageDockViewHeightConstraint;
@property (strong, nonatomic) IBOutlet UIProgressView *messageSendProgress;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *messageTextBoxHeightConstraint;
@property (strong, nonatomic) IBOutlet UILabel *backgroundText;



- (IBAction)sendMessageAction:(id)sender;
-(void)databaseToMessageArray: (NSMutableArray*)array;
-(void)createMessageNotificationBeforeClosingApp;



@end
