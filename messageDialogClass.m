//
//  messageDialogClass.m
//  LookUp
//
//  Created by Divey Punj on 17/04/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "messageDialogClass.h"


ServerQueryBridge *serverMessage;

NSMutableArray *tempMessageArray, *cleanMessageArray;
NSInteger *tableScrollPosition;
float padding, myMessagePadding;
int screenSize;
NSTimer *progressTimer;
static int count;
BOOL allowedToLoadMoreMessages;
int skipCount;
UIRefreshControl *refreshControl;
float messageDockFrame, defaultTextMessageHeight;
NSManagedObjectContext *context_mdc;
NSManagedObject *managedDataObject_mdc;
AppDelegate *delegate_mdc;
NSFetchRequest *userDataRequest_mdc;
NSArray *userDataArray_mdc;
NSString *entityId_mdc;
ChatCellSettings *chatCellSettings;

@implementation messageDialogClass
@synthesize otherUserId, messageDialogTable, otherUserName, sendMessageButton, messagesScrollView, sendingMessageLabel, messageDockViewHeightConstraint, messageTextBox, messageSendProgress, messageTextBoxHeightConstraint, previousMessages;

-(void)viewDidLoad{
    [super viewDidLoad];

    messageDialogTable.separatorColor= [UIColor clearColor];
    
    //Get entityId and username from key chain
    entityId_mdc = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    //Initialise Core data object
    NSError *error;
    context_mdc = [self managedObjectContext];
    userDataRequest_mdc= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_mdc.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_mdc];
    userDataArray_mdc = [context_mdc executeFetchRequest:userDataRequest_mdc error:&error];
    managedDataObject_mdc= [userDataArray_mdc objectAtIndex:0];
    
    //Create Notifications
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(messageRecieved:) name:SINCH_MESSAGE_RECIEVED object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(myMessageDelivered:) name:SINCH_MESSAGE_SENT object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(messageFailed:) name:SINCH_MESSAGE_FAILED object:nil];
    //[[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(myMessageDelivered:) name:SINCH_MESSAGE_DELIVERED object:nil];
    //Listen out for message Array
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(messageArrayLoaded:) name:@"messageArrayLoaded" object:nil];
    
    [self initialise];

    chatCellSettings = [ChatCellSettings getInstance];
    [chatCellSettings setSenderBubbleColorHex:@"007AFF"];
    [chatCellSettings setReceiverBubbleColorHex:@"DFDEE5"];
    [chatCellSettings setSenderBubbleNameTextColorHex:@"FFFFFF"];
    [chatCellSettings setReceiverBubbleNameTextColorHex:@"000000"];
    [chatCellSettings setSenderBubbleMessageTextColorHex:@"FFFFFF"];
    [chatCellSettings setReceiverBubbleMessageTextColorHex:@"000000"];
    [chatCellSettings setSenderBubbleTimeTextColorHex:@"FFFFFF"];
    [chatCellSettings setReceiverBubbleTimeTextColorHex:@"000000"];
    
    [chatCellSettings setSenderBubbleFontWithSizeForName:[UIFont boldSystemFontOfSize:11]];
    [chatCellSettings setReceiverBubbleFontWithSizeForName:[UIFont boldSystemFontOfSize:11]];
    [chatCellSettings setSenderBubbleFontWithSizeForMessage:[UIFont systemFontOfSize:14]];
    [chatCellSettings setReceiverBubbleFontWithSizeForMessage:[UIFont systemFontOfSize:14]];
    [chatCellSettings setSenderBubbleFontWithSizeForTime:[UIFont systemFontOfSize:11]];
    [chatCellSettings setReceiverBubbleFontWithSizeForTime:[UIFont systemFontOfSize:11]];
    
    [chatCellSettings senderBubbleTailRequired:YES];
    [chatCellSettings receiverBubbleTailRequired:YES];
    
    [messageDialogTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [messageDialogTable registerClass:[ChatTableViewCell class] forCellReuseIdentifier:@"chatSend"];
    
    [messageDialogTable registerClass:[ChatTableViewCell class] forCellReuseIdentifier:@"chatReceive"];
    
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void)viewWillAppear:(BOOL)animated{

    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self loadMessages];

}

-(void)viewDidAppear:(BOOL)animated{
     [super viewDidAppear:animated];

    
}

-(void)viewDidDisappear:(BOOL)animated{
    
    //Create a notification for incoming messages
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(messageRecievedInBackground:) name:@"messageRecievedInBackground" object:nil];
    
    //[self goBackNavigation];

}

-(void)initialise{
    
    //DESIGN INITIALISATION
    sendingMessageLabel.text= @"";
    //Add users name to the top
    self.navigationItem.title= otherUserName;
    
    //Initialise Variables
    serverMessage= [[ServerQueryBridge alloc]init];
    [serverMessage initialiseBackend];
    [serverMessage initialiseCoreData:entityId_mdc];
    
    //Tap gesture to remove keyboard
    UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeKeyboard:)];
    [self.messageDialogTable addGestureRecognizer:tap];
    
    tempMessageArray= [[NSMutableArray alloc]init];
    cleanMessageArray= [[NSMutableArray alloc]init];
    
    self.messageTextBox.delegate= self;
    padding= 15.0;
    myMessagePadding= 15.0;
    screenSize=0;
    screenSize= [self getScreenSize];
    
    count=0;
    messageSendProgress.progress= 100;
    
    //allowedToLoadMoreMessages= NO;
    skipCount=0;
    
    //TEST
    refreshControl = [[UIRefreshControl alloc]init];
    refreshControl.backgroundColor= [UIColor redColor];
    refreshControl.tintColor= [UIColor whiteColor];
    refreshControl.attributedTitle= [[NSAttributedString alloc]initWithString:@"Loading more messages.."];
    [refreshControl addTarget:self action:@selector(loadMoreMessages) forControlEvents:UIControlEventValueChanged];
    
    [messageDialogTable addSubview:refreshControl];
    
    messageDockFrame= messageTextBox.frame.size.height;
    
    defaultTextMessageHeight= 35;
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    //[self scrollToBottom];
}

-(int)getScreenSize{
    //Check screen size to adjust the view size
    if([[UIDevice currentDevice]userInterfaceIdiom]== UIUserInterfaceIdiomPhone){
        if([[UIScreen mainScreen]bounds].size.width == 320){
            //iPhone 4s
            return 305;
        }
        else{
            //iPhone 6 and more
            return 356;
            
        }
    }
    else{
        //iPad, so nothing
        return 0;
    }

}

-(void)loadMessages{

    //Check if there are messages on device, if so, then show them
    previousMessages = [[NSMutableArray alloc]initWithArray:[self getMessagesFromDevice:otherUserId]];
    
    if([previousMessages count]==0){
        //Get messages from server
        [self loadMessageWithCount:0];
    }
    else{
        //Show device messages
        tempMessageArray= [[NSMutableArray alloc]initWithArray:previousMessages];
        messageDialogTable.dataSource= self;
        messageDialogTable.delegate= self;
        [messageDialogTable reloadData];
    }
    [self scrollToBottom];
}

-(void)loadMessageWithCount:(int)count{
    //Create a notification
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(messagesLoaded:) name:@"messagesLoaded" object:nil];
    
    [serverMessage getMessagesFromServerAsSender:entityId_mdc recipient:otherUserId skipCount:count notiName:@"messagesLoaded"];
}

-(void)messagesLoaded:(NSNotification*)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"messagesLoaded" object:nil];
    //Check if tempMessageArray is empty, if then init this array with messages, otherwise append
    if([cleanMessageArray count]==0){
        cleanMessageArray = [[NSMutableArray alloc]initWithArray:[[notification userInfo]objectForKey:@"messageData"]];
        
        if([cleanMessageArray count]==0){
            //Show background text
            if (tempMessageArray.count) {
                _backgroundText.hidden= YES;
            }
            else{
                _backgroundText.hidden= NO;
            }
            
            if([HALO_FEEDBACK_ID isEqualToString:otherUserId]){
                _backgroundText.text= @"Send us a message! Give us feedback or ask a question!";
            }
            else{
                _backgroundText.text= @"You dont have any messages yet, send a message!";
            }
            [self.view layoutIfNeeded];
        }
        else{
            _backgroundText.hidden= YES;
        }
    }
    else{
        [cleanMessageArray addObjectsFromArray:[[notification userInfo]objectForKey:@"messageData"]];
        _backgroundText.hidden= YES;
    }
    
    if (cleanMessageArray.count == 0) {
        [refreshControl endRefreshing];
        [self scrollToBottom];
        return;
    }
    else{
        NSLog(@"Count = %lu",(unsigned long)tempMessageArray.count);
    }
    //Reverse the tempMessageArray
    tempMessageArray = [[[cleanMessageArray reverseObjectEnumerator] allObjects] mutableCopy];
    
    //Only allow 50 messages to get through to be saved on device
    if([tempMessageArray count]<50){
        [self saveMessagesToDevice:tempMessageArray chatMateId:otherUserId];
    }
    else{
        //Get lower limit
        NSRange endRange= NSMakeRange(tempMessageArray.count >= 50? tempMessageArray.count - 50:0, MIN(tempMessageArray.count, 50));
        
        NSArray *tempArray= [tempMessageArray subarrayWithRange:endRange];
        [self saveMessagesToDevice:tempArray chatMateId:otherUserId];
    }
    
    NSLog(@"Messages: %@", tempMessageArray);
    
    [refreshControl endRefreshing];
    messageDialogTable.dataSource= self;
    messageDialogTable.delegate= self;
    [messageDialogTable reloadData];
    [self scrollToBottom];
    
}



-(void)scrollToBottom{

    
    [messageDialogTable scrollRectToVisible:CGRectMake(0, messageDialogTable.contentSize.height - messageDialogTable.bounds.size.height, messageDialogTable.bounds.size.width, messageDialogTable.bounds.size.height) animated:YES];
}


//*********************************Tableview Methods********************
#pragma mark User interface behavioral methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    //return [tempMessageArray count];
    if([tempMessageArray count]==0){
        return 0;
    }
    else{
        return [tempMessageArray count];
    }

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ChatTableViewCell *chatCell;
    
    if(tempMessageArray.count==0){
        NSLog(@"No messages to display");
        //Display no messages label
    }
    else{
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:tempMessageArray[indexPath.row]];
        //NSLog(@"TEXT: %@",[dict objectForKey:@"text"]);
                          
        NSString *message= [dict objectForKey:@"text"];
        NSDate *date= [dict objectForKey:@"timestamp"];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
        NSInteger hour = [components hour];
        NSInteger minute = [components minute];
        NSString *messageTime= [[NSString stringWithFormat:@"%ld",(long)hour]stringByAppendingString:@":"];
        messageTime= [messageTime stringByAppendingString:[NSString stringWithFormat:@"%ld",(long)minute]];
        
        
        if([[dict objectForKey:@"senderId"] isEqualToString:entityId_mdc]){
            
            /*Uncomment second line and comment first to use XIB instead of code*/
            chatCell = (ChatTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"chatSend"];
            //chatCell = (ChatTableViewCellXIB *)[tableView dequeueReusableCellWithIdentifier:@"chatSend"];
            
            chatCell.chatMessageLabel.text = message;
            
            chatCell.chatNameLabel.text = @"";
            
            chatCell.chatTimeLabel.text = messageTime;
            
            chatCell.chatUserImage.image = [UIImage imageWithData:[managedDataObject_mdc valueForKey:@"userDP"]];
            
            /*Comment this line is you are using XIB*/
            chatCell.authorType = iMessageBubbleTableViewCellAuthorTypeSender;

        }
        else{
            /*Uncomment second line and comment first to use XIB instead of code*/
            chatCell = (ChatTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"chatReceive"];
            if (!chatCell) {
                chatCell = [[ChatTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                     reuseIdentifier:@"chatReceive"];
            }
            //chatCell = (ChatTableViewCellXIB *)[tableView dequeueReusableCellWithIdentifier:@"chatReceive"];
            
            chatCell.chatMessageLabel.text = message;
            
            chatCell.chatNameLabel.text = @"";
            
            chatCell.chatTimeLabel.text = messageTime;
            
            if(_otherUserImage){
                chatCell.chatUserImage.image = _otherUserImage;
            }
            else{
                chatCell.chatUserImage.image = [UIImage imageNamed:@"defaultUser"];
            }
            
            
            
            /*Comment this line is you are using XIB*/
            chatCell.authorType = iMessageBubbleTableViewCellAuthorTypeReceiver;

        }
        
        
    }

    return chatCell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:tempMessageArray[indexPath.row]];
        
    NSString *message= [dict objectForKey:@"text"];
    
    CGSize size;
    
    CGSize Namesize;
    CGSize Timesize;
    CGSize Messagesize;
    
    NSArray *fontArray = [[NSArray alloc] init];
    
    //Get the chal cell font settings. This is to correctly find out the height of each of the cell according to the text written in those cells which change according to their fonts and sizes.
    //If you want to keep the same font sizes for both sender and receiver cells then remove this code and manually enter the font name with size in Namesize, Messagesize and Timesize.
    if([[dict objectForKey:@"senderId"] isEqualToString:entityId_mdc])
    {
        fontArray = chatCellSettings.getSenderBubbleFontWithSize;
    }
    else
    {
        fontArray = chatCellSettings.getReceiverBubbleFontWithSize;
    }
    
    //Find the required cell height
    Namesize = [@"Name" boundingRectWithSize:CGSizeMake(220.0f, CGFLOAT_MAX)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName:fontArray[0]}
                                     context:nil].size;
    
    
    
    Messagesize = [message boundingRectWithSize:CGSizeMake(220.0f, CGFLOAT_MAX)
                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                 attributes:@{NSFontAttributeName:fontArray[1]}
                                                    context:nil].size;
    
    
    Timesize = [@"Time" boundingRectWithSize:CGSizeMake(220.0f, CGFLOAT_MAX)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName:fontArray[2]}
                                     context:nil].size;
    
    
    size.height = Messagesize.height + Namesize.height + Timesize.height + 48.0f;
    
    return size.height;
    
}


//****************************************************************************************


//**********************************MESSAGE SEND*********************************************

- (IBAction)sendMessageAction:(id)sender {
    //TEST, turn the timer on for progress
    NSLog(@"Lenth = %lu" , (unsigned long)self.messageTextBox.text.length);
    
    
    

    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[self.messageTextBox.text stringByTrimmingCharactersInSet: set] length] == 0)
    {
        return;
    }
    
    
    
    
    if (![self.messageTextBox.text isEqualToString:@""]) {
        
        
        while ([self.messageTextBox.text length]) {
            NSString *lastChar = [self.messageTextBox.text substringFromIndex:[self.messageTextBox.text length] - 1];

            if ([lastChar isEqualToString:@"\n"]) {
                self.messageTextBox.text = [self.messageTextBox.text substringToIndex:[self.messageTextBox.text length]-1];
            }
            else{
                break;
            }

            
        }

        
        
        count=0;
        messageSendProgress.progress= 0.0;
        progressTimer= [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(timerForProgress) userInfo:nil repeats:YES];
        
        //Close keyboard
        //[self.view endEditing:YES];
        
        //Disable button first
        sendMessageButton.enabled= NO;
        //Display sending message label
        sendingMessageLabel.text= @"Sending....";
        //Send the message
        AppDelegate *appDelegate= (AppDelegate *)[[UIApplication sharedApplication]delegate];
        [appDelegate sendTextMessage:self.messageTextBox.text toRecipient:otherUserId name:[managedDataObject_mdc valueForKey:@"name"]];
        //Clear the textbox
        self.messageTextBox.text= @"";
        
        _backgroundText.hidden= YES;
    }
}

//**********************************MESSAGE DELIVERED/SENT*********************************************
-(void)myMessageDelivered: (NSNotification*)notification{
    
    NSLog(@"Message Delivered");
    chatMessageHistory *chatMessage= [[notification userInfo]objectForKey:@"message"];

    //Add this message to device array
    NSMutableDictionary *message = [[NSMutableDictionary alloc]init];
    [message setObject:chatMessage.messageId forKey:@"messageId"];
    [message setObject:chatMessage.senderId forKey:@"senderId"];
    [message setObject:[chatMessage.recipientIds objectAtIndex:0] forKey:@"recipientId"];
    [message setObject:chatMessage.text forKey:@"text"];
    [message setObject:chatMessage.timestamp forKey:@"timestamp"];
    [message setObject:@"NO" forKey:@"messageFailed"];

    [tempMessageArray addObject:message];
    [self.messageDialogTable reloadData];
    [self saveIncomingMessageToDev:message sentByMe:YES];
    [self scrollToBottom];
    
    //Enable Send button again
    sendMessageButton.enabled= YES;
    sendingMessageLabel.text= @"Sent";
    
    [self messageSentProgress];
}



//**********************************MESSAGE DELIVERY FAILED****************************************
-(void)messageFailed: (NSNotification*)notification{
    
    NSString *messageFailed = [[notification userInfo]objectForKey:@"failed"];
    chatMessageHistory *failedMessage = [[notification userInfo]objectForKey:@"message"];
    if([messageFailed isEqualToString:@"YES"]){
        //Go through and mark the message that failed
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]init];
        for(int i=0; i< [tempMessageArray count]==0; i++){
            tempDict = [[NSMutableDictionary alloc]initWithDictionary:[tempMessageArray objectAtIndex:i]];
            if([failedMessage.messageId isEqualToString:[tempDict objectForKey:@"messageId"]]){
                [tempDict setObject:@"YES" forKey:@"failed"];
            }
        }
        //FIGURE OUT HOW TO SAVE THIS MESSAGE
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Failed"
                                     message:@"The Message You are trying to send cannot be delivered."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                       [self messageSentProgress];
                                   }];

        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        //Reload table
        [self.messageDialogTable reloadData];
        sendMessageButton.enabled= YES;

    }
    
}


//**********************************MESSAGE RECIEVED*********************************************
-(void)messageRecieved:(NSNotification*)notification{

    NSLog(@"Message Recieved");
    chatMessageHistory *chatMessage= [[notification userInfo]objectForKey:@"message"];
    
    //Add this message to device array
    NSMutableDictionary *message = [[NSMutableDictionary alloc]init];
    [message setObject:chatMessage.messageId forKey:@"messageId"];
    [message setObject:chatMessage.senderId forKey:@"senderId"];
    [message setObject:[chatMessage.recipientIds objectAtIndex:0] forKey:@"recipientId"];
    [message setObject:chatMessage.text forKey:@"text"];
    [message setObject:chatMessage.timestamp forKey:@"timestamp"];
    [message setObject:@"NO" forKey:@"messageFailed"];
    
    //Check if this message, if the current recipient is open then show it, otherwise save to sever and device.
    if([chatMessage.senderId isEqualToString:otherUserId]){
        //Show message
        [tempMessageArray addObject:message];
        [self.messageDialogTable reloadData];
        [self scrollToBottom];
    }
    else{
        //Nothing
    }
    
}

-(void)saveIncomingMessageToDev:(NSMutableDictionary*)dict sentByMe:(BOOL)sentByMe{
    
    NSError *error;
    NSFetchRequest *request= [[NSFetchRequest alloc]initWithEntityName:@"Friends"];
    if(sentByMe){
        request.predicate = [NSPredicate predicateWithFormat:@"chatMateId == %@", [dict valueForKey:@"recipientId"]];
    }
    else{
        request.predicate = [NSPredicate predicateWithFormat:@"chatMateId == %@", [dict valueForKey:@"senderId"]];
    }
    
    request.sortDescriptors= @[[NSSortDescriptor sortDescriptorWithKey:@"chatMateId" ascending:YES]];
    NSArray *results= [context_mdc executeFetchRequest:request error:&error];
    
    if([results count]==0){
        //Create new
        NSMutableArray *tempNewArray= [[NSMutableArray alloc]init];
        [tempNewArray addObject:dict];
        NSData *messageData= [NSKeyedArchiver archivedDataWithRootObject: tempNewArray];
        NSManagedObject *newMessageObject= [NSEntityDescription insertNewObjectForEntityForName:@"Friends" inManagedObjectContext:context_mdc];
        [newMessageObject setValue:messageData forKey:@"messageDict"];
        if(sentByMe){
            [newMessageObject setValue:[dict valueForKey:@"recipientId"] forKey:@"chatMateId"];
        }
        else{
            [newMessageObject setValue:[dict valueForKey:@"senderId"] forKey:@"chatMateId"];
            [newMessageObject setValue:@"Yes" forKey:@"newMessage"];
        }
    }
    else{
        NSManagedObject *messageObj= [results objectAtIndex:0];
        NSMutableArray *tempArray= [[NSMutableArray alloc]initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:[messageObj valueForKey:@"messageDict"]]];
        
        if([tempArray count]<50){
            [tempArray addObject:dict];
        }
        else{
            //Remove the top message
            [tempArray removeObjectAtIndex:0];
            //Add the new message
            [tempArray addObject:dict];
        }
        
        //Archive again and save to device
        NSData *messageData= [NSKeyedArchiver archivedDataWithRootObject: tempArray];
        [messageObj setValue:messageData forKey:@"messageDict"];
        
        if(sentByMe){
            //Nothing
        }
        else{
            [messageObj setValue:@"Yes" forKey:@"newMessage"];
        }
    }
    if(![context_mdc save:&error]){
        NSLog(@"FAILED TO SAVE MESSAGES:%@",error);
    }
    else{
        NSLog(@"MESSAGES SAVED TO DEVICE");
    }
}

-(void)confirmNewMessage{
    NSError *error;
    NSFetchRequest *request= [[NSFetchRequest alloc]initWithEntityName:@"Friends"];
    request.predicate = [NSPredicate predicateWithFormat:@"chatMateId == %@", otherUserId];
    
    request.sortDescriptors= @[[NSSortDescriptor sortDescriptorWithKey:@"chatMateId" ascending:YES]];
    NSArray *results= [context_mdc executeFetchRequest:request error:&error];
    
    if([results count]==0){
        //Create new
        NSManagedObject *newMessageObject= [NSEntityDescription insertNewObjectForEntityForName:@"Friends" inManagedObjectContext:context_mdc];
        [newMessageObject setValue:@"No" forKey:@"newMessage"];
    }
    else{
        NSManagedObject *messageObj= [results objectAtIndex:0];
        [messageObj setValue:@"No" forKey:@"newMessage"];

    }
    if(![context_mdc save:&error]){
        NSLog(@"FAILED TO SAVE MESSAGES:%@",error);
    }
    else{
        NSLog(@"MESSAGES SAVED TO DEVICE");
    }
}

-(void)loadMoreMessages{
    
    skipCount= skipCount + 50;
    [self loadMessageWithCount:skipCount];
}

-(void)messageArrayLoaded: (NSNotification*)notification{

    [messageDialogTable reloadData];
    if(skipCount==0){
        [self scrollToBottom];
    }
    else{
        //Nothing
        [refreshControl endRefreshing];
    }
    
}


//The next 4 methods are responsible to allow text view to be seen when keyboard is open
//*************************KEYBOARD METHODS******************************//
-(void) closeKeyboard: (UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
    //[self resignFirstResponder];
}


-(void)textViewDidBeginEditing:(UITextView *)textView{
    [UIView animateWithDuration:1 animations:^{
        
        if(messageDockFrame == defaultTextMessageHeight){
            messageDockViewHeightConstraint.constant= 260;//260 or 310
            messageTextBoxHeightConstraint.constant= 35;
        }
        else{
            messageDockViewHeightConstraint.constant= 260 + messageDockFrame;
            messageTextBoxHeightConstraint.constant= messageDockFrame;
        }
        
    } completion:nil];

}

-(void)textViewDidEndEditing:(UITextView *)textView{
    [UIView animateWithDuration:1 animations:^{

    messageDockViewHeightConstraint.constant= 50;
    messageTextBoxHeightConstraint.constant= 35;
    messageDockFrame= 35;

        
    } completion:nil];
}


//********************************************************************************




-(void)goBackNavigation{
    UINavigationController *navControl= self.navigationController;
    [navControl popViewControllerAnimated:YES];
}


-(void)checkScreenSize{
    //Check screen size to adjust the view size
    int sizeAdd=0;
    if([[UIDevice currentDevice]userInterfaceIdiom]== UIUserInterfaceIdiomPhone){
        if([[UIScreen mainScreen]bounds].size.width == 320){
            //iPhone 4s
            sizeAdd= 305;
        }
        else{
            //iPhone 6 and more
            sizeAdd= 356;
            
        }
    }
    else{
        //iPad, so nothing
    }
    

}


-(BOOL)textView:(UITextView *)_textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if (_textView.text.length >= 200&& range.length == 0)
    {
        return NO; // return NO to not change text
    }
    
    [self adjustFrames];
    return YES;
}


-(void) adjustFrames
{
    
    CGRect textFrame = messageTextBox.frame;

    textFrame.size.height = messageTextBox.contentSize.height;
    
    int frameHeight= messageTextBox.frame.size.height;
    
    if(frameHeight > messageDockFrame){
        messageDockFrame= frameHeight;
        //Adjust the message dock height as well
        messageDockViewHeightConstraint.constant= messageDockViewHeightConstraint.constant+ 20;
    }
    
    messageTextBox.frame = textFrame;

}

-(void)timerForProgress{
    
    count++;
    
    if(count <=1000){
        messageSendProgress.progress= (float)count/100.0f;
    }
    else{
        [progressTimer invalidate];
        progressTimer= nil;
    }
}

-(void)messageSentProgress{
    count=1000;
    messageSendProgress.progress= 100;
}


-(NSMutableArray*)getMessagesFromDevice: (NSString*)chatMateId{
    
    NSError *error;
    
    
    NSFetchRequest *request= [[NSFetchRequest alloc]initWithEntityName:@"Friends"];
    request.predicate = [NSPredicate predicateWithFormat:@"chatMateId == %@", chatMateId];
    request.sortDescriptors= @[[NSSortDescriptor sortDescriptorWithKey:@"chatMateId" ascending:YES]];
    NSArray *results= [context_mdc executeFetchRequest:request error:&error];
    
    if([results count]==0){
        return [[NSMutableArray alloc]init];
    }
    else{
        NSManagedObject *readObject= [results objectAtIndex:0];
        
        NSMutableArray *tempArray= [[NSMutableArray alloc]initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:[readObject valueForKey:@"messageDict"]]];
        
        //Change the new message flag
        [readObject setValue:@"No" forKey:@"newMessage"];
        if(![context_mdc save:&error]){
            NSLog(@"FAILED TO SAVE MESSAGES:%@",error);
        }
        else{
            NSLog(@"MESSAGES SAVED TO DEVICE");
        }
        
        
        return tempArray;
    }
}

-(void)saveMessagesToDevice:(NSMutableArray*)messages chatMateId:(NSString*)chatMateId{
    
    
    NSError *error;
    
    NSData *messageData= [NSKeyedArchiver archivedDataWithRootObject: messages];
    
    NSFetchRequest *request= [[NSFetchRequest alloc]initWithEntityName:@"Friends"];
    request.predicate = [NSPredicate predicateWithFormat:@"chatMateId == %@", chatMateId];
    request.sortDescriptors= @[[NSSortDescriptor sortDescriptorWithKey:@"chatMateId" ascending:YES]];
    
    NSArray *results= [context_mdc executeFetchRequest:request error:&error];
    
    if([results count]==0){
        //Add new
        NSManagedObject *newMessageObject= [NSEntityDescription insertNewObjectForEntityForName:@"Friends" inManagedObjectContext:context_mdc];
        [newMessageObject setValue:messageData forKey:@"messageDict"];
        [newMessageObject setValue:chatMateId forKey:@"chatMateId"];
    }
    else{
        //Update existing
        NSManagedObject *updateMessageObject= [results objectAtIndex:0];
        [updateMessageObject setValue:messageData forKey:@"messageDict"];
    }
    
    if(![context_mdc save:&error]){
        NSLog(@"FAILED TO SAVE MESSAGES:%@",error);
    }
    else{
        NSLog(@"MESSAGES SAVED TO DEVICE");
    }
}

-(void)saveData{
    NSError *error;
    if(![context_mdc save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
    }
}


@end
