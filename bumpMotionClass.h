//
//  bumpMotionClass.h
//  LookUp
//
//  Created by Divey Punj on 27/03/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>

@interface bumpMotionClass : NSObject 

-(void)initliaseBumpDetection;
-(void)startBumpDetection;

@end
