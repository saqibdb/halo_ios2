//
//  chatMessageHistoryLocal.h
//  LookUp
//
//  Created by Divey Punj on 21/08/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface chatMessageHistoryLocal : NSObject <NSCoding>

@property (nonatomic, strong)NSString* messageId;
@property (nonatomic, strong)NSArray* recipientIds;
@property (nonatomic, strong)NSString* senderId;
@property (nonatomic, strong)NSString* text;
@property (nonatomic, strong)NSDictionary* headers;
@property (nonatomic, strong)NSDate* timestamp;

-(id)initWithCoder:(NSCoder *)aDecoder;
-(void)encodeWithCoder:(NSCoder *)aCoder;

@end
