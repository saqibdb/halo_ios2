//
//  chatMatesCustomCell.m
//  LookUp
//
//  Created by Divey Punj on 15/04/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "chatMatesCustomCell.h"



@implementation chatMatesCustomCell
@synthesize chatMateNameLabel, chatMatePhoto, messageContactBorder, messagePreviewLab, removeChatMateButton;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)removeChatMateAction:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"removeChatMateNoti" object:self userInfo:@{@"indexRow": [NSString stringWithFormat:@"%ld",[sender tag]]}];
}

- (IBAction)acceptAction:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptFriendRequest" object:self userInfo:@{@"indexRow": [NSString stringWithFormat:@"%ld",[sender tag]]}];
}

- (IBAction)rejectAction:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"rejectFriendRequest" object:self userInfo:@{@"indexRow": [NSString stringWithFormat:@"%ld",[sender tag]]}];
}

- (void) setCell : (NSString *)chatMateName imageUrl: (NSString*)imageUrl entityId:(NSString*)eID prevMessage: (NSString*)prevMessage showRemove:(BOOL)showRemove indexRow:(NSInteger)indexRow isNewMessage:(BOOL)isNewMessage mateStatus:(NSString*)mateStatus{
    
    //Set prev message
    messagePreviewLab.text= prevMessage;
    
    //Set name
    chatMateNameLabel.text= chatMateName;
    
    //Set image
    NSURL *url= [NSURL URLWithString:imageUrl];
    
    if(imageUrl){
        [chatMatePhoto sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"image_no_question_2.png"]];
    }
    else{
        NSLog(@"****No chat image****");
        [chatMatePhoto setImage:[UIImage imageNamed:@"image_no_question_2.png"]];
    }
    
    //Make the image view circular
    chatMatePhoto.layer.cornerRadius= chatMatePhoto.frame.size.width/2;
    chatMatePhoto.clipsToBounds= YES;
    
    //Check the status of the chat mates
    if([mateStatus isEqualToString:@"Accepted"]){
        _acceptBut.hidden= YES;
        _rejectBut.hidden= YES;
    }
    else if([mateStatus isEqualToString:@"Awaiting"]){
        _acceptBut.hidden= NO;
        _rejectBut.hidden= NO;
        [_acceptBut setTag:indexRow];
        [_rejectBut setTag:indexRow];
    }
    else if([mateStatus isEqualToString:@"Pending"]){
        messagePreviewLab.text= @"Pending request..";
        _acceptBut.hidden= YES;
        _rejectBut.hidden= YES;
    }
    else{
        _acceptBut.hidden= YES;
        _rejectBut.hidden= YES;
    }
    
    if(showRemove && ([mateStatus isEqualToString:@"Accepted"]||[mateStatus isEqualToString:@"Pending"])){
        NSLog(@"TAG: %ld",(long)indexRow);
        removeChatMateButton.hidden= NO;
        [removeChatMateButton setTag:indexRow];
    }
    else{
        removeChatMateButton.hidden= YES;
    }
    
    if(isNewMessage){
        _messNewInd.hidden= NO;
    }
    else{
        _messNewInd.hidden= YES;
    }
}



@end
