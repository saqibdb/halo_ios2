//
//  rightSideDrawerMenu.m
//  LookUp
//
//  Created by Divey Punj on 10/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "rightSideDrawerMenu.h"

NSManagedObjectContext *context_rsm;
NSManagedObject *managedDataObject_rsm;
AppDelegate *delegate_rsm;
NSFetchRequest *userDataRequest_rsm;
NSArray *userDataArray_rsm;
NSString *entityId_rsm;
NSMutableArray *locationArray_rsm;
ServerQueryBridge *servForRSM;

@interface rightSideDrawerMenu ()

@end

@implementation rightSideDrawerMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    //Get entityId and username from key chain
    entityId_rsm = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    //Initialise Core data object
    NSError *error;
    context_rsm = [self managedObjectContext];
    userDataRequest_rsm= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_rsm.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_rsm];
    userDataArray_rsm = [context_rsm executeFetchRequest:userDataRequest_rsm error:&error];
    
    if([userDataArray_rsm count]==0){
        //Log user out
        [self logoutCommand];
    }
    else{
        managedDataObject_rsm= [userDataArray_rsm objectAtIndex:0];
        
        [_locTab2 setDelegate:self];
        [_locTab2 setDataSource:self];
        
        servForRSM= [[ServerQueryBridge alloc ]init];
        [servForRSM initialiseBackend];
        [servForRSM initialiseCoreData:entityId_rsm];
        
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    //Load event locations from device
    NSError *error;
    NSFetchRequest *eventRequest= [[NSFetchRequest alloc]initWithEntityName:@"Events"];
    eventRequest.predicate= [NSPredicate predicateWithFormat:@"venueName == %@",[managedDataObject_rsm valueForKey:@"venueName"]];
    NSArray *eventsArray = [context_rsm executeFetchRequest:eventRequest error:&error];
    
    if([eventsArray count]==0){
        locationArray_rsm= [[NSMutableArray alloc]init];
        [locationArray_rsm addObject:@"At the bar"];
        [locationArray_rsm addObject:@"Sitting Area"];
        [locationArray_rsm addObject:@"Near the toilets"];
        [locationArray_rsm addObject:@"Outside area"];
        [locationArray_rsm addObject:@"Near the entrance"];
    }
    else{
        NSManagedObject *eventObj= [eventsArray objectAtIndex:0];
        locationArray_rsm = [[NSMutableArray alloc]initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:[eventObj valueForKey:@"venueLocations"]]];
    }

    _profileFilterLab.text= [managedDataObject_rsm valueForKey:@"statusFilter"];
    _currentLoc.text= [managedDataObject_rsm valueForKey:@"insideLocation"];
    [_locTab2 reloadData];
    
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//******************************TABLE VIEW METHODS**********************************


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return locationArray_rsm.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Main table view
    
    
    rightSideDrawerTabCell *cell= [tableView dequeueReusableCellWithIdentifier:@"chkLocCell2"];
    
    if(cell == nil){
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: @"leftSliderCell"];
    }
    
    [cell setCell:[locationArray_rsm objectAtIndex:indexPath.row]];
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    //sliderMenuToSettingsSegue
    
    [managedDataObject_rsm setValue:[locationArray_rsm objectAtIndex:indexPath.row] forKey:@"insideLocation"];
    [self saveData];
    _currentLoc.text= [locationArray_rsm objectAtIndex:indexPath.row];
}


-(void)saveData{
    NSError *error;
    if(![context_rsm save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
        //Save server
        [servForRSM saveData:@"" entityId:entityId_rsm];

    }
}


- (IBAction)haloProfFiltRed:(id)sender {
    _profileFilterLab.text= @"Red";
    [[NSNotificationCenter defaultCenter]postNotificationName:@"filterProfiles" object:nil userInfo:@{@"filterBy":@"Red"}];
    [managedDataObject_rsm setValue:@"Red" forKey:@"statusFilter"];
    [self saveData];
}

- (IBAction)haloProfFiltYellow:(id)sender {
    _profileFilterLab.text= @"Amber";
    [[NSNotificationCenter defaultCenter]postNotificationName:@"filterProfiles" object:nil userInfo:@{@"filterBy":@"Amber"}];
    [managedDataObject_rsm setValue:@"Amber" forKey:@"statusFilter"];
    [self saveData];
}

- (IBAction)haloProfFiltGreen:(id)sender {
    _profileFilterLab.text= @"Green";
    [[NSNotificationCenter defaultCenter]postNotificationName:@"filterProfiles" object:nil userInfo:@{@"filterBy":@"Green"}];
    [managedDataObject_rsm setValue:@"Green" forKey:@"statusFilter"];
    [self saveData];
}

//***************LOG OUT METHODS*****************
-(void)logoutCommand{
    //Clear device data
    [self deleteCoreDataObjects];
    [self deleteCoreDataMessageObject];
    
    //Log out Kinvey
    [servForRSM logOutUser];
    
    //Notify user profile that user has logged out
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logoutStopAction" object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"loggedOutWork" object:nil];
    
    //Segue to sign up
    //[self performSegueWithIdentifier:@"sliderMenuToSignupSegue" sender:self];
    AppDelegate *tempDel = [[UIApplication sharedApplication]delegate];
    tempDel.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"initialLoginView"];
}

-(void)deleteCoreDataObjects{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    NSArray *tempArray = [context_rsm executeFetchRequest:fetchRequest error:&error];
    
    if([tempArray count]==0 || tempArray==nil){
        //Nothing
    }
    else{
        for(int i=0; i<[tempArray count]; i++){
            [context_rsm deleteObject:[tempArray objectAtIndex:i]];
        }
    }
    
    
    if(![context_rsm save:&error]){
        NSLog(@"Save failed:%@",error);
    }
}

-(void)deleteCoreDataMessageObject{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Friends"];
    NSArray *tempArray = [context_rsm executeFetchRequest:fetchRequest error:&error];
    
    if([tempArray count]==0 || tempArray==nil){
        //Nothing
    }
    else{
        for(int i=0; i<[tempArray count]; i++){
            [context_rsm deleteObject:[tempArray objectAtIndex:i]];
        }
    }
    
    
    if(![context_rsm save:&error]){
        NSLog(@"Save failed:%@",error);
    }
}




@end
