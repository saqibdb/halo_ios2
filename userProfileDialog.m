//
//  userProfileDialog.m
//  LookUp
//
//  Created by Divey Punj on 27/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "userProfileDialog.h"


ServerQueryBridge *serverForUserProfDialog;
BOOL imageLoaded, largeImageOn;

@implementation userProfileDialog

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:NO];
    
    serverForUserProfDialog= [[ServerQueryBridge alloc]init];
    [serverForUserProfDialog initialiseBackend];
    
    //Load event image
    //Check if image data is empty
    imageLoaded= NO;
    largeImageOn= YES;
    [_otherUsrProfTable setDelegate:self];
    [_otherUsrProfTable setDataSource:self];
    
    [_userImageSmall setAlpha:0];
    _userNameSmall.text= _otherUserName;
    [_userNameSmall setAlpha:0];
    
    if(_otherUserImageSmallUrl){
        //Add image to both large and small, first small
        NSURL *url= [NSURL URLWithString:_otherUserImageSmallUrl];
        [_userImageSmall sd_setImageWithURL:url];
        [_userImageLarge sd_setImageWithURL:url];
        [_userImageLarge setAlpha:.2];
        [_userImageLarge setTintColor:[UIColor whiteColor]];
        [self imageLoader];
        //Get Large image from server
        [self loadImage];
    }

}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
    //Remove image notification
    [[NSNotificationCenter defaultCenter]removeObserver:@"otherUserImageLoaded"];
    imageLoaded= NO;
}

-(void)imageLoader{
    
    if(imageLoaded==NO) {
        imageLoaded= YES;
        _imageLargeLoader.hidden= NO;
        [_imageLargeLoader startAnimating];
        _imageLargeLoadLab.hidden= NO;
    }
    else{
        imageLoaded= NO;
        _imageLargeLoader.hidden= YES;
        [_imageLargeLoader stopAnimating];
        _imageLargeLoadLab.hidden= YES;
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"SCROLL OFFSET: %f",scrollView.contentOffset.y);
    if(scrollView.contentOffset.y<=0){
        //At the top so increase height of events view and reduce height of table
        largeImageOn= YES;
        [self.view layoutIfNeeded];
        _userImageView.contentMode = UIViewContentModeRedraw;
        [UIView animateWithDuration:.6f animations:^{
            _userImageview_const_height.constant= USER_IMAGE_VIEW_MORE_HEIGHT;
            _userImageLarge_const_height.constant= USER_IMAGE_VIEW_MORE_HEIGHT;
            [self.view layoutIfNeeded];
            [_userImageLarge setAlpha:1];
            [_userImageSmall setAlpha:0];
            [_userNameSmall setAlpha:0];
        }];
        
    }
    else{
        largeImageOn= NO;
        [self.view layoutIfNeeded];
        _userImageView.contentMode = UIViewContentModeRedraw;
        [UIView animateWithDuration:.6f animations:^{
            _userImageview_const_height.constant= USER_IMAGE_VIEW_LESS_HEIGHT;
            _userImageLarge_const_height.constant= USER_IMAGE_VIEW_LESS_HEIGHT;
            [self.view layoutIfNeeded];
            [_userImageLarge setAlpha:0];
            [_userImageSmall setAlpha:1];
            [_userNameSmall setAlpha:1];
        }];
        
       
    }
}


-(void)loadImage{
    
    //Listen out for image
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(otherUserImageLoaded:) name:@"otherUserImageLoaded" object:nil];
    //Call server method to download image
    
    //dispatch_queue_t queue= dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    //dispatch_async(queue, ^{
        
    [serverForUserProfDialog downloadLargeImageByName:[_userEntityId stringByAppendingString:@"Large.png"] notificationName:@"otherUserImageLoaded" keyName:@"otherUserImage"];
    
    //});
}

-(void)otherUserImageLoaded:(NSNotification*)notification{

    NSString* imageUrlStr= [[notification userInfo]objectForKey:@"otherUserImage"];
    NSURL *url= [NSURL URLWithString:imageUrlStr];
    [_userImageLarge sd_setImageWithURL:url];
    [_userImageLarge setTintColor:[UIColor clearColor]];
    if(largeImageOn){
        [_userImageLarge setAlpha:1];
    }
    else{
        //Nothing
    }
    [self imageLoader];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Main table view
    
    
    otherUserProfCell *cell= [tableView dequeueReusableCellWithIdentifier:@"haloUserCell"];
    
    if(cell == nil){
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: @"haloUserCell"];
    }
    
    
    if(indexPath.row==0){
        [cell setCell:_otherUserName location:@"name" subtitle:@""];
    }
    else if(indexPath.row==1){
        [cell setCell:_otherUserAge location:@"info" subtitle:@"Age"];
    }
    else if(indexPath.row==2){
        [cell setCell:_otherUserOrientation location:@"info" subtitle:@"Orientation"];
    }
    else if(indexPath.row==3){
        [cell setCell:_otherUserStatus location:@"info" subtitle:@"Status"];
    }
    else if(indexPath.row==4){
        [cell setCell:_otherUserSearchingOrientation location:@"info" subtitle:@"Looking for"];
    }
    else if(indexPath.row==5){
        [cell setCell:_lastSeenLoc location:@"info" subtitle:@"Last Seen"];
    }
    else if(indexPath.row>=6){
        [cell setCell:_otherUserInterests location:@"info" subtitle:@"Interests"];
    }
    else{
        //Nothing
    }

    return cell;
    
}


@end
