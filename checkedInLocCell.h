//
//  checkedInLocCell.h
//  LookUp
//
//  Created by Divey Punj on 10/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface checkedInLocCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *locLab;

-(void)setCell:(NSString*)loc;

@end
