//
//  myStatsViewController.m
//  LookUp
//
//  Created by Divey Punj on 30/5/17.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "myStatsViewController.h"


NSMutableArray *myConnectionsArray;
NSString *entityId_myStat;
ServerQueryBridge *servForMyStat;

@interface myStatsViewController ()

@end

@implementation myStatsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    myConnectionsArray= [[NSMutableArray alloc]init];
    
    //Get entityId and username from key chain
    entityId_myStat = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    
    servForMyStat = [[ServerQueryBridge alloc]init];
    [servForMyStat initialiseBackend];
    [servForMyStat initialiseCoreData:entityId_myStat];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(myFriendsLoaded:) name:@"myFriendsLoaded" object:nil];
    
    
    if(myConnectionsArray){
        [servForMyStat getMyFriendsList:entityId_myStat notiName:@"myFriendsLoaded"];
    }
    else{
        [self showProgressView];
        [servForMyStat getMyFriendsList:entityId_myStat notiName:@"myFriendsLoaded"];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)myFriendsLoaded:(NSNotification*)notification{
    
    NSString *response= [[notification userInfo]objectForKey:@"error"];
    
    if([response isEqualToString:@"Yes"]){
        //Error
    }
    else{

        NSMutableArray *tempArray= [[NSMutableArray alloc]initWithArray:[[notification userInfo]objectForKey:@"friendsList"]];
        
        for(int i=0; i<[tempArray count];i++){
            NSMutableDictionary *tempDict= [[NSMutableDictionary alloc]initWithDictionary:[tempArray objectAtIndex:i]];
            if([[tempDict objectForKey:@"friendName"]isEqualToString:@"Halo Status"] || [[tempDict objectForKey:@"friendshipStatus"]isEqualToString:@"Pending"] || [[tempDict objectForKey:@"friendshipStatus"]isEqualToString:@"Awaiting"]){
                //Don't add
            }
            else{
                [myConnectionsArray addObject:tempDict];
            }
        }
        
        if([myConnectionsArray count]==0){
            [self popMessage:@"No stats at this stage. Time to get out and talk to more people!" title:@"Note"];
            _myConnectionsLab.text= @"0";
        }
        else{
            _myConnectionsLab.text= [NSString stringWithFormat:@"%lu",(unsigned long)[myConnectionsArray count]];
        }
            
        //Reload the table
        _myStatTable.delegate= self;
        _myStatTable.dataSource= self;
        [_myStatTable reloadData];

        [self hideProgressView];
    }
    
}

//******************************TABLE VIEW METHODS**********************************
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(myConnectionsArray.count==0){
        return 0;
    }
    else{
        return myConnectionsArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    myStatCell *cell= [tableView dequeueReusableCellWithIdentifier:@"myStatCell"];
    
    if(cell == nil){
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: @"ChatMateCell"];
    }
    
    //Add info array to the server data class and access each entity from there
    NSMutableDictionary *chatMateDict= [[NSMutableDictionary alloc]initWithDictionary:[myConnectionsArray objectAtIndex:indexPath.row]];
    
    [cell setCell:[chatMateDict objectForKey:@"friendName"] status:[chatMateDict objectForKey:@"friendStatus"] venueName:[chatMateDict objectForKey:@"venueName"] timeStamp:[chatMateDict objectForKey:@"timeStamp"]];
    
    return cell;
}


-(void)showProgressView{
    _progressIndView.hidden= NO;
    _progressInd.hidden= NO;
    _progressLab.hidden= NO;
}

-(void)hideProgressView{
    _progressIndView.hidden= YES;
    _progressInd.hidden= YES;
    _progressLab.hidden= YES;
}

-(void)popMessage:(NSString*)message title:(NSString*)title{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
