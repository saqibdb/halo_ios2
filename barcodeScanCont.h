//
//  barcodeScanCont.h
//  LookUp
//
//  Created by Divey Punj on 12/05/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "barcodeView.h"

@interface barcodeScanCont : UIViewController <AVCaptureMetadataOutputObjectsDelegate>

@property (strong, nonatomic) IBOutlet UIView *camView;
@property (strong, nonatomic) IBOutlet UILabel *infoLab;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic)BOOL isReading;
@property (strong, nonatomic) barcodeView *barcodeViewVar;
@property (strong, nonatomic) IBOutlet UIButton *qrCodeBut;



-(BOOL)startReading;
-(void)stopReading;
- (IBAction)myQRAction:(id)sender;

@end
