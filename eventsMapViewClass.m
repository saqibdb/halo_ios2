//
//  eventsMapViewClass.m
//  LookUp
//
//  Created by Divey Punj on 8/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "eventsMapViewClass.h"


userClass *userDataForMap;
events *eventsInfoForMap;
eventsServerClass *eventsServerForMap;
NSMutableArray* eventArray;
NSMutableArray* eventTitlesArray;
NSString* selectedAnnotation;
BOOL shouldSegue;

@interface eventsMapViewClass ()

@end

@implementation eventsMapViewClass
@synthesize eventsMapView, standardMapBut, satelliteMapBut, hybridMapBut;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initialise];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialise{
    userDataForMap= [[userClass alloc]init];
    
    eventsInfoForMap= [[events alloc]init];
    
    eventsServerForMap= [[eventsServerClass alloc]init];
    
    eventArray= [[NSMutableArray alloc]init];
    
    eventTitlesArray= [[NSMutableArray alloc]init];
    
    selectedAnnotation= @"";
    
    eventsMapView.delegate= self;
    
    shouldSegue= NO;
    
    [standardMapBut setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [satelliteMapBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [hybridMapBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self setUpMap];
}

-(void)setUpMap{
    
    //Set coordinates
    double latNum= [[userDataForMap getLatitude]doubleValue];
    double lonNum= [[userDataForMap getLongitude]doubleValue];
    
    MKCoordinateRegion region= {{0.0,0.0},{0.0,0.0}};
    region.center.latitude= latNum;
    region.center.longitude= lonNum;
    region.span.longitudeDelta= .10f; //How much zoom to start with
    region.span.latitudeDelta= .10f; //How much zoom to start with
    
    [eventsMapView setRegion:region animated:YES];
    [self setAnnotations];
    
}

-(void)setAnnotations{
    
    //Create a mutable annotations array
    NSMutableArray* annotationsArray= [[NSMutableArray alloc]init];
    //Get events array from events;
    eventArray= [[NSMutableArray alloc]initWithArray:[eventsInfoForMap getEventsArray]];
    
    //Create Annotations
    eventMapViewAnnotationClass *annotation;
    CLLocationCoordinate2D location;
    
    //Run a for loop for events array and add the annotations to another array
    for(int i=0; i<eventArray.count; i++){
        eventsServerForMap= [eventArray objectAtIndex:i];
        //Create annotation
        annotation= [[eventMapViewAnnotationClass alloc]init];
        location.latitude= eventsServerForMap.geocoord.coordinate.latitude;
        location.longitude= eventsServerForMap.geocoord.coordinate.longitude;
        annotation.coordinate= location;
        annotation.title= eventsServerForMap.eventName;
        annotation.subtitle= eventsServerForMap.eventAddress;
        [annotationsArray addObject:annotation];
        [eventTitlesArray addObject:eventsServerForMap.eventName];
    }
    
    //Add the annotation array to mapview
    [eventsMapView addAnnotations:annotationsArray];
}

//Delegate method for annotations
-(MKAnnotationView*) mapView:(MKMapView *)mapView viewForAnnotation:(nonnull id<MKAnnotation>)annotation{
   
    
    MKPinAnnotationView *mapPin= nil;
    static NSString *defaultPinID= @"defaultPin";
    mapPin= (MKPinAnnotationView *)[eventsMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if(mapPin== nil) {
        mapPin= [[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        mapPin.canShowCallout= YES;
        //selectedAnnotation= [annotation title];
        UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [infoButton addTarget:self action:@selector(pinToEvent:) forControlEvents:UIControlEventTouchUpInside];
        mapPin.rightCalloutAccessoryView= infoButton;
    }
    else{
        mapPin.annotation= annotation;
    }
    
    return mapPin;
}

-(void)mapView:(MKMapView*)mapView annotationView:(nonnull MKAnnotationView *)view calloutAccessoryControlTapped:(nonnull UIControl *)control{
    
    selectedAnnotation= [view.annotation title];
    [self performSegueWithIdentifier:@"eventAnnotationSegue" sender:0];
    //NSLog(@"in annotation");
}

-(void)pinToEvent:(id)sender{
    /*
    eventsServerForMap= [eventArray objectAtIndex:[eventTitlesArray indexOfObject:selectedAnnotation]];
    //self.activeEventDialog= [[eventDialog alloc]init];
    self.activeEventDialog.eventName= eventsServerForMap.eventName;
    self.activeEventDialog.eventDate= eventsServerForMap.eventDateTime;
    self.activeEventDialog.eventAddress= eventsServerForMap.eventAddress;
    self.activeEventDialog.eventAttendees= eventsServerForMap.eventAttendees;
    //[self performSegueWithIdentifier:@"eventAnnotationSegue" sender:self];
    
    //[self prepareForSegue:self sender:nil];
    shouldSegue= YES;*/
    
   // [self performSegueWithIdentifier:@"eventAnnotationSegue" sender:sender];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    self.activeEventDialog= segue.destinationViewController;
    
    if([segue.identifier isEqualToString:@"eventAnnotationSegue"]){
        
        self.activeEventDialog= segue.destinationViewController;
        NSInteger eventIndex= [eventTitlesArray indexOfObject:selectedAnnotation];
        //Get selected event data and push forward
        eventsServerForMap= [eventArray objectAtIndex:eventIndex];
        self.activeEventDialog.eventName= eventsServerForMap.eventName;
        self.activeEventDialog.eventDate= eventsServerForMap.eventDateTime;
        self.activeEventDialog.eventAddress= eventsServerForMap.eventAddress;
        self.activeEventDialog.eventAttendees= eventsServerForMap.eventAttendees;
        self.activeEventDialog.eventId= eventsServerForMap.eventId;
        
        return;
    }
    
}


-(IBAction)setMap:(id)sender{
    switch (((UISegmentedControl*)sender).selectedSegmentIndex) {
        case 0:
            eventsMapView.mapType= MKMapTypeStandard;
            break;
        case 1:
            eventsMapView.mapType= MKMapTypeSatellite;
            break;
        case 2:
            eventsMapView.mapType= MKMapTypeHybrid;
            break;
            
        default:
            break;
    }
}

- (IBAction)standardMapAction:(id)sender {
    
    eventsMapView.mapType= MKMapTypeStandard;
    [standardMapBut setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [satelliteMapBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [hybridMapBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)satelliteMapAction:(id)sender {
    eventsMapView.mapType= MKMapTypeSatellite;
    [standardMapBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [satelliteMapBut setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [hybridMapBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)hybridMapAction:(id)sender {
    eventsMapView.mapType= MKMapTypeHybrid;
    [standardMapBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [satelliteMapBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [hybridMapBut setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
}

@end
