//
//  eventCollectionView.h
//  LookUp
//
//  Created by Divey Punj on 30/01/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface eventCollectionView : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UIImageView *event_image;
@property (strong, nonatomic) IBOutlet UILabel *event_venue;
@property (strong, nonatomic) IBOutlet UILabel *event_name;
@property (strong, nonatomic) IBOutlet UILabel *event_date;
@property (strong, nonatomic) IBOutlet UILabel *event_month;
@property (strong, nonatomic) IBOutlet UIButton *event_button;


- (IBAction)eventCheckAction:(id)sender;

-(void)setCell:(NSString*)name venue:(NSString*)venue date:(NSString*)date month:(NSString*)month imageURL:(NSString*)imageUrl;



@end
