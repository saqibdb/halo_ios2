//
//  checkedInView.h
//  LookUp
//
//  Created by Divey Punj on 8/02/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "haloUsersProfileCells.h"
#import "ServerQueryBridge.h"
#import "userProfileDialog.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SAMKeychain/SAMKeychain.h>
#import "AppDelegate.h"
#import "SERVICES.h"
#import "location.h"


@interface checkedInView : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate>



//#define TAP_CONNECT_VIEW_ON_HEIGHT  225
#define TAP_CONNECT_VIEW_ON_HEIGHT 0//54
//#define TAP_CONNECT_BUTTON_ON_BOTCON 180
//#define TAP_CONNECT_BUTTON_OFF_BOTCON 0
#define NUMBER_OF_USER_PROFILES_SHOW 15
//100 136
#define PROFILE_CELL_WIDTH_6S 100
#define PROFILE_CELL_HEIGHT_6S 136
#define PROFILE_CELL_WIDTH_4S 80
#define PROFILE_CELL_HEIGHT_4S 100



@property (strong, nonatomic) IBOutlet UICollectionView *userProfilesCollection;
@property (strong, nonatomic) IBOutlet UIImageView *profileLoadingImage;


@property (strong, nonatomic) userProfileDialog *activeProfileDialog;

@property (strong, nonatomic) IBOutlet UIButton *reloadButton;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segcon_filter;

- (IBAction)reloadAction:(id)sender;
- (IBAction)leftSideMenuAction:(id)sender;
- (IBAction)moreViewAction:(id)sender;
- (IBAction)segcon_filter_action:(id)sender;


//Creadits
//<div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div> - LOCATION ICON



- (IBAction)addedMeAction:(id)sender;
- (IBAction)addFriendAction:(id)sender;






@end
