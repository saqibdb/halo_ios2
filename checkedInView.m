//
//  checkedInView.m
//  LookUp
//
//  Created by Divey Punj on 8/02/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "checkedInView.h"

BOOL tapConViewOn;
NSMutableArray *haloUserProfileRed, *haloUserProfileAmber, *haloUserProfileGreen, *haloUserProfRedTemp, *haloUserProfAmberTemp, *haloUserProfGreenTemp, *haloUserProfileAll, *haloUserProfAllTemp;
NSMutableDictionary *haloUserImagesRed, *haloUserImagesAmber, *haloUserImagesGreen, *haloUserImagesAll, *connectingFriendDict,*currentTapUserDetailsForPeripheral;
ServerQueryBridge *serverForCheckedInView;
UIView *imageCircle;
UIRefreshControl *profileRefreshControl;
NSString* currentResponseFromCentral,*lightShowColour, *entityId_chk, *currentFilter;
NSTimer *lightShowTimer, *profileSearchTimer, *tapTimer;
UILabel *noProfilesInfoLab;
NSManagedObjectContext *context_chk;
NSManagedObject *managedDataObject_chk;
AppDelegate *delegate_chk;
NSFetchRequest *userDataRequest_chk;
NSArray *userDataArray_chk;
location *locForChk;

@interface checkedInView ()

@end

@implementation checkedInView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Get entityId and username from key chain
    entityId_chk = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    
    //Initialise Core data object
    NSError *error;
    context_chk = [self managedObjectContext];
    userDataRequest_chk= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_chk.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_chk];
    userDataArray_chk = [context_chk executeFetchRequest:userDataRequest_chk error:&error];
    managedDataObject_chk= [userDataArray_chk objectAtIndex:0];
    
    
    //Initialise loading circle
    if([[self checkScreenSize]isEqualToString:@"Small"]){
        imageCircle= [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 - 50, self.view.frame.size.height/2-50, 100, 100)];
    }
    else if([[self checkScreenSize]isEqualToString:@"Medium"]){
        imageCircle= [[UIView alloc]initWithFrame:CGRectMake(137, 283, 100, 100)];
    }
    else{
        imageCircle= [[UIView alloc]initWithFrame:CGRectMake(156, 312, 100, 100)];
    }
    
    //Intialise Classes
    serverForCheckedInView= [[ServerQueryBridge alloc]init];
    [serverForCheckedInView initialiseBackend];
    [serverForCheckedInView initialiseCoreData:entityId_chk];
    
    //Update location again
    locForChk = [[location alloc]init];
    [locForChk initialiseLocation];

    
    //Intialise Variables
    currentFilter= @"All";
    haloUserImagesRed= [[NSMutableDictionary alloc]init];
    haloUserImagesAmber= [[NSMutableDictionary alloc]init];
    haloUserImagesGreen= [[NSMutableDictionary alloc]init];
    haloUserImagesAll = [[NSMutableDictionary alloc]init];
    haloUserProfileRed= [[NSMutableArray alloc]init];
    haloUserProfileAmber= [[NSMutableArray alloc]init];
    haloUserProfileGreen= [[NSMutableArray alloc]init];
    haloUserProfRedTemp= [[NSMutableArray alloc]init];
    haloUserProfAmberTemp = [[NSMutableArray alloc]init];
    haloUserProfGreenTemp= [[NSMutableArray alloc]init];
    haloUserProfileAll= [[NSMutableArray alloc]init];
    haloUserProfAllTemp= [[NSMutableArray alloc]init];
    
    tapConViewOn= NO;
    _reloadButton.hidden= YES;

    //Pull down refresh code
    profileRefreshControl= [[UIRefreshControl alloc]init];
    [profileRefreshControl addTarget:self action:@selector(refreshProfiles) forControlEvents:UIControlEventValueChanged];
    [_userProfilesCollection addSubview:profileRefreshControl];
    _userProfilesCollection.alwaysBounceVertical = YES;
    
    //Create a notification for when logged out
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(userLoggedOut:) name:@"userLoggedOut" object:nil];
    
    //Save profile so the checkin info can be saved
    [serverForCheckedInView saveData:@"" entityId:entityId_chk];
    

    //Create notification for new message alert
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(newMessageAlertInCheckedin:) name:@"newMessageAlertInCheckedin" object:nil];
    //Create a notifcation for Filter input
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(filterProfiles:) name:@"filterProfiles" object:nil];
    //Create a notification to check if user is still in region when app is active
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(backToForeChkIn:) name:@"backToForeChkIn" object:nil];
    

    //Clear cached image data
    //Clear cached images data
    [self clearCachedImagesData];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    //Update location
    [locForChk getLocation];
    
    if([haloUserProfileRed count]== 0 && [haloUserProfileAmber count] == 0 && [haloUserProfileGreen count] == 0 && [haloUserProfileAll count]==0){
        //Reload
        [self filterSelAll:0];
        noProfilesInfoLab.hidden= YES;
        _reloadButton.hidden= YES;
    }
    /*else if([haloUserProfileGreen count]==0){
        //Reload
        [self filterSelGreen:0];
        noProfilesInfoLab.hidden= YES;
        _reloadButton.hidden= YES;
    }*/
    else{
        //Nothing
    }
    //TEST
    //[serverForCheckedInView canCheckIn:[[managedDataObject_chk valueForKey:@"latitude"]doubleValue] lon:[[managedDataObject_chk valueForKey:@"longitude"]doubleValue] isCheckedIn:@"NO" isActive:@"YES" notiName:@""];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    //Check for verification and segue out if needed
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(checkVerificationChkIn:) name:@"checkVerificationChkIn" object:nil];
    [serverForCheckedInView verifiedAs:entityId_chk notiName:@"checkVerificationChkIn"];
    
    if([[managedDataObject_chk valueForKey:@"regionFlag"]isEqualToString:@"No"]){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
        //Nothing
    }
    
    //Enable the tab bar buttons
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:FALSE];
    
    //Check if new message
    if([[managedDataObject_chk valueForKey:@"newMessage"]isEqualToString:@"Yes"]){
        
    }
    else{
        
    }
    
    //Nothing
    [self.navigationItem setTitle:[managedDataObject_chk valueForKey:@"venueName"]];
    [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
 
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//USER PROFILE LOAD AND SHOW METHODS

-(void)fetchHaloProfiles:(NSString*)status{
    
    //Get all the people around me
    NSString *gender= [managedDataObject_chk valueForKey:@"otherUserGender"];
    NSString *orientation= [managedDataObject_chk valueForKey:@"otherUserOrientation"];
    NSString *age_from= [managedDataObject_chk valueForKey:@"otherUserAgeFrom"];
    NSString *age_to= [managedDataObject_chk valueForKey:@"otherUserAgeTo"];
    
    
    //Create a notification
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"fetchedHaloProfiles" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(fetchedHaloProfiles:) name:@"fetchedHaloProfiles" object:nil];
    //Fetch from server
    [serverForCheckedInView getOtherUserProfiles:0 gender:gender age_from:age_from age_to:age_to ort:orientation status:status];
}

-(void)fetchedHaloProfiles:(NSNotification*)notification{
    
    NSLog(@"Recieved the profiles");
    //[[NSNotificationCenter defaultCenter]removeObserver:self name:@"fetchedHaloProfiles" object:nil];
    
    //Filter this user out from list
    NSMutableArray *tempArray= [[NSMutableArray alloc]initWithArray:[[notification userInfo]objectForKey:@"otherUserProfileData"]];
    
    for(int i=0; i<[tempArray count];i++){
        if([entityId_chk isEqualToString:[[tempArray objectAtIndex:i]objectForKey:@"entityId"]]){
            [tempArray removeObjectAtIndex:i];
        }
    }
    
    NSString *notiForStatus= [[notification userInfo]objectForKey:@"notiForStatus"];

    NSLog(@"STATUSSSS: %@",notiForStatus);
    
    if([notiForStatus isEqualToString:@"Red"]){
        haloUserProfileRed= [[NSMutableArray alloc]initWithArray:tempArray];
        if([haloUserProfileRed count]==0){
            [_userProfilesCollection reloadData];
            [profileSearchTimer fire];
        }
        else{
            //Get the images
            haloUserProfRedTemp= [[NSMutableArray alloc]initWithArray:haloUserProfileRed];
            [self loadMoreHaloUserPhotos:haloUserImagesRed filterArray:haloUserProfRedTemp status:@"Red"origArray:haloUserProfileRed];
        }
        
    }
    else if([notiForStatus isEqualToString:@"Yellow"]){
        haloUserProfileAmber= [[NSMutableArray alloc]initWithArray:tempArray];
        if([haloUserProfileAmber count]==0){
            [_userProfilesCollection reloadData];
            [profileSearchTimer fire];
        }
        else{
            //Get the images
            haloUserProfAmberTemp= [[NSMutableArray alloc]initWithArray:haloUserProfileAmber];
            [self loadMoreHaloUserPhotos:haloUserImagesAmber filterArray:haloUserProfAmberTemp status:@"Amber" origArray:haloUserProfileAmber];
        }
    }
    else if([notiForStatus isEqualToString:@"Green"]){
        
        haloUserProfileGreen= [[NSMutableArray alloc]initWithArray:tempArray];
        if([haloUserProfileGreen count]==0){
            [_userProfilesCollection reloadData];
            [profileSearchTimer fire];
        }
        else{
            //Get the images
            haloUserProfGreenTemp= [[NSMutableArray alloc]initWithArray:haloUserProfileGreen];
            [self loadMoreHaloUserPhotos:haloUserImagesGreen filterArray:haloUserProfGreenTemp status:@"Green" origArray:haloUserProfileGreen];
        }
        
    }
    else{
        haloUserProfileAll = [[NSMutableArray alloc]initWithArray:tempArray];
        if([haloUserProfileAll count]==0){
            [_userProfilesCollection reloadData];
            [profileSearchTimer fire];
        }
        else{
            //Get the images
            haloUserProfAllTemp = [[NSMutableArray alloc]initWithArray:haloUserProfileAll];
            [self loadMoreHaloUserPhotos:haloUserImagesAll filterArray:haloUserProfAllTemp status:@"All" origArray:haloUserProfileAll];
        }
    }
    
    /*if([currentFilter isEqualToString:@"Red"]){
        haloUserProfileRed= [[NSMutableArray alloc]initWithArray:tempArray];
        if([haloUserProfileRed count]==0){
            [_userProfilesCollection reloadData];
            [profileSearchTimer fire];
        }
        else{
            //Get the images
            haloUserProfRedTemp= [[NSMutableArray alloc]initWithArray:haloUserProfileRed];
            [self loadMoreHaloUserPhotos:haloUserImagesRed filterArray:haloUserProfRedTemp status:@"Red"origArray:haloUserProfileRed];
        }
        
    }
    else if([currentFilter isEqualToString:@"Amber"]){
        haloUserProfileAmber= [[NSMutableArray alloc]initWithArray:tempArray];
        if([haloUserProfileAmber count]==0){
            [_userProfilesCollection reloadData];
            [profileSearchTimer fire];
        }
        else{
            //Get the images
            haloUserProfAmberTemp= [[NSMutableArray alloc]initWithArray:haloUserProfileAmber];
            [self loadMoreHaloUserPhotos:haloUserImagesAmber filterArray:haloUserProfAmberTemp status:@"Amber" origArray:haloUserProfileAmber];
        }
    }
    else if([currentFilter isEqualToString:@"Green"]){
        
        haloUserProfileGreen= [[NSMutableArray alloc]initWithArray:tempArray];
        if([haloUserProfileGreen count]==0){
            [_userProfilesCollection reloadData];
            [profileSearchTimer fire];
        }
        else{
            //Get the images
            haloUserProfGreenTemp= [[NSMutableArray alloc]initWithArray:haloUserProfileGreen];
            [self loadMoreHaloUserPhotos:haloUserImagesGreen filterArray:haloUserProfGreenTemp status:@"Green" origArray:haloUserProfileGreen];
        }
        
    }
    else{
        haloUserProfileAll = [[NSMutableArray alloc]initWithArray:tempArray];
        if([haloUserProfileAll count]==0){
            [_userProfilesCollection reloadData];
            [profileSearchTimer fire];
        }
        else{
            //Get the images
            haloUserProfAllTemp = [[NSMutableArray alloc]initWithArray:haloUserProfileAll];
            [self loadMoreHaloUserPhotos:haloUserImagesAll filterArray:haloUserProfAllTemp status:@"All" origArray:haloUserProfileAll];
        }
    }*/
    
}

-(void)loadMoreHaloUserPhotos:(NSMutableDictionary*)imageDict filterArray:(NSMutableArray*)filterArray status:(NSString*)status origArray:(NSMutableArray*)origArray{
    //Check if the count of imageDict is equal to haloUserProfilesFilter count, if not then load
    NSMutableDictionary *userInfoDict;
    
    if([imageDict count]==[origArray count]){
        //No need to load any more
        [profileRefreshControl endRefreshing];
        [self showIndicator:NO];
    }
    else{
        if([imageDict count]<0){
            //Don't load more
            [profileRefreshControl endRefreshing];
        }
        else{
            //Load more
            //Create a temp array everytime to hold the entity ids in
            NSMutableArray *tempArray= [[NSMutableArray alloc]init];
            
            //Check if there are 8 profiles available, if not then get all, otherwise get 8
            if([filterArray count]==0 || filterArray == nil){
                //Dont load any more
            }
            else if([filterArray count]<= NUMBER_OF_USER_PROFILES_SHOW){
                //Get all
                for(int i=0; i<[filterArray count]; i++){
                    userInfoDict= [[NSMutableDictionary alloc]initWithDictionary:[filterArray objectAtIndex:i]];
                    [tempArray addObject:[[userInfoDict objectForKey:@"entityId"] stringByAppendingString:@"Small.png"]];
                }
            }
            else{
                //Get the set number only
                for(int i=0; i<=NUMBER_OF_USER_PROFILES_SHOW; i++){
                    userInfoDict= [[NSMutableDictionary alloc]initWithDictionary:[filterArray objectAtIndex:i]];
                    [tempArray addObject:[[userInfoDict objectForKey:@"entityId"] stringByAppendingString:@"Small.png"]];

                }
                //Delete set number of elements
                 [filterArray removeObjectsInArray:[filterArray subarrayWithRange:NSMakeRange(0, NUMBER_OF_USER_PROFILES_SHOW)]];
                if([status isEqualToString:@"Red"]){
                    haloUserProfRedTemp = [[NSMutableArray alloc]initWithArray:filterArray];
                }
                else if([status isEqualToString:@"Amber"]){
                    haloUserProfAmberTemp = [[NSMutableArray alloc]initWithArray:filterArray];
                }
                else if([status isEqualToString:@"Green"]){
                    haloUserProfGreenTemp = [[NSMutableArray alloc]initWithArray:filterArray];
                }
                else{
                    haloUserProfAllTemp = [[NSMutableArray alloc]initWithArray:filterArray];
                }
                
            }
            
            [self loadImagesInGroups:tempArray status:status];

        }
    }
    
    NSLog(@"End of image loading start");
}

-(void)loadImagesInGroups:(NSMutableArray*)array status:(NSString*)status{

    if([array count]==0 || array==nil){
        
        
    }
    else{
        //Call server to fetch images
        if([status isEqualToString:@"Red"]){
            //Create a notification to listen for images
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(groupImagesLoadedRed:) name:@"groupImagesLoadedRed" object:nil];
            
            [serverForCheckedInView downloadSmallImageByNames:array notiName:@"groupImagesLoadedRed" keyName:@"otherUserImages"];
        }
        else if([status isEqualToString:@"Amber"]){
            //Create a notification to listen for images
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(groupImagesLoadedAmber:) name:@"groupImagesLoadedAmber" object:nil];
            [serverForCheckedInView downloadSmallImageByNames:array notiName:@"groupImagesLoadedAmber" keyName:@"otherUserImages"];
        }
        else if([status isEqualToString:@"Green"]){
            //Create a notification to listen for images
            NSLog(@"PROFILE IMAGE ARRAY %lu",(unsigned long)[array count]);
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(groupImagesLoadedGreen:) name:@"groupImagesLoadedGreen" object:nil];
            [serverForCheckedInView downloadSmallImageByNames:array notiName:@"groupImagesLoadedGreen" keyName:@"otherUserImages"];
        }
        else{
            //Create a notification to listen for images
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(groupImagesLoadedAll:) name:@"groupImagesLoadedAll" object:nil];
            [serverForCheckedInView downloadSmallImageByNames:array notiName:@"groupImagesLoadedAll" keyName:@"otherUserImages"];
        }

    }
    
}

-(void)groupImagesLoadedRed:(NSNotification*)notification{
    
    //Remove notification
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"groupImagesLoadedRed" object:nil];
    
    //Read value from notification and append to image dictionary
        if([haloUserImagesRed count]==0){
            haloUserImagesRed= [[NSMutableDictionary alloc]initWithDictionary:[[notification userInfo]objectForKey:@"otherUserImages"]];
            
            
            //Show Collection view
            [_userProfilesCollection setDelegate:self];
            [_userProfilesCollection setDataSource:self];
            [_userProfilesCollection reloadData];
        }
        else{
            [haloUserImagesRed addEntriesFromDictionary:[[notification userInfo]objectForKey:@"otherUserImages"]];
            [_userProfilesCollection reloadData];
        }
    [self showIndicator:NO];
    //Turn timer off
    [profileSearchTimer invalidate];
    [profileRefreshControl endRefreshing];
}

-(void)groupImagesLoadedAmber:(NSNotification*)notification{
    
    //Remove notification
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"groupImagesLoadedAmber" object:nil];

    if([haloUserImagesAmber count]==0){
        haloUserImagesAmber= [[NSMutableDictionary alloc]initWithDictionary:[[notification userInfo]objectForKey:@"otherUserImages"]];
        
        
        //Show Collection view
        [_userProfilesCollection setDelegate:self];
        [_userProfilesCollection setDataSource:self];
        [_userProfilesCollection reloadData];
    }
    else{
        [haloUserImagesAmber addEntriesFromDictionary:[[notification userInfo]objectForKey:@"otherUserImages"]];
        [_userProfilesCollection reloadData];
    }
    //Turn timer off
    [profileSearchTimer invalidate];
    [self showIndicator:NO];
    [profileRefreshControl endRefreshing];
}

-(void)groupImagesLoadedGreen:(NSNotification*)notification{
    //Remove notification
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"groupImagesLoadedGreen" object:nil];

    if([haloUserImagesGreen count]==0){
        haloUserImagesGreen= [[NSMutableDictionary alloc]initWithDictionary:[[notification userInfo]objectForKey:@"otherUserImages"]];

        //Show Collection view
        [_userProfilesCollection setDelegate:self];
        [_userProfilesCollection setDataSource:self];
        [_userProfilesCollection reloadData];
    }
    else{
        [haloUserImagesGreen addEntriesFromDictionary:[[notification userInfo]objectForKey:@"otherUserImages"]];
        [_userProfilesCollection reloadData];
    }
    //Turn timer off
    [profileSearchTimer invalidate];
    [self showIndicator:NO];
    [profileRefreshControl endRefreshing];
}

-(void)groupImagesLoadedAll:(NSNotification*)notification{
    //Remove notification
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"groupImagesLoadedAll" object:nil];
    
    if([haloUserImagesAll count]==0){
        haloUserImagesAll= [[NSMutableDictionary alloc]initWithDictionary:[[notification userInfo]objectForKey:@"otherUserImages"]];
        
        //Show Collection view
        [_userProfilesCollection setDelegate:self];
        [_userProfilesCollection setDataSource:self];
        [_userProfilesCollection reloadData];
    }
    else{
        [haloUserImagesAll addEntriesFromDictionary:[[notification userInfo]objectForKey:@"otherUserImages"]];
        [_userProfilesCollection reloadData];
    }
    //Turn timer off
    [profileSearchTimer invalidate];
    [self showIndicator:NO];
    [profileRefreshControl endRefreshing];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
  
        //Other User Profiles
        
    if([currentFilter isEqualToString:@"Red"]){
        NSLog(@"Count: %lu",(unsigned long)[haloUserImagesRed count]);
        return [haloUserImagesRed count];
    }
    else if([currentFilter isEqualToString:@"Amber"]){
        return [haloUserImagesAmber count];
    }
    else if([currentFilter isEqualToString:@"Green"]){
        return [haloUserImagesGreen count];
    }
    else{
        return [haloUserImagesAll count];
    }
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
       //Other user profiles
        
        static NSString *cellIdentifier= @"haloUsersCell";
        
        haloUsersProfileCells *otherUserProfCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
 
            
        NSMutableDictionary *userInfoDict;
    
        if([currentFilter isEqualToString:@"Red"]){
            if([haloUserProfileRed count]==0 || haloUserProfileRed ==nil){
                [profileSearchTimer fire];
            }
            else{
                userInfoDict= [[NSMutableDictionary alloc]initWithDictionary: [haloUserProfileRed objectAtIndex:indexPath.row]];
                //Send to cell to display
                [otherUserProfCell setCell:[haloUserImagesRed objectForKey:[[userInfoDict objectForKey:@"entityId"] stringByAppendingString:@"Small.png"]] name: [userInfoDict objectForKey:@"name"]];
            }
        }
        else if([currentFilter isEqualToString:@"Amber"]){
            if([haloUserProfileAmber count]==0 || haloUserProfileAmber == nil){
                [profileSearchTimer fire];
            }
            else{
                userInfoDict= [[NSMutableDictionary alloc]initWithDictionary: [haloUserProfileAmber objectAtIndex:indexPath.row]];
                //Send to cell to display
                [otherUserProfCell setCell:[haloUserImagesAmber objectForKey:[[userInfoDict objectForKey:@"entityId"] stringByAppendingString:@"Small.png"]] name:[userInfoDict objectForKey:@"name"]];
            }
            
        }
        else if([currentFilter isEqualToString:@"Green"]){
            if([haloUserProfileGreen count]==0 || haloUserProfileGreen == nil){
                [profileSearchTimer fire];
            }
            else{
                userInfoDict= [[NSMutableDictionary alloc]initWithDictionary: [haloUserProfileGreen objectAtIndex:indexPath.row]];
                //Send to cell to display
                [otherUserProfCell setCell:[haloUserImagesGreen objectForKey:[[userInfoDict objectForKey:@"entityId"] stringByAppendingString:@"Small.png"]] name:[userInfoDict objectForKey:@"name"]];
            }
        }
        else{
            if([haloUserProfileAll count]==0 || haloUserProfileAll == nil){
                [profileSearchTimer fire];
            }
            else{
                userInfoDict= [[NSMutableDictionary alloc]initWithDictionary: [haloUserProfileAll objectAtIndex:indexPath.row]];
                //Send to cell to display
                [otherUserProfCell setCell:[haloUserImagesAll objectForKey:[[userInfoDict objectForKey:@"entityId"] stringByAppendingString:@"Small.png"]] name:[userInfoDict objectForKey:@"name"]];
            }

        }

        return otherUserProfCell;

}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int s= scrollView.contentOffset.y;
    int g= scrollView.frame.size.height;
    BOOL endOfTable = NO;
    
    if([currentFilter isEqualToString:@"Red"]){
        endOfTable = (scrollView.contentOffset.y >= ((haloUserImagesRed.count * 30) - scrollView.frame.size.height)); // Here 197 is row height
    }
    else if([currentFilter isEqualToString:@"Amber"]){
        endOfTable = (scrollView.contentOffset.y >= ((haloUserImagesAmber.count * 30) - scrollView.frame.size.height)); // Here 197 is row height
    }
    else if([currentFilter isEqualToString:@"Green"]){
        endOfTable = (scrollView.contentOffset.y >= ((haloUserImagesGreen.count * 30) - scrollView.frame.size.height)); // Here 197 is row height
    }
    else{
        endOfTable = (scrollView.contentOffset.y >= ((haloUserImagesAll.count * 30) - scrollView.frame.size.height)); // Here 197 is row height
    }
    
    if (endOfTable && !scrollView.dragging && !scrollView.decelerating && !profileRefreshControl.refreshing)
    {
        //Load more data here
        if([currentFilter isEqualToString:@"Red"]) {
            [self loadMoreHaloUserPhotos:haloUserImagesRed filterArray:haloUserProfRedTemp status:@"Red"origArray:haloUserProfileRed];
        }
        else if([currentFilter isEqualToString:@"Amber"]){
            [self loadMoreHaloUserPhotos:haloUserImagesAmber filterArray:haloUserProfAmberTemp status:@"Amber" origArray:haloUserProfileAmber];
        }
        else if([currentFilter isEqualToString:@"Green"]){
            [self loadMoreHaloUserPhotos:haloUserImagesGreen filterArray:haloUserProfGreenTemp status:@"Green" origArray:haloUserProfileGreen];
        }
        else{
            [self loadMoreHaloUserPhotos:haloUserImagesAll filterArray:haloUserProfAllTemp status:@"All" origArray:haloUserProfileAll];
        }
       
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Adjust cell size for screen size
    if([[self checkScreenSize]isEqualToString:@"Small"]){
        return CGSizeMake(PROFILE_CELL_WIDTH_4S, PROFILE_CELL_HEIGHT_4S);
    }
    else if([[self checkScreenSize]isEqualToString:@"Medium"]){
        return CGSizeMake(PROFILE_CELL_WIDTH_6S, PROFILE_CELL_HEIGHT_6S);
    }
    else{
        return CGSizeMake(PROFILE_CELL_WIDTH_6S, PROFILE_CELL_HEIGHT_6S);
    }
}

-(NSString*)checkScreenSize{
    //Check screen size to adjust the view size
    
    if([[UIDevice currentDevice]userInterfaceIdiom]== UIUserInterfaceIdiomPhone){
        if([[UIScreen mainScreen]bounds].size.width == 320){
            //iPhone 4s
            return @"Small";
        }
        else if([[UIScreen mainScreen]bounds].size.width == 375){
            //iPhone 6
            return @"Medium";
        }
        else{
            //iPhone 6 Plus
            return @"Large";
        }
    }
    else{
        //iPad, so nothing
        return @"Large";
    }
}

/*- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"haloUserProfileDialog" sender:self];
}*/

/*-(CGSize)collectionView:(UICollectionView*)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    
        CGSize size= CGSizeMake(120, 112);
        return size;
}*/
//*********************


//FILTER METHODS

-(void)filterProfiles:(NSNotification*)notification{
    NSString *filter= [[notification userInfo]objectForKey:@"filterBy"];
    
    if([filter isEqualToString:@"Red"]){
        currentFilter= @"Red";
        [self filterSelRed:0];
    }
    else if([filter isEqualToString:@"Amber"]){
        currentFilter= @"Amber";
        [self filterSelAmber:0];
    }
    else{
        currentFilter= @"Green";
        [self filterSelGreen:0];
    }
}

- (IBAction)filterSelRed:(id)sender {
    [self clearNoProfLab];
    if([haloUserProfileRed count]==0){
        [profileSearchTimer invalidate];
        profileSearchTimer= [NSTimer scheduledTimerWithTimeInterval:PROFILE_SEARCH_TIME target:self selector:@selector(profileSearchTimerMethod) userInfo:nil repeats:NO];
        [self showIndicator:YES];
        [self fetchHaloProfiles:@"Red"];
    }
    else{
        //Nothing
        [_userProfilesCollection reloadData];
    }
}

- (IBAction)filterSelAmber:(id)sender {
    [self clearNoProfLab];
    if([haloUserProfileAmber count]==0){
        [profileSearchTimer invalidate];
        profileSearchTimer= [NSTimer scheduledTimerWithTimeInterval:PROFILE_SEARCH_TIME target:self selector:@selector(profileSearchTimerMethod) userInfo:nil repeats:NO];
        [self showIndicator:YES];
        [self fetchHaloProfiles:@"Yellow"];
    }
    else{
        //Nothing
        [_userProfilesCollection reloadData];
    }
}

- (IBAction)filterSelGreen:(id)sender {
    [self clearNoProfLab];
    if([haloUserProfileGreen count]==0){
        [profileSearchTimer invalidate];
        profileSearchTimer= [NSTimer scheduledTimerWithTimeInterval:PROFILE_SEARCH_TIME target:self selector:@selector(profileSearchTimerMethod) userInfo:nil repeats:NO];
        [self showIndicator:YES];
        [self fetchHaloProfiles:@"Green"];
    }
    else{
        //Nothing
        [_userProfilesCollection reloadData];
    }
}

- (IBAction)filterSelAll:(id)sender {
    [self clearNoProfLab];
    if([haloUserProfileAll count]==0){
        [profileSearchTimer invalidate];
        profileSearchTimer= [NSTimer scheduledTimerWithTimeInterval:PROFILE_SEARCH_TIME target:self selector:@selector(profileSearchTimerMethod) userInfo:nil repeats:NO];
        [self showIndicator:YES];
        [self fetchHaloProfiles:@"All"];
    }
    else{
        //Nothing
        [_userProfilesCollection reloadData];
        [self showIndicator:NO];
    }
}

//INDICATOR CODE
-(void)showIndicator:(BOOL)flag{
    
    if(flag){
        
        _userProfilesCollection.hidden= YES;
        
        //Show user profile pic or Halo pic
        self.profileLoadingImage.hidden= NO;
        UIImage *profileDP= [UIImage imageWithData:[managedDataObject_chk valueForKey:@"userDP"]];
        
        if(profileDP == nil || profileDP == [NSNull null]){
            //Show Halo pic
            self.profileLoadingImage.image= [UIImage imageNamed:@"haloL.png"];
        }
        else{
            if([[self checkScreenSize]isEqualToString:@"Small"]){
                self.profileLoadingImage.layer.cornerRadius= 50;
            }
            else if([[self checkScreenSize]isEqualToString:@"Medium"]){
                self.profileLoadingImage.layer.cornerRadius= 50;
            }
            else{
                self.profileLoadingImage.layer.cornerRadius= 50;
            }
            
            self.profileLoadingImage.clipsToBounds= YES;
            self.profileLoadingImage.image= profileDP;
        }
        
        [imageCircle setBackgroundColor:[UIColor cyanColor]];
        [imageCircle setAlpha:0.5];

        //Show circle
        imageCircle.hidden= NO;
        imageCircle.layer.cornerRadius= 50;
        
        [self.view addSubview:imageCircle];
        
        CABasicAnimation *scaleAnimation= [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        scaleAnimation.duration= .3;
        scaleAnimation.repeatCount= HUGE_VAL;
        scaleAnimation.autoreverses= YES;
        scaleAnimation.fromValue= [NSNumber numberWithFloat:1.3];
        scaleAnimation.toValue= [NSNumber numberWithFloat:0.8];
        
        
        [imageCircle.layer addAnimation:scaleAnimation forKey:@"scale"];
        
        //Bring image to front
        [self.view bringSubviewToFront:self.profileLoadingImage];
        
    }
    else{
        self.profileLoadingImage.hidden= YES;
        imageCircle.hidden= YES;
        [self.view willRemoveSubview:imageCircle];
        if([haloUserImagesRed count]== 0 && [haloUserImagesAmber count] == 0 && [haloUserImagesGreen count] == 0 && [haloUserImagesAll count]==0){
            _userProfilesCollection.hidden= YES;
        }
        else{
            _userProfilesCollection.hidden= NO;
        }
        
    }
}



//*************GENERAL METHODS***************************************************************

//SEGUE METHOD TO DIALOG
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"haloUserProfDialog"]){
        
        self.activeProfileDialog= segue.destinationViewController;
        NSInteger userIndex= [[_userProfilesCollection indexPathForCell:(UICollectionViewCell *)sender]row];
        //Get selected event data and push forward
        NSMutableDictionary *userInfoDict;
        
        if([currentFilter isEqualToString:@"Red"]){
            userInfoDict= [[NSMutableDictionary alloc]initWithDictionary:[haloUserProfileRed objectAtIndex:userIndex]];
        }
        else if([currentFilter isEqualToString:@"Amber"]){
            userInfoDict= [[NSMutableDictionary alloc]initWithDictionary:[haloUserProfileAmber objectAtIndex:userIndex]];
        }
        else if([currentFilter isEqualToString:@"Green"]){
            userInfoDict= [[NSMutableDictionary alloc]initWithDictionary:[haloUserProfileGreen objectAtIndex:userIndex]];
        }
        else{
            userInfoDict= [[NSMutableDictionary alloc]initWithDictionary:[haloUserProfileAll objectAtIndex:userIndex]];
        }
        
        
        
        self.activeProfileDialog.otherUserName= [userInfoDict objectForKey:@"name"];
        self.activeProfileDialog.otherUserAge= [[userInfoDict objectForKey:@"userAge"] stringValue];
        self.activeProfileDialog.otherUserStatus= [userInfoDict objectForKey:@"status"];;
        self.activeProfileDialog.otherUserInterests= [userInfoDict objectForKey:@"interests"];
        self.activeProfileDialog.otherUserOrientation= [userInfoDict objectForKey:@"orientation"];;
        self.activeProfileDialog.userEntityId= [userInfoDict objectForKey:@"entityId"];
        self.activeProfileDialog.otherUserSearchingOrientation= [userInfoDict objectForKey:@"otherUserOrientation"];;
        self.activeProfileDialog.lastSeenLoc= [userInfoDict objectForKey:@"insideLocation"];;
        
        if([currentFilter isEqualToString:@"Red"]){
                self.activeProfileDialog.otherUserImageSmallUrl= [haloUserImagesRed objectForKey:[[userInfoDict objectForKey:@"entityId"] stringByAppendingString:@"Small.png"]];
        }
        else if([currentFilter isEqualToString:@"Amber"]){
                self.activeProfileDialog.otherUserImageSmallUrl= [haloUserImagesAmber objectForKey:[[userInfoDict objectForKey:@"entityId"] stringByAppendingString:@"Small.png"]];
        }
        else if([currentFilter isEqualToString:@"Green"]){
                self.activeProfileDialog.otherUserImageSmallUrl= [haloUserImagesGreen objectForKey:[[userInfoDict objectForKey:@"entityId"] stringByAppendingString:@"Small.png"]];
        }
        else{
            self.activeProfileDialog.otherUserImageSmallUrl= [haloUserImagesAll objectForKey:[[userInfoDict objectForKey:@"entityId"] stringByAppendingString:@"Small.png"]];
        }

        
        return;
    }
}

-(void)refreshProfiles{
    
    //Clear cached images data
    [self clearCachedImagesData];
    
    NSLog(@"Refreshing");
    if([currentFilter isEqualToString:@"Red"]){
        haloUserImagesRed = [[NSMutableDictionary alloc]init];
        [self fetchHaloProfiles:@"Red"];
    }
    else if([currentFilter isEqualToString:@"Amber"]){
        haloUserImagesAmber = [[NSMutableDictionary alloc]init];
        [self fetchHaloProfiles:@"Yellow"];
    }
    else if([currentFilter isEqualToString:@"Green"]){
        haloUserImagesGreen = [[NSMutableDictionary alloc]init];
        [self fetchHaloProfiles:@"Green"];
    }
    else{
        haloUserImagesAll = [[NSMutableDictionary alloc]init];
        [self fetchHaloProfiles:@"All"];
    }
}

-(void)userLoggedOut:(NSNotification*)notification{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)profileSearchTimerMethod{
    
    if([currentFilter isEqualToString:@"Red"]){
        if([haloUserImagesRed count]==0){
            //Turn off loader view
            [self showIndicator:NO];
            //Display message
            if([[self checkScreenSize]isEqualToString:@"Small"]){
                noProfilesInfoLab= [[UILabel alloc]initWithFrame:CGRectMake(10, 60, self.view.frame.size.width-10, 300)];
            }
            else{
                noProfilesInfoLab= [[UILabel alloc]initWithFrame:CGRectMake(10, self.view.frame.size.height/4, self.view.frame.size.width-10, 300)];
            }
            [noProfilesInfoLab setTextAlignment:NSTextAlignmentCenter];
            [noProfilesInfoLab setTextColor:[UIColor whiteColor]];
            noProfilesInfoLab.text= @"We could not find any halo users that match your preference. Make your preference general and try again.";
            noProfilesInfoLab.hidden= NO;
            [noProfilesInfoLab setNumberOfLines:3];
            [self.view addSubview:noProfilesInfoLab];
            _reloadButton.hidden= NO;
        }
    }
    else if([currentFilter isEqualToString:@"Amber"]){
        if([haloUserImagesAmber count] == 0){
            //Turn off loader view
            [self showIndicator:NO];
            //Display message
            if([[self checkScreenSize]isEqualToString:@"Small"]){
                noProfilesInfoLab= [[UILabel alloc]initWithFrame:CGRectMake(10, 60, self.view.frame.size.width-10, 300)];
            }
            else{
                noProfilesInfoLab= [[UILabel alloc]initWithFrame:CGRectMake(10, self.view.frame.size.height/4, self.view.frame.size.width-10, 300)];
            }
            [noProfilesInfoLab setTextAlignment:NSTextAlignmentCenter];
            [noProfilesInfoLab setTextColor:[UIColor whiteColor]];
            noProfilesInfoLab.text= @"We could not find any halo users that match your preference. Make your preference general and try again.";
            noProfilesInfoLab.hidden= NO;
            [noProfilesInfoLab setNumberOfLines:3];
            [self.view addSubview:noProfilesInfoLab];
            _reloadButton.hidden= NO;
        }
    }
    else if([currentFilter isEqualToString:@"Green"]){
        if([haloUserImagesGreen count]==0){
            //Turn off loader view
            [self showIndicator:NO];
            //Display message
            if([[self checkScreenSize]isEqualToString:@"Small"]){
                noProfilesInfoLab= [[UILabel alloc]initWithFrame:CGRectMake(10, 60, self.view.frame.size.width-10, 300)];
            }
            else{
                noProfilesInfoLab= [[UILabel alloc]initWithFrame:CGRectMake(10, self.view.frame.size.height/4, self.view.frame.size.width-10, 300)];
            }
            
            [noProfilesInfoLab setTextAlignment:NSTextAlignmentCenter];
            [noProfilesInfoLab setTextColor:[UIColor whiteColor]];
            noProfilesInfoLab.text= @"We could not find any halo users that match your preference. Make your preference general and try again.";
            noProfilesInfoLab.hidden= NO;
            [noProfilesInfoLab setNumberOfLines:3];
            [self.view addSubview:noProfilesInfoLab];
            _reloadButton.hidden= NO;
        }
    }
    else{
        if([haloUserImagesAll count]==0){
            //Turn off loader view
            [self showIndicator:NO];
            //Display message
            if([[self checkScreenSize]isEqualToString:@"Small"]){
                noProfilesInfoLab= [[UILabel alloc]initWithFrame:CGRectMake(10, 60, self.view.frame.size.width-10, 300)];
            }
            else{
                noProfilesInfoLab= [[UILabel alloc]initWithFrame:CGRectMake(10, self.view.frame.size.height/4, self.view.frame.size.width-10, 300)];
            }
            
            [noProfilesInfoLab setTextAlignment:NSTextAlignmentCenter];
            [noProfilesInfoLab setTextColor:[UIColor whiteColor]];
            noProfilesInfoLab.text= @"We could not find any halo users that match your preference. Make your preference general and try again.";
            noProfilesInfoLab.hidden= NO;
            [noProfilesInfoLab setNumberOfLines:3];
            [self.view addSubview:noProfilesInfoLab];
            _reloadButton.hidden= NO;
        }
    }

}

- (IBAction)reloadAction:(id)sender {
   
    //[self fetchHaloProfiles];
    if([currentFilter isEqualToString:@"Red"]){
        [self filterSelRed:0];
    }
    else if([currentFilter isEqualToString:@"Amber"]){
        [self filterSelAmber:0];
    }
    else if([currentFilter isEqualToString:@"Green"]){
        [self filterSelGreen:0];
    }
    else{
        [self filterSelAll:0];
    }

    [self clearNoProfLab];
}

-(void)clearNoProfLab{
    _reloadButton.hidden= YES;
    noProfilesInfoLab.hidden= YES;
}

- (IBAction)leftSideMenuAction:(id)sender {
    AppDelegate *appDelegateTemp= (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegateTemp.drawerContainer toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (IBAction)moreViewAction:(id)sender {
    AppDelegate *appDelegateTemp= (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegateTemp.drawerContainer toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

- (IBAction)segcon_filter_action:(id)sender {
    
    if(_segcon_filter.selectedSegmentIndex==0){
        _segcon_filter.tintColor = [UIColor grayColor];
        currentFilter = @"All";
        [self filterSelAll:0];
    }
    else if(_segcon_filter.selectedSegmentIndex==1){
        _segcon_filter.tintColor = [UIColor redColor];
        currentFilter= @"Red";
        [self filterSelRed:0];
    }
    else if(_segcon_filter.selectedSegmentIndex==2){
        _segcon_filter.tintColor = [UIColor orangeColor];
        currentFilter= @"Amber";
        [self filterSelAmber:0];
    }
    else{
        _segcon_filter.tintColor = [UIColor greenColor];
        currentFilter= @"Green";
        [self filterSelGreen:0];
    }


}

-(void)newMessageAlertInCheckedin:(NSNotification*)notification{
    [managedDataObject_chk setValue:@"Yes" forKey:@"newMessage"];
    [self saveData];
}

-(void)saveData{
    NSError *error;
    if(![context_chk save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
    }
}

- (IBAction)leftSliderMenuButton:(id)sender {
    
    AppDelegate *appDelegateTemp= (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegateTemp.drawerContainer toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    
}

-(void)backToForeChkIn:(NSNotification*)notification{
    
    if([[managedDataObject_chk valueForKey:@"regionFlag"]isEqualToString:@"No"]){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
        NSLog(@"Stay checked in");
    }
}

-(void)clearCachedImagesData{
    //Clear cached images data
    SDImageCache *imageCahe= [SDImageCache sharedImageCache];
    [imageCahe clearMemory];
    [imageCahe clearDisk];
}

-(void)checkVerificationChkIn:(NSNotification*)notification{
    NSString *response= [[notification userInfo]objectForKey:@"response"];
    
    if([response isEqualToString:@"Yes"] || [response isEqualToString:@"Maybe"]|| [response isEqualToString:@"error"]){
        //Dont do any thing
        
    }
    else{
        //Send to verification page
        [self performSegueWithIdentifier:@"chkdInToEVSegue" sender:self];
    }
}

@end
