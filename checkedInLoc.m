//
//  checkedInLoc.m
//  LookUp
//
//  Created by Divey Punj on 9/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "checkedInLoc.h"

NSManagedObjectContext *context_chkLoc;
NSManagedObject *managedDataObject_chkLoc;
AppDelegate *delegate_chkLoc;
NSFetchRequest *userDataRequest_chkLoc;
NSArray *userDataArray_chkLoc;
NSString *entityId_chkLoc;
NSMutableArray *locationArray;
ServerQueryBridge *serverForChkLoc;

@interface checkedInLoc ()

@end

@implementation checkedInLoc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Get entityId and username from key chain
    entityId_chkLoc = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    //Initialise Core data object
    NSError *error;
    context_chkLoc = [self managedObjectContext];
    userDataRequest_chkLoc= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_chkLoc.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_chkLoc];
    userDataArray_chkLoc = [context_chkLoc executeFetchRequest:userDataRequest_chkLoc error:&error];
    managedDataObject_chkLoc= [userDataArray_chkLoc objectAtIndex:0];

    //Initialise Location Array
    //Load event locations from device
    NSFetchRequest *eventRequest= [[NSFetchRequest alloc]initWithEntityName:@"Events"];
    eventRequest.predicate= [NSPredicate predicateWithFormat:@"venueName == %@",[managedDataObject_chkLoc valueForKey:@"venueName"]];
    NSArray *eventsArray = [context_chkLoc executeFetchRequest:eventRequest error:&error];
    
    if([eventsArray count]==0){
        locationArray= [[NSMutableArray alloc]init];
        [locationArray addObject:@"At the bar"];
        [locationArray addObject:@"Sitting Area"];
        [locationArray addObject:@"Near the toilets"];
        [locationArray addObject:@"Outside area"];
        [locationArray addObject:@"Near the entrance"];
    }
    else{
        NSManagedObject *eventObj= [eventsArray objectAtIndex:0];
        locationArray = [[NSMutableArray alloc]initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:[eventObj valueForKey:@"venueLocations"]]];
    }
    
    [_checkedInLocTab setDelegate:self];
    [_checkedInLocTab setDataSource:self];
    
    serverForChkLoc= [[ServerQueryBridge alloc]init];
    [serverForChkLoc initialiseBackend];
    [serverForChkLoc initialiseCoreData:entityId_chkLoc];
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//******************************TABLE VIEW METHODS**********************************


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return locationArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Main table view
    
    
    checkedInLocCell *cell= [tableView dequeueReusableCellWithIdentifier:@"locCell1"];
    
    if(cell == nil){
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: @"leftSliderCell"];
    }
    
    [cell setCell:[locationArray objectAtIndex:indexPath.row]];
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    //sliderMenuToSettingsSegue
    
    [managedDataObject_chkLoc setValue:[locationArray objectAtIndex:indexPath.row] forKey:@"insideLocation"];
    [self saveData];
}


-(void)saveData{
    NSError *error;
    if(![context_chkLoc save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
        //Save server
        [serverForChkLoc saveData:@"" entityId:entityId_chkLoc];
        [self performSegueWithIdentifier:@"chkLocToImgSeg" sender:self];
    }
}



@end
