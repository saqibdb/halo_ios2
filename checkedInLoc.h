//
//  checkedInLoc.h
//  LookUp
//
//  Created by Divey Punj on 9/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "checkedInLocCell.h"
#import "ServerQueryBridge.h"

@interface checkedInLoc : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *checkedInLocTab;




@end
