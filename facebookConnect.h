//
//  facebookConnect.h
//  LookUp
//
//  Created by Divey Punj on 10/07/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <AddressBook/AddressBook.h>
#import "config.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface facebookConnect : NSObject
-(void)initialise;
-(void)getFBPosts;
-(void)getFBPhotoIds;
-(void)clearExistingToken;
-(void)setCurrentToken;
-(void)getFBPhotos:(NSMutableArray*)photoIds;

@end
