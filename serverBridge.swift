//
//  serverBridge.swift
//  LookUp
//
//  Created by Divey Punj on 23/02/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

import Foundation
import Kinvey
import CoreLocation
import MapKit
import ObjectMapper

let userDataStore = DataStore<userDataStorage>.collection(.network)
let eventsStore = DataStore<eventsStoreClass>.collection(.network)
let messageStore = DataStore<messageStoreClass>.collection(.network)
let venueAnalyticsStore = DataStore<venueAnalyticsClass>.collection(.network)
let userFriendStore = DataStore<userFriendsClass>.collection(.network)

let fileStore = FileStore.getInstance()


@objc class serverBridgeClass : NSObject{
    
    var userDataToSave = userDataStorage()
    var eventData = eventsStoreClass()
    var eventImageURLs = Dictionary<String, Any>()
    var haloUsersImageUrls = Dictionary<String, Any>()
    var allUrlCount = 0
    var loadedUrlCount = 0
    var haloImageUrlCount = 0
    var haloImageLoadedUrlCount = 0
    
    @objc func setupClient(){
 
        /*Kinvey.sharedClient.initialize(appKey: "kid_-yILbT8l0g", appSecret: "fb439300a5194f5da93c5b45a2331c39") { (initial, error) in
            if error != nil{
                print("Initialise Error")
            }
            else{
                print("Initialise Successful")
            }
        }*/
        
        Kinvey.sharedClient.initialize(appKey: "kid_BJtROc3l-", appSecret: "4916af6a51f44841a29b7b111951b2ed") { (initial, error) in
            if error != nil{
                print("Initialise Error")
            }
            else{
                print("Initialise Successful")
            }
        }
        
        //Setup Push Notifications
        Kinvey.sharedClient.push.registerForPush(){succeed, error in
            print("succeed")
            if let error = error {
                print("error")
            }
        }
    }
    
    @objc func checkActiveUser() ->Bool{
        
        if Kinvey.sharedClient.activeUser != nil{
            //Has active user
            return true
        }
        else{
            //No active user
            return false
        }
    }
    
    
    @objc func signupUser(username: String, password: String){
    
        User.signup(username:username, password: password){user, error in
            if user != nil{
                //User Created
                //CREATE PLACEHOLDER IN DATA STORE
                //Add user id to data store class
                self.userDataToSave.entityId = user?.userId
                self.userDataToSave.email = username
                self.userDataToSave.verifiedUser = "No"
                //Save data to server
                userDataStore.save(&self.userDataToSave, completionHandler: { (userData, error) in
                    if error != nil {
                        //Error
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "newUserCreated"), object: nil, userInfo: ["message": "error", "entityId":userData?.entityId, "errorMessage":"Unable to create a new user. Please try again, or contact us."])
                    }
                    else{
                        //Saved to server
                        //Post notification back to ServerQueryBridge
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "newUserCreated"), object: nil, userInfo: ["message":"isDone", "entityId":userData?.entityId])
                    }
                })
            }
            else{
                print(error?.localizedDescription);
                NotificationCenter.default.post(name: Notification.Name(rawValue: "newUserCreated"), object: nil, userInfo: ["message": "error", "entityId":"none", "errorMessage":"Username already exists, please try a different username."])
            }
        }
    }
    
    @objc func verifyUserEmail(email: String){
        /*User.sendEmailConfirmation(forUsername: email){result, error in
            if(error != nil){
                //Error
            }
            else{
                //Success
            }
        }*/
        
        User.sendEmailConfirmation(forUsername: email) { result in
            if(result == nil){
                //Error
            }
            else{
                //Success
            }
        }
    }
    
    @objc func addAdditionalUserInfo(entityId: String, email: String){
        
        User.get(userId: entityId){user, error in
            
            if user != nil{
                user?.email = email
                user?.save()
            }
        }
    }
    
    @objc func saveData(notiName: String, entityId: String, name: String, email: String, gender: String, status: String, interest: String, orientation: String, userAge: NSNumber, otherUserGender: String, otherUserOrientation: String, messageMates: Array <Any>, insideLocation: String, myLocLat: Double, myLocLon: Double, checkedIn: String, venueName: String, verifiedUser: String, otherUserAgeFrom: String, otherUserAgeTo: String){
        
        self.userDataToSave.entityId = entityId
        self.userDataToSave.name = name
        self.userDataToSave.email = email
        self.userDataToSave.gender = gender
        self.userDataToSave.status = status
        self.userDataToSave.interests = interest
        self.userDataToSave.orientation = orientation
        self.userDataToSave.userAge = userAge
        self.userDataToSave.otherUserGender = otherUserGender
        self.userDataToSave.otherUserAge = ""
        self.userDataToSave.otherUserOrientation = otherUserOrientation
        self.userDataToSave.messageMates = messageMates
        self.userDataToSave.insideLocation = insideLocation
        self.userDataToSave.geocoord = GeoPoint(latitude: myLocLat, longitude: myLocLon)
        self.userDataToSave.checkedInStat = checkedIn
        self.userDataToSave.venueName = venueName
        self.userDataToSave.verifiedUser = verifiedUser
        self.userDataToSave.otherUserAgeFrom = otherUserAgeFrom
        self.userDataToSave.otherUserAgeTo = otherUserAgeTo
        
        //Save data to server
        userDataStore.save(&self.userDataToSave, completionHandler: { (userData, error) in
            if error != nil {
                //Error
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["profileSaveConfirm": "Failed"])
            }
            else{
                //Saved to server
                //Post notification back to ServerQueryBridge
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["profileSaveConfirm":"Saved"])
            }
        })
    }
    
    @objc func getHaloProfiles(fromAge: NSNumber, toAge: NSNumber, locCoord: CLLocationCoordinate2D, gender: String, orientation: String, status: String, checkedInStat: String, venueName: String){

        
        let genderQuery = NSPredicate(format: "gender == %@",gender)
        let locQuery = NSPredicate(format: "geolocation = %@", MKCircle(center: locCoord, radius: 100))
        let ageQuery = NSPredicate(format: "userAge BETWEEN {%@, %@}",fromAge,toAge)
        
        var statQuery = NSPredicate()
        
        if(status == "All"){
            
        }
        else{
            statQuery = NSPredicate(format: "status == %@",status)
        }
        
        let chkQuery = NSPredicate(format: "checkedInStat == %@",checkedInStat)
        let venueNameQuery = NSPredicate(format: "venueName == %@", venueName)
        
        //Check for orientation and make the search based on it
        var profileQueryPred = NSCompoundPredicate()
        if(orientation == "Any"){
            profileQueryPred = NSCompoundPredicate(andPredicateWithSubpredicates: [ageQuery,locQuery, genderQuery, statQuery, chkQuery, venueNameQuery])
        }
        else{
            let oritQuery1 = NSPredicate(format: "orientation == %@",orientation)
            let oritQuery2 = NSPredicate(format: "orientation == %@", "Not Disclosed");
            
            let oritPredOr = NSCompoundPredicate(orPredicateWithSubpredicates: [oritQuery1, oritQuery2])
            let commonPred = NSCompoundPredicate(andPredicateWithSubpredicates: [ageQuery,locQuery, genderQuery, statQuery, chkQuery, venueNameQuery])
            profileQueryPred = NSCompoundPredicate(andPredicateWithSubpredicates: [oritPredOr, commonPred])
        }

        let profileQuery = Query(predicate: profileQueryPred)
        
        //Fetch from data store
        userDataStore.find(profileQuery) { (data, error) in
            
            if error != nil{
                //Failed
            }
            else{
                //Success
                NotificationCenter.default.post(name: Notification.Name(rawValue: "fetchedHaloProfiles"), object: nil, userInfo: ["otherUserProfileData": self.convertDataToArray(data: data!), "notiForStatus": status])
            }
        }
    }
    
    @objc func login(username: String, password: String){
        User.login(username: username, password: password){user, error in
            if error != nil{
                //Failed login
                NotificationCenter.default.post(name: Notification.Name(rawValue: "loginSuccessful"), object: nil, userInfo: ["result":"No"])
            }
            else{
                //Success login
                NotificationCenter.default.post(name: Notification.Name(rawValue: "loginSuccessful"), object: nil, userInfo: ["result":"Yes", "userId":user?.userId, "username":user?.username])
            }
        }
        
    }
    
    @objc func logout(){
        Kinvey.sharedClient.activeUser?.logout()
    }
    
    @objc func passwordReset(emailAddress: String){
        
        User.resetPassword(usernameOrEmail: emailAddress){result in

        NotificationCenter.default.post(name: Notification.Name(rawValue: "passwordResetResult"), object: nil, userInfo: nil)
            
        }
    }
    
    @objc func loadUserProfile(entityId: String, notiName: String){
        
        let profileQuery = Query(format: "_id == %@",entityId)
        
        //Fetch from data store
        userDataStore.find(profileQuery) { (data, error) in

            if error != nil{
                //Failed
                   NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["userProfile": Dictionary<String, Any>()])
            }
            else{
                //Success
                if data?.count == 0{
                   NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["userProfile": Dictionary<String, Any>()])
                }
                else{
                   NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["userProfile":self.parseData(data: (data?[0])!)])
                }
                
            }
        }

    }
    
    
    func parseData(data: userDataStorage) -> Dictionary<String, Any>{
        let userData = ["entityId": data.entityId, "name": data.name, "email": data.email, "gender": data.gender, "status": data.status, "interests":data.interests, "orientation": data.orientation, "userAge": data.userAge, "otherUserGender": data.otherUserGender, "otherUserAge": data.otherUserAge, "otherUserOrientation": data.otherUserOrientation, "messageMates": data.messageMates, "insideLocation": data.insideLocation, "checkedInStat": data.checkedInStat, "venueName": data.venueName, "verifiedUser":data.verifiedUser, "otherUserAgeFrom": data.otherUserAgeFrom, "otherUserAgeTo": data.otherUserAgeTo] as [String : Any]
        
        return userData 
        
    }
    
    func convertDataToArray(data: Array<userDataStorage>) -> Array<Any>{
        var tempArray = [AnyObject]()
        for i in 0..<data.count{
            tempArray.append(self.parseData(data: (data[i])) as AnyObject)
        }
        return tempArray
    }
    
    
    //EVENTS METHODS
    @objc func getNearByEvents(locCoord: CLLocationCoordinate2D, radius: Double, notiName: String, keyName: String){
        let locQuery = Query(format: "geolocation = %@", MKCircle(center: locCoord, radius: radius))
        
        //Fetch from data store
        eventsStore.find(locQuery) { (data, error) in
            
            if error != nil{
                //Failed
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: [keyName: "error", "status":"failed"])
            }
            else{
                //Success
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: [keyName: self.convertDataToArrayForEvents(data: data!), "status":"success"])
            }
        }
        
        
    }
    func parseDataForEvents(data: eventsStoreClass) -> Dictionary<String, Any>{
        let userData = ["eventId": data.entityId, "eventName": data.eventName, "eventDate": data.eventDate, "eventAddress": data.eventAddress, "eventImageId":data.eventImageId, "eventCoordLat":data.geocoord?.latitude, "eventCoordLon":data.geocoord?.longitude, "insideLocations":data.insideLocations, "venueName":data.venueName, "promoCode":data.promoCode] as [String : Any]
        
        return userData
        
    }
    
    func convertDataToArrayForEvents(data: Array<eventsStoreClass>) -> Array<Any>{
        var tempArray = [AnyObject]()
        for i in 0..<data.count{
            tempArray.append(self.parseDataForEvents(data: (data[i])) as AnyObject)
        }
        
        return tempArray
        
    }
    
    
    //IMAGE DOWNLOADING METHODS
    //EVENT IMAGES
    @objc func downloadEventImages(imageIds: Array<String>, notiName: String, keyName: String){
        
        self.allUrlCount = imageIds.count
        for i in 0..<imageIds.count{
            downloadImageForEvents(imageId: imageIds[i], notiName: notiName, keyName: keyName)
        }
    }
    
    
    
    func downloadImageForEvents(imageId: String, notiName: String, keyName: String){

        let file = File()
        file.fileId = imageId
        
        
        fileStore.download(file){(file, url: URL?, error)in
            
            if let file = file, let url = url{
                //Success
                
                //Add the url to a dictionary
                self.eventImageURLs[file.fileId!] = url.absoluteString
                //Check if count is achieved
                self.loadedUrlCount = self.loadedUrlCount + 1
                //Then notify back
                if self.loadedUrlCount >= self.allUrlCount{
                    NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: [keyName: self.eventImageURLs])
                }
            }
            else{
                //Failed
            }
        }
    }
    
    //HALO USER IMAGES

    @objc func uploadUserImage(image: UIImage, imageId: String, notiName: String, name: String){
        
        let uploadFileStore = FileStore.getInstance()
        
        let file = File()
        file.publicAccessible = true
        file.fileId = imageId
        file.fileName = name
        let acl = Acl();
        acl.globalRead.value = true;
        file.acl = acl;

        uploadFileStore.upload(file, image: image){file, error in
            if error != nil{
                //Failed
                print("ERROR IN UPLOADING IMAGE: %@",error?.localizedDescription as Any)
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["profilePicConf": "notSaved"])
            }
            else{
                print("SUCCESS IN UPLOADING IMAGE");
                //Success
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["profilePicConf": "Saved"])
                
            }
        }
    }
    @objc func downloadSmallImagesByIds(imageIds: Array<String>, notiName: String, keyName: String){
        
        //Initialise
        haloUsersImageUrls = Dictionary<String, Any>()
        self.haloImageLoadedUrlCount = 0
        
        self.haloImageUrlCount = imageIds.count
        for i in 0..<imageIds.count{
            downloadImageForHaloUsers(imageId: imageIds[i], notiName: notiName, keyName: keyName)
        }
        
    }
    
    func downloadImageForHaloUsers(imageId: String, notiName: String, keyName: String){
        
        //let file = File()
        //file.fileId = imageId
    
        let file = File()
        file.fileId = imageId
        
        fileStore.download(file){(file, url: URL?, error)in
            
            if let file = file, let url = url{
                //Success
                
                //Add the url to a dictionary
                self.haloUsersImageUrls[file.fileId!] = url.absoluteString
                //Check if count is achieved
                self.haloImageLoadedUrlCount = self.haloImageLoadedUrlCount + 1
                //Then notify back
                if self.haloImageLoadedUrlCount >= self.haloImageUrlCount{
                    print(self.haloUsersImageUrls.count)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: [keyName: self.haloUsersImageUrls])
                }
            }
            else{
                //Failed
                //Add a dummy URL
                self.haloUsersImageUrls[(file?.fileId)!] = "none"
                //Check if count is achieved
                self.haloImageLoadedUrlCount = self.haloImageLoadedUrlCount + 1
                print("Error %@", error?.localizedDescription)
            }
        }
    }
    
    @objc func downloadLargeImage(imageId: String, notiName: String, keyName: String){
        
        let file = File()
        file.fileId = imageId
        
        
        fileStore.download(file){(file, url: URL?, error)in
            
            if let file = file, let url = url{
                //Success
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: [keyName: url.absoluteString])
            }
            else{
                //Failed

            }
        }

    }
    
    //SOCIAL IDENTITY LOGIN
    @objc func loginWithSocialID(facebookDict: Dictionary<String, Any>, accessToken: String){
        let faceDict = ["access_token": accessToken]
        
        User.login(authSource: .facebook, faceDict){user, error in
            if error != nil{
                //Failed
                print(error?.localizedDescription)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "serverError"), object: nil, userInfo: ["error":"Unable to log in with Facebook. Please contact us."]);
            }
            else{
                //Check if this user already exists by checking the placeholder store.
                let profileQuery = Query(format: "_id == %@",user?.userId)
                //Fetch from data store
                userDataStore.find(profileQuery) { (data, error) in
                    
                    if error != nil{
                        //Send back a failed notification
                        print(error?.localizedDescription);
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "serverError"), object: nil, userInfo: ["error":"Unable to log in with Facebook. Please contact us."]);
                    }
                    else{
                        //Success
                        if data?.count == 0{
                            //User does not exist, create a new one
                            //CREATE PLACEHOLDER IN DATA STORE
                            //Add user id to data store class
                            self.userDataToSave.entityId = user?.userId
                            self.userDataToSave.email = facebookDict["email"] as! String?
                            self.userDataToSave.verifiedUser = "Yes"
                            //Save data to server
                            userDataStore.save(&self.userDataToSave, completionHandler: { (userData, error) in
                                if error != nil {
                                    //Error
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "serverError"), object: nil, userInfo: ["error":"Unable to log in with Facebook. Please contact us."]);
                                }
                                else{
                                    //Saved to server
                                    //Post notification back to ServerQueryBridge
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "fbUserSignedIn"), object: nil, userInfo: ["message":"isDone", "entityId":userData?.entityId])
                                }
                            })
                        }
                        else{
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "fbUserLoggedIn"), object: nil, userInfo: ["userData":self.parseData(data: (data?[0])!)])
                        }
                        
                    }
                }
            }
        }

    }
    
    //MESSAGE METHODS
    @objc func loadMessages(recipientId:String, senderId: String, skipCount: Int, notiName: String){

        let uSendQ = NSPredicate(format: "senderId == %@",recipientId)
        let iRecieveQ = NSPredicate(format: "recipientIds == %@",senderId)
        let iSendQ = NSPredicate(format: "senderId == %@",senderId)
        let uRecieveQ = NSPredicate(format: "recipientIds == %@",recipientId)

        
        let messageQueryPredAnd1 = NSCompoundPredicate(andPredicateWithSubpredicates: [uSendQ,iRecieveQ])
        let messageQueryPredAnd2 = NSCompoundPredicate(andPredicateWithSubpredicates: [iSendQ,uRecieveQ])
        let messageQueryPredOr = NSCompoundPredicate(orPredicateWithSubpredicates: [messageQueryPredAnd1, messageQueryPredAnd2])
        
        let sortDescriptor = NSSortDescriptor(key: "timestamp", ascending: false)
        
        let finalQuery = Query(predicate: messageQueryPredOr, sortDescriptors: [sortDescriptor])
        
        finalQuery.skip = skipCount
        finalQuery.limit = 50;
        
        
        //Fetch from data store
        messageStore.find(finalQuery) { (data, error) in
            
            if error != nil{
                //Failed
            }
            else{
                //Success
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["messageData": self.convertMessagesToArray(data: data!)])
            }
        }
    }
    
    func convertMessagesToArray(data: Array<messageStoreClass>) -> Array<Any>{
        var tempArray = [AnyObject]()
        for i in 0..<data.count{
            tempArray.append(self.parseMessageData(data: (data[i])) as AnyObject)
        }
        return tempArray
    }
    
    func parseMessageData(data: messageStoreClass) -> Dictionary<String, Any>{
        let userData = ["senderId": data.senderId, "recipientId": data.recipientIds, "timestamp": data.timestamp, "text": data.text] as [String : Any]
        
        return userData
        
    }
    
    @objc func checkForExistingMessages(recipientId: String, senderId: String, text: String, timeStamp: Date, notiName: String){
        let iSendQ = NSPredicate(format: "senderId == %@",senderId)
        let uRecieveQ = NSPredicate(format: "recipientIds == %@",recipientId)
        let textQ = NSPredicate(format: "text == %@",text)
        //let timeStampQ = NSPredicate(format: "timestamp == %@", timeStamp as CVarArg)
        
        
        let messageQueryPredAnd = NSCompoundPredicate(andPredicateWithSubpredicates: [iSendQ,uRecieveQ, textQ])
        
        
        let finalQuery = Query(predicate: messageQueryPredAnd)

        //Fetch from data store
        messageStore.find(finalQuery) { (data, error) in
            
            if error != nil{
                //Failed
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["response": "error"])
            }
            else{
                //Success
                if((data?.count)! > 0){
                    NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["response": "YES", "messageData": self.convertMessagesToArray(data: data!)])
                }
                else{
                    NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["response": "NO"])
                }
                
            }
        }
    }
    
    @objc func saveMessage(text:String, senderId:String, recipientId:String, timestamp:Date, messageId: String){
        
        
        let messageToSave = messageStoreClass()
        
        messageToSave.text = text
        messageToSave.senderId = senderId
        messageToSave.recipientIds = recipientId
        messageToSave.timestamp = timestamp
        messageToSave.messageId = messageId
        
        
        //Save data to server
        messageStore.save(messageToSave, completionHandler: { (message, error) in
            if error != nil {
                //Error
            }
            else{
                //Saved to server
                print("Message saved to server")
            }
        })
    }
    
    //Tapped methods
    @objc func getUserWithId(entityId: String, notiName: String, keyName: String){
        
        let userQuery = Query(format: "_id == %@",entityId)
        
        //Fetch from data store
        userDataStore.find(userQuery) { (data, error) in
            
            if error != nil{
                //Failed
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: [keyName: Dictionary<String, Any>()])
            }
            else{
                //Success
                if data?.count == 0{
                    NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: [keyName: Dictionary<String, Any>()])
                }
                else{
                    NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: [keyName:self.parseData(data: (data?[0])!)])
                }
                
            }
        }
    }
    
    @objc func addVenueAnalyticsData(userEntId: String, checkedInTime: Date, checkedOutTime: Date, venueName: String, eventName: String, userLocLat: Double, userLocLon: Double){
        
        let venueAnalyticsData = venueAnalyticsClass()
        
        venueAnalyticsData.userEntId = userEntId
        venueAnalyticsData.checkedInTime = checkedInTime
        venueAnalyticsData.checkedOutTime = checkedOutTime
        venueAnalyticsData.venueName = venueName
        venueAnalyticsData.eventName = eventName
        venueAnalyticsData.userLoc = GeoPoint(latitude: userLocLat, longitude: userLocLon)
        
        
        //Save data to server
        venueAnalyticsStore.save(venueAnalyticsData, completionHandler: { (analyticData, error) in
            if error != nil {
                //Error
            }
            else{
                //Saved
            }
        })
    }
    
    @objc func connectUsers(friendId: String, myId: String, myName: String,friendName: String, isHalo: String, friendStatus: String, venueName: String, timeStamp: String, myStatus: String, notiName: String){

        let params: JsonDictionary = ["myId":myId, "myName": myName, "friendId":friendId, "friendName":friendName, "isHalo":isHalo, "friendStatus": friendStatus, "venueName": venueName, "timeStamp": timeStamp, "myStatus": myStatus];
        
        CustomEndpoint.execute("AddNewFriend", params: CustomEndpoint.Params(params)) { (response: JsonDictionary?, error) in
            if response != nil {
            //custom endpoint succeed
            print("Endpoint reached")
            NotificationCenter.default.post(name: Notification.Name(rawValue:notiName), object: nil, userInfo: ["error": "No", "response":response as Any])
            
            } else {
            //handle error
                
            print("Endpoint failed")
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["error": "Yes"])
            }
        }
    }
    
    @objc func friendRequestResponse(myId: String, friendId: String, myResponse: String){
        let params: JsonDictionary = ["myId": myId, "friendId": friendId, "myResponse": myResponse]
        
        CustomEndpoint.execute("FriendshipResponse", params: CustomEndpoint.Params(params)) { (response: JsonDictionary?, error) in
            if response != nil {
                //custom endpoint succeed
                print("Endpoint reached")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "friendRequestNoti"), object: nil, userInfo: ["error": "No"])
                
            } else {
                //handle error
                print("Endpoint failed")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "friendRequestNoti"), object: nil, userInfo: ["error": "Yes"])
            }
        }
        
        /*CustomEndpoint.execute("BarcodeConnectConfirmation", params: CustomEndpoint.Params(params)) { (response: JsonDictionary?, error) in
            if response != nil {
                //custom endpoint succeed
                print("Endpoint reached")
                
            } else {
                //handle error
                print("Endpoint failed")
            }
        }*/
    }
    
    @objc func removeExistingFriend(friendId: String, myId: String){
        
        let params: JsonDictionary = ["myId": myId, "friendId": friendId]
        
        CustomEndpoint.execute("RemoveExistingFriend", params: CustomEndpoint.Params(params)) { (response: JsonDictionary?, error) in
            if response != nil {
                //custom endpoint succeed
                print("Endpoint reached")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "removeExistingFriendNoti"), object: nil, userInfo: ["error": "No"])
                
            } else {
                //handle error
                print("Endpoint failed")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "removeExistingFriendNoti"), object: nil, userInfo: ["error": "Yes"])
            }
        }

    }
    
    @objc func sendEmailVerification(email: String, emailBody: String){
        //Create email array
        var emailArray: [String] = [email]
        
        let params: JsonDictionary = ["to":emailArray, "subject":"Halo Email Verification", "body":emailBody, "reply_to":"no-reply@halostatus.com"]
        
        CustomEndpoint.execute("SendEmailVerification", params: CustomEndpoint.Params(params)) { (response: JsonDictionary?, error) in
            if let response = response {
                //custom endpoint succeed
                print("Endpoint reached")
                
            } else {
                //handle error
                print("Endpoint failed")
                print(error?.localizedDescription)
            }
        }
    }
    
    
    @objc func updateUsernameAndEmail(entityId: String, email: String){
        
        User.get(userId: entityId){user, error in
            
            if user != nil{
                user?.email = email
                user?.username = email
                user?.save()
            }
        }
    }
    
    @objc func getMyFriendsList(entityId: String, notiName: String){
        
        let profileQuery = Query(format: "myId == %@",entityId)
        
        //Fetch from friends store
        userFriendStore.find(profileQuery) { (data, error) in
            
            if error != nil{
                //Failed
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["friendsList": Dictionary<String, Any>(), "error":"Yes"])
            }
            else{
                //Success
                if data?.count == 0{
                    NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["friendsList": Dictionary<String, Any>(), "error":"Yes"])
                }
                else{
                    NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["friendsList":self.convertFriendsDataToArray(data:data!), "error":"No"])
                }
                
            }
        }

    }
    
    func parseDataForFriends(data: userFriendsClass) -> Dictionary<String, Any>{
        let userData = ["friendshipStatus": data.friendshipStatus, "friendName": data.friendName, "friendId": data.friendId, "myId": data.myId, "friendStatus": data.friendStatus, "venueName": data.venueName, "timeStamp": data.timeStamp] as [String : Any]
        
        return userData
        
    }
    
    func convertFriendsDataToArray(data: Array<userFriendsClass>) -> Array<Any>{
        var tempArray = [AnyObject]()
        for i in 0..<data.count{
            tempArray.append(self.parseDataForFriends(data: (data[i])) as AnyObject)
        }
        return tempArray
    }

    //********************LOCATION CODE**********************
    struct GeolocationParam: Mappable {
        var latitude: Double?
        var longitude: Double?
        var isCheckedIn: String?
        var isActive: String?
        
        init(_ latitude: Double, _ longitude: Double, _ isCheckedIn: String, _ isActive: String){
            self.latitude = latitude
            self.longitude = longitude
            self.isCheckedIn = isCheckedIn
            self.isActive = isActive
        }
        
        init?(map: Map){
            
        }
        
        public mutating func mapping(map: Map){
            latitude <- map["latitude"]
            longitude <- map["longitude"]
            isCheckedIn <- map["isCheckedIn"]
            isActive <- map["isActive"]
        }
    }
    
    @objc func canCheckIn(latitude: Double, longitude: Double, isCheckedIn: String, isActive: String, notiName: String){
        
        let params = GeolocationParam(latitude, longitude, isCheckedIn, isActive)
        
        CustomEndpoint.execute("FindCheckInVenue", params: CustomEndpoint.Params(params)) { (response: JsonDictionary?, error) in
            if response != nil {
                //custom endpoint succeed
                print("Endpoint reached")
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["error": "No", "response":response as Any])
                
            } else {
                //handle error
                print("Endpoint failed")
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["error": "Yes"])
            }
        }
    }

    @objc func verifiedAs(entityId: String, notiName: String){
        
        let userQuery = Query(format: "_id == %@",entityId)
        
        //Fetch from data store
        userDataStore.find(userQuery) { (data, error) in
            
            if error != nil{
                //Failed
                NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["response": "error"])
            }
            else{
                //Success
                if data?.count == 0{
                    NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["response": "error"])
                }
                else{
                    NotificationCenter.default.post(name: Notification.Name(rawValue: notiName), object: nil, userInfo: ["response":data?[0].verifiedUser])
                }
                
            }
        }

    }
    
    
    
}

//CLASS FOR DATA STORAGE
@objc class userDataStorage:Entity{

    
    //dynamic var userEntityId: String?
    dynamic var name: String?
    dynamic var gender: String?
    dynamic var email: String?
    dynamic var status: String?
    dynamic var interests: String?
    dynamic var userAge: NSNumber?
    dynamic var orientation: String?
    dynamic var otherUserAge: String?
    dynamic var otherUserGender: String?
    dynamic var otherUserOrientation: String?
    dynamic var geocoord: GeoPoint?
    dynamic var messageMates: Array<Any>?
    dynamic var insideLocation : String?
    dynamic var checkedInStat : String?
    dynamic var venueName: String?
    dynamic var verifiedUser: String?
    dynamic var otherUserAgeFrom: String?
    dynamic var otherUserAgeTo: String?
    
    override class func collectionName() -> String {
        return "userDataStorage" //This must be the name of the backend store
    }
    
    override func propertyMapping(_ map: Map) {
        super.propertyMapping(map)
        
        //userEntityId <- ("_id", map["_id"])
        name <- ("name", map["name"])
        gender <- ("gender", map["gender"])
        email <- ("email", map["email"])
        status <- ("status", map["status"])
        interests <- ("interest", map["interest"])
        userAge <- ("userAge", map["userAge"])
        orientation <- ("orientation", map["orientation"])
        otherUserAge <- ("otherUserAge", map["otherUserAge"])
        otherUserGender <- ("otherUserGender", map["otherUserGender"])
        otherUserOrientation <- ("otherUserOrientation", map["otherUserOrientation"])
        geocoord <- ("geolocation", map["geolocation"])
        messageMates <- ("messageMates", map["messageMates"])
        insideLocation <- ("insideLocation", map["insideLocation"])
        checkedInStat <- ("checkedInStat", map["checkedInStat"])
        venueName <- ("venueName", map["venueName"])
        verifiedUser <- ("verifiedUser", map["verifiedUser"])
        otherUserAgeFrom <- ("otherUserAgeFrom", map["otherUserAgeFrom"])
        otherUserAgeTo <- ("otherUserAgeTo", map["otherUserAgeTo"])
    }
    
}

//CLASS FOR EVENTS
@objc class eventsStoreClass: Entity{
    
    dynamic var eventName: String?
    dynamic var eventDate: String?
    dynamic var eventAddress: String?
    dynamic var geocoord: GeoPoint?
    dynamic var eventImageId: String?
    dynamic var insideLocations: Array<String>?
    dynamic var venueName: String?
    dynamic var promoCode: String?
    
    override class func collectionName() -> String{
        return "eventsStore"
    }
    
    override func propertyMapping(_ map: Map) {
        super.propertyMapping(map)
        
        eventName <- ("eventName", map["eventName"])
        eventDate <- ("eventDate", map["eventDate"])
        eventAddress <- ("eventAddress", map["eventAddress"])
        geocoord <- ("geolocation", map["geolocation"])
        eventImageId <- ("eventImageId", map["eventImageId"])
        insideLocations <- ("insideLocations", map["insideLocations"])
        venueName <- ("venueName", map["venueName"])
        promoCode <- ("promoCode", map["promoCode"])
    }
    
}

//CLASS FOR MESSAGES
@objc class messageStoreClass: Entity{
    
    dynamic var recipientIds: String?
    dynamic var timestamp: Date?
    dynamic var senderId: String?
    dynamic var text: String?
    dynamic var messageId: String?
    
    
    override class func collectionName() -> String{
        return "userMessageStore"
    }
    
    override func propertyMapping(_ map: Map) {
        super.propertyMapping(map)
        
        recipientIds <- ("recipientIds", map["recipientIds"])
        timestamp <- ("timestamp",map["timestamp"],KinveyDateTransform())
        senderId <- ("senderId", map["senderId"])
        text <- ("text", map["text"])
        messageId <- ("messageId", map["messageId"])
    }
    
}

@objc class venueAnalyticsClass: Entity{
    dynamic var userEntId: String?
    dynamic var checkedInTime: Date?
    dynamic var checkedOutTime: Date?
    dynamic var venueName: String?
    dynamic var eventName: String?
    dynamic var userLoc: GeoPoint?
    
    override class func collectionName() -> String{
        return "VenueAnalytics"
    }
    
    override func propertyMapping(_ map: Map) {
        super.propertyMapping(map)
        
        userEntId <- ("userEntId", map["userEntId"])
        checkedInTime <- ("checkedInTime",map["checkedInTime"],KinveyDateTransform())
        checkedOutTime <- ("checkedOutTime", map["checkedOutTime"])
        venueName <- ("venueName", map["venueName"])
        eventName <- ("eventName", map["eventName"])
        userLoc <- ("userLoc", map["userLoc"])
    }
    
}

@objc class userFriendsClass: Entity{
    
    dynamic var friendshipStatus: String?
    dynamic var friendName: String?
    dynamic var friendId: String?
    dynamic var myId: String?
    dynamic var friendStatus: String?
    dynamic var venueName: String?
    dynamic var timeStamp: String?
    
    override class func collectionName() -> String{
        return "userFriendsStore"
    }
    
    override func propertyMapping(_ map: Map) {
        super.propertyMapping(map)
        
        friendshipStatus <- ("friendshipStatus", map["friendshipStatus"])
        friendName <- ("friendName", map["friendName"])
        friendId <- ("friendId", map["friendId"])
        myId <- ("myId", map["myId"])
        friendStatus <- ("friendStatus", map["friendStatus"]);
        venueName <- ("venueName", map["venueName"]);
        timeStamp <- ("timestamp",map["connectDate"])
    }
}



















