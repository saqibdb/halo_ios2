//
//  config.h
//  LookUp
//
//  Created by Divey Punj on 17/04/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//
#ifndef config_h
#define config_h

//General
#define NAVTABBARRED 20
#define NAVTABBARGREEN 20
#define NAVTABBARBLUE 20


//User Profile
#define mainTimerValueMin 0
#define mainTimerValueMax 8
#define locationTimerValue 5
#define statusHaloWidth 80
#define statusHaloHeight 62
#define redStatusHaloXPos 0
#define redStatusHaloYPos 0
#define yellowStatusHaloXPos 135
#define yellowStatusHaloYPos 0
#define greenStatusHaloXPos 279
#define greenStatusHaloYPos 0
#define shadowOffsetWidth   2
#define shadowOffsetHeight  2
#define shadowOpacityValue   50
#define shadowRadiusValue    40.0
#define nonCheckinTimerValMin 4
#define nonCheckinTimerValMax 20
#define eventTimerValMin 5
#define eventTimerValMax 10
#define adTimerValMin 2
#define adTimerValMax 6
#define newsEventDefaultTime 10
#define generalInfoTime 5
#define newsEventImageLeadingOrigCons 210
#define newsEventTitleLabLeadingOrigCons 8
#define PROFILE_SEARCH_TIME 20

//User Settings
#define PROFILE_IMAGE_ANGLE 90

//Messaging

#define SINCH_APPLICATION_KEY @"3d0eba9f-c719-4cc8-a470-80dcf32c93a8"
#define SINCH_APPLICATION_SECRET @"h1hS+PHgP0a63rb9cQ2cCw=="
#define SINCH_ENVIRONMENT_HOST @"sandbox.sinch.com"
#define SINCH_MESSAGE_RECIEVED @"SINCH_MESSAGE_RECIEVED"
#define SINCH_MESSAGE_SENT @"SINCH_MESSAGE_SENT"
#define SINCH_MESSAGE_DELIVERED @"SINCH_MESSAGE_DELIVERED"
#define SINCH_MESSAGE_FAILED @"SINCH_MESSAGE_FAILED"

//Popup Message Box
#define ANIMATION_DURATION .25

//Events
#define EVENTS_WITHIN_RADIUS .5
#define EVENTS_MAP_REGION_SPAN_LON_DELTA .10f
#define EVENTS_MAP_REGION_SPAN_LAT_DELTA .10f

//Server
#define age18 @"18"
#define age25 @"25"
#define age26 @"26"
#define age36 @"36"
#define age37 @"37"
#define age50 @"50"
#define age51 @"51"
#define age65 @"65"


//RSS Reader
#define rssUrl @"https://www.facebook.com/feeds/page.php?format=rss20&id=10210323817478701"
#define exampleUrl @"http://www.geekylemon.com?apps/blog/entries/feed/rss"

//Facebook
#define fbHaloAppId @"606333462857907"
#define fbHaloAppSecret @"2c8568e90478ceb59b853a4287a09876"
#define fbHaloUrl @"https://graph.facebook.com/divs.punj/feed"


//Halo
#define HALO_FEEDBACK_ID @"59044a8de79e838641b4b9cb"





























#endif /* config_h */
