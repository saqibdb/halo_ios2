//
//  userProfileData.m
//  LookUp
//
//  Created by Divey Punj on 22/02/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "userProfileData.h"

ServerQueryBridge *serverForProfData;
UIImage *originalPhoto;
NSManagedObjectContext *context_profData;
NSManagedObject *managedDataObject_profData;
AppDelegate *delegate_profData;
NSFetchRequest *userDataRequest_profData;
NSArray *userDataArray_profData;
NSString *entityId_profData;


@interface userProfileData ()

@end

@implementation userProfileData

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    entityId_profData = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    //Initialise Core data object
    NSError *error;
    context_profData = [self managedObjectContext];
    userDataRequest_profData= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_profData.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_profData];
    userDataArray_profData = [context_profData executeFetchRequest:userDataRequest_profData error:&error];
    managedDataObject_profData= [userDataArray_profData objectAtIndex:0];

    //Initialise Server
    serverForProfData= [[ServerQueryBridge alloc]init];
    [serverForProfData initialiseBackend];
    [serverForProfData initialiseCoreData:entityId_profData];
    
    if([UIImage imageWithData:[managedDataObject_profData valueForKey:@"userDP"]]== [NSNull null]){
        //Nothing
    }
    else{
        _profilePicture.image= [UIImage imageWithData:[managedDataObject_profData valueForKey:@"userDP"]];
    }
    
    _saveButton.layer.cornerRadius= 15;
    _saveButton.clipsToBounds= YES;
}


-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)imagePickLibrary:(id)sender {
    imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate= self;
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:imagePicker animated:YES completion:NULL];
}

- (IBAction)imagePickCamera:(id)sender {
    takePhotoPicker= [[UIImagePickerController alloc]init];
    takePhotoPicker.delegate= self;
    [takePhotoPicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [self presentViewController:takePhotoPicker animated:YES completion:NULL];
    
}

- (IBAction)profileSave:(id)sender {
    
    if(originalPhoto){
        [_saveButton setEnabled:NO];
        [_saveButton setTitle:@"Saving.." forState:UIControlStateNormal];
        //Send the photo to sign up set up. Even if the request is not from them. Post the notification
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userDPSelected" object:self userInfo:@{@"userDP":originalPhoto}];
        //Save photo to device
        [managedDataObject_profData setValue:UIImagePNGRepresentation(originalPhoto) forKey:@"userDP"];
        [self saveData];
        //Save DP to server
        [self uploadDP];
    }
    else{
        [self popMessage:@"Profile picture cannot be empty" title:@"Error"];
    }
    


}

- (IBAction)cancelAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)uploadDP{
    //Select image from memory
    //Upload image through uploading method, create notification and disable the save button until uploaded
    //Create notification
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(imageUploadedToServer:) name:@"imageUploadedToServer" object:nil];
    
    //Resize image to two different sizes
    //SMALL IMAGE
    //[serverForProfData uploadSmallImage:[self resizeImage:originalPhoto scaleToSize:CGSizeMake(100, 100)] entityId:entityId_profData notiName:@"imageUploadedToServer" name:[managedDataObject_profData valueForKey:@"email"]];
    //LARGE IMAGE
    //[serverForProfData uploadLargeImage:[self resizeImage:originalPhoto scaleToSize:CGSizeMake(400, 400)] entityId:entityId_profData notiName:@"imageUploadedToServer" name:[managedDataObject_profData valueForKey:@"email"]];
    
    [serverForProfData uploadSmallImage:[self resizeImage:originalPhoto reqWidth:100.0 reqHeight:120.0] entityId:entityId_profData notiName:@"" name:[managedDataObject_profData valueForKey:@"email"]];
    
    [serverForProfData uploadLargeImage:[self resizeImage:originalPhoto reqWidth:380.0 reqHeight:400.0] entityId:entityId_profData notiName:@"imageUploadedToServer" name:[managedDataObject_profData valueForKey:@"email"]];

}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    originalPhoto= [self rotateImage:[info objectForKeyedSubscript:UIImagePickerControllerOriginalImage]];

    [_profilePicture setImage:originalPhoto];

    _imageScrollview.minimumZoomScale= 0.1;
    _imageScrollview.maximumZoomScale= 6.0;
    _imageScrollview.delegate= self;
    
    //Show small image
    [self showSmallImage:originalPhoto];
    [self dismissViewControllerAnimated:YES completion:NULL];

}

-(void)showSmallImage:(UIImage*)image{
    _smallImage.layer.cornerRadius= 50;
    _smallImage.clipsToBounds= YES;
    _smallImage.image= image;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(void)imageUploadedToServer:(NSNotification*)notification{
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"imageUploadedToServer" object:nil];
    
    NSString *response= [[notification userInfo]objectForKey:@"profilePicConf"];
    
    if([response isEqualToString:@"notSaved"]){
        //Re-upload
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(imageUploadedToServer:) name:@"imageUploadedToServer" object:nil];
        [self uploadDP];
    }
    else{
        //Nothing
    }
    
    [_saveButton setEnabled:YES];
    [_saveButton setTitle:@"Save" forState:UIControlStateNormal];
    //Close
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(UIImage *)rotateImage:(UIImage *)image{
    
    CGImageRef imgRef= image.CGImage;
    CGFloat width= CGImageGetWidth(imgRef);
    CGFloat height= CGImageGetHeight(imgRef);
    CGSize imageSize= CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGRect bounds= CGRectMake(0, 0, width, height);
    CGFloat boundheight;
    CGFloat scaleRatio= bounds.size.width/ width;
    CGAffineTransform transform= CGAffineTransformIdentity;
    UIImageOrientation orientation= image.imageOrientation;
    
    if(orientation == UIImageOrientationUp){
        //transform= CGAffineTransformIdentity;
        //NSLog(@"Image orient up");
        return image;
    }
    else if(orientation== UIImageOrientationUpMirrored){
        transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
        transform= CGAffineTransformScale(transform, 1.0, -1.0);
    }
    else if(orientation == UIImageOrientationDown){
        transform= CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
        transform= CGAffineTransformRotate(transform, M_PI);
    }
    else if(orientation== UIImageOrientationDownMirrored){
        transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
        transform= CGAffineTransformScale(transform, 1.0, -1.0);
    }
    else if(orientation == UIImageOrientationLeftMirrored){
        boundheight= bounds.size.height;
        bounds.size.height= bounds.size.width;
        bounds.size.width= boundheight;
        transform= CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
        transform= CGAffineTransformScale(transform, -1.0, 1.0);
        transform= CGAffineTransformRotate(transform, 3.0*M_PI/2.0);
    }
    else if(orientation == UIImageOrientationLeft){
        boundheight= bounds.size.height;
        bounds.size.height= bounds.size.width;
        bounds.size.width= boundheight;
        transform= CGAffineTransformMakeTranslation(0.0, imageSize.width);
        transform= CGAffineTransformRotate(transform, 3.0*M_PI/2.0);
    }
    else if(orientation == UIImageOrientationRightMirrored){
        boundheight = bounds.size.height;
        bounds.size.height= bounds.size.width;
        bounds.size.width= boundheight;
        transform= CGAffineTransformMakeScale(-1.0, 1.0);
        transform= CGAffineTransformRotate(transform, M_PI/2.0);
    }
    else if(orientation== UIImageOrientationRight){
        boundheight= bounds.size.height;
        bounds.size.height= bounds.size.width;
        bounds.size.width= boundheight;
        transform= CGAffineTransformMakeTranslation(imageSize.height, 0.0);
        transform= CGAffineTransformRotate(transform, M_PI/2.0);
    }
    else{
        //nothing
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context= UIGraphicsGetCurrentContext();
    if(orientation == UIImageOrientationRight || orientation == UIImageOrientationLeft){
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else{
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy= UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
    
}


-(UIImage *)resizeImage:(UIImage *)image reqWidth:(float)reqWidth reqHeight:(float)reqHeight{
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = reqWidth/reqHeight;
    
    if(imgRatio!=maxRatio){
        if(imgRatio < maxRatio){
            imgRatio = reqHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = reqHeight;
        }
        else{
            imgRatio = reqWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = reqWidth;
        }
    }
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;

    
   /* float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = 320.0/480.0;
    
    if(imgRatio!=maxRatio){
        if(imgRatio < maxRatio){
            imgRatio = 480.0 / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = 480.0;
        }
        else{
            imgRatio = 320.0 / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = 320.0;
        }
    }
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;*/

}



- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    [self showSmallImage:_profilePicture.image];
    return self.profilePicture;
}

-(void)scrollViewDidEndZooming:(UIScrollView*)scrollView withView:(nullable UIView *)view atScale:(CGFloat)scale{
    //[self showSmallImage:_profilePicture.image];
}


-(void)saveData{
    NSError *error;
    if(![context_profData save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
    }
}


-(void)popMessage:(NSString*)message title:(NSString*)title{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}




@end
