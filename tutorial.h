//
//  tutorial.h
//  LookUp
//
//  Created by Divey Punj on 6/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tutorial : UIViewController


@property (strong, nonatomic) IBOutlet UIImageView *tutImg;
@property (strong, nonatomic) IBOutlet UIButton *prevTutBut;
@property (strong, nonatomic) IBOutlet UIButton *nextTutBut;
@property (strong, nonatomic) IBOutlet UIButton *doneTutBut;


- (IBAction)nextTut:(id)sender;
- (IBAction)prevTut:(id)sender;
- (IBAction)doneTut:(id)sender;


@end
