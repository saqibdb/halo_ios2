//
//  barcodeScanCont.m
//  LookUp
//
//  Created by Divey Punj on 12/05/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "barcodeScanCont.h"
NSString *scannedResult;
BOOL methodLock;

@interface barcodeScanCont ()



@end

@implementation barcodeScanCont

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   /* _isReading= NO;
    _captureSession= nil;
    
    scannedResult= @"";
    
    [self startScanning];*/
    
    _qrCodeBut.layer.cornerRadius= 20;
    _qrCodeBut.clipsToBounds= YES;
}



-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    methodLock= NO;
    _isReading= NO;
    _captureSession= nil;
    
    scannedResult= @"";
    
    [self startScanning];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)startScanning{
    if (!_isReading) {
        if ([self startReading]) {
            _infoLab.text= @"Show me your friend's QR code";
        }
    }
    else{
        [self stopReading];
    }
    
    _isReading = !_isReading;
}


- (BOOL)startReading {
    NSError *error;
    
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    _captureSession = [[AVCaptureSession alloc] init];
    [_captureSession addInput:input];
    
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_camView.layer.bounds];
    [_camView.layer addSublayer:_videoPreviewLayer];
    
    [_captureSession startRunning];
    
    return YES;
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            [_infoLab performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            _isReading = NO;
            [self performSelectorOnMainThread:@selector(popBack:) withObject:[metadataObj stringValue] waitUntilDone:NO];
        }
    }
}

-(void)popBack:(NSString*)str{
    //Send notification
    //[[NSNotificationCenter defaultCenter]postNotificationName:@"scannerResult" object:nil userInfo:@{@"result":str}];
    //[self.navigationController popViewControllerAnimated:NO];
    
    if(methodLock == NO){
        methodLock= YES;
        scannedResult= str;
        [self performSegueWithIdentifier:@"camToBarcodeSegue" sender:self];
    }

    
}

-(void)stopReading{
    [_captureSession stopRunning];
    _captureSession = nil;
    
    [_videoPreviewLayer removeFromSuperlayer];
}

- (IBAction)myQRAction:(id)sender {
    scannedResult= @"No";
    [self performSegueWithIdentifier:@"camToBarcodeSegue" sender:self];
}


//Segue to message dialog
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"camToBarcodeSegue"]){
        self.barcodeViewVar= segue.destinationViewController;
        self.barcodeViewVar.scannedStr= scannedResult;
        return;
    }
}


@end
