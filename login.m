//
//  login.m
//  LookUp
//
//  Created by Divey Punj on 10/04/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "login.h"


ServerQueryBridge *ser;
UITextField *activeFieldForLogin;
NSString *entityId_log;
NSManagedObjectContext *context_log;
NSManagedObject *newUserObject_log;
AppDelegate *delegate_log;
NSFetchRequest *userDataRequest_log;
NSArray *userDataArray_log;
NSString *loadedUsername;

@implementation login
@synthesize usernameText, passwordText,logInView, loginScrollView, loginButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _loginView.hidden= YES;
    //Animate entrance
    double delay= 0.5;
    dispatch_time_t popTime= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self animateEntrance];
    });
    
    //Initialise Variables
    loginButton.enabled= YES;
    [loginButton setTitle:@"Login" forState:UIControlStateNormal];
    [loginButton setTitle:@"Logging in.." forState:UIControlStateDisabled];
    loadedUsername= @"";
    //GENERAL
    loginButton.layer.cornerRadius= 20;
    loginButton.clipsToBounds= YES;

    
    ser= [[ServerQueryBridge alloc]init];
    [ser initialiseBackend];
    //Initalise other functions
    //Tap gesture to remove keyboard
    UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeKeyboard:)];
    tap.cancelsTouchesInView= NO;
    [self.view addGestureRecognizer:tap];
    
    
    [self changeNavigationBarColor:[UIColor blackColor]];
    
    [self registerForKeyboardNotifications];

}

-(void)animateEntrance{
    [_loginView setAlpha:0];
    _loginView.hidden= NO;
    [UIView animateWithDuration:0.3 animations:^{
        [_loginView setAlpha:1];
        [self.view layoutIfNeeded];
    }];
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

- (IBAction)loginAction:(id)sender {
    if([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable){
        //Connection unavailable
        [self loginMessagePopupBox:@"No internet connection available. Please check your connection."];
    }
    else{
        if([usernameText.text isEqualToString:@""] || [passwordText.text isEqualToString:@""]){
            loginButton.enabled= YES;
            NSString* tempMessage= @"Text fields must not be left empty!";
            [self loginMessagePopupBox:tempMessage];
            
        }
        else{
            //Stop user interaction
            [self.view setUserInteractionEnabled:NO];
            //Disable login button and change its text
            loginButton.enabled= NO;
            //Create notification for logging in checks
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loginSuccessful:) name:@"loginSuccessful" object:nil];
            [ser loginUser:usernameText.text password:passwordText.text];
            
        }
    }
}

-(void) loginSuccessful: (NSNotification*)notification{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"loginSuccessful" object:nil];
    NSString *result= [[notification userInfo]objectForKey:@"result"];
    NSString *entityId = [[notification userInfo]objectForKey:@"userId"];
    loadedUsername= [[notification userInfo]objectForKey:@"username"];
    
    if([result isEqualToString:@"Yes"]){
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(profileLoadedByLogin:) name:@"profileLoadedByLogin" object:nil];
        //Load my profile from server
        [ser loadMyProfile:entityId notiName:@"profileLoadedByLogin"];
    }
    else{
        [self.view setUserInteractionEnabled:YES];
        [self loginMessagePopupBox:@"Login Failed! Username or password is incorrect."];
    }

    
}

-(void)profileLoadedByLogin:(NSNotification*)notification{
    //Notify root controller

    NSMutableDictionary *dict= [[NSMutableDictionary alloc]initWithDictionary:[[notification userInfo]objectForKey:@"userProfile"]];
    
    if([dict count]==0){
        //Error, profile not loaded
        [self loginMessagePopupBox:@"Unable to load your profile, please retry or contact Halo."];
    }
    else{
        //Save the entity id to the key chain
        [SAMKeychain setPassword:[dict objectForKey:@"entityId"]forService:@"entityId" account:@"haloStatus-entityId"];
        entityId_log = [dict objectForKey:@"entityId"];
        
        //Create new core data
        //Add new
        context_log = [self managedObjectContext];

        newUserObject_log= [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context_log];
        
        [newUserObject_log setValue:entityId_log forKey:@"entityId"];
        [newUserObject_log setValue:loadedUsername forKey:@"email"];
        [self saveData];

        //Initialise Core data object
        NSError *error;
        
        //Check if the user is not verified, then send them to the verification screen
        
        if([[dict objectForKey:@"verifiedUser"]isEqualToString:@"No"]){
            //Send to verification screen
            [self performSegueWithIdentifier:@"loginToEVSegue" sender:self];
        }
        else{
            //Verified User
            //Save the dict to device
            if([[dict objectForKey:@"verifiedUser"]isEqualToString:@"Maybe"]){
                [newUserObject_log setValue:@"Maybe" forKey:@"verifiedUser"];
            }
            else{
                [newUserObject_log setValue:@"Yes" forKey:@"verifiedUser"];
            }
            
            [newUserObject_log setValue:[dict objectForKey:@"name"] forKey:@"name"];
            [newUserObject_log setValue:[dict objectForKey:@"gender"] forKey:@"gender"];
            [newUserObject_log setValue:[dict objectForKey:@"status"] forKey:@"status"];
            [newUserObject_log setValue:[dict objectForKey:@"interests"] forKey:@"interest"];
            [newUserObject_log setValue:[dict objectForKey:@"userAge"] forKey:@"userAge"];
            [newUserObject_log setValue:[dict objectForKey:@"orientation"] forKey:@"orientation"];
            [newUserObject_log setValue:[dict objectForKey:@"otherUserOrientation"] forKey:@"otherUserOrientation"];
            [newUserObject_log setValue:[dict objectForKey:@"otherUserGender"] forKey:@"otherUserGender"];
            NSData *chatMateArray= [NSKeyedArchiver archivedDataWithRootObject: [dict objectForKey:@"messageMates"]];
            [newUserObject_log setValue:chatMateArray forKey:@"messageMates"];
            
            [newUserObject_log setValue:[dict objectForKey:@"insideLocation"] forKey:@"insideLocation"];
            [newUserObject_log setValue:[dict objectForKey:@"otherUserAgeFrom"] forKey:@"otherUserAgeFrom"];
            [newUserObject_log setValue:[dict objectForKey:@"otherUserAgeTo"] forKey:@"otherUserAgeTo"];
            
            [self saveData];
            
            //Get the user DP from server
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(userDPLoaded:) name:@"userDPLoaded" object:nil];
            [ser downloadLargeImageByName:[entityId_log stringByAppendingString:@"Small.png"] notificationName:@"userDPLoaded" keyName:@"dpLogin"];

        }
        
        
    }
}

//Segue to message dialog
/*-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"loginToEVSegue"]){
        
        self.emailVeriCont= segue.destinationViewController;
        self.emailVeriCont.userEmail= loadedUsername;
        
        return;
    }
}*/


-(void)userDPLoaded:(NSNotification*)notification{
    //Get image and save to device
    NSString* imageUrlStr= [[notification userInfo]objectForKey:@"dpLogin"];
    NSURL *url= [NSURL URLWithString:imageUrlStr];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *img= [UIImage imageWithData:data];
    
    //Save image to device
    [newUserObject_log setValue:UIImagePNGRepresentation(img) forKey:@"userDP"];
    [self saveData];
    
    //Allow user interaction
    [self.view setUserInteractionEnabled:YES];
    
    //Dismiss login screen
    //Post notification that login is completed
    [[NSNotificationCenter defaultCenter] postNotificationName:@"signUpToAddDel" object:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)loginCancelButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)loginMessagePopupBox: (NSString*)message{
    
    loginButton.enabled= YES;
    usernameText.text= @"";
    passwordText.text= @"";
    
    UIAlertController *alert= [UIAlertController alertControllerWithTitle:@"ERROR" message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* okButton= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)changeNavigationBarColor: (UIColor*)colour{

    NSArray *version= [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if([[version objectAtIndex:0] intValue] >= 7){
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = colour;
        self.navigationController.navigationBar.translucent= NO;
        
    }
    else{
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor= colour;
        self.navigationController.navigationBar.translucent= NO;
    }
}

//The next 4 methods are responsible to allow text view to be seen when keyboard is open
//*************************KEYBOARD METHODS******************************//

-(void) registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:self.view.window];
}

-(void) keyboardWasShown: (NSNotification *) aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGRect keyPadFrame=[[UIApplication sharedApplication].keyWindow convertRect:[[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue] fromView:self.view];
    CGSize kbSize =keyPadFrame.size;
    CGRect activeRect=[self.view convertRect:activeFieldForLogin.frame fromView:activeFieldForLogin.superview];
    CGRect aRect = self.view.bounds;
    aRect.size.height -= ((kbSize.height)+30);
    CGPoint origin =  activeRect.origin;
    origin.y -= loginScrollView.contentOffset.y;
    if (!CGRectContainsPoint(aRect, origin)) {
        CGPoint scrollPoint = CGPointMake(0.0,CGRectGetMaxY(activeRect)-(aRect.size.height));
        [loginScrollView setContentOffset:scrollPoint animated:YES];
    }
}

-(void) keyboardWillBeHidden :(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGRect keyPadFrame=[[UIApplication sharedApplication].keyWindow convertRect:[[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue] fromView:self.view];
    CGSize kbSize =keyPadFrame.size;
    CGRect activeRect=[self.view convertRect:activeFieldForLogin.frame fromView:activeFieldForLogin.superview];
    CGRect aRect = self.view.bounds;
    aRect.size.height += ((kbSize.height)+30);
    
    CGPoint origin =  activeRect.origin;
    origin.y += loginScrollView.contentOffset.y;
    CGPoint scrollPoint = CGPointMake(0.0,0.0);
    [loginScrollView setContentOffset:scrollPoint animated:YES];
}

-(void) textFieldDidBeginEditing: (UITextField *) textField{
    activeFieldForLogin= textField;
}

-(void) textFieldDidEndEditing: (UITextField *) textField{
    activeFieldForLogin= nil;
}

-(void) closeKeyboard: (UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
}

-(void)saveData{
    NSError *error;
    if(![context_log save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE for login");
    }
}

@end
