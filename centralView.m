//
//  centralView.m
//  LookUp
//
//  Created by Divey Punj on 17/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "centralView.h"


int dataSmoothCounter;
ServerQueryBridge *serverForCentral;
messageClass *messageClassForCentral;
peripheralView *peripheralViewForCentral;
NSManagedObjectContext *context_cent;
NSManagedObject *managedDataObject_cent;
AppDelegate *delegate_cent;
NSFetchRequest *userDataRequest_cent;
NSArray *userDataArray_cent;
NSString *entityId_cent;

@interface centralView ()

@end

@implementation centralView

-(void)initialiseCoredata:(NSString*)entityId{
    //Initialise Core data object
    entityId_cent = entityId;
    NSError *error;
    context_cent = [self managedObjectContext];
    userDataRequest_cent= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_cent.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_cent];
    userDataArray_cent = [context_cent executeFetchRequest:userDataRequest_cent error:&error];
    managedDataObject_cent= [userDataArray_cent objectAtIndex:0];
}

- (void)initialise {
    // Do any additional setup after loading the view.
    //_centralManager = [[CBCentralManager alloc]initWithDelegate:self queue:nil];
    _centralManager= [[CBCentralManager alloc]initWithDelegate:self queue:dispatch_get_main_queue() options:@{CBCentralManagerOptionShowPowerAlertKey: @(YES)}];
    
    _data= [[NSMutableData alloc]init];
    dataSmoothCounter=0;
    messageClassForCentral= [[messageClass alloc]init];
    peripheralViewForCentral= [[peripheralView alloc]init];

}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void)centralManagerDidUpdateState:(CBCentralManager *)central{
    
    if(central.state == CBCentralManagerStatePoweredOn){
        //Scan
        [_centralManager scanForPeripheralsWithServices: @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]] options:@{CBCentralManagerScanOptionAllowDuplicatesKey: @YES}];
    }
    else if(central.state == CBCentralManagerStatePoweredOff){
        
        NSLog(@"Central Manager state powered off. Check Bluetooth");
        //Send alert message
        NSLog(@"******NO***********");
    }
    else if(central.state == CBCentralManagerStateResetting){
        NSLog(@"Central Manager state resetting. Check Bluetooth");
        //Send alert message
        NSLog(@"******NO***********");
    }
    else if(central.state == CBCentralManagerStateUnknown){
        NSLog(@"Central Manager state unknown. Check Bluetooth");
        //Send alert message
        NSLog(@"******NO***********");
    }
    else if(central.state == CBCentralManagerStateUnsupported){
        NSLog(@"Central Manager state unsupported. Check Bluetooth");
        //Send alert message
        NSLog(@"******NO***********");
    }
    else{
        NSLog(@"Central Manager state unauthorised. Check Bluetooth");
        //Send alert message
        NSLog(@"******NO***********");
    }
    
}

-(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI{
    
    
    double distance=0;
    distance= pow(10, (2-[RSSI integerValue])/(10*2));
    
    if(distance <= 10.0){
        dataSmoothCounter= dataSmoothCounter + 1;
    }
    else{
        //Nothing
    }
    
    if(dataSmoothCounter >= 50){
        if(_discoveredPeripheral != peripheral){
            //Save a local copy of the peripheral so core bluetooth does not get rid of it
            _discoveredPeripheral= peripheral;
            //Connect
            [_centralManager connectPeripheral:peripheral options:nil];
            //Stop Scanning
            [self.centralManager stopScan];
        }
        dataSmoothCounter= 0;
    }

}

-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    NSLog(@"Failed to connect");
}

-(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral{
    
    
    NSLog(@"Did connect to peripheral");
    //_discoveredPeripheral= peripheral;
    [_data setLength:0];
        
    peripheral.delegate= self;
    [peripheral discoverServices:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]]];


}
-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error{
    NSLog(@"Did discover service");
    for(CBService *service in peripheral.services){
        [peripheral discoverCharacteristics:@[[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID],[CBUUID UUIDWithString:RECIEVER_CHARACTERISTIC_OTHER_UUID],[CBUUID UUIDWithString:RECIEVER_CHARACTERISTIC_ANSWER_UUID]] forService:service];
        
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:
    (NSError *)error{
    
    //NSLog(@"Did discover characteristics");
    
    for(CBCharacteristic *characteristic in service.characteristics){
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]]){
            //Read value from characteristics
            [peripheral readValueForCharacteristic:characteristic];
            
            //[peripheral setNotifyValue: YES forCharacteristic: characteristic];
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:RECIEVER_CHARACTERISTIC_OTHER_UUID]]){
            //Send back my id
            //Convert to NSData
            //NSDictionary *dict= @{@"name":@"Divey", @"userId":@"58675656876"};
            //_dataToSendToPeripheral= [NSKeyedArchiver archivedDataWithRootObject: dict];
            //_dataToSendToPeripheral= [entityId_cent dataUsingEncoding:NSUTF8StringEncoding];
            
            //Write to characteristic
            [peripheral writeValue:_dataToSendToPeripheral forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:RECIEVER_CHARACTERISTIC_ANSWER_UUID]]){
            _discoveredPeripheralCharacteristic= characteristic;
        }
        else{
            //Nothing
        }
        
    }
}
-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    if(error){
        NSLog(@"Error: %@",error);
        return;
    }
    
    NSString *stringFromData= [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
    
        BOOL contactExists= NO;
        
        //Read chat mate array
    
    NSMutableArray *tempArray;
  
    if([[managedDataObject_cent valueForKey:@"messageMates"]length]==0){
        //Nothing
    }
    else{
        tempArray= [[NSMutableArray alloc]initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:[managedDataObject_cent valueForKey:@"messageMates"]]];
        
        //Check that this contact does not exist already
        for(int i=0; i<tempArray.count; i++){
            
            
            if([[[tempArray objectAtIndex:i]objectForKey:@"chatMateId"] isEqualToString:stringFromData]){
                contactExists= YES;
                break;
            }
            else{
                contactExists= NO;
            }
        }
    }

        if(contactExists==NO){
            //Create a notification for friends request answer
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(friendNotiAnswer:) name:@"friendNotiAnswer" object:nil];
            
            //Post notification for UserProfile
            [[NSNotificationCenter defaultCenter]postNotificationName:@"tapIdReceived" object:self userInfo:@{@"tapUserId": stringFromData}];
            
        }
        else{
            NSLog(@"Mate already exists");
            //Create a notification for friends request answer
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(friendNotiAnswer:) name:@"friendNotiAnswer" object:nil];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"friendNotiAnswer" object:self userInfo:@{@"friendRequestAnswer": @"ALR"}];
            
            //Disconnect with Peripheral
            [_centralManager cancelPeripheralConnection:peripheral];
            
            //Restart scanning
            //[self initialise];
            [_centralManager scanForPeripheralsWithServices: @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]] options:@{CBCentralManagerScanOptionAllowDuplicatesKey: @YES}];
        }
}


-(void)friendNotiAnswer:(NSNotification*)notification{
    //Write back to characteristic
    
    NSString *answer= [[notification userInfo]objectForKey:@"friendRequestAnswer"];
    
    //Convert to NSData
    _dataToSendToPeripheral= [answer dataUsingEncoding:NSUTF8StringEncoding];
    
    //Write to characteristic
    [_discoveredPeripheral writeValue:_dataToSendToPeripheral forCharacteristic:_discoveredPeripheralCharacteristic type:CBCharacteristicWriteWithResponse];

    //Disconnect from peripheral
    [_centralManager cancelPeripheralConnection:_discoveredPeripheral];
    //Restart scanning
    //[self initialise];
    [_centralManager scanForPeripheralsWithServices: @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]] options:@{CBCentralManagerScanOptionAllowDuplicatesKey: @YES}];
}

-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    if(error){
        NSLog(@"Error: %@",error);
    }
    
}

-(void)stopScan{
    [_centralManager stopScan];
}

@end
