//
//  messageClass.m
//  LookUp
//
//  Created by Divey Punj on 13/04/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "messageClass.h"


ServerQueryBridge *serverForMates;
userSettings *sets;
NSMutableArray *chatMates, *filteredMates;
NSMutableDictionary *chatMateImages;
NSMutableDictionary *messageDict;
BOOL showRemoveButton, friendFilterOn;
UIRefreshControl *refreshMates;
NSManagedObjectContext *context_mc;
NSManagedObject *managedDataObject_mc;
AppDelegate *delegate_mc;
NSFetchRequest *userDataRequest_mc;
NSArray *userDataArray_mc;
NSString *entityId_mc, *username_mc, *currentMateId;

@implementation messageClass
@synthesize chatMatesTable, noMatesLabel, messageTableView, messageViewHolder, messageContactLoadIndicator, chatMateEditButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //*****DESIGN***************
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(NAVTABBARRED/255.0) green:(NAVTABBARGREEN/255.0) blue:(NAVTABBARBLUE/255.0) alpha:1.0]];
    [self.tabBarController.tabBar setBarTintColor:[UIColor colorWithRed:(NAVTABBARRED/255.0) green:(NAVTABBARGREEN/255.0) blue:(NAVTABBARBLUE/255.0) alpha:1.0]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self hideProgressView];
    
    //Get entityId and username from key chain
    entityId_mc = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    username_mc = [SAMKeychain passwordForService:@"username" account:@"haloStatus-username"];
    
    //Initialise Core data object
    NSError *error;
    context_mc = [self managedObjectContext];
    userDataRequest_mc= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_mc.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_mc];
    userDataArray_mc = [context_mc executeFetchRequest:userDataRequest_mc error:&error];
    managedDataObject_mc= [userDataArray_mc objectAtIndex:0];
    
    //Initialise classes
    serverForMates= [[ServerQueryBridge alloc]init];
    [serverForMates initialiseBackend];
    [serverForMates initialiseCoreData:entityId_mc];
    
    //Set up refresh control
    refreshMates = [[UIRefreshControl alloc]init];
    refreshMates.backgroundColor= [UIColor redColor];
    refreshMates.tintColor= [UIColor whiteColor];
    refreshMates.attributedTitle= [[NSAttributedString alloc]initWithString:@"Loading.."];
    [refreshMates addTarget:self action:@selector(refreshMatesArray) forControlEvents:UIControlEventValueChanged];
    [chatMatesTable addSubview:refreshMates];
    
    //General
    currentMateId= @"";
    friendFilterOn= YES;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(newMessageNoti:) name:@"newMessageNoti" object:nil];
    
    //Create notification for Logout
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(messageLogoutAction:) name:@"messageLogoutAction" object:nil];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
    currentMateId= @"";
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    
    //Set newMessage to No
    [managedDataObject_mc setValue:@"No" forKey:@"newMessage"];
    [self saveData];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    //TEST
    //[serverForMates connectUsers:@"98453h5j345283857544" myId:@"59290a3e07d049762ad5e077" myName:@"Divey" friendName:@"Halo" isHalo:@"NO"];
    
    //Enable the tab bar buttons
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:TRUE];
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:TRUE];
    
    [self.navigationController setNavigationBarHidden:NO];
    if([chatMates count]==0){
        //Load chat mates from server
        [self showIndicatorDuringLoad];
        [self getChatMatesFromDB];
    }
    else{
        //Reload
        //chatMatesTable.delegate= self;
        //chatMatesTable.dataSource= self;
        //self.chatMatesTable.allowsMultipleSelectionDuringEditing= NO;
        //[chatMatesTable reloadData];
        //Quietly reload chat mates from server
        [self getChatMatesFromDB];
    }
}

-(void)messageLogoutAction:(NSNotification*)notification{
    //Initialise everything
    chatMates= [[NSMutableArray alloc]init];
    
}

-(void)getChatMatesFromDB{
    
    //Create a notification
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(myFriendsListNoti:) name:@"myFriendsListNoti" object:nil];
    //Load Profile
    NSLog(@"Entity ID = %@",entityId_mc);
    [serverForMates getMyFriendsList:entityId_mc notiName:@"myFriendsListNoti"];
    
}

-(void)myFriendsListNoti:(NSNotification*) notification{
    
    NSString *response= [[notification userInfo]objectForKey:@"error"];
    
    if([response isEqualToString:@"Yes"]){
        //Error
    }
    else{
        chatMates= [[NSMutableArray alloc]initWithArray:[[notification userInfo]objectForKey:@"friendsList"]];
        
        //Get images from server
        [self getImagesForContacts];
        
        //Reload the table
        chatMatesTable.delegate= self;
        chatMatesTable.dataSource= self;
        [self friendFilterAction:0];
        
        [self removeIndicatorAfterLoad];
        [refreshMates endRefreshing];
    }
    
    /*
    NSMutableDictionary *dict= [[NSMutableDictionary alloc]initWithDictionary:[[notification userInfo]objectForKey:@"userProfile"]];
    
    if([dict count]==0){
        //Error, profile not loaded
        NSLog(@"Unable to load profile");
    }
    else{
        //Save the dict to device
        if([dict objectForKey:@"messageMates"] == nil || [[dict objectForKey:@"messageMates"]count]==0){
            //No mates, add Halo
            
            chatMates= [[NSMutableArray alloc]init];
            
            NSMutableDictionary *tempdict= [[NSMutableDictionary alloc]init];
            [tempdict setValue:@"Halo" forKey:@"chatMateName"];
            [tempdict setValue:HALO_FEEDBACK_ID forKey:@"chatMateId"];
            [tempdict setValue:@"Accepted" forKey:@"status"];
            [chatMates insertObject:tempdict atIndex:0];
            
            [managedDataObject_mc setValue:[NSKeyedArchiver archivedDataWithRootObject: chatMates] forKey:@"messageMates"];
            [self saveData];
            
            [serverForMates saveData:@"" entityId:entityId_mc];
            
            //Get images from server
            [self getImagesForContacts];
            
            //Reload the table
            chatMatesTable.delegate= self;
            chatMatesTable.dataSource= self;
            [self friendFilterAction:0];
        }
        else{
            chatMates = [[NSMutableArray alloc]initWithArray:[dict objectForKey:@"messageMates"]];
            NSLog(@"Mates : %@",chatMates);
            //Check if Halo exists, if not then add it, then save to server.
            if([[[chatMates objectAtIndex:0]objectForKey:@"chatMateId"]isEqualToString:HALO_FEEDBACK_ID]){
                [managedDataObject_mc setValue:[NSKeyedArchiver archivedDataWithRootObject: chatMates] forKey:@"messageMates"];
                [self saveData];
            }
            else{
                NSMutableDictionary *tempdict= [[NSMutableDictionary alloc]init];
                [tempdict setValue:@"Halo" forKey:@"chatMateName"];
                [tempdict setValue:HALO_FEEDBACK_ID forKey:@"chatMateId"];
                [tempdict setValue:@"Accepted" forKey:@"status"];
                [chatMates insertObject:tempdict atIndex:0];
                
                [managedDataObject_mc setValue:[NSKeyedArchiver archivedDataWithRootObject: chatMates] forKey:@"messageMates"];
                [self saveData];
                
                [serverForMates saveData:@"" entityId:entityId_mc];
            }
            
            //Get images from server
            [self getImagesForContacts];
            
            //Reload the table
            chatMatesTable.delegate= self;
            chatMatesTable.dataSource= self;
            [self friendFilterAction:0];
        }
        
    }
    [self removeIndicatorAfterLoad];
    [refreshMates endRefreshing];*/
}

-(void)refreshMatesArray{
    [self getChatMatesFromDB];
}

- (IBAction)chatMateRemoveAction:(id)sender {
    
    if(showRemoveButton){
        [chatMateEditButton setTitle:@"Edit" forState:UIControlStateNormal];
        showRemoveButton= NO;
        [chatMatesTable reloadData];
    }
    else{
        [chatMateEditButton setTitle:@"Done" forState:UIControlStateNormal];
        showRemoveButton= YES;
        [chatMatesTable reloadData];
    }
}

- (IBAction)testBackButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)requestFilterAction:(id)sender {
    
    friendFilterOn= NO;
    filteredMates= [[NSMutableArray alloc]init];
    
    for(int i=0; i<[chatMates count]; i++){
        if([[[chatMates objectAtIndex:i]objectForKey:@"friendshipStatus"]isEqualToString:@"Awaiting"]||[[[chatMates objectAtIndex:i]objectForKey:@"friendshipStatus"]isEqualToString:@"Pending"]){
            [filteredMates addObject:[chatMates objectAtIndex:i]];
        }
    }
    
    [_requestsBut setTitle:[NSString stringWithFormat:@"Requests(%lu)",(unsigned long)[filteredMates count]] forState:UIControlStateNormal];
    
    [chatMatesTable reloadData];
    
}

- (IBAction)friendFilterAction:(id)sender {
    friendFilterOn= YES;
    filteredMates= [[NSMutableArray alloc]init];
    
    for(int i=0; i<[chatMates count]; i++){
        if([[[chatMates objectAtIndex:i]objectForKey:@"friendshipStatus"]isEqualToString:@"Accepted"]){
            [filteredMates addObject:[chatMates objectAtIndex:i]];
        }
    }
    
    if(([chatMates count]-[filteredMates count])>=0){
        int count= [chatMates count]-[filteredMates count];
        
        [_requestsBut setTitle:[NSString stringWithFormat:@"Requests(%lu)",(unsigned long)count] forState:UIControlStateNormal];
    }
    
    [chatMatesTable reloadData];
}


/*//USE THIS METHOD FOR ADDING MATES
 [self addMates:@"Test1"];
 [self addMates:@"Test2"];
 [self addMates:@"Test3"];
 [self addMates:@"Test4"];
 [self addMates:@"Test5"];
 [self addMates:@"Test6"];
 [self addMates:@"Test7"];
 [self addMates:@"Test8"];
 [self addMates:@"Test9"];
 [self addMates:@"Test10"];*/



//Segue to message dialog
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"openMessageDialog"]){
        
        NSInteger chatMateIndex= [[self.chatMatesTable indexPathForCell:(UITableViewCell *)sender]row];
        NSMutableDictionary *tempDict= [chatMates objectAtIndex:chatMateIndex];
        self.activeMessageDialog= segue.destinationViewController;
        self.activeMessageDialog.otherUserId= [tempDict objectForKey:@"friendId"];
        self.activeMessageDialog.otherUserName= [tempDict objectForKey:@"friendName"];
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[chatMateImages objectForKey:[[tempDict objectForKey:@"friendId"]stringByAppendingString:@"Small.png"]]]];
        UIImage *result = [UIImage imageWithData:data];
        self.activeMessageDialog.otherUserImage= result;
        currentMateId= [tempDict objectForKey:@"friendId"];
        
        return;
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    NSInteger chatMateIndex= [[self.chatMatesTable indexPathForCell:(UITableViewCell *)sender]row];
    NSMutableDictionary *tempDict= [chatMates objectAtIndex:chatMateIndex];
    
    if([identifier isEqualToString:@"openMessageDialog"] && friendFilterOn)
    {
        //Segue
        return YES;
    }
    else{
        //Dont Segue
        return NO;
    }
}

-(void)showIndicatorDuringLoad{
    //Hide table view
    messageTableView.hidden= YES;
    //Start the indicator
    messageContactLoadIndicator.hidden= NO;
    [messageContactLoadIndicator startAnimating];
    noMatesLabel.text= @"Loading your chats..";
}

-(void)removeIndicatorAfterLoad{
    //Fade in the message contacts
    //[messageTableView setAlpha:0.0];
    messageTableView.hidden= NO;
    [messageViewHolder addSubview:messageTableView];
    //Stop the indicator and hide message
    [messageContactLoadIndicator stopAnimating];
    messageContactLoadIndicator.hidden= YES;
    noMatesLabel.text= @"";
}
-(void)noMatesAfterLoad{
    [messageContactLoadIndicator stopAnimating];
    messageContactLoadIndicator.hidden= YES;
    noMatesLabel.text= @"You don't have any mates in your contacts..";
}

//******************************TABLE VIEW METHODS**********************************
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(filteredMates.count==0){
        return 0;
    }
    else{
        return filteredMates.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    chatMatesCustomCell *cell= [tableView dequeueReusableCellWithIdentifier:@"ChatMateCell"];
    
    if(cell == nil){
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: @"ChatMateCell"];
    }
    
    //Add info array to the server data class and access each entity from there
    NSMutableDictionary *chatMateDict;
    if(chatMates.count==0){
        //Do nothing
    }
    else{
        chatMateDict= [[NSMutableDictionary alloc]initWithDictionary:[filteredMates objectAtIndex:indexPath.row]];
    }
    NSString *otherUserName= [chatMateDict objectForKey:@"friendName"];
    
    
    //Get messages from core data and save to messages
    NSMutableArray *tempArray= [[NSMutableArray alloc]initWithArray:[self getMessagesFromDevice:[chatMateDict objectForKey:@"friendId"]]];
    NSString *prevMessage;
    if([tempArray count]==0){
        prevMessage= @"";
    }
    else{
        prevMessage= [[tempArray lastObject]objectForKey:@"text"];
    }
    
    //Create a notification for receiving remove notifications from cell
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(removeChatMateNoti:) name:@"removeChatMateNoti" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(acceptFriendRequest:) name:@"acceptFriendRequest" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(rejectFriendRequest:) name:@"rejectFriendRequest" object:nil];
    
    if([[chatMateDict objectForKey:@"friendName"]isEqualToString:@"Halo Status"]){
        
            [cell setCell:otherUserName imageUrl:[chatMateImages objectForKey:[[chatMateDict objectForKey:@"friendId"]stringByAppendingString:@"Small.png"]] entityId:[chatMateDict objectForKey:@"friendId"] prevMessage:prevMessage showRemove:NO indexRow:indexPath.row isNewMessage:[self isNewMessage:[chatMateDict objectForKey:@"friendId"]] mateStatus:[chatMateDict objectForKey:@"friendshipStatus"]];
    }
    else{
            [cell setCell:otherUserName imageUrl:[chatMateImages objectForKey:[[chatMateDict objectForKey:@"friendId"]stringByAppendingString:@"Small.png"]] entityId:[chatMateDict objectForKey:@"friendId"] prevMessage:prevMessage showRemove:showRemoveButton indexRow:indexPath.row isNewMessage:[self isNewMessage:[chatMateDict objectForKey:@"friendId"]] mateStatus:[chatMateDict objectForKey:@"friendshipStatus"]];
    }
    

    
    return cell;
}

-(void)removeChatMateNoti:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"removeChatMateNoti" object:nil];
    int indexRow= [[[notification userInfo]objectForKey:@"indexRow"]integerValue];
    
    [self deleteContactConfirm:indexRow];
    //[self deleteChatMate:indexRow];
    
    
}

-(void)acceptFriendRequest:(NSNotification*)notification{
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"acceptFriendRequest" object:nil];
    
    int indexRow= [[[notification userInfo]objectForKey:@"indexRow"]integerValue];
    
    if(friendFilterOn){
        //Nothing
    }
    else{
        NSMutableDictionary *tempDict;
        for(int i=0; i<[chatMates count];i++){
            if([[[chatMates objectAtIndex:i]objectForKey:@"friendId"]isEqualToString:[[filteredMates objectAtIndex:indexRow]objectForKey:@"friendId"]]){
                //Load the dict from array and update its value
                tempDict= [[NSMutableDictionary alloc]initWithDictionary:[chatMates objectAtIndex:i]];
                break;
            }
        }
        
        //Restart notification
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(acceptFriendRequest:) name:@"acceptFriendRequest" object:nil];
        
        //Start progress view
        [self showProgressView];
        //Ask server to update Friend's status
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(friendRequestNoti:) name:@"friendRequestNoti" object:nil];
        [serverForMates friendRequestResponse:entityId_mc friendId:[tempDict objectForKey:@"friendId"] myResponse:@"Accepted"];
        //[serverForMates friendRequestResponse:@"5922e6d3246437c37579a2f2" friendId:@"59290a3e07d049762ad5e077" myResponse:@"Accepted"];
    }
}

-(void)rejectFriendRequest:(NSNotification*)notification{
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"rejectFriendRequest" object:nil];
    
    int indexRow= [[[notification userInfo]objectForKey:@"indexRow"]integerValue];
    
    if(friendFilterOn){
        //Nothing
    }
    else{
        NSMutableDictionary *tempDict;
        for(int i=0; i<[chatMates count];i++){
            if([[[chatMates objectAtIndex:i]objectForKey:@"friendId"]isEqualToString:[[filteredMates objectAtIndex:indexRow]objectForKey:@"friendId"]]){
                tempDict= [[NSMutableDictionary alloc]initWithDictionary:[chatMates objectAtIndex:i]];
                break;
            }
        }
        
        //Restart notification
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(rejectFriendRequest:) name:@"rejectFriendRequest" object:nil];
        
        //Start progress view
        [self showProgressView];
        //Ask server to update Friend's status
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(friendRequestNoti:) name:@"friendRequestNoti" object:nil];
        //Ask server to reject the Friend Request
         [serverForMates friendRequestResponse:entityId_mc friendId:[tempDict objectForKey:@"friendId"] myResponse:@"Rejected"];
    }
}

-(void)friendRequestNoti:(NSNotification*)notification{
    NSString *response = [[notification userInfo]objectForKey:@"error"];

    [self hideProgressView];
    
    if([response isEqualToString:@"Yes"]){
        //Error
        [self popMessage:@"Error in performing request, please try again or contact us." title:@"Error"];
    }
    else{
        //Noting
        [self getChatMatesFromDB];
    }
}

-(BOOL)tableView:(UITableView*)tableView canEditRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if(indexPath.row==0){
        return NO;
    }
    else{
        return NO;
    }
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
       // [self deleteChatMate:indexPath.row];
    }
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

-(void)getImagesForContacts {
    
    //Convert Ids into names
    NSMutableArray *namesArray= [[NSMutableArray alloc]init];
    for(int i=0; i<chatMates.count; i++){
        NSMutableDictionary *tempDict= [[NSMutableDictionary alloc]initWithDictionary:[chatMates objectAtIndex:i]];
        
        [namesArray addObject:[[tempDict objectForKey:@"friendId"]stringByAppendingString:@"Small.png"]];
    }
    
    //Create a notification
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(chatMatesImagesLoaded:) name:@"chatMatesImagesLoaded" object:nil];
    //Send to server
    [serverForMates downloadSmallImageByNames:namesArray notiName:@"chatMatesImagesLoaded" keyName:@"chatMateImages"];
    
}

-(void)chatMatesImagesLoaded:(NSNotification*)notification {
    chatMateImages = [[NSMutableDictionary alloc]initWithDictionary:[[notification userInfo]objectForKey:@"chatMateImages"]];
    [chatMatesTable reloadData];
}

-(void)deleteChatMate:(NSInteger)index{


            NSMutableDictionary *tempDict;
            for(int i=0; i<[chatMates count];i++){
                if([[[chatMates objectAtIndex:i]objectForKey:@"friendId"]isEqualToString:[[filteredMates objectAtIndex:index]objectForKey:@"friendId"]]){
                    tempDict= [[NSMutableDictionary alloc]initWithDictionary:[chatMates objectAtIndex:i]];
                    break;
                }
            }
            
            //Restart notification
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(removeChatMateNoti:) name:@"removeChatMateNoti" object:nil];
            
            //Start progress view
            [self showProgressView];
            //Ask server to update Friend's status
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(removeExistingFriendNoti:) name:@"removeExistingFriendNoti" object:nil];
            //Ask server to remove the friend
            [serverForMates removeExistingFriend:[tempDict objectForKey:@"friendId"] myId:entityId_mc];

    
}

-(void)removeExistingFriendNoti:(NSNotification*)notification{
    
     NSString *response = [[notification userInfo]objectForKey:@"error"];
    
    [self hideProgressView];
    
    if([response isEqualToString:@"Yes"]){
        //Error
        [self popMessage:@"Error in performing request, please try again or contact us." title:@"Error"];
    }
    else{
        //Noting
        [self getChatMatesFromDB];
    }

}

- (IBAction)finderActionInMessage:(id)sender{
    //if([[userInfoForMessages getCheckinInfo]isEqualToString:@"Yes"]){
    [self.navigationController popViewControllerAnimated:YES];
    //}
}

-(void)saveData{
    NSError *error;
    if(![context_mc save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
    }
}

-(NSMutableArray*)getMessagesFromDevice: (NSString*)chatMateId{
    
    NSError *error;
    
    
    NSFetchRequest *request= [[NSFetchRequest alloc]initWithEntityName:@"Friends"];
    request.predicate = [NSPredicate predicateWithFormat:@"chatMateId == %@", chatMateId];
    request.sortDescriptors= @[[NSSortDescriptor sortDescriptorWithKey:@"chatMateId" ascending:YES]];
    NSArray *results= [context_mc executeFetchRequest:request error:&error];
    
    if([results count]==0){
        return nil;
    }
    else{
        NSManagedObject *readObject= [results objectAtIndex:0];
        //NSMutableDictionary *tempDict= [[NSMutableDictionary alloc]initWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithData:[readObject valueForKey:@"chatDict"]]];
        
        NSMutableArray *tempArray= [[NSMutableArray alloc]initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:[readObject valueForKey:@"messageDict"]]];
        
        return tempArray;
    }
}

-(BOOL)isNewMessage:(NSString*)chatMateId{
    
    NSError *error;
    
    
    NSFetchRequest *request= [[NSFetchRequest alloc]initWithEntityName:@"Friends"];
    request.predicate = [NSPredicate predicateWithFormat:@"chatMateId == %@", chatMateId];
    request.sortDescriptors= @[[NSSortDescriptor sortDescriptorWithKey:@"chatMateId" ascending:YES]];
    NSArray *results= [context_mc executeFetchRequest:request error:&error];
    
    if([results count]==0){
        return NO;
    }
    else{
        NSManagedObject *readObject= [results objectAtIndex:0];
        NSString* newMessage = [readObject valueForKey:@"newMessage"];
        
        if([newMessage isEqualToString:@"Yes"]){
            return YES;
        }
        else{
            return NO;
        }
    }
}

-(void)newMessageNoti:(NSNotification*)notification{
    [chatMatesTable reloadData];
}

-(void)showProgressView{
    _progressViewIndicator.hidden= NO;
    _progressViewLab.hidden= NO;
    _progressView.hidden= NO;
}

-(void)hideProgressView{
    _progressViewIndicator.hidden= YES;
    _progressViewLab.hidden= YES;
    _progressView.hidden= YES;
}

-(void)popMessage:(NSString*)message title:(NSString*)title{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)deleteContactConfirm: (NSInteger)index{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Are you sure?"
                                 message:@"Once you remove a friend, you will have to reconnect in person in order to message them again."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                               actionWithTitle:@"Yes"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   [self deleteChatMate:index];
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                actionWithTitle:@"Cancel"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    
    [alert addAction:yesButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


@end
