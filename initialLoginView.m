//
//  initialLoginView.m
//  LookUp
//
//  Created by Divey Punj on 14/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "initialLoginView.h"
NSString *email_fb, *name_fb, *entityId_fbAccess;
UIImage* userDP_fb;
ServerQueryBridge *server_fbAccess;
NSManagedObjectContext *context_fbAccess;
NSManagedObject *newUserDataObj_fbAccess;
AppDelegate *delegate_fbAccess;
NSFetchRequest *userDataRequest_fbAccess;
NSArray *userDataArray_fbAccess;
BOOL fbProfSaved;

@interface initialLoginView ()

@end

@implementation initialLoginView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _fbButton.hidden= YES;
    _loginButton.hidden= YES;
    _signupButton.hidden= YES;
    
    //OTHER VARIABLES
    name_fb= @"";
    email_fb= @"";
    userDP_fb= nil;
    fbProfSaved= NO;
    server_fbAccess= [[ServerQueryBridge alloc]init];
    [server_fbAccess initialiseBackend];
    
    //GENERAL
    _fbButton.layer.cornerRadius= 20;
    _fbButton.clipsToBounds= YES;
    _loginButton.layer.cornerRadius= 20;
    _loginButton.clipsToBounds= YES;
    _signupButton.layer.cornerRadius= 20;
    _signupButton.clipsToBounds= YES;
    [self hideLoadView];
    
    [self startShow];
    
    //Ask user for their status
    double delayBut= 0.5;
    dispatch_time_t popTimeBut= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayBut * NSEC_PER_SEC));
    dispatch_after(popTimeBut, dispatch_get_main_queue(), ^(void){
        [self buttonAnimation];
    });

    //Create error notification for server
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(serverError:) name:@"serverError" object:nil];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];

    if(fbProfSaved){
        fbProfSaved= NO;
        //Show tutorial from here
//[[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(tutorialComplete:) name:@"fbAccessToTut" object:nil];
        [self performSegueWithIdentifier:@"fbAccessToTut" sender:self];
    }
}

-(void)startShow{
    //SHOW
    _beforeLab.hidden= YES;
    _img_man.hidden= YES;
    _img_wom1.hidden= YES;
    _img_wom2.hidden= YES;
    _img_wom3.hidden= YES;
    _img_bubble.hidden= YES;
    _afterInfo.hidden= YES;
    _showApp.hidden= YES;
    _replayShowBut.hidden= YES;
    _beforeLab.text= @"Before Halo";
    [_img_bubble setImage:[UIImage imageNamed:@"show_bubble_1.png"]];
    [_img_wom1 setImage:[UIImage imageNamed:@"show_woman.png"]];
    [_img_wom2 setImage:[UIImage imageNamed:@"show_woman.png"]];
    [_img_wom3 setImage:[UIImage imageNamed:@"show_woman.png"]];
    
    //double delay= 1.0;
    //dispatch_time_t popTime= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    //dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
       // [self showBeforeAnimation];
    //});

}

-(void)buttonAnimation{
    [_fbButton setAlpha:0];
    [_loginButton setAlpha:0];
    [_signupButton setAlpha:0];
    
    _fbButton.hidden= NO;
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
        [_fbButton setAlpha:1];
        [self.view layoutIfNeeded];
    } completion:nil];
    _loginButton.hidden= NO;
    [UIView animateWithDuration:0.7 delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
        [_loginButton setAlpha:1];
        [self.view layoutIfNeeded];
    } completion:nil];
    _signupButton.hidden= NO;
    [UIView animateWithDuration:0.9 delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
        [_signupButton setAlpha:1];
        [self.view layoutIfNeeded];
    } completion:nil];
    
}

-(void)showBeforeAnimation{
    
    //Show label
    
    [_beforeLab setAlpha:0];
    _beforeLab.hidden= NO;
    [UIView animateWithDuration:0.2 animations:^{
        [_beforeLab setAlpha:1];
        [self.view layoutIfNeeded];
    }];
    
    double delay= 1.0;
    dispatch_time_t popTime= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //Show man
        [_img_man setAlpha:0];
        _img_man.hidden= NO;
        [UIView animateWithDuration:0.2 animations:^{
            [_img_man setAlpha:1];
            [self.view layoutIfNeeded];
        }];
    });
    
    double delay1= 2.0;
    dispatch_time_t popTime1= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay1 * NSEC_PER_SEC));
    dispatch_after(popTime1, dispatch_get_main_queue(), ^(void){
        //Show all woman
        [_img_wom1 setAlpha:0];
        [_img_wom2 setAlpha:0];
        [_img_wom3 setAlpha:0];
        _img_wom1.hidden= NO;
        _img_wom2.hidden= NO;
        _img_wom3.hidden= NO;
        [UIView animateWithDuration:0.2 animations:^{
            [_img_wom1 setAlpha:1];
            [_img_wom2 setAlpha:1];
            [_img_wom3 setAlpha:1];
            [self.view layoutIfNeeded];
        }];

    });
    
    double delay2= 3.0;
    dispatch_time_t popTime2= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay2 * NSEC_PER_SEC));
    dispatch_after(popTime2, dispatch_get_main_queue(), ^(void){
        //Show man
        [_img_bubble setAlpha:0];
        _img_bubble.hidden= NO;
        [UIView animateWithDuration:0.2 animations:^{
            [_img_bubble setAlpha:1];
            [self.view layoutIfNeeded];
        }];
    });
    double delay3= 4.0;
    dispatch_time_t popTime3= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay3 * NSEC_PER_SEC));
    dispatch_after(popTime3, dispatch_get_main_queue(), ^(void){
        //Show man
        [_img_bubble setImage:[UIImage imageNamed:@"show_bubble_2.png"]];
    });
    double delay4= 5.0;
    dispatch_time_t popTime4= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay4 * NSEC_PER_SEC));
    dispatch_after(popTime4, dispatch_get_main_queue(), ^(void){
        //Show man
        [_img_bubble setImage:[UIImage imageNamed:@"show_bubble_3.png"]];
    });
    double delay5= 6.0;
    dispatch_time_t popTime5= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay5 * NSEC_PER_SEC));
    dispatch_after(popTime5, dispatch_get_main_queue(), ^(void){
        //Show man
        [_img_bubble setImage:[UIImage imageNamed:@"show_bubble_4.png"]];
    });
    
    //Show After halo animation
    double delay6= 7.0;
    dispatch_time_t popTime6= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay6 * NSEC_PER_SEC));
    dispatch_after(popTime6, dispatch_get_main_queue(), ^(void){
        _beforeLab.text= @"But Now";
        _img_bubble.hidden= YES;
    });
    
    //Show After halo animation
    double delay7= 9.0;
    dispatch_time_t popTime7= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay7 * NSEC_PER_SEC));
    dispatch_after(popTime7, dispatch_get_main_queue(), ^(void){
        _afterInfo.hidden= NO;
        _afterInfo.text= @"You know who is who around you, with Halo!";
        _showApp.hidden= NO;
    });
    
    double delay8= 10.0;
    dispatch_time_t popTime8= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay8 * NSEC_PER_SEC));
    dispatch_after(popTime8, dispatch_get_main_queue(), ^(void){
        [_img_wom1 setImage:[UIImage imageNamed:@"show_wom_green.png"]];
        [_img_wom2 setImage:[UIImage imageNamed:@"show_wom_orange.png"]];
        [_img_wom3 setImage:[UIImage imageNamed:@"show_wom_red.png"]];
    });
    
    double delay9= 12.0;
    dispatch_time_t popTime9= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay9 * NSEC_PER_SEC));
    dispatch_after(popTime9, dispatch_get_main_queue(), ^(void){
        _replayShowBut.hidden= NO;
    });
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)facebookLogin:(id)sender {
    
    if([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable){
        //Connection unavailable
        [self errorPopUp:@"No internet connection available. Please check your connection."];
    }
    else{

        FBSDKLoginManager *login = [[FBSDKLoginManager alloc]init];
        [login logInWithReadPermissions:@[@"public_profile", @"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if(error){
                NSLog(@"Error: %@", error);
                [self errorPopUp:@"Error Signing Up using Facebook. Please try again."];
            }
            else if (result.isCancelled){
                NSLog(@"Cancelled");
            }
            else{
                NSLog(@"Logged in");
                //Show load view
                //[self showLoadView];
                //Stop user interaction
                [self.view setUserInteractionEnabled:NO];
                //Show load view
                [self showLoadView];
                //Get user info
                [self getFacebookData];
            }
        }];
    }
}

-(void)getFacebookData{
    NSMutableDictionary *parameters= [NSMutableDictionary dictionary];
    [parameters setValue:@"id, name, email" forKey:@"fields"];
    
    [[[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:parameters]startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if(!error){
            
            //Email cannot be empty
            if([result[@"email"]isEqualToString:@""] || result[@"email"]==nil){
                [self errorPopUp:@"We need your email address to Sign In. Please use the manual Sign up option."];
            }
            else{
                [self getUserFacebookPic:result[@"id"] name:result[@"name"] email:result[@"email"]];
            }

        }
        else{
            [self errorPopUp:@"Error signing in with Facebook, please try again."];
        }
    }];
}

-(void)getUserFacebookPic:(NSString*)userId name:(NSString*)name email:(NSString*)email{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",userId]];
    NSData *data= [NSData dataWithContentsOfURL:url];
    
    //Save image, name and email data to device
    name_fb = name;
    email_fb= email;
    userDP_fb= [UIImage imageWithData:data];
    
    //Create user
    NSMutableDictionary *faceDict= [[NSMutableDictionary alloc]init];
    [faceDict setValue:name forKey:@"name"];
    [faceDict setValue:email forKey:@"email"];
    
    if([FBSDKAccessToken currentAccessToken]) {
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(fbUserSignedIn:) name:@"fbUserSignedIn" object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(fbUserLoggedIn:) name:@"fbUserLoggedIn" object:nil];
        
        [server_fbAccess createUserUsingFacebook:faceDict accessToken:[FBSDKAccessToken currentAccessToken].tokenString];
    }
    else{
        [self errorPopUp:@"Facebook token expired. Please contact us."];
    }
    
}


-(void)fbUserSignedIn:(NSNotification*)notification{
    
    NSString* isDone= [[notification userInfo]objectForKey:@"message"];
    
    if([isDone isEqualToString:@"isDone"]){
        
        //Save username and entity id in KeyChain
        [SAMKeychain setPassword:email_fb forService:@"username" account:@"haloStatus-username"];
        [SAMKeychain setPassword:[[notification userInfo]objectForKey:@"entityId"] forService:@"entityId" account:@"haloStatus-entityId"];
        
        entityId_fbAccess = [[notification userInfo]objectForKey:@"entityId"];
        
        //Using this entity id, create a new user in core data
        context_fbAccess = [self managedObjectContext];
        //Add new
        newUserDataObj_fbAccess= [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context_fbAccess];
        [newUserDataObj_fbAccess setValue:[[notification userInfo]objectForKey:@"entityId"] forKey:@"entityId"];
        [newUserDataObj_fbAccess setValue:email_fb forKey:@"email"];
        [newUserDataObj_fbAccess setValue:@"Yes" forKey:@"verifiedUser"];
        
        //Save user email to User
        [server_fbAccess addAdditionalUserInfo:entityId_fbAccess email:email_fb];
        
        //Connect user with Halo
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSString* dateString = [dateFormatter stringFromDate:[NSDate date]];
        //Create a notification to wait for the Halo connection
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(haloConnectedByFB:) name:@"haloConnectedByFB" object:nil];
        [server_fbAccess connectUsers:HALO_FEEDBACK_ID myId:entityId_fbAccess myName:email_fb friendName:@"Halo Status" friendStatus:@"" venueName:@"" timeStamp:dateString isHalo:@"YES" myStatus:@"" notiName:@"haloConnectedByFB"];
        
        
        if(name_fb){
            [newUserDataObj_fbAccess setValue:name_fb forKey:@"name"];
        }
        else{
            
        }
        
        if(userDP_fb){
            [newUserDataObj_fbAccess setValue:UIImagePNGRepresentation(userDP_fb) forKey:@"userDP"];
            //[self uploadDP];
        }
        else{
            
        }
        
        [self saveData];
        
        
    }
    else{
        //Dont Segue
        [self errorPopUp:@"Unable to log into Facebook. Please contact us."];
    }

}

-(void)haloConnectedByFB:(NSNotification*)notification{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(fbAccessSignUpCompleted:) name:@"fbAccessSignUpCompleted" object:nil];
    [self performSegueWithIdentifier:@"fbAccessToProfSet" sender:self];
}


-(void)fbAccessSignUpCompleted:(NSNotification*)notification{
    fbProfSaved= YES;

}

-(void)fbUserLoggedIn:(NSNotification*)notification{
    
    NSMutableDictionary *dict= [[NSMutableDictionary alloc]initWithDictionary:[[notification userInfo]objectForKey:@"userData"]];
    
    if([dict count]==0){
        //Error, profile not loaded
        [self errorPopUp:@"Unable to load your profile, please retry or contact Halo."];
    }
    else{
        //Save the entity id to the key chain
        [SAMKeychain setPassword:[dict objectForKey:@"entityId"]forService:@"entityId" account:@"haloStatus-entityId"];
        entityId_fbAccess = [dict objectForKey:@"entityId"];
        
        //Create new core data
        //Add new
        context_fbAccess = [self managedObjectContext];
        
        newUserDataObj_fbAccess= [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context_fbAccess];
        
        [newUserDataObj_fbAccess setValue:entityId_fbAccess forKey:@"entityId"];
        [newUserDataObj_fbAccess setValue:@"Yes" forKey:@"verifiedUser"];
        [self saveData];
        
        //Initialise Core data object
        NSError *error;
        
        //Save the dict to device
        [newUserDataObj_fbAccess setValue:[dict objectForKey:@"name"] forKey:@"name"];
        [newUserDataObj_fbAccess setValue:email_fb forKey:@"email"];
        [newUserDataObj_fbAccess setValue:[dict objectForKey:@"gender"] forKey:@"gender"];
        [newUserDataObj_fbAccess setValue:[dict objectForKey:@"status"] forKey:@"status"];
        [newUserDataObj_fbAccess setValue:[dict objectForKey:@"interests"] forKey:@"interest"];
        [newUserDataObj_fbAccess setValue:[dict objectForKey:@"userAge"] forKey:@"userAge"];
        [newUserDataObj_fbAccess setValue:[dict objectForKey:@"orientation"] forKey:@"orientation"];
        [newUserDataObj_fbAccess setValue:[dict objectForKey:@"otherUserOrientation"] forKey:@"otherUserOrientation"];
        [newUserDataObj_fbAccess setValue:[dict objectForKey:@"otherUserGender"] forKey:@"otherUserGender"];
        NSData *chatMateArray= [NSKeyedArchiver archivedDataWithRootObject: [dict objectForKey:@"messageMates"]];
        [newUserDataObj_fbAccess setValue:chatMateArray forKey:@"messageMates"];
        [newUserDataObj_fbAccess setValue:[dict objectForKey:@"insideLocation"] forKey:@"insideLocation"];
        [newUserDataObj_fbAccess setValue:[dict objectForKey:@"otherUserAgeFrom"] forKey:@"otherUserAgeFrom"];
        [newUserDataObj_fbAccess setValue:[dict objectForKey:@"otherUserAgeTo"] forKey:@"otherUserAgeTo"];
        
        [self saveData];
        
        //Get the user DP from server
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(userDPLoaded:) name:@"userDPLoaded" object:nil];
        [server_fbAccess downloadLargeImageByName:[entityId_fbAccess stringByAppendingString:@"Small.png"] notificationName:@"userDPLoaded" keyName:@"dpLogin"];
        
    }
}

-(void)userDPLoaded:(NSNotification*)notification{
    //Get image and save to device
    NSString* imageUrlStr= [[notification userInfo]objectForKey:@"dpLogin"];
    NSURL *url= [NSURL URLWithString:imageUrlStr];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *img= [UIImage imageWithData:data];
    
    //Save image to device
    [newUserDataObj_fbAccess setValue:UIImagePNGRepresentation(img) forKey:@"userDP"];
    [self saveData];
    
    //Allow user interaction
    [self.view setUserInteractionEnabled:YES];
    
    //Dismiss login screen
    //Post notification that login is completed
    [[NSNotificationCenter defaultCenter] postNotificationName:@"signUpToAddDel" object:self];
    
}

- (IBAction)loginAction:(id)sender {
}

- (IBAction)signupAction:(id)sender {
}

- (IBAction)replayShowAction:(id)sender {
    [self startShow];
}

-(void)errorPopUp: (NSString*)message{
    
    UIAlertController *alert= [UIAlertController alertControllerWithTitle:@"ERROR" message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* okButton= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void)saveData{
    NSError *error;
    if(![context_fbAccess save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
    }
}

-(void)showLoadView{
    _loadingView.hidden= NO;
    _loadText.hidden= NO;
    [_loadingIndicator startAnimating];
}

-(void)hideLoadView{
    _loadingView.hidden= YES;
    [_loadingIndicator stopAnimating];
    _loadText.hidden= YES;
}

-(void)serverError:(NSNotification*)notification{
    NSString *error= [[notification userInfo]objectForKey:@"error"];
    
    [self errorPopUp:error];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"fbAccessToProfSet"]){
        userSettings *setView= [segue destinationViewController];
        setView.fromView= @"facebook";
    }
}

@end
