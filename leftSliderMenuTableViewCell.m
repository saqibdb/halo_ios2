//
//  leftSliderMenuTableViewCell.m
//  LookUp
//
//  Created by Divey Punj on 2/09/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "leftSliderMenuTableViewCell.h"

@implementation leftSliderMenuTableViewCell
@synthesize buttonName;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCell:(NSString*)locButtonName img:(UIImage*)img{
    buttonName.text= locButtonName;
    _optionIconImageView.image= img;
}



@end
