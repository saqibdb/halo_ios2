//
//  checkedInImg.h
//  LookUp
//
//  Created by Divey Punj on 9/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface checkedInImg : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *chkImg;
@property (strong, nonatomic) IBOutlet UIButton *yeButton;
@property (strong, nonatomic) IBOutlet UIButton *continueButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *yesHeightConst;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imgHeightConst;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contHeightConst;




@end
