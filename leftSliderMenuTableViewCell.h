//
//  leftSliderMenuTableViewCell.h
//  LookUp
//
//  Created by Divey Punj on 2/09/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface leftSliderMenuTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *buttonName;
@property (strong, nonatomic) IBOutlet UIImageView *optionIconImageView;

-(void)setCell:(NSString*)locButtonName img:(UIImage*)img;


@end
