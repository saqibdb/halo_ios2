//
//  emailVerificationViewController.h
//  LookUp
//
//  Created by Divey Punj on 17/05/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerQueryBridge.h"
#import "AppDelegate.h"


@interface emailVerificationViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *doneBut;
@property (strong, nonatomic) IBOutlet UITextField *accessCodeText;

@property(strong, nonatomic)NSString* userEmail;
@property (strong, nonatomic) IBOutlet UIView *emailChangeView;
@property (strong, nonatomic) IBOutlet UITextField *emailChangeText;
@property (strong, nonatomic) IBOutlet UIButton *skipBut;




- (IBAction)doneAction:(id)sender;
- (IBAction)resendCodeAction:(id)sender;
- (IBAction)emailChangeAct:(id)sender;
- (IBAction)changeEmailActBut:(id)sender;
- (IBAction)closeChangeEmailViewAct:(id)sender;
- (IBAction)skipAction:(id)sender;


@end
