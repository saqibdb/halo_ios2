//
//  ServerQueryBridge.m
//  LookUp
//
//  Created by Divey Punj on 14/02/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerQueryBridge.h"
#import "signUp.h"
#import "login.h"
#import "messageClass.h"
#import "messageDialogClass.h"
#import "config.h"
#import "LookUp-Swift.h"

NSManagedObjectContext *context_server;
NSManagedObject *managedDataObject_server;
AppDelegate *delegate_server;
NSFetchRequest *userDataRequest_server;
NSArray *userDataArray_server;



@interface ServerQueryBridge()
//@property (nonatomic,retain) id<KCSStore> serverStore;
//@property (nonatomic,retain) KCSCachedStore* downloadStore;
@property (nonatomic,retain) UIImage* userImage;


@end

login *loginClass;
messageClass *messages;
messageDialogClass *messageDialog;
serverBridgeClass *currentUser;


NSUserDefaults *deviceSaved;
NSMutableArray *tempArray;
NSMutableArray *matesInfo;
NSMutableArray *matesArray;
NSMutableArray *myProfile;

BOOL isDoneLoadingImage;
UIImage* otherUserImage;
NSMutableArray *historicalMessagesAsSender, *historicalMessagesAsRecepient;

//KCSQuery *otherUsersQuery;

@implementation ServerQueryBridge

-(void)initialiseBackend{
    currentUser= [[serverBridgeClass alloc]init];
}

-(void)initialiseCoreData:(NSString*)entityId{

    NSError *error;
    context_server = [self managedObjectContext];
    userDataRequest_server= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_server.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId];
    userDataArray_server = [context_server executeFetchRequest:userDataRequest_server error:&error];
    managedDataObject_server= [userDataArray_server objectAtIndex:0];
    
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

/*-(void) serverPing{
    
    [KCSPing pingKinveyWithBlock:^(KCSPingResult *result){
        
        if(result.pingWasSuccessful){
            NSLog(@"Yes Successful Ping");
        }
        else{
            NSLog(@"No Unsuccessful Ping");
        }
        
    }];
}*/

-(BOOL)checkActiveUser{
    
    if([currentUser checkActiveUser]){
        return YES;
    }
    else{
        return NO;
    }
}

-(void)logOutUser{
    //Delete core data object
    [self deleteCoreDataObjects];
    //Log out user
    [currentUser logout];
    NSLog(@"User Logged Out.");
}

-(void)deleteCoreDataObjects{
    context_server= [self managedObjectContext];
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    NSArray *tempArray = [context_server executeFetchRequest:fetchRequest error:&error];
    
    if([tempArray count]==0 || tempArray==nil){
        //Nothing
    }
    else{
        for(int i=0; i<[tempArray count]; i++){
            [context_server deleteObject:[tempArray objectAtIndex:i]];
        }
    }
    
    
    if(![context_server save:&error]){
        NSLog(@"Save failed:%@",error);
    }
    else{
        NSLog(@"Saved");
    }
    
}

-(void) createUser: (NSString*)username password:(NSString*)password{
    
    [currentUser signupUserWithUsername:username password:password];

}

-(void)saveData:(NSString*)notiName entityId:(NSString*)entityId{
    
    if(![entityId isEqualToString:@""]){
      
        NSString *lat= [managedDataObject_server valueForKey:@"latitude"];
        NSString *lon= [managedDataObject_server valueForKey:@"longitude"];
        
        //Save default values if required
        //[self saveDefaultData];
        
        NSMutableArray *messageMates;
        if([managedDataObject_server valueForKey:@"messageMates"]==nil){
            
        }
        else{
            messageMates= [[NSMutableArray alloc]initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:[managedDataObject_server valueForKey:@"messageMates"]]];

        }

        [currentUser saveDataWithNotiName:notiName entityId: entityId name: [managedDataObject_server valueForKey:@"name"] email:[managedDataObject_server valueForKey:@"email"] gender:[managedDataObject_server valueForKey:@"gender"] status:[managedDataObject_server valueForKey:@"status"] interest:[managedDataObject_server valueForKey:@"interest"] orientation:[managedDataObject_server valueForKey:@"orientation"] userAge:[managedDataObject_server valueForKey:@"userAge"] otherUserGender:[managedDataObject_server valueForKey:@"otherUserGender"] otherUserOrientation:[managedDataObject_server valueForKey:@"otherUserOrientation"] messageMates:messageMates insideLocation:[managedDataObject_server valueForKey:@"insideLocation"] myLocLat:[lat doubleValue] myLocLon:[lon doubleValue] checkedIn:[managedDataObject_server valueForKey:@"checkedIn"] venueName:[managedDataObject_server valueForKey:@"venueName"] verifiedUser:[managedDataObject_server valueForKey:@"verifiedUser"]otherUserAgeFrom:[managedDataObject_server valueForKey:@"otherUserAgeFrom"] otherUserAgeTo:[managedDataObject_server valueForKey:@"otherUserAgeTo"]];

    }
    else{
        NSLog(@"Cannot Save data: EntityId is empty");
        NSMutableDictionary* tempDict= [[NSMutableDictionary alloc]init];
        [tempDict setObject:@"entityError" forKey:@"profileSaveConfirm"];
        [[NSNotificationCenter defaultCenter]postNotificationName:notiName object:self userInfo:tempDict];
    }
    
}


//NEW METHOD TO FETCH USER PROFILES

-(void)getOtherUserProfiles:(int) skipCount gender:(NSString*)gender age_from:(NSString*)age_from age_to:(NSString*)age_to ort:(NSString*)ortin status:(NSString*)status{

    NSString *locGen, *locOrit;
    
    if([[managedDataObject_server valueForKey:@"otherUserAgeFrom"] isEqualToString:@""] || [managedDataObject_server valueForKey:@"otherUserAgeFrom"]==nil){
       age_from= @"18";
    }
    else{
        //Nothing
    }
    
    if([[managedDataObject_server valueForKey:@"otherUserAgeTo"]isEqualToString:@""] || [managedDataObject_server valueForKey:@"otherUserAgeTo"]==nil){
        age_to= @"100";
    }
    else{
        
    }
    
    //AGE
    NSNumberFormatter *from= [[NSNumberFormatter alloc]init];
    from.numberStyle= NSNumberFormatterDecimalStyle;
    NSNumber *from_number= [from numberFromString:age_from];
    NSNumberFormatter *to= [[NSNumberFormatter alloc]init];
    to.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *to_number= [to numberFromString:age_to];
    
    //LOCATION
    NSString *lat= [managedDataObject_server valueForKey:@"latitude"];
    NSString *lon= [managedDataObject_server valueForKey:@"longitude"];
    CLLocationCoordinate2D geoLoc= CLLocationCoordinate2DMake([lat doubleValue], [lon doubleValue]);
    
    //Checkin Info
    NSString *venueName= [managedDataObject_server valueForKey:@"venueName"];
    
    if(venueName == nil ||[venueName isEqualToString:@""]){
        venueName= @"Error";
    }
    else{
        //Nothing
    }
    
    if(gender== nil || [gender isEqualToString:@""]){
        //Nothing
        locGen= @"Female";
    }
    else{
        locGen= gender;
    }
    
    if(ortin==nil || [ortin isEqualToString:@""]){
        locOrit= @"Straight";
    }
    else{
        locOrit= ortin;
    }
    
    [currentUser getHaloProfilesFromAge:from_number toAge:to_number locCoord:geoLoc gender:locGen orientation:locOrit status:status checkedInStat:@"Yes" venueName:venueName];
}


-(void)loginUser: (NSString*)username password: (NSString*)password{
   
    [currentUser loginWithUsername:username password:password];
    
}

-(void)addAdditionalUserInfo:(NSString*)entityId email:(NSString*)email{
    [currentUser addAdditionalUserInfoWithEntityId:entityId email:email];
}

-(void)resetPassword:(NSString*)email{
    [currentUser passwordResetWithEmailAddress:email];
}

-(void)loadMyProfile:(NSString*)entityId notiName:(NSString*)notiName{
    
    if(entityId == nil){
        NSLog(@"Server Error: Unable to load profile. Username is empty");
    }
    else{
        [currentUser loadUserProfileWithEntityId:entityId notiName:notiName];
    }
}

-(void)fetchNearByEvents: (double)distance notiName:(NSString*)notiName keyName:(NSString*)keyName{
    
    //Get location coordinates
    NSString *lat= [managedDataObject_server valueForKey:@"latitude"];
    NSString *lon= [managedDataObject_server valueForKey:@"longitude"];

    CLLocationCoordinate2D geoLoc= CLLocationCoordinate2DMake([lat doubleValue], [lon doubleValue]);
    
    [currentUser getNearByEventsWithLocCoord:geoLoc radius:distance notiName:notiName keyName:keyName];
    
    //[currentUser getNearByEventsWithLocCoord:geoLoc];
}

-(void)downloadImagesForEvents:(NSArray*)fileIds notificationName: (NSString*)notiName keyName:(NSString*)keyName{
    
    [currentUser downloadEventImagesWithImageIds:fileIds notiName:notiName keyName:keyName];
}

-(void)createUserUsingFacebook: (NSDictionary*)facebookDict accessToken:(NSString*)accessToken{
    
    [currentUser loginWithSocialIDWithFacebookDict:facebookDict accessToken:accessToken];
    
}

-(void)uploadSmallImage:(UIImage*)image entityId: (NSString*)eID notiName:(NSString*)notiName name:(NSString*)name{
    
    NSString *fileId= [eID stringByAppendingString:@"Small.png"];
    name= [name stringByAppendingString:@" DP Small"];
    [currentUser uploadUserImageWithImage:image imageId:fileId notiName:notiName name:name];
}

-(void)uploadLargeImage:(UIImage*)image entityId: (NSString*)eID notiName:(NSString*)notiName name:(NSString*)name{
    
    NSString *fileId= [eID stringByAppendingString:@"Large.png"];
    name= [name stringByAppendingString:@" DP Large"];
    [currentUser uploadUserImageWithImage:image imageId:fileId notiName:notiName name:name];
}

-(void)downloadSmallImageByNames:(NSMutableArray*)fileIds notiName: (NSString*)notiName keyName:(NSString*)keyName{
    
    [currentUser downloadSmallImagesByIdsWithImageIds:fileIds notiName:notiName keyName:keyName];
}

-(void)downloadLargeImageByName:(NSString *)fileId notificationName:(NSString *)notiName keyName:(NSString *)keyName{
    
    [currentUser downloadLargeImageWithImageId:fileId notiName:notiName keyName:keyName];
}

-(void)addVenueAnalyticsData:(NSString*)entId checkedInTime:(NSDate*)checkedInTime checkedOutTime:(NSDate*)checkedOutTime venueName:(NSString*)venueName eventName:(NSString*)eventName userLocLat:(NSString*)userLocLat userLocLon:(NSString*)userLocLon{
    
    
    
    [currentUser addVenueAnalyticsDataWithUserEntId:entId checkedInTime:checkedInTime checkedOutTime:checkedOutTime venueName:venueName eventName:eventName userLocLat:[userLocLat doubleValue] userLocLon:[userLocLon doubleValue]];

}

-(void)connectUsers:(NSString*)friendID myId:(NSString*)myId myName:(NSString*)myName friendName:(NSString*)friendName friendStatus:(NSString*)friendStatus venueName:(NSString*)venueName timeStamp:(NSString*)timeStamp isHalo:(NSString*)isHalo myStatus:(NSString*)myStatus notiName: (NSString*)notiName{
    
    [currentUser connectUsersWithFriendId:friendID myId:myId myName:myName friendName:friendName isHalo:isHalo friendStatus:friendStatus venueName:venueName timeStamp:timeStamp myStatus:myStatus notiName:notiName];
}

-(void)friendRequestResponse:(NSString*)myId friendId:(NSString*)friendId myResponse:(NSString*)myResponse{
    
    [currentUser friendRequestResponseWithMyId:myId friendId:friendId myResponse:myResponse];
    
}

-(void)removeExistingFriend:(NSString*)friendId myId:(NSString*)myId{
    [currentUser removeExistingFriendWithFriendId:friendId myId:myId];
}

-(void)sendEmailVerification:(NSString*)email emailBody:(NSString*)emailBody{
    [currentUser sendEmailVerificationWithEmail:email emailBody:emailBody];
}

-(void)updateUsernameAndEmail:(NSString*)entityId email:(NSString*)email{
    [currentUser updateUsernameAndEmailWithEntityId:entityId email:email];
}

-(void)getMyFriendsList:(NSString*)entityId notiName:(NSString*)notiName{
    [currentUser getMyFriendsListWithEntityId:entityId notiName:notiName];
}

-(void)canCheckIn:(double)lat lon:(double)lon isCheckedIn:(NSString*)isCheckedIn isActive:(NSString*)isActive notiName:(NSString*)notiName{
    [currentUser canCheckInLatitude:lat longitude:lon isCheckedIn:isCheckedIn isActive:isActive notiName:notiName];
}

-(void)checkForExistingMessages:(NSString*)recipient senderId:(NSString*)senderId text:(NSString*)text timestamp:(NSDate*)timestamp notiName:(NSString*)notiName{
    [currentUser checkForExistingMessagesWithRecipientId:recipient senderId:senderId text:text timeStamp:timestamp notiName:notiName];
}


-(void)verifiedAs:(NSString*)entityId notiName:(NSString*)notiName{
    [currentUser verifiedAsEntityId:entityId notiName:notiName];
}


-(void)saveDefaultData{


   /* if([managedDataObject_server valueForKey:@"userAge"]==nil){
        //Convert string age to NSNumber
        NSNumberFormatter *n= [[NSNumberFormatter alloc]init];
        n.numberStyle= NSNumberFormatterDecimalStyle;
        NSNumber *ageInNumber;
        ageInNumber= [n numberFromString:@"18"];
        [userClassData setUserAge:ageInNumber];
        
    }
 
    if([userClassData getGender]==nil || [[userClassData getGender]isEqualToString:@""]){
        [userClassData setGender:@"Male"];
    }
    if([userClassData getStatus]==nil || [[userClassData getStatus]isEqualToString:@""]){
        [userClassData setStatus:@"Green"];
    }
    if([userClassData getInterest]==nil || [[userClassData getInterest]isEqualToString:@""]){
        [userClassData setInterest:@" "];
    }
    if([userClassData getMyOrientation]==nil || [[userClassData getMyOrientation]isEqualToString:@""]){
        [userClassData setMyOrientation:@"Straight"];
    }
    if([userClassData getOtherUserGender]==nil || [[userClassData getOtherUserGender]isEqualToString:@""]){
        [userClassData setOtherUserGender:@"Female"];
    }
    if([userClassData getOtherUserOrientation]==nil || [[userClassData getOtherUserOrientation]isEqualToString:@""]){
        [userClassData setOtherUserOrientation:@"Straight"];
    }
    if([userClassData getOtherUserAge_to]==nil || [[userClassData getOtherUserAge_to]isEqualToString:@""]){
        [userClassData setOtherUserAge_to:@"100"];
    }
    if([userClassData getOtherUserAge_from]==nil || [[userClassData getOtherUserAge_from]isEqualToString:@""]) {
        [userClassData setOtherUserAge_from:@"18"];
    }
    [userClassData saveUserData:@"" notiNeed:NO];*/
    
}

-(void)getMessagesFromServerAsSender: (NSString*)senderId recipient:(NSString*)recipientId skipCount:(int)skipCount notiName:(NSString*)notiName{
    
    [currentUser loadMessagesWithRecipientId:recipientId senderId:senderId skipCount:skipCount notiName:notiName];

}

-(void)saveMessagesToServer: (NSString*)messageId reciptId:(NSString*)recipientId sendrIds: (NSString*)senderIds text:(NSString*)messageText date:(NSDate*)timeStamp{
    
    [currentUser saveMessageWithText:messageText senderId:senderIds recipientId:recipientId timestamp:timeStamp messageId:messageId];
}

-(void)getUserWithId: (NSString*)entityId notiName:(NSString*)notiName keyName:(NSString*)keyName{
    
    [currentUser getUserWithIdWithEntityId:entityId notiName:notiName keyName:keyName];
}


/*


//kKCSAscending






-(void)connectUserToHaloMessenger:(NSString*)email userId: (NSString*)userId{
    
    [KCSCustomEndpoints callEndpoint:@"connectToHaloMessenger" params:@{@"chatMateName":email, @"userId":userId} completionBlock:^(id results, NSError *error) {
        if(results){
            NSLog(@"Connected with Halo Messenger");
        }
        else{
            //Error
            NSLog(@"Error in connecting with Halo Messenger");
        }
    }];
}








*/














@end
