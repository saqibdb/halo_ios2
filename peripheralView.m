//
//  peripheralView.m
//  LookUp
//
//  Created by Divey Punj on 17/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "peripheralView.h"


ServerQueryBridge *serverClassForPeripheral;
BOOL isPoweredOnPer, isConnected;
NSTimer *connectionTimeOutTimer;
NSString *bluetoothStatusMessage;
NSString *entityId_peri;

@interface peripheralView ()

@end

@implementation peripheralView


- (void)initiliase {
    
    //Get entityId and username from key chain
    entityId_peri = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    _peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];
    isPoweredOnPer= NO;
    serverClassForPeripheral= [[ServerQueryBridge alloc]init];
    [serverClassForPeripheral initialiseBackend];
    [serverClassForPeripheral initialiseCoreData:entityId_peri];
    isConnected= NO;

}

-(void)connectionTimeOutTimer{
    if(isConnected){
        //Nothing
    }
    else{
        [_peripheralManager stopAdvertising];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"tapInfoMessages" object:self userInfo:@{@"tapConnectMessage": @"Could not find any users. Try again later."}];
    }
}

-(void)startAdvertising{
    
    if(isPoweredOnPer){

        _transferCharacteristic = [[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID] properties:CBCharacteristicPropertyRead value:nil permissions:CBAttributePermissionsReadable];
        
        _recieverCharacteristicOtherId =[[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:RECIEVER_CHARACTERISTIC_OTHER_UUID] properties:CBCharacteristicPropertyRead|CBCharacteristicPropertyWrite value:nil permissions:CBAttributePermissionsReadable|CBAttributePermissionsWriteable];
        
        _recieverCharacteristicOtherAnswer =[[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:RECIEVER_CHARACTERISTIC_ANSWER_UUID] properties:CBCharacteristicPropertyRead|CBCharacteristicPropertyWrite value:nil permissions:CBAttributePermissionsReadable|CBAttributePermissionsWriteable];
        
        CBMutableService *transferService= [[CBMutableService alloc]initWithType:[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID] primary:YES];
        
        transferService.characteristics= @[_transferCharacteristic, _recieverCharacteristicOtherId, _recieverCharacteristicOtherAnswer];
        
        [_peripheralManager addService:transferService];
        [_peripheralManager startAdvertising:@{CBAdvertisementDataServiceUUIDsKey: @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]]}];
        

        

    }
    else{
        //Nothing
        [[NSNotificationCenter defaultCenter]postNotificationName:@"tapInfoMessages" object:self userInfo:@{@"tapConnectMessage": bluetoothStatusMessage}];
    }
    
    
    isConnected= NO;
    
    
    //Start a connection time out timer
    [connectionTimeOutTimer invalidate];
    connectionTimeOutTimer= [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(connectionTimeOutTimer) userInfo:nil repeats:YES];
    

}

-(void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral{
    
    if(peripheral.state== CBPeripheralManagerStatePoweredOn){
        isPoweredOnPer= YES;
        NSLog(@"Powered On");
    }
    else if(peripheral.state== CBPeripheralManagerStatePoweredOff){
        NSLog(@"Peripheral Manager powered off. Check Bluetooth");
        //Send out error alert
        isPoweredOnPer= NO;
        bluetoothStatusMessage= @"Bluetooth is off, please check.";
        [[NSNotificationCenter defaultCenter]postNotificationName:@"peripheralErrors" object:nil userInfo:@{@"error":@"bluetoothOff"}];

    }
    else if(peripheral.state== CBPeripheralManagerStateUnknown){
        NSLog(@"Peripheral Manager state unknown. Check Bluetooth");
        //Send out error alert
        isPoweredOnPer= NO;
        bluetoothStatusMessage= @"This device does not support Bluetooth";
    }
    else if(peripheral.state== CBPeripheralManagerStateResetting){
        NSLog(@"Peripheral Manager state resetting. Check Bluetooth");
        //Send out error alert
        isPoweredOnPer= NO;
        bluetoothStatusMessage= @"This device does not support Bluetooth";
    }
    else if(peripheral.state== CBPeripheralManagerStateUnsupported){
        NSLog(@"Peripheral Manager state unsupported. Check Bluetooth");
        //Send out error alert
        isPoweredOnPer= NO;
        bluetoothStatusMessage= @"This device does not support Bluetooth";
    }
    else{
        NSLog(@"Peripheral Manager state unauthorised. Check Bluetooth");
        //Send out error alert
        isPoweredOnPer= NO;
        bluetoothStatusMessage= @"This device does not support Bluetooth";
    }
}

-(void) peripheralManager:(CBPeripheralManager *)peripheral didAddService:(CBService *)service error:(NSError *)error{
    
    if(error){
        NSLog(@"Error publishing service: %@", [error localizedDescription]);
    }
    else{
        NSLog(@"Did add service: %@",service.UUID);
    }
}

-(void) peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error{
    if(error){
        NSLog(@"Error in Advertising: %@", error);
    }
    else{
        [[NSNotificationCenter defaultCenter]postNotificationName:@"tapInfoMessages" object:self userInfo:@{@"tapConnectMessage": @"Looking for users to connect with.."}];
        NSLog(@"Started Advertising");
    }
}


-(void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveWriteRequests:(NSArray<CBATTRequest *> *)requests{
    
    isConnected= YES;
    [connectionTimeOutTimer invalidate];
    connectionTimeOutTimer= nil;
    
    if([[requests objectAtIndex:0].characteristic.UUID isEqual:_recieverCharacteristicOtherId.UUID]){
        
        NSString *stringFromData= [[NSString alloc] initWithData:[requests objectAtIndex:0].value encoding:NSUTF8StringEncoding];
        NSLog(@"Recieved from Central: %@",stringFromData);
        
        if([stringFromData isEqualToString:@""]||[stringFromData isEqualToString:@"No"]||[stringFromData isEqualToString:@"Yes"]||[stringFromData isEqualToString:@"ALR"]){
            //Nothing
        }
        else{
            //Send to server
            [serverClassForPeripheral getUserWithId:stringFromData notiName:@"peripheralSideServer" keyName:@"peripheralSideTapUserName"];
        }
        
        [_peripheralManager respondToRequest:[requests objectAtIndex:0] withResult:CBATTErrorSuccess];
    }
    else if([[requests objectAtIndex:0].characteristic.UUID isEqual:_recieverCharacteristicOtherAnswer.UUID])
    {
        NSString *stringFromData= [[NSString alloc] initWithData:[requests objectAtIndex:0].value encoding:NSUTF8StringEncoding];
        
        //Check if the answer is ALR or NO, stop advertising, lose connection
        if([stringFromData isEqualToString:@"ALR"]||[stringFromData isEqualToString:@"No"]||[stringFromData isEqualToString:@"Yes"]){
            [_peripheralManager stopAdvertising];
        }
        else{
            //Nothing
        }
        
        //Send response to userProfile
        [[NSNotificationCenter defaultCenter]postNotificationName:@"peripheralSideTapInfo" object:self userInfo:@{@"peripheralSideInfo": stringFromData}];
        
        [_peripheralManager respondToRequest:[requests objectAtIndex:0] withResult:CBATTErrorSuccess];
    }
    
}


- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveReadRequest:(CBATTRequest *)request {
    
    isConnected= YES;
    [connectionTimeOutTimer invalidate];
    connectionTimeOutTimer= nil;
    
    if([request.characteristic.UUID isEqual:_transferCharacteristic.UUID]){
        if(request.offset > _transferCharacteristic.value.length){
            [_peripheralManager respondToRequest:request withResult:CBATTErrorInvalidOffset];
        }
        else{
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"tapInfoMessages" object:self userInfo:@{@"tapConnectMessage": @"Awaiting Response.."}];
            
            NSString *text= entityId_peri;
            
            _dataToSend= [text dataUsingEncoding:NSUTF8StringEncoding];
            _transferCharacteristic.value= _dataToSend;
            request.value= [_transferCharacteristic.value subdataWithRange:NSMakeRange(request.offset, _transferCharacteristic.value.length-request.offset)];
            [_peripheralManager respondToRequest:request withResult:CBATTErrorSuccess];
        }
    }
}

-(void)stopAdvertising{
    [_peripheralManager stopAdvertising];
    isConnected= NO;
}




@end
