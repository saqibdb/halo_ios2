//
//  checkedInStatus.m
//  LookUp
//
//  Created by Divey Punj on 9/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "checkedInStatus.h"

NSString* entityId_chkStat;
NSManagedObjectContext *context_chkStat;
NSManagedObject *managedDataObject_chkStat;
AppDelegate *delegate_chkStat;
NSFetchRequest *userDataRequest_chkStat;
NSArray *userDataArray_chkStat;

@interface checkedInStatus ()

@end

@implementation checkedInStatus

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if([[self checkScreenSize]isEqualToString:@"Small"]){
        _heightConstraint.constant= 15;
        [_redStatBut setFrame:CGRectMake((self.view.frame.size.width/2-butSizSmall/2), redButYPosSm, butSizSmall, butSizSmall)];
        [_amberStatBut setFrame:CGRectMake((self.view.frame.size.width/2-butSizSmall/2), amberButYPosSm, butSizSmall, butSizSmall)];
        [_greenStatBut setFrame:CGRectMake((self.view.frame.size.width/2-butSizSmall/2), greenButYPosSm, butSizSmall, butSizSmall)];
    }
    else{
        _heightConstraint.constant= 50;
        [_redStatBut setFrame:CGRectMake((self.view.frame.size.width/2-butSizLarge/2), redButYPosLar, butSizLarge, butSizLarge)];
        [_amberStatBut setFrame:CGRectMake((self.view.frame.size.width/2-butSizLarge/2), amberButYPosLar, butSizLarge, butSizLarge)];
        [_greenStatBut setFrame:CGRectMake((self.view.frame.size.width/2-butSizLarge/2), greenButYPosLar, butSizLarge, butSizLarge)];
    }
    [self.view layoutIfNeeded];
    
    /*[_redStatusInfo setTranslatesAutoresizingMaskIntoConstraints:NO];
    //[_amberStatusInfo setTranslatesAutoresizingMaskIntoConstraints:NO];
    //[_greenStatusInfo setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self setStatInfoLayout:_redStatusInfo with:_redStatBut];
    //[self setStatInfoLayout:_amberStatusInfo with:_amberStatBut];
    //[self setStatInfoLayout:_greenStatusInfo with:_greenStatBut];
    [self.view updateConstraints];
    [self.view setNeedsLayout];*/
    
    
    
    //Get entityId and username from key chain
    entityId_chkStat = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    //Initialise Core data object
    NSError *error;
    context_chkStat = [self managedObjectContext];
    userDataRequest_chkStat= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_chkStat.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_chkStat];
    userDataArray_chkStat = [context_chkStat executeFetchRequest:userDataRequest_chkStat error:&error];
    managedDataObject_chkStat= [userDataArray_chkStat objectAtIndex:0];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    _redStatusInfo.hidden= YES;
    _amberStatusInfo.hidden= YES;
    _greenStatusInfo.hidden= YES;
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)redStatAct:(id)sender {
    
    [managedDataObject_chkStat setValue:@"Red" forKey:@"status"];
    [self saveData];
}

- (IBAction)amberStatAct:(id)sender {
    [managedDataObject_chkStat setValue:@"Yellow" forKey:@"status"];
    [self saveData];
}

- (IBAction)greenStatAct:(id)sender {
    [managedDataObject_chkStat setValue:@"Green" forKey:@"status"];
    [self saveData];
}

- (IBAction)statusInfoAction:(id)sender {
    
    _redStatusInfo.hidden= NO;
    _amberStatusInfo.hidden= NO;
    _greenStatusInfo.hidden= NO;
}

-(void)saveData{
    NSError *error;
    if(![context_chkStat save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
        [self performSegueWithIdentifier:@"statToLocSegue" sender:self];
    }
}

-(NSString*)checkScreenSize{
    //Check screen size to adjust the view size
    
    if([[UIDevice currentDevice]userInterfaceIdiom]== UIUserInterfaceIdiomPhone){
        if([[UIScreen mainScreen]bounds].size.width == 320){
            //iPhone 4s
            return @"Small";
        }
        else if([[UIScreen mainScreen]bounds].size.width == 375){
            //iPhone 6
            return @"Medium";
        }
        else{
            //iPhone 6 Plus
            return @"Large";
        }
    }
    else{
        //iPad, so nothing
        return @"Large";
    }
}

-(void)setStatInfoLayout: (UILabel*)item with:(UIButton*)withBut{
   //Trailing
  /*  NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:item
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1
                                   constant:2];
    
    //Leading
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:item
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:withBut
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1
                                   constant:5];
    
    //Top
    NSLayoutConstraint *top =   [NSLayoutConstraint
                                 constraintWithItem:item
                                 attribute:NSLayoutAttributeTop
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:self.view
                                 attribute:NSLayoutAttributeBottom
                                 multiplier:1
                                 constant:300];
    
    //Height to be fixed for SubView same as AdHeight
    NSLayoutConstraint *height = [NSLayoutConstraint
                                  constraintWithItem:item
                                  attribute:NSLayoutAttributeHeight
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:nil
                                  attribute:NSLayoutAttributeNotAnAttribute
                                  multiplier:1
                                  constant:20];
    
    
    [self.view addConstraint:trailing];
    [self.view addConstraint:top];
    [self.view addConstraint:leading];
    
    //[self.view addConstraints:@[trailing, leading, top]];
    
    [item addConstraint:height];*/
    
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(_redStatusInfo, _redStatBut);

    //[self.viewLabels addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[boton(22)]" options:0 metrics:nil views:@{@"boton":miBoton}]];
    //[self.viewLabels addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[boton(40)]" options:0 metrics:nil views:@{@"boton":miBoton}]];
    [self.view addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(10)-[redStatusInfo]-(10)-|"
                                             options:0
                                             metrics:nil views:@{@"redStatBut":_redStatBut, @"redStatusInfo":_redStatusInfo}]];
    [self.view addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_redStatusInfo(5)]|"
                                             options:0
                                             metrics:nil views:viewsDictionary]];

}

































@end
