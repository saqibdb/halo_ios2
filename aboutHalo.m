//
//  aboutHalo.m
//  LookUp
//
//  Created by Divey Punj on 17/09/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "aboutHalo.h"

@interface aboutHalo ()

@end

@implementation aboutHalo
@synthesize infoView, infoLabel, infoTextView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    infoView.hidden= YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)closeViewAction:(id)sender {
    infoView.hidden= YES;
}

- (IBAction)aboutHaloAction:(id)sender {
    
    infoView.hidden= NO;
    infoLabel.text= @"Halo";
    infoTextView.text= @"Halo is a new dating app with an aim to bring back face to face communication. Tired of asking the awkward question ""Are you single?"", use Halo to know who is who around you! The Halo Status app turns participating venues/events into a traffic light party, but without you having to dress in green. Check-in to the venue, select your status (red, amber, green) and you will be able to search for the status and interests of others at the venue, as they will be with you.\n\nFind someone you like? Find them and talk to them rather than sending a message saying ""hey"". If the attraction is mutual, then exchange details within the app which enables you to send messages, then organise a future catch up! No longer do you need to hide out at home swiping through endless profiles. It is time to get out there and do it the old fashioned way… with help from Halo!";
    
    
    //Halo is a dating app that is bringing back traffic light parties in your mobile phones. Green dress not your thing? Well use Halo to let people around you know of your status and only Halo users can see your status. Halo actually makes you get out of your comfort zone to talk to real people. Now there will not be any fake profiles or chat bots, because the only way to connect with someone on Halo is by talking to them first. Here is how Halo works, you go to a Halo supporting venue (pubs, clubs, gyms etc) and use Halo to find out what people's status is. Green means they are approachable and are looking for a chat. Amber is that they are happy to chat, but things may be complicated. Red is Do not disturb. Now you never have to ask the random question Are you single?
}

- (IBAction)userAgreementAction:(id)sender {
    
    infoView.hidden= NO;
    infoLabel.text= @"User Agreement";
    infoTextView.text= @"--Terms of use--: In addition to the Apple App Store End User License Agreement, you are also agreeing to the following terms of use: User Generated Content - User generated content refers to any images, texts, videos or messages that are uploaded by the user using Halo. The user shall not upload any offensive material in the form of images, videos, text, status updates and messages.Offensive material includes but is not limited to pornographic material, nudity, racist material or any such material deemed offensive by Halo. Halo will broadcast the user content to allow other people to view their profiles. The content broadcasted will be the users name, profile picture, interests, orientation, status, age and gender. With this agreement you are allowing Halo to broadcast your profile when you check in at a Halo event. NOTE: When the user is not checked in, their profile is not broadcasted. --Profile Bans--Halo content is monitored regularly and any offensive material found may lead to termination of the account and a profile ban for a period of time or lifetime. Halo holds the right to ban offensive profiles and users to protect other Halo users. Halo will give 2 warnings to the user, but a third offence will lead to a direct termination of the account. --Blocking Users--The user has the ability to delete people from their chats by swiping left their name. NOTE: Once you press delete on a user, there is no way for both parties to message each other, unless they both connect with each other again in person. --Using Halo--Halo is an enabler that will provide information about a user that you may use to approach them and start a conversation. NOTE: Halo does not take any responsibility for the conversation not starting or ending well. When the user is not checked in, the app will display our future events and our Facebook page, so you can plan your next event. When the user is at a Halo event, they are asked to check in. Once checked in, the user can see other profiles in the area. NOTE: Halo takes no responsibility for other users not matching your filtering criteria at the event. To avoid this, please check your filter and try to generalise it if required. --User License--The license obtained by the user is for the specific user and shall not be sold to any one else. The user shall not try to reverse engineer the code and the app. The user shall not try to affect the Halo data base in any way. Any malicious activity will lead to a direct termination of the Halo account. --Halo Duty of Care--All user data is stored in a protected server which is monitored regularly by the Halo team. The user data that is broadcasted is user name, profile picture, interests, age, status, orientation and gender. The user data that is not broadcasted is the users email, username, password and chat messages.";
    
}

- (IBAction)privacyPolicyAction:(id)sender {
}

- (IBAction)reportAction:(id)sender {
    
    infoView.hidden= NO;
    infoLabel.text= @"Reporting";
    infoTextView.text= @"You can block any user in your chats by swiping left and pressing the delete button. NOTE: Once you press Delete, there is no way of either of the parties of contacting each other, unless they reconnect by meeting each other in person. For any other reporting, please send Halo a message using your chats :).";
}

- (IBAction)closeAboutAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
