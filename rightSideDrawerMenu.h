//
//  rightSideDrawerMenu.h
//  LookUp
//
//  Created by Divey Punj on 10/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "rightSideDrawerTabCell.h"
#import "ServerQueryBridge.h"

@interface rightSideDrawerMenu : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *locTab2;
@property (strong, nonatomic) IBOutlet UILabel *currentLoc;
@property (strong, nonatomic) IBOutlet UILabel *profileFilterLab;
@property (strong, nonatomic) IBOutlet UILabel *promotionLab;


- (IBAction)haloProfFiltRed:(id)sender;
- (IBAction)haloProfFiltYellow:(id)sender;
- (IBAction)haloProfFiltGreen:(id)sender;

@end
