//
//  facebookConnect.m
//  LookUp
//
//  Created by Divey Punj on 10/07/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "facebookConnect.h"
NSMutableArray *imageURLs;
NSInteger imageCount, imageDownloadCounter;

@implementation facebookConnect

-(void)initialise{
    imageURLs= [[NSMutableArray alloc]init];
    imageCount=0;
    imageDownloadCounter=0;
}

-(void)clearExistingToken{
    [FBSDKAccessToken setCurrentAccessToken:nil];
}

-(void)getNewAppToken{
    
}

-(void)setCurrentToken{
    FBSDKAccessToken *token= [[FBSDKAccessToken alloc]initWithTokenString:@"|" permissions:nil declinedPermissions:nil appID:fbHaloAppId userID:nil expirationDate:nil refreshDate:nil];
    [FBSDKAccessToken setCurrentAccessToken:token];
}
-(void)getFBPosts{
    //TinderAustralia
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"/halodating?fields=posts" parameters:nil HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if(!error){
            //NSLog(@"FBConnect Result: %@", result);
           
            NSDictionary *posts= [result objectForKey:@"posts"];
            NSArray *data= [posts objectForKey:@"data"];
            //NSLog(@"Message: %@", [[data objectAtIndex:0]objectForKey:@"message"]);
           
            //Put the posts in an array
            NSMutableArray* haloPosts= [[NSMutableArray alloc]init];
            for (int i=0; i< data.count; i++) {
                NSString* tempStr= [[data objectAtIndex:i]objectForKey:@"message"];
                
                
                if(tempStr == nil){
                    NSLog(@"fbPostMessage: Empty message");
                }
                else{
                    
                    //Create a new dictionary
                    NSMutableDictionary *tempDict= [[NSMutableDictionary alloc]init];
                    [tempDict setValue: [[data objectAtIndex:i]objectForKey:@"message"] forKey:@"message"];
                    [tempDict setValue:@"" forKey:@"URL"];
                    [tempDict setValue:@"" forKey:@"photoName"];
                    [tempDict setValue:nil forKey:@"image"];
                    
                    [haloPosts addObject:tempDict];
                }
                
                //[haloPosts addObject:[[data objectAtIndex:i]objectForKey:@"message"]];
            }
            //Notify the root controller that posts are ready
            [[NSNotificationCenter defaultCenter]postNotificationName:@"fbPostsFetched" object:self userInfo:@{@"fbFetchedPosts": haloPosts}];
            
        }
        else{
            NSLog(@"FBConnect Error: %@", error);
        }
    }];
 
 }

-(void)getFBPhotoIds{
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"/halodating/photos?type=uploaded" parameters:nil HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if(!error){
            //NSDictionary *photos= [result objectForKey:@"photos"];
            NSArray *data= [result objectForKey:@"data"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"fbPhotoIdsLoaded" object:self userInfo:@{@"fbPhotoIds": data}];
        }
        else{
            NSLog(@"FBConnect Error: %@", error);
        }
    }];
    
    
    /*FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"/halodating/photos?type=uploaded" parameters:nil HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if(!error){
            //NSDictionary *photos= [result objectForKey:@"photos"];
           NSArray *data= [result objectForKey:@"data"];
            imageCount= [data count];
            for(int i=0; i<data.count;i++){
                NSString* photoId= [[data objectAtIndex:i]objectForKey:@"id"];
                [self getPhoto:photoId];
            }
            
        }
        else{
            NSLog(@"FBConnect Error: %@", error);
        }
    }];*/
}

-(void)getFBPhotos:(NSMutableArray*)photoIds{
    
    //Store the data count
    imageCount= [photoIds count];
    //Initialise ImageURL to remove previous images
    [imageURLs removeAllObjects];
    imageDownloadCounter=0;
    for(int i=0;i<photoIds.count;i++){
        NSString* photoId= [[photoIds objectAtIndex:i]objectForKey:@"id"];
        [self getPhoto:photoId];
    }
    
    
}

-(void)getPhoto:(NSString*)photoId{
    
    NSString* path= [@"/" stringByAppendingString:photoId];
    path= [path stringByAppendingString:@"?fields=images,name"];
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:path parameters:nil HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if(!error){
            NSArray *photoDataArray= [result objectForKey:@"images"];
            NSString* photoName= [result objectForKey:@"name"];
            NSDictionary *photoDataDict= [photoDataArray objectAtIndex:0];
            
            //Create a dictionary to hold the url and the name
            NSMutableDictionary *tempDict= [[NSMutableDictionary alloc]init];
            [tempDict setValue:[photoDataDict objectForKey:@"source"] forKey:@"URL"];
            
            if(photoName == nil){
                [tempDict setValue:@"" forKey:@"photoName"];
            }
            else{
                [tempDict setValue:photoName forKey:@"photoName"];
            }
            [imageURLs addObject: tempDict];
            imageDownloadCounter++;
            
            if(imageDownloadCounter >= imageCount){
                //Send notification
                //Notify the root controller that posts are ready
                [[NSNotificationCenter defaultCenter]postNotificationName:@"fbPhotosFetched" object:self userInfo:@{@"fbFetchedPhotos": imageURLs}];
            }
            
        }
        else{
            NSLog(@"FBConnect Error: %@", error);
        }
    }];
}

//NEW METHODS


@end
