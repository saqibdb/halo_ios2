//
//  checkedInImg.m
//  LookUp
//
//  Created by Divey Punj on 9/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "checkedInImg.h"

NSString* entityId_chkImg;
NSManagedObjectContext *context_chkImg;
NSManagedObject *managedDataObject_chkImg;
AppDelegate *delegate_chkImg;
NSFetchRequest *userDataRequest_chkImg;
NSArray *userDataArray_chkImg;

@interface checkedInImg ()

@end

@implementation checkedInImg

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //87,71,69
    if([[self checkScreenSize]isEqualToString:@"Small"]){
        _yesHeightConst.constant= 30;
        _imgHeightConst.constant= 30;
        _contHeightConst.constant= 30;
    }
    else{
        _yesHeightConst.constant= 87;
        _imgHeightConst.constant= 71;
        _contHeightConst.constant= 69;
    }
    [self.view layoutIfNeeded];
    
    //GENERAL
    _yeButton.layer.cornerRadius= 15;
    _yeButton.clipsToBounds= YES;
    _continueButton.layer.cornerRadius= 15;
    _continueButton.clipsToBounds= YES;
    
    //Get entityId and username from key chain
    entityId_chkImg = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    //Initialise Core data object
    NSError *error;
    context_chkImg = [self managedObjectContext];
    userDataRequest_chkImg= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_chkImg.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_chkImg];
    userDataArray_chkImg = [context_chkImg executeFetchRequest:userDataRequest_chkImg error:&error];
    managedDataObject_chkImg= [userDataArray_chkImg objectAtIndex:0];

    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    //Load current DP
    UIImage* userDP= [UIImage imageWithData:[managedDataObject_chkImg valueForKey:@"userDP"]];
    
    if(userDP == [NSNull null]){
        //Nothing
    }
    else{
        _chkImg.image= userDP;
        _chkImg.layer.cornerRadius= 50;
        _chkImg.clipsToBounds= YES;
    }
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)checkScreenSize{
    //Check screen size to adjust the view size
    
    if([[UIDevice currentDevice]userInterfaceIdiom]== UIUserInterfaceIdiomPhone){
        if([[UIScreen mainScreen]bounds].size.width == 320){
            //iPhone 4s
            return @"Small";
        }
        else if([[UIScreen mainScreen]bounds].size.width == 375){
            //iPhone 6
            return @"Medium";
        }
        else{
            //iPhone 6 Plus
            return @"Large";
        }
    }
    else{
        //iPad, so nothing
        return @"Large";
    }
}


@end
