//
//  leftSideSliderMenu.h
//  LookUp
//
//  Created by Divey Punj on 2/09/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "leftSliderMenuTableViewCell.h"
#import "userSettings.h"
#import "ServerQueryBridge.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>

@interface leftSideSliderMenu : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *leftSldierMenuOptions;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *statusSlider;
@property (strong, nonatomic) IBOutlet UIButton *greenStatusButton;

@property (strong, nonatomic) IBOutlet UILabel *checkInLocationLab;
@property (strong, nonatomic) IBOutlet UITextView *checkInLocationText;
@property (strong, nonatomic) IBOutlet UILabel *statusLab;



@property (strong, nonatomic) IBOutlet UISwitch *checkinSwitch;
@property (strong, nonatomic) IBOutlet UIImageView *userProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *userProfileName;
@property (strong, nonatomic) IBOutlet UIButton *redStatBut;
@property (strong, nonatomic) IBOutlet UIButton *amberStatBut;
@property (strong, nonatomic) IBOutlet UIButton *greenStatBut;






- (IBAction)greenStatusAction:(id)sender;
- (IBAction)yellowStatusAction:(id)sender;
- (IBAction)redStatusAction:(id)sender;
- (IBAction)checkinSwitchAction:(id)sender;











@end
