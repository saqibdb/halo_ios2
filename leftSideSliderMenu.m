//
//  leftSideSliderMenu.m
//  LookUp
//
//  Created by Divey Punj on 2/09/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "leftSideSliderMenu.h"

userSettings *userSettingsForSlider;
ServerQueryBridge *serverForSiderMenu;
NSManagedObjectContext *context_lssm;
NSManagedObject *managedDataObject_lssm;
AppDelegate *delegate_lssm;
NSFetchRequest *userDataRequest_lssm;
NSArray *userDataArray_lssm;
NSString *entityId_lssm;

NSMutableArray *optionsArray;

@implementation leftSideSliderMenu
@synthesize leftSldierMenuOptions, statusSlider, greenStatusButton, checkinSwitch, checkInLocationLab;

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    //Get entityId and username from key chain
    entityId_lssm = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    
    if([entityId_lssm isEqualToString:@""] || entityId_lssm == nil){
        //Nothing
    }
    else{

        //Initialise Core data object
        NSError *error;
        context_lssm = [self managedObjectContext];
        userDataRequest_lssm= [[NSFetchRequest alloc]initWithEntityName:@"User"];
        userDataRequest_lssm.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_lssm];
        userDataArray_lssm = [context_lssm executeFetchRequest:userDataRequest_lssm error:&error];
        
        if([userDataArray_lssm count]==0){
            //Log out user
            [self logoutCommand];
        }
        else{
            //Initialise server
            serverForSiderMenu= [[ServerQueryBridge alloc]init];
            [serverForSiderMenu initialiseBackend];
            [serverForSiderMenu initialiseCoreData:entityId_lssm];
            
            managedDataObject_lssm = [userDataArray_lssm objectAtIndex:0];
            
            if([[managedDataObject_lssm valueForKey:@"checkedIn"]isEqualToString:@"Yes"]){
                if([managedDataObject_lssm valueForKey:@"venueName"]==nil || [[managedDataObject_lssm valueForKey:@"venueName"] isEqualToString:@""]){
                    self.checkInLocationText.text= @"Nowhere";
                    [checkinSwitch setOn:YES];
                }
                else{
                    self.checkInLocationText.text= [managedDataObject_lssm valueForKey:@"venueName"];
                    [checkinSwitch setOn:YES];
                }
            }
            else{
                self.checkInLocationText.text= @"Nowhere";
                [checkinSwitch setOn:NO];
            }
            
            
            if([[managedDataObject_lssm valueForKey:@"status"] isEqualToString:@"Green"]){
                self.statusLab.text= @"Green";
                [_greenStatBut setAlpha:1];
                [_amberStatBut setAlpha:0.25];
                [_redStatBut setAlpha:0.25];
                _userProfilePic.layer.borderColor= ([[UIColor greenColor]CGColor]);
            }
            else if([[managedDataObject_lssm valueForKey:@"status"] isEqualToString:@"Yellow"]){
                self.statusLab.text= @"Amber";
                [_greenStatBut setAlpha:0.25];
                [_amberStatBut setAlpha:1];
                [_redStatBut setAlpha:0.25];
                _userProfilePic.layer.borderColor= ([[UIColor orangeColor]CGColor]);
            }
            else if([[managedDataObject_lssm valueForKey:@"status"] isEqualToString:@"Red"]){
                self.statusLab.text= @"Red";
                [_greenStatBut setAlpha:0.25];
                [_amberStatBut setAlpha:0.25];
                [_redStatBut setAlpha:1];
                _userProfilePic.layer.borderColor= ([[UIColor redColor]CGColor]);
            }
            else{
                self.statusLab.text= @"Green";
                [_greenStatBut setAlpha:1];
                [_amberStatBut setAlpha:0.25];
                [_redStatBut setAlpha:0.25];
                _userProfilePic.layer.borderColor= ([[UIColor greenColor]CGColor]);
            }
            
            
            //Set profile picture
            UIImage *image= [UIImage imageWithData:[managedDataObject_lssm valueForKey:@"userDP"]];
            _userProfilePic.layer.cornerRadius= 25;
            _userProfilePic.clipsToBounds= YES;
            _userProfilePic.layer.borderWidth= 2;
            
            if(image==nil || image == [NSNull null]){
                //Show Halo logo
                self.userProfilePic.image= [UIImage imageNamed:@"haloL.png"];
            }
            else{
                //Display profile pic
                self.userProfilePic.image= image;
            }
            
            //Set profile name
            NSString *userName= [managedDataObject_lssm valueForKey:@"name"];
            if(userName== nil || [userName isEqualToString:@""]){
                self.userProfileName.text= @"Halo User";
            }
            else{
                self.userProfileName.text= userName;
            }
            
            
        }

        }

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    optionsArray = [[NSMutableArray alloc]init];
    [optionsArray addObject:@"My Profile"];
    [optionsArray addObject:@"My Discounts"];
    [optionsArray addObject:@"My Statistics"];
    [optionsArray addObject:@"Halo Facebook"];
    [optionsArray addObject:@"Logout"];
    [optionsArray addObject:@"About Halo"];
    
    [leftSldierMenuOptions setDelegate:self];
    [leftSldierMenuOptions setDataSource:self];
    
    userSettingsForSlider= [[userSettings alloc]init];
    [userSettings initialize];
    


}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)greenStatusAction:(id)sender {
    self.statusLab.text= @"Green";
    [managedDataObject_lssm setValue:@"Green" forKey:@"status"];
    [self saveData];
    [serverForSiderMenu saveData:@"none" entityId:entityId_lssm];
    
    [_greenStatBut setAlpha:1];
    [_amberStatBut setAlpha:0.25];
    [_redStatBut setAlpha:0.25];
    _userProfilePic.layer.borderColor= ([[UIColor greenColor]CGColor]);
}


- (IBAction)yellowStatusAction:(id)sender {
    self.statusLab.text= @"Amber";
    [managedDataObject_lssm setValue:@"Yellow" forKey:@"status"];
    [self saveData];
    [serverForSiderMenu saveData:@"none" entityId:entityId_lssm];
    
    [_greenStatBut setAlpha:0.25];
    [_amberStatBut setAlpha:1];
    [_redStatBut setAlpha:0.25];
    _userProfilePic.layer.borderColor= ([[UIColor orangeColor]CGColor]);
}

- (IBAction)redStatusAction:(id)sender {
    self.statusLab.text= @"Red";
    [managedDataObject_lssm setValue:@"Red" forKey:@"status"];
    [self saveData];
    [serverForSiderMenu saveData:@"none" entityId:entityId_lssm];
    
    [_greenStatBut setAlpha:0.25];
    [_amberStatBut setAlpha:0.25];
    [_redStatBut setAlpha:1];
    _userProfilePic.layer.borderColor= ([[UIColor redColor]CGColor]);
}

- (IBAction)checkinSwitchAction:(id)sender {
    
    if(checkinSwitch.isOn){
        //On
        //Check if user is in a region, otherwise give pop-up saying no
        if([[managedDataObject_lssm valueForKey:@"regionFlag"]isEqualToString:@"Yes"]){
            //Can check in
            [[NSNotificationCenter defaultCenter] postNotificationName:@"forceCheckin" object:self userInfo:@{@"checkin":@"Yes"}];
        }
        else{
            //Cannot check in
            NSString* message= @"You have to be at a Halo event to check-in. Please look out for more events!";
            UIAlertController *alert= [UIAlertController alertControllerWithTitle:@"ERROR" message:message preferredStyle:UIAlertControllerStyleActionSheet];
            
            UIAlertAction* okButton= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
            [checkinSwitch setOn:NO];
        }
        
    }
    else{
        //Off
        //Set checkin to off
        [[NSNotificationCenter defaultCenter] postNotificationName:@"forceCheckin" object:self userInfo:@{@"checkin":@"No"}];
        
        checkInLocationLab.text= @"Nowhere";
    }
    
}


//******************************TABLE VIEW METHODS**********************************


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return optionsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Main table view
    
    
    leftSliderMenuTableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:@"leftSliderCell"];
    
    if(cell == nil){
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: @"leftSliderCell"];
    }
    
   else if([[optionsArray objectAtIndex:indexPath.row]isEqualToString:@"My Discounts"]){
            [cell setCell:[optionsArray objectAtIndex:indexPath.row] img:[UIImage imageNamed:@"leftside_optionIcon_dc.png"]];
    }
   else if([[optionsArray objectAtIndex:indexPath.row]isEqualToString:@"My Statistics"]){
        [cell setCell:[optionsArray objectAtIndex:indexPath.row] img:[UIImage imageNamed:@"leftside_optionIcon_stat.png"]];
    }
   else if([[optionsArray objectAtIndex:indexPath.row]isEqualToString:@"Halo Facebook"]){
        [cell setCell:[optionsArray objectAtIndex:indexPath.row] img:[UIImage imageNamed:@"leftside_optionIcon_fb.png"]];
    }
   else if([[optionsArray objectAtIndex:indexPath.row]isEqualToString:@"Logout"]){
        [cell setCell:[optionsArray objectAtIndex:indexPath.row] img:[UIImage imageNamed:@"ftside_optionIcon_logout.png"]];
    }
   else if([[optionsArray objectAtIndex:indexPath.row]isEqualToString:@"About Halo"]){
       [cell setCell:[optionsArray objectAtIndex:indexPath.row] img:[UIImage imageNamed:@"leftside_optionIcon_halo.png"]];
   }
   else{
        [cell setCell:[optionsArray objectAtIndex:indexPath.row] img:[UIImage imageNamed:@"testImage.png"]];
    }

    

    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    //sliderMenuToSettingsSegue
    
    if(indexPath.row==0){
        //My Profile
        [self performSegueWithIdentifier:@"sliderMenuToSettingsSegue" sender:self];
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    }
    else if(indexPath.row==1){
        //My Promotions
        if([[managedDataObject_lssm valueForKey:@"checkedIn"]isEqualToString:@"Yes"]){
            [self performSegueWithIdentifier:@"leftDrawerToPromoSegue" sender:self];
        }
        else{
            [self popMessage:@"You need to be checked in at a venue to see your discounts." title:@"Note"];
        }
    }
    else if(indexPath.row == 2){
        [self performSegueWithIdentifier:@"leftMenuToStatsSegue" sender:self];
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    }
    else if(indexPath.row==3){
        //Halo Facebook
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://facebook.com/halostatus"]];
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    }
    else if(indexPath.row==4){
        //Log out
        
        //Ask to confirm log out
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Logout"
                                                                                 message:@"Are you sure you want to log out?"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Yes"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil]; //You can use a block here to handle a press on this button
        UIAlertAction *actionYes= [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self logoutCommand];
        }];
        
        UIAlertAction *actionNo= [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
        
        [alertController addAction:actionYes];
        [alertController addAction:actionNo];
        [self presentViewController:alertController animated:YES completion:nil];
        

    }
    else{
        //About Halo
        [self performSegueWithIdentifier:@"sliderMenuToAboutSegue" sender:self];
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    }
    
}

-(void)logoutCommand{
    
    //Check out first
    [managedDataObject_lssm setValue:@"No" forKey:@"checkedIn"];
    [self saveData];
    [serverForSiderMenu saveData:@"" entityId:entityId_lssm];
    
    //Clear device data
    [self deleteCoreDataObjects];
    [self deleteCoreDataMessageObject];
    
    //Log out Kinvey
    [serverForSiderMenu logOutUser];
    
    //Notify user profile that user has logged out
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logoutStopAction" object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"loggedOutWork" object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"messageLogoutAction" object:nil];
}

-(NSString*)checkScreenSize{
    //Check screen size to adjust the view size
    
    if([[UIDevice currentDevice]userInterfaceIdiom]== UIUserInterfaceIdiomPhone){
        if([[UIScreen mainScreen]bounds].size.width == 320){
            //iPhone 4s
            return @"Small";
        }
        else{
            //iPhone 6 and more
            return @"Large";
            
        }
    }
    else{
        //iPad, so nothing
        return @"Large";
    }
}



-(void)saveData{
    NSError *error;
    if(![context_lssm save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
    }
}

-(void)deleteCoreDataObjects{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    NSArray *tempArray = [context_lssm executeFetchRequest:fetchRequest error:&error];
    
    if([tempArray count]==0 || tempArray==nil){
        //Nothing
    }
    else{
        for(int i=0; i<[tempArray count]; i++){
            [context_lssm deleteObject:[tempArray objectAtIndex:i]];
        }
    }
    
    
    if(![context_lssm save:&error]){
        NSLog(@"Save failed:%@",error);
    }
    else{
        AppDelegate *tempDel = [[UIApplication sharedApplication]delegate];
        tempDel.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"initialLoginView"];
    }
}

-(void)deleteCoreDataMessageObject{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Friends"];
    NSArray *tempArray = [context_lssm executeFetchRequest:fetchRequest error:&error];
    
    if([tempArray count]==0 || tempArray==nil){
        //Nothing
    }
    else{
        for(int i=0; i<[tempArray count]; i++){
            [context_lssm deleteObject:[tempArray objectAtIndex:i]];
        }
    }
    
    
    if(![context_lssm save:&error]){
        NSLog(@"Save failed:%@",error);
    }
}

-(void)popMessage:(NSString*)message title:(NSString*)title{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


@end
