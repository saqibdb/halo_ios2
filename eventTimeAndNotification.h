//
//  eventTimeAndNotification.h
//  LookUp
//
//  Created by Divey Punj on 22/07/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "userClass.h"

@interface eventTimeAndNotification : NSObject

-(void)setAlarm:(NSString*)dateStr eventId:(NSString*)eventId eventName: (NSString*)eventName;
-(void)deleteOldEvents;

@end
