//
//  camOverlayController.m
//  LookUp
//
//  Created by Divey Punj on 12/05/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "camOverlayController.h"

@interface camOverlayController ()

@end

@implementation camOverlayController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _photoView.layer.borderWidth= 2.0f;
    _photoView.layer.borderColor= [UIColor whiteColor].CGColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)takePhotoAction:(id)sender {
    [_barCodePhotoRef takePicture];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *barImg = [info objectForKeyedSubscript:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    //Crop image with view location
    [self cropImage:[self rotateImage:barImg]];
    
}

-(void)cropImage:(UIImage*)img{
    CGRect clippedRect  = CGRectMake(_photoView.frame.origin.x, _photoView.frame.origin.y, photoViewWidLarge, photoViewHigLarge);
    CGImageRef imageRef = CGImageCreateWithImageInRect([img CGImage], clippedRect);
    UIImage *newImage   = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
}

-(UIImage *)rotateImage:(UIImage *)image{
    
    CGImageRef imgRef= image.CGImage;
    CGFloat width= CGImageGetWidth(imgRef);
    CGFloat height= CGImageGetHeight(imgRef);
    CGSize imageSize= CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGRect bounds= CGRectMake(0, 0, width, height);
    CGFloat boundheight;
    CGFloat scaleRatio= bounds.size.width/ width;
    CGAffineTransform transform= CGAffineTransformIdentity;
    UIImageOrientation orientation= image.imageOrientation;
    
    if(orientation == UIImageOrientationUp){
        //transform= CGAffineTransformIdentity;
        //NSLog(@"Image orient up");
        return image;
    }
    else if(orientation== UIImageOrientationUpMirrored){
        transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
        transform= CGAffineTransformScale(transform, 1.0, -1.0);
    }
    else if(orientation == UIImageOrientationDown){
        transform= CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
        transform= CGAffineTransformRotate(transform, M_PI);
    }
    else if(orientation== UIImageOrientationDownMirrored){
        transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
        transform= CGAffineTransformScale(transform, 1.0, -1.0);
    }
    else if(orientation == UIImageOrientationLeftMirrored){
        boundheight= bounds.size.height;
        bounds.size.height= bounds.size.width;
        bounds.size.width= boundheight;
        transform= CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
        transform= CGAffineTransformScale(transform, -1.0, 1.0);
        transform= CGAffineTransformRotate(transform, 3.0*M_PI/2.0);
    }
    else if(orientation == UIImageOrientationLeft){
        boundheight= bounds.size.height;
        bounds.size.height= bounds.size.width;
        bounds.size.width= boundheight;
        transform= CGAffineTransformMakeTranslation(0.0, imageSize.width);
        transform= CGAffineTransformRotate(transform, 3.0*M_PI/2.0);
    }
    else if(orientation == UIImageOrientationRightMirrored){
        boundheight = bounds.size.height;
        bounds.size.height= bounds.size.width;
        bounds.size.width= boundheight;
        transform= CGAffineTransformMakeScale(-1.0, 1.0);
        transform= CGAffineTransformRotate(transform, M_PI/2.0);
    }
    else if(orientation== UIImageOrientationRight){
        boundheight= bounds.size.height;
        bounds.size.height= bounds.size.width;
        bounds.size.width= boundheight;
        transform= CGAffineTransformMakeTranslation(imageSize.height, 0.0);
        transform= CGAffineTransformRotate(transform, M_PI/2.0);
    }
    else{
        //nothing
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context= UIGraphicsGetCurrentContext();
    if(orientation == UIImageOrientationRight || orientation == UIImageOrientationLeft){
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else{
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy= UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
    
}


@end
