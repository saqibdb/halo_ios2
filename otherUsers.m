//
//  otherUsers.m
//  LookUp
//
//  Created by Divey Punj on 21/02/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "otherUsers.h"
#import "ServerQueryBridge.h"
#import "userClass.h"
#import "serverUserData.h"
#import "userProfile.h"
#import "messageClass.h"

ServerQueryBridge *serverAccessForUsers;
NSMutableArray *otherUsersArray;
userClass *userInfoData;
serverUserData *serverUserDataClass;
userProfile *userProf;
NSMutableArray *otherUsersForTalkingArray;
messageClass *messageMates;

@implementation otherUsers

-(void) initialiseOtherUsers{
    
    otherUsersArray= [[NSMutableArray alloc]init];
    serverAccessForUsers= [[ServerQueryBridge alloc]init];
    userInfoData= [[userClass alloc]init];
    [userInfoData userInitialise];
    serverUserDataClass= [[serverUserData alloc]init];
    userProf= [[userProfile alloc]init];
    otherUsersForTalkingArray =[[NSMutableArray alloc]init];
    messageMates= [[messageClass alloc]init];
}

-(void)generateOtherUserArray: (NSMutableArray*)usersArray{
    
    NSMutableArray* returnedProfiles= [[NSMutableArray alloc]init];
    
    for (int i=0; i<usersArray.count; i++) {
        NSMutableDictionary* profile= [[NSMutableDictionary alloc]init];
        serverUserDataClass= [usersArray objectAtIndex:i];
        NSString *tempName= serverUserDataClass.name;
        NSString *tempStatus= serverUserDataClass.status;
        NSString *tempInterest= serverUserDataClass.interest;
        UIImage *userImage= serverUserDataClass.userImage;
        NSString *entityId= serverUserDataClass.entityId;

        
        if([userInfoData.getEntityId isEqualToString:serverUserDataClass.entityId]){
            //Nothing
        }
        else{
            if(tempName==nil){
                tempName= @"";
            }
            if(tempStatus == nil){
                tempStatus= @"";
            }
            if(tempInterest== nil){
                tempInterest= @"";
            }
            if(userImage == nil){
                userImage= [UIImage imageNamed:@"No Image.png"];
            }
            
            [profile setObject:tempName forKey:@"name"];
            [profile setObject:tempStatus forKey:@"status"];
            [profile setObject:tempInterest forKey:@"interest"];
            [profile setObject:userImage forKey:@"userImage"];
            [profile setObject:entityId forKey:@"entityId"];
            [returnedProfiles addObject:profile];
        }
    }
    otherUsersForTalkingArray= returnedProfiles;
    [userProf updateOtherUserProfTable:returnedProfiles];
}
-(void)findOtherUsersUsingBluetooth{
    
}

-(void)filterOtherUsersWithGender: (NSString *)gender{
    
}
-(void)filterOtherUSersWithStatus: (NSString*)status{
    
}
-(void)getOtherUserProfileUpdate{

    
}
-(NSString*)getOtherUserName: (NSString*)eID{
    
    NSLog(@"Get other user name");
    
    for(int i=0; i<otherUsersForTalkingArray.count;i++){
        NSDictionary *tempDict= [otherUsersForTalkingArray objectAtIndex:i];
        if([eID isEqualToString: tempDict[@"entityId"]]){
            return tempDict[@"name"];
        }
        else{
            //Nothing
        }
    }
    return @"";
}

-(UIImage*)getOtherUserImage: (NSString*)eID{
    /*NSLog(@"Get other user image");
    
    for(int i=0; i<otherUsersForTalkingArray.count;i++){
        NSDictionary *tempDict= [otherUsersForTalkingArray objectAtIndex:i];
        if([eID isEqualToString: tempDict[@"entityId"]]){
            return tempDict[@"userImage"];
        }
        else{
            //Nothing
        }
    }*/
    return nil;
}












@end
