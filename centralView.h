//
//  centralView.h
//  LookUp
//
//  Created by Divey Punj on 17/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "SERVICES.h"
#import "ServerQueryBridge.h"
#import "messageClass.h"
#import "peripheralView.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>

@interface centralView : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>

NS_ASSUME_NONNULL_BEGIN
@property (nonatomic, strong) CBCentralManager *centralManager;
@property (nonatomic, strong) CBPeripheral *discoveredPeripheral;
@property (strong, nonatomic) NSMutableData *data;
@property (strong, nonatomic) NSData *dataToSendToPeripheral;
@property (strong, nonnull) CBCharacteristic *discoveredPeripheralCharacteristic;

NS_ASSUME_NONNULL_END

- (void)initialise;
-(void)stopScan;
-(void)initialiseCoredata:(NSString*)entityId;

@end
