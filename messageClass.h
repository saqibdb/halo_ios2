//
//  messageClass.h
//  LookUp
//
//  Created by Divey Punj on 13/04/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "messageDialogClass.h"
#import "ServerQueryBridge.h"
#import "chatMatesCustomCell.h"
#import "userSettings.h"
#import "chatMessageHistory.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface messageClass : UIViewController <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *chatMatesTable;
@property (strong, nonatomic) messageDialogClass *activeMessageDialog;
@property (strong, nonatomic) IBOutlet UILabel *noMatesLabel;
@property (strong, nonatomic) IBOutlet UIView *messageViewHolder;
@property (strong, nonatomic) IBOutlet UIView *messageTableView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *messageContactLoadIndicator;
@property (strong, nonatomic) IBOutlet UIButton *chatMateEditButton;
@property (strong, nonatomic) IBOutlet UIButton *finderTabButton;
@property (strong, nonatomic) IBOutlet UIButton *requestsBut;
@property (strong, nonatomic) IBOutlet UIView *progressView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progressViewIndicator;
@property (strong, nonatomic) IBOutlet UILabel *progressViewLab;






- (IBAction)finderActionInMessage:(id)sender;
-(void)initialise;
-(void)getChatMatesArrayFromDB;
- (IBAction)chatMateRemoveAction:(id)sender;
- (IBAction)testBackButton:(id)sender;
- (IBAction)requestFilterAction:(id)sender;
- (IBAction)friendFilterAction:(id)sender;



@end
