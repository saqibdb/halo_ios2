//
//  barcodeView.m
//  LookUp
//
//  Created by Divey Punj on 12/05/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "barcodeView.h"


camOverlayController *camOverlay;
NSString* entityId_bCode;
NSManagedObjectContext *context_bCode;
NSManagedObject *managedDataObject_bCode;
AppDelegate *delegate_bCode;
NSFetchRequest *userDataRequest_bCode;
NSArray *userDataArray_bCode;
ServerQueryBridge *server_bCode;

@interface barcodeView ()

@end

@implementation barcodeView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _bcInfoLab.text= @"";
    //Get entityId and username from key chain
    entityId_bCode = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    
    NSError *error;
    context_bCode = [self managedObjectContext];
    userDataRequest_bCode= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_bCode.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_bCode];
    userDataArray_bCode = [context_bCode executeFetchRequest:userDataRequest_bCode error:&error];
    managedDataObject_bCode= [userDataArray_bCode objectAtIndex:0];
    
    server_bCode= [[ServerQueryBridge alloc]init];
    [server_bCode initialiseBackend];
    [server_bCode initialiseCoreData:entityId_bCode];
    
    
    [self createBarcode];
    
    _addFriendBut.layer.cornerRadius= 15;
    _addFriendBut.clipsToBounds= YES;
    [self hideProgressView];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    if([_scannedStr isEqualToString:@"No"]){
        
    }
    else{
        [self scannerResult:_scannedStr];
    }

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    //Create notification for messages back from barcodr scanner
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(scannerResult:) name:@"scannerResult" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//**************************BARCODE CODE*************************************
-(void)createBarcode{
    NSError *error= nil;
    NSString *encodingStr= @"Halo~";
    encodingStr= [encodingStr stringByAppendingString:[managedDataObject_bCode valueForKey:@"name"]];
    encodingStr= [encodingStr stringByAppendingString:@"~"];
    encodingStr= [encodingStr stringByAppendingString:[managedDataObject_bCode valueForKey:@"status"]];
    encodingStr= [encodingStr stringByAppendingString:@"~"];
    ZXMultiFormatWriter *writer = [[ZXMultiFormatWriter alloc] init];
    ZXBitMatrix* result = [writer encode:[encodingStr stringByAppendingString:entityId_bCode]
                                  format:kBarcodeFormatQRCode
                                   width:500
                                  height:500
                                   error:&error];
    
    if (result) {
        CGImageRef image = [[ZXImage imageWithMatrix:result] cgimage];
        UIImage *img= [UIImage imageWithCGImage:image];
        _barcodeImg.image= img;
        // This CGImageRef image can be placed in a UIImage, NSImage, or written to a file.
    } else {
        NSString *errorMessage = [error localizedDescription];
        
    }
}

- (IBAction)addedMeAction:(id)sender {
    
    [self.tabBarController setSelectedIndex:1];
    
}

- (IBAction)addFriendAction:(id)sender {
    _bcInfoLab.text= @"";

    [self presentViewController:barCodePhotoTake animated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *barImg = [info objectForKeyedSubscript:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self decodeBarCode:[barImg CGImage]];
    
}

- (IBAction)cancelAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)decodeBarCode:(CGImageRef)imageToDecode{
    
    ZXLuminanceSource *source = [[ZXCGImageLuminanceSource alloc] initWithCGImage:imageToDecode];
    ZXBinaryBitmap *bitmap = [ZXBinaryBitmap binaryBitmapWithBinarizer:[ZXHybridBinarizer binarizerWithSource:source]];
    
    NSError *error = nil;
    
    // There are a number of hints we can give to the reader, including
    // possible formats, allowed lengths, and the string encoding.
    ZXDecodeHints *hints = [ZXDecodeHints hints];
    
    ZXMultiFormatReader *reader = [ZXMultiFormatReader reader];
    ZXResult *result = [reader decode:bitmap
                                hints:hints
                                error:&error];
    if (result) {
        // The coded result as a string. The raw data can be accessed with
        // result.rawBytes and result.length.
        NSString *contents = result.text;
        //Show info
        _bcInfoLab.text= @"Sending friend request..";
 
        // The barcode format, such as a QR code or UPC-A
        ZXBarcodeFormat format = result.barcodeFormat;
    } else {
        // Use error to determine why we didn't get a result, such as a barcode
        // not being found, an invalid checksum, or a format inconsistency.
        //Show info
        _bcInfoLab.text= @"Unable to read barcode, please try again.";

    }
}

-(void)scannerResult:(NSString*)result{
    //[[NSNotificationCenter defaultCenter]removeObserver:self name:@"scannerResult" object:nil];
    
    _scannedStr= @"No";
    
    _bcInfoLab.text= @"";
    //NSString *result= [[notification userInfo]objectForKey:@"result"];
    
    if([result containsString:@"Halo"]){
        //Halo Barcode
        NSArray *name= [result componentsSeparatedByString:@"~"];
        NSString *entityId= name[3];
        
        if([entityId isEqualToString:@""]|| entityId== nil){
            _bcInfoLab.text= @"Unable to read barcode, please try again";
        }
        else{
            
            [self showProgressView];
            //[self performSelectorOnMainThread:@selector(showProgressView) withObject:nil waitUntilDone:NO];
            //Create a notification for sending the friend request
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(friendRequestSentNoti:) name:@"friendRequestSentNoti" object:nil];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
            NSString* dateString = [dateFormatter stringFromDate:[NSDate date]];
            
            [server_bCode connectUsers:entityId myId:entityId_bCode myName:[managedDataObject_bCode valueForKey:@"name"] friendName:name[1] friendStatus:name[2] venueName:[managedDataObject_bCode valueForKey:@"venueName"] timeStamp:dateString isHalo:@"NO" myStatus:[managedDataObject_bCode valueForKey:@"status"] notiName:@"friendRequestSentNoti"];
            
            /*
            //Check if friend exists
            NSMutableArray *tempArray= [[NSMutableArray alloc]initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:[managedDataObject_bCode valueForKey:@"messageMates"]]];
            
            BOOL mateExists= NO;
            if([tempArray count]==0){
                //Nothing
            }
            else{
                //Check if friend exists
                int index=0;
                for(int i=0; i<[tempArray count]; i++){
                    if([entityId isEqualToString:[[tempArray objectAtIndex:i]objectForKey:@"chatMateId"]])
                    {
                        mateExists= YES;
                        index=i;
                        break;
                    }
                    else{
                        mateExists= NO;
                    }
                }
                
                if(mateExists){
                    
                    NSString *message= @"";
                    
                    if([[[tempArray objectAtIndex:index]objectForKey:@"status"]isEqualToString:@"Accepted"]){
                        message= @"You are already friends with ";
                    }
                    else{
                        message= @"You already have a pending request with ";
                    }
                    message= [message stringByAppendingString:[NSString stringWithFormat:@"%@",name[1]]];
                    _bcInfoLab.text= message;
                }
                else{
                    NSString *message= @"Friend request sent to ";
                    message= [message stringByAppendingString:[NSString stringWithFormat:@"%@",name[1]]];
                    _bcInfoLab.text= message;
                    
                    //Add this friend to my friends list with Pending status
                    NSMutableDictionary *tempDict= [[NSMutableDictionary alloc]init];
                    [tempDict setValue:name[1] forKey:@"chatMateName"];
                    [tempDict setValue:entityId forKey:@"chatMateId"];
                    [tempDict setValue:@"Pending" forKey:@"status"];
                    
                    //Save to device and server
                    [tempArray addObject:tempDict];
                    [managedDataObject_bCode setValue:[NSKeyedArchiver archivedDataWithRootObject: tempArray] forKey:@"messageMates"];
                    [self saveData];
                    
                    //Send request to server
                    //[server_bCode connectUsers:entityId myId:entityId_bCode myName:[managedDataObject_bCode valueForKey:@"name"]];
                }
            }*/
        }
    }
    else{
        _bcInfoLab.text= @"This is not a Halo barcode, please try again.";
    }
}

-(void)friendRequestSentNoti:(NSNotification*)notification{
    
    NSString *error= [[notification userInfo]objectForKey:@"error"];

    [self hideProgressView];
    
    if([error isEqualToString:@"Yes"]){
        [self popMessage:@"Unable to send friend request, please try again." title:@"Error"];
    }
    else{
        NSMutableDictionary *tempDict= [[NSMutableDictionary alloc]initWithDictionary:[[notification userInfo]objectForKey:@"response"]];
        NSString *responseStr;
        
        if(tempDict){
            //User exists
            responseStr= [tempDict objectForKey:@"friendshipStatus"];
            if([responseStr isEqualToString:@"Accepted"]){
                _bcInfoLab.text= [NSString stringWithFormat:@"You are already friends with: %@",[tempDict objectForKey:@"friendName"]];
            }
            else if([responseStr isEqualToString:@"Pending"]){
                _bcInfoLab.text= [NSString stringWithFormat:@"You have a pending request with: %@",[tempDict objectForKey:@"friendName"]];
            }
            else if([responseStr isEqualToString:@"Awaiting"]){
                _bcInfoLab.text= [NSString stringWithFormat:@"You have an awaiting request from: %@",[tempDict objectForKey:@"friendName"]];
            }
            else{
                //User does not exist
                _bcInfoLab.text= @"Friend request sent.";
            }
        }
        else{
            [self popMessage:@"Unable to send friend request, please try again." title:@"Error"];
        }
 
    }
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void)saveData{
    NSError *error;
    if(![context_bCode save:&error]){
        NSLog(@"FAILED TO ADD NEW FRIEND TO DEVICE:%@",error);
    }
    else{
        //Save server
        [server_bCode saveData:@"" entityId:entityId_bCode];
    }
}

-(void)showProgressView{
    _progressIndicator.hidden= NO;
    _progressLab.hidden= NO;
    _progressView.hidden= NO;
}

-(void)hideProgressView{
    _progressIndicator.hidden= YES;
    _progressLab.hidden= YES;
    _progressView.hidden= YES;
}

-(void)popMessage:(NSString*)message title:(NSString*)title{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
