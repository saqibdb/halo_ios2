//
//  otherUserProfCell.m
//  LookUp
//
//  Created by Divey Punj on 13/02/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "otherUserProfCell.h"
UITextView *textview;

@implementation otherUserProfCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setCell:(NSString*)info location:(NSString*)loc subtitle:(NSString*)subTitle{
    if([loc isEqualToString:@"name"]){
        _nameLabel.text= info;
        _subtitleLab.hidden= YES;
        _nameLabel.hidden= NO;
        _subInfoLab.hidden= YES;
        textview.hidden= YES;
        _interestTextview.hidden= YES;
    }
    else if([subTitle isEqualToString:@"Interests"]){
        _subtitleLab.text= subTitle;
        _nameLabel.hidden= YES;
        _subtitleLab.hidden= NO;
        _subInfoLab.hidden= YES;
        _interestTextview.hidden= NO;
        
        _interestTextview.text= info;
    
       /* //Create a text view
        textview= [[UITextView alloc]initWithFrame:CGRectMake(5, _subtitleLab.frame.size.height+5, 300, 100)];
        textview.hidden= NO;
        textview.editable= NO;
        [textview setBackgroundColor:[UIColor clearColor]];
        [textview setTextColor:[UIColor whiteColor]];
        textview.text= info;
        [textview isScrollEnabled];
        [textview isUserInteractionEnabled];
        [self addSubview:textview];*/
    }
    else if([loc isEqualToString:@"info"]){
        _subtitleLab.text= subTitle;
        _subInfoLab.text= info;
        _nameLabel.hidden= YES;
        _subtitleLab.hidden= NO;
        _subInfoLab.hidden= NO;
        textview.hidden= YES;
        _interestTextview.hidden= YES;
    }

}

@end
