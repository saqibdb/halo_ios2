//
//  userProfileDialog.h
//  LookUp
//
//  Created by Divey Punj on 27/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerQueryBridge.h"
#import "otherUserProfCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

//400,140

#define USER_IMAGE_VIEW_MORE_HEIGHT 328
#define USER_IMAGE_VIEW_LESS_HEIGHT 120


@interface userProfileDialog : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UILabel *otherUserNameDialogLab;
@property (strong, nonatomic) IBOutlet UIImageView *otherUserImageDialogImageView;
@property (strong, nonatomic) IBOutlet UILabel *otherUserStatusDialogLab;
@property (strong, nonatomic) IBOutlet UILabel *otherUserAgeDialogLab;
@property (strong, nonatomic) IBOutlet UILabel *otherUserOrientationDialogLab;
@property (strong, nonatomic) IBOutlet UITextView *otherUserInterestsDialogLab;
@property (strong, nonatomic) IBOutlet UILabel *otherUserSearchingForGender;
@property (strong, nonatomic) IBOutlet UILabel *otherUserSearchingForOrient;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *imageLoadingIndicator;
@property (strong, nonatomic) IBOutlet UILabel *imageLoadingLab;


@property(strong, nonatomic)NSString* otherUserName;
@property(strong, nonatomic)NSString* otherUserStatus;
@property(strong, nonatomic)NSString* otherUserAge;
@property(strong, nonatomic)NSString* otherUserOrientation;
@property(strong, nonatomic)NSString* otherUserInterests;
@property(strong, nonatomic)NSString* otherUserSearchingGender;
@property(strong, nonatomic)NSString* otherUserSearchingOrientation;
@property(strong, nonatomic)NSString* otherUserImageSmallUrl;
@property(strong, nonatomic)NSString* userEntityId;
@property(strong, nonatomic)NSString* lastSeenLoc;

@property (strong, nonatomic) IBOutlet UITableView *otherUsrProfTable;

@property (strong, nonatomic) IBOutlet UIImageView *userImageLarge;
@property (strong, nonatomic) IBOutlet UIImageView *userImageSmall;
@property (strong, nonatomic) IBOutlet UIView *userImageView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *userImageview_const_height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *userImageLarge_const_height;
@property (strong, nonatomic) IBOutlet UILabel *userNameSmall;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *imageLargeLoader;
@property (strong, nonatomic) IBOutlet UILabel *imageLargeLoadLab;









@end
