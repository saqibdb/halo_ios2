//
//  baseView.m
//  LookUp
//
//  Created by Divey Punj on 29/01/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "baseView.h"

ServerQueryBridge *serverForBase;
location *baseLocation;

BOOL initialiseForNewUser, reloadEvents, stopRegionNotification, allEventViewOn, zoomIntoLoc, allowLoading;
NSMutableArray *allVenues;
NSTimer* baseLocationTimer, *connectionTimer, *locDependTimer;
int selectedIndex;
NSString *entityId, *username_base;
NSManagedObjectContext *context_base;
NSManagedObject *managedDataObject_base;
AppDelegate *delegate_base, *appDelegate;
NSFetchRequest *userDataRequest_base;
NSArray *userDataArray_base;
NSString *nextEventStr;

@interface baseView ()

@end

@implementation baseView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //*****DESIGN***************
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(NAVTABBARRED/255.0) green:(NAVTABBARGREEN/255.0) blue:(NAVTABBARBLUE/255.0) alpha:1.0]];
    [self.tabBarController.tabBar setBarTintColor:[UIColor colorWithRed:(NAVTABBARRED/255.0) green:(NAVTABBARGREEN/255.0) blue:(NAVTABBARBLUE/255.0) alpha:1.0]];
    _mainLoaderView.hidden= YES;
    
    //*****CLASS INITIALISATION*********************
    serverForBase= [[ServerQueryBridge alloc]init];
    baseLocation= [[location alloc]init];
    appDelegate= (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    //*****RANDOM*************
    _allVenueBut.hidden= YES;
    allowLoading= NO;
    double delayLoader= 0.5;
    dispatch_time_t popTimeBut= dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayLoader * NSEC_PER_SEC));
    dispatch_after(popTimeBut, dispatch_get_main_queue(), ^(void){
        [self showMainLoaderView];
    });
    nextEventStr = @"";
    
    //Check for internet connection
    if([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable){
        //Connection unavailable
    }
    else{
        
        //Check if user is active, if active then they have signed in using Facebook. If not active, then they have signed in manually and need to verify their email address
        
        if([serverForBase checkActiveUser]){
            
            //Active user
            //Come back to this view
            //Get entityId and username from key chain
            entityId = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
            username_base = [SAMKeychain passwordForService:@"username" account:@"haloStatus-username"];
            
            //Initialse server
            [serverForBase initialiseBackend];
            
            //Initialise Core data object
            NSError *error;
            context_base = [self managedObjectContext];
            userDataRequest_base= [[NSFetchRequest alloc]initWithEntityName:@"User"];
            userDataRequest_base.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId];
            userDataArray_base = [context_base executeFetchRequest:userDataRequest_base error:&error];
            
            
            
            if([userDataArray_base count]==0){
                [self logoutCommand];
            }
            else{
                
                [baseLocation initialiseLocation];
                
                allowLoading= YES;
                
                managedDataObject_base= [userDataArray_base objectAtIndex:0];
                //Initialise core data for server
                [serverForBase initialiseCoreData:entityId];
                
                //Initiliase Sinch client and get it started
                [appDelegate initSinchClient:entityId];
                
                
                //****MAP VIEW************************************
                //Top view 8
                _venueInfoViewTopConst.constant= self.view.frame.size.height+20;
                [self.view layoutIfNeeded];
                _allVenueBut.layer.cornerRadius= 15;
                _allVenueBut.clipsToBounds= YES;
                allEventViewOn= NO;
                [_allVenueTable setDelegate:self];
                [_allVenueTable setDataSource:self];
                _baseMapView.showsUserLocation= YES;
                [_baseMapView setDelegate:self];
                zoomIntoLoc= YES;
                selectedIndex= -1;
                
                //Load profile
                [self loadProfile];
                
            }

        }
        else{
            [self performSegueWithIdentifier:@"emailVeriReqSegue" sender:self];
        }
    }
    

    //NOTIFICATIONS
    //Create a notification for getting region information from location
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(regionInfoRecieved:) name:@"regionInfoRecieved" object:nil];
    
    //Create a notification to turn check in alert back on
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(turnOnCheckinAlert:) name:@"turnOnCheckinAlert" object:nil];
    
    //Create notification for closing timers when logged out
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(logoutStopAction:) name:@"logoutStopAction" object:nil];
    
    //Create a notifcation for forcing check out
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(forceCheckin:) name:@"forceCheckin" object:nil];
    
    //Create notification for new message alert
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(newMessageAlertInBase:) name:@"newMessageAlertInBase" object:nil];

    //******CHECK FOR INTERNET*********************
    [connectionTimer invalidate];
    connectionTimer= [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkConnectionMethod) userInfo:nil repeats:YES];


}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    
    //Enable the tab bar buttons
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:TRUE];
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:TRUE];
    
    [self hideNextEventView];
 
    if(allowLoading){
        
        //Check for verification and segue out if needed
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(checkVerificationBase:) name:@"checkVerificationBase" object:nil];
        [serverForBase verifiedAs:entityId notiName:@"checkVerificationBase"];
        
        //Segue to checked in view if ready
        if([[managedDataObject_base valueForKey:@"checkedIn"]isEqualToString:@"Yes"]){
            [baseLocationTimer invalidate];
            //Segue to checked in view
            [self performSegueWithIdentifier:@"checkedInSegue" sender:self];
        }
        else{
            
            //Update location everytime this page is in view
            [baseLocation getLocation];
            
            if([[managedDataObject_base valueForKey:@"latitude"]isEqualToString:@""]||[managedDataObject_base valueForKey:@"latitude"]==nil){
                //Start timer to keep checking for location
                [baseLocationTimer invalidate];
                baseLocationTimer= [NSTimer scheduledTimerWithTimeInterval:locationTimerValue target:self selector:@selector(locationTimerMethod) userInfo:nil repeats:YES];
            }
            else{
                //Do all things that require location
                [baseLocation fetchLocation];
                [self getEvents];
            }
            
            //Start location timer
            stopRegionNotification= NO;
            
            //Check if new message is there
            if([[managedDataObject_base valueForKey:@"newMessage"]isEqualToString:@"Yes"]){
                _messageInd_Baseview.hidden= NO;
            }
            else{
                _messageInd_Baseview.hidden= YES;
            }
        }
        
    }
    
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
 
}

-(void)showMainLoaderView{
    [_mainLoaderView setAlpha:0.0];

    [UIView animateWithDuration:0.3 animations:^{
        _mainLoaderView.hidden= NO;
        [_mainLoaderView setAlpha:1.0];
    }];
    _mainLoaderLab.text= @"Finding your location..";
}

-(void)hideMainLoaderView{
    _mainLoaderView.hidden= YES;
    _mainLoaderLab.text= @"";
}

//***********************Region Monitoring*************************************
-(void)regionInfoRecieved:(NSNotification*)notification{
    
    //Show alert only once
    //Check if already checked in, if so dont do anything
    if([[managedDataObject_base valueForKey:@"checkedIn"] isEqualToString:@"Yes"]){
        //Nothing
    }
    else{
        //Get the region identifier and show alert
        //Show alert view
        if(stopRegionNotification){
            //Nothing
        }
        else{
            //Only show if you are in region
            if([[managedDataObject_base valueForKey:@"regionFlag"]isEqualToString:@"Yes"]){
                NSMutableDictionary *dict= [[NSMutableDictionary alloc]initWithDictionary:[[notification userInfo]objectForKey:@"regionDict"]];
                UIAlertView *checkInAlert= [[UIAlertView alloc]initWithTitle:@"Check In!" message:[dict objectForKey:@"regionMessage"] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
                
                //Save venue name to device
                [managedDataObject_base setValue:[dict objectForKey:@"venueName"] forKey:@"venueName"];
                [self saveData];
                
                checkInAlert.tag= 1; //Checkin
                [checkInAlert show];
                
                //Stop region monitoring
                stopRegionNotification= YES;
            }
            else{
                //Nothing
            }

        }
        
    }

}

-(void)locationTimerMethod{
    
    if([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable){
        //Connection unavailable
        
    }
    else{
        //Check if my location is available
        if([[managedDataObject_base valueForKey:@"latitude"]isEqualToString:@""] || [managedDataObject_base valueForKey:@"latitude"]==nil){
            NSLog(@"Getting Location");
            //Get location again
            //Update location everytime this page is in view
            [baseLocation getLocation];
        }
        else{
            NSLog(@"Getting Region Info");
            //Region Monitoring
            [baseLocation fetchLocation];
            [self getEvents];
            [baseLocationTimer invalidate];
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
        //For checking in
        if(buttonIndex == 0){
            //Code for cancel button
            //Set a flag to stop notifying the user
            stopRegionNotification= YES;
            
        }
        else if(buttonIndex == 1){
            [baseLocationTimer invalidate];
            //Code for yes button
            //Change checkin info
            [managedDataObject_base setValue:@"Yes" forKey:@"checkedIn"];
            [self saveData];
            //Save analytics data to server
            [serverForBase addVenueAnalyticsData:entityId checkedInTime:[NSDate date] checkedOutTime:[NSDate date] venueName:[managedDataObject_base valueForKey:@"venueName"] eventName:@"None" userLocLat:[managedDataObject_base valueForKey:@"latitude"] userLocLon:[managedDataObject_base valueForKey:@"longitude"]];
            //Segue to checked in view
            [self performSegueWithIdentifier:@"checkedInSegue" sender:self];
            stopRegionNotification= NO;
        }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"checkedInSegue"]) {
        checkedInStatus *nextViewCon = segue.destinationViewController;
        //[nextViewCon setHidesBottomBarWhenPushed:YES];
        nextViewCon.navigationItem.hidesBackButton = YES;
    }
}

-(void)turnOnCheckinAlert:(NSNotification*)notification{
    stopRegionNotification= NO;
    [serverForBase saveData:@"" entityId:entityId];
}


//******************************EVENTS**********************************
-(void)getEvents{
    //Get all events from server
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(eventsFetched:) name:@"eventsFetched" object:nil];

    [serverForBase fetchNearByEvents:40000000000000000.0 notiName:@"eventsFetched" keyName:@"eventsArray"];
}

-(void)eventsFetched:(NSNotification*)notification{
  
    _mainLoaderLab.text= @"Loading venues..";
    //Remove notification
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"eventsFetched" object:nil];
    
    NSString *status= [[notification userInfo]objectForKey:@"status"];
    NSLog(@"STATUS: %@",status);
    if([status isEqualToString:@"failed"]){
        //Error
        
    }
    else{
        NSMutableArray* tempEventsArray = [[NSMutableArray alloc]initWithArray:[[notification userInfo]objectForKey:@"eventsArray"]];
        
        if([tempEventsArray count]==0){;
            //Dont display anything
        }
        else{
            _allVenueBut.hidden= NO;
            allVenues= [[NSMutableArray alloc]initWithArray:tempEventsArray];
            [_allVenueTable reloadData];
            
            //Set pins
            CLLocationCoordinate2D temp;
            baseMapAnnotations *tempAn;
            NSString *anotSubtitle= @"";
            for(int i=0; i<[tempEventsArray count];i++){
                anotSubtitle= @"Next event is ";
                anotSubtitle= [anotSubtitle stringByAppendingString:[[tempEventsArray objectAtIndex:i]objectForKey:@"eventName"]];
                anotSubtitle= [anotSubtitle stringByAppendingString: @" on "];
                anotSubtitle= [anotSubtitle stringByAppendingString:[[tempEventsArray objectAtIndex:i]objectForKey:@"eventDate"]];
                
                temp= CLLocationCoordinate2DMake([[[tempEventsArray objectAtIndex:i]objectForKey:@"eventCoordLat"]doubleValue],[[[tempEventsArray objectAtIndex:i]objectForKey:@"eventCoordLon"]doubleValue]);
                tempAn= [[baseMapAnnotations alloc]initWithTitle:[[tempEventsArray objectAtIndex:i]objectForKey:@"venueName"] subtitle:anotSubtitle Location:temp];
                [_baseMapView addAnnotation:tempAn];
            }
            //Hide main loader
            [self hideMainLoaderView];
            //Find next event
            [self getNextEvent:tempEventsArray];
        }
    }
}

-(void)getNextEvent:(NSMutableArray *)events{
    
    NSString *dateStr = @"";
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *date;
    NSTimeInterval closestInterval = DBL_MAX;
    int index=0;
    
    for (int i=0; i<[events count]; i++) {
        
        dateStr= [[events objectAtIndex:i]objectForKey:@"eventDate"];
        [dateFormat setDateFormat:@"dd/MM/yyyy"];
        date = [dateFormat dateFromString:dateStr];
        
        NSTimeInterval interval = ABS([date timeIntervalSinceDate:[NSDate date]]);
        
        if (interval < closestInterval) {
            closestInterval = interval;
            index = i;
        }
    }
    
    nextEventStr= [NSString stringWithFormat:@"Find other Halo users at our next event at %@ on %@",[[events objectAtIndex:index]objectForKey:@"venueName"],[[events objectAtIndex:index]objectForKey:@"eventDate"]];
    
    _nextEventLab.text= nextEventStr;
    [self showNextEventView];
}

//******************************TABLE VIEW METHODS**********************************
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if(allVenues.count==0){
        return 0;
    }
    else{
        return allVenues.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellID= @"";
    
    venueEventCell *cell= (venueEventCell*)[tableView dequeueReusableCellWithIdentifier:cellID];
    if(cell == nil){
        NSArray *nib= [[NSBundle mainBundle]loadNibNamed:@"allVenueTableCell" owner:self options:nil];
        cell= [nib objectAtIndex:0];
    }
    if(selectedIndex == indexPath.row){
        //Do expansion
        
    }
    else{
        //Close cell
    }
    
    cell.venueName.text= [[allVenues objectAtIndex:indexPath.row]objectForKey:@"venueName"];
    cell.eventName.text= [[allVenues objectAtIndex:indexPath.row]objectForKey:@"eventName"];
    cell.eventDate.text= [[allVenues objectAtIndex:indexPath.row]objectForKey:@"eventDate"];
    cell.eventAddress.text= [[allVenues objectAtIndex:indexPath.row]objectForKey:@"eventAddress"];

    cell.selectionStyle= UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if(selectedIndex == indexPath.row){
        return 198;
    }
    else{
        return 65;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if(selectedIndex == indexPath.row){
        selectedIndex = -1;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        return;
    }
    
    if(selectedIndex  != -1){
        NSIndexPath *prevPath= [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        selectedIndex = indexPath.row;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:prevPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    selectedIndex = indexPath.row;
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)checkScreenSize{
    //Check screen size to adjust the view size
    
    if([[UIDevice currentDevice]userInterfaceIdiom]== UIUserInterfaceIdiomPhone){
        if([[UIScreen mainScreen]bounds].size.width == 320){
            //iPhone 4s
            return @"Small";
        }
        else if([[UIScreen mainScreen]bounds].size.width == 375){
            //iPhone 6
            return @"Medium";
        }
        else{
            //iPhone 6 Plus
            return @"Large";
        }
    }
    else{
        //iPad, so nothing
        return @"Large";
    }
}


-(void)logoutStopAction:(NSNotification*)notification{
    
    //Stop all timers
    [baseLocationTimer invalidate];
    //Ensure the check in off
    [managedDataObject_base setValue:@"No" forKey:@"checkedIn"];
    [self saveData];
    
    //Need to allow initialise to be called again
    initialiseForNewUser = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedOut" object:self];
}

- (IBAction)leftSliderMenuButton:(id)sender {
    
    AppDelegate *appDelegateTemp= (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegateTemp.drawerContainer toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    
}

-(void)forceCheckin:(NSNotification*)notification{
    
    NSString* info= [[notification userInfo]objectForKey:@"checkin"];
    
    if([info isEqualToString:@"Yes"]){
        //Change checkin info
        [managedDataObject_base setValue:@"Yes" forKey:@"checkedIn"];
        [self saveData];
        //Segue to checked in view
        [self performSegueWithIdentifier:@"checkedInSegue" sender:self];
        stopRegionNotification= NO;
    }
    else{
        [managedDataObject_base setValue:@"No" forKey:@"checkedIn"];
        [self saveData];
        [serverForBase saveData:@"" entityId:entityId];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void)loadProfile{
    //Create notification for closing timers when logged out
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(myProfileLoaded:) name:@"myProfileLoaded" object:nil];
    [serverForBase loadMyProfile:entityId notiName:@"myProfileLoaded"];
}

-(void)myProfileLoaded:(NSNotification*)notification{
    
    NSMutableDictionary *dict= [[NSMutableDictionary alloc]initWithDictionary:[[notification userInfo]objectForKey:@"userProfile"]];
    
    if([dict count]==0){
        //Error, profile not loaded
        
    }
    else{
        //Save the dict to device
        [managedDataObject_base setValue:[dict objectForKey:@"name"] forKey:@"name"];
        [managedDataObject_base setValue:[dict objectForKey:@"gender"] forKey:@"gender"];
        [managedDataObject_base setValue:[dict objectForKey:@"status"] forKey:@"status"];
        [managedDataObject_base setValue:[dict objectForKey:@"interests"] forKey:@"interest"];
        [managedDataObject_base setValue:[dict objectForKey:@"userAge"] forKey:@"userAge"];
        [managedDataObject_base setValue:[dict objectForKey:@"orientation"] forKey:@"orientation"];
        [managedDataObject_base setValue:[dict objectForKey:@"otherUserOrientation"] forKey:@"otherUserOrientation"];
        [managedDataObject_base setValue:[dict objectForKey:@"otherUserGender"] forKey:@"otherUserGender"];
        NSData *chatMateArray= [NSKeyedArchiver archivedDataWithRootObject: [dict objectForKey:@"messageMates"]];
        [managedDataObject_base setValue:chatMateArray forKey:@"messageMates"];
        [managedDataObject_base setValue:[dict objectForKey:@"insideLocation"] forKey:@"insideLocation"];
        NSLog(@"LOADING MY PROFILE!!!!!!!!!!!!!!");
        [self saveData];
    }

}

-(void)newMessageAlertInBase:(NSNotification*)notification{
    [managedDataObject_base setValue:@"Yes" forKey:@"newMessage"];
    _messageInd_Baseview.hidden= NO;
}
-(void)saveData{
    NSError *error;
    if(![context_base save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
    }
}


-(void)deleteCoreDataObjects{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    NSArray *tempArray = [context_base executeFetchRequest:fetchRequest error:&error];
    
    if([tempArray count]==0 || tempArray==nil){
        //Nothing
    }
    else{
        for(int i=0; i<1; i++){
            [context_base deleteObject:[tempArray objectAtIndex:i]];
        }
    }
    
    
    if(![context_base save:&error]){
        NSLog(@"Save failed:%@",error);
    }
    
}

//***************LOG OUT METHODS*****************
-(void)logoutCommand{
    //Clear device data
    [self deleteCoreDataObjects];
    [self deleteCoreDataMessageObject];
    
    //Log out Kinvey
    [serverForBase logOutUser];
    
    //Notify user profile that user has logged out
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logoutStopAction" object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"loggedOutWork" object:nil];
    
    //Segue to sign up
    //[self performSegueWithIdentifier:@"sliderMenuToSignupSegue" sender:self];
    AppDelegate *tempDel = [[UIApplication sharedApplication]delegate];
    tempDel.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"initialLoginView"];
}

-(void)deleteCoreDataMessageObject{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Friends"];
    NSArray *tempArray = [context_base executeFetchRequest:fetchRequest error:&error];
    
    if([tempArray count]==0 || tempArray==nil){
        //Nothing
    }
    else{
        for(int i=0; i<[tempArray count]; i++){
            [context_base deleteObject:[tempArray objectAtIndex:i]];
        }
    }
    
    
    if(![context_base save:&error]){
        NSLog(@"Save failed:%@",error);
    }
}

-(void)checkConnectionMethod{
    
    if([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable){
        [self performSegueWithIdentifier:@"noInternetSegue" sender:self];
    }
}

//*****************************MAP VIEW*****************************************
-(void)mapView:(MKMapView*)mapView didUpdateUserLocation:(nonnull MKUserLocation *)userLocation{
    
    if(zoomIntoLoc){
        zoomIntoLoc= NO;
        CLLocationCoordinate2D coord= _baseMapView.userLocation.location.coordinate;
        MKCoordinateRegion region= MKCoordinateRegionMakeWithDistance(coord, 100000, 100000);
        [_baseMapView setRegion:region animated:YES];
    }
    else{
        //Nothing
    }
}

-(MKAnnotationView*)mapView:(MKMapView*)mapView viewForAnnotation:(nonnull id<MKAnnotation>)annotation{
    
    if([annotation isKindOfClass:[baseMapAnnotations class]]){
        baseMapAnnotations *myLoc= (baseMapAnnotations*)annotation;
        
        MKAnnotationView *annotationView= [_baseMapView dequeueReusableAnnotationViewWithIdentifier:@"baseMapAnnotations"];
        
        if(annotationView == nil){
            annotationView = myLoc.annotationView;
        }
        else{
            annotationView.annotation= annotation;
        }
        
        annotationView.rightCalloutAccessoryView.hidden= YES;
        
        return annotationView;
    }
    else{
        return nil;
    }
}

- (IBAction)allVenueAction:(id)sender {
    
    if(allEventViewOn){
        allEventViewOn= NO;
        [_allVenueBut setTitle:@"All Halo Venues" forState:UIControlStateNormal];
        _venueInfoViewTopConst.constant = self.view.frame.size.height+20;
        [UIView animateWithDuration:.3 animations:^{
            [self.view layoutIfNeeded];
        }];
        selectedIndex= -1;
        [_allVenueTable reloadData];
    }
    else{
        [self hideNextEventView];
        allEventViewOn= YES;
        [_allVenueBut setTitle:@"Close" forState:UIControlStateNormal];
        _venueInfoViewTopConst.constant = 8;
        [UIView animateWithDuration:.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }

}
- (IBAction)nextEventCloseAct:(id)sender {
    [self hideNextEventView];
}

-(void)showNextEventView{
    _nextEventView.hidden= NO;
    _nextEventLab.hidden= NO;
}

-(void)hideNextEventView{
    _nextEventView.hidden= YES;
    _nextEventLab.hidden= YES;
}

-(void)checkVerificationBase:(NSNotification*)notification{
    NSString *response= [[notification userInfo]objectForKey:@"response"];
    
    if([response isEqualToString:@"Yes"] || [response isEqualToString:@"Maybe"]|| [response isEqualToString:@"error"]){
        //Dont do any thing
        
    }
    else{
        //Send to verification page
        [self performSegueWithIdentifier:@"baseToEVSegue" sender:self];
    }
}


@end
