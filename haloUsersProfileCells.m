//
//  haloUsersProfileCells.m
//  LookUp
//
//  Created by Divey Punj on 11/02/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "haloUsersProfileCells.h"

@implementation haloUsersProfileCells

-(void)setCell:(NSString*)imageUrl name:(NSString*)name{
    
   /* if(image){
        
        if([[self checkScreenSize]isEqualToString:@"Small"]){
            _userImage.layer.cornerRadius= 35;
        }
        else if([[self checkScreenSize]isEqualToString:@"Medium"]){
            _userImage.layer.cornerRadius= 50;
        }
        else{
            _userImage.layer.cornerRadius= 50;
        }
        _userImage.clipsToBounds= YES;
        _userImage.image= image;
    }
    else{
        //Nothing
    }*/
    
    NSURL *url= [NSURL URLWithString:imageUrl];
    
    if(imageUrl){
        if([[self checkScreenSize]isEqualToString:@"Small"]){
            _userImage.layer.cornerRadius= 32;
        }
        else if([[self checkScreenSize]isEqualToString:@"Medium"]){
            _userImage.layer.cornerRadius= 50;
        }
        else{
            _userImage.layer.cornerRadius= 50;
        }
        
        [_userImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"No_image_qM.png"]];
    }
    else{
        NSLog(@"****FAILED****");
        [_userImage setImage:[UIImage imageNamed:@"No_image_qM.png"]];
    }
    
    _userName.text= name;
    
}

-(NSString*)checkScreenSize{
    //Check screen size to adjust the view size
    
    if([[UIDevice currentDevice]userInterfaceIdiom]== UIUserInterfaceIdiomPhone){
        if([[UIScreen mainScreen]bounds].size.width == 320){
            //iPhone 4s
            return @"Small";
        }
        else if([[UIScreen mainScreen]bounds].size.width == 375){
            //iPhone 6
            return @"Medium";
        }
        else{
            //iPhone 6 Plus
            return @"Large";
        }
    }
    else{
        //iPad, so nothing
        return @"Large";
    }
}


-(UIImage*)imageResize:(UIImage*)image{
    CGSize size= CGSizeMake(100, 100);
    
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage  *newImage= UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

























@end
