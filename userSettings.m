//
//  userSettings.m
//  LookUp
//
//  Created by Divey Punj on 16/03/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "userSettings.h"

ServerQueryBridge *servBrdge;


NSData *imageData;
NSArray *genderPicker;
NSArray *orientationPicker;
NSArray *agePicker;
NSString *myName;
NSString *myGender;
int myAge;
NSString *myOrientation;
NSString *otherUserGender;
NSString *otherUserOrientation;
NSString *otherUserAge;
NSString *otherUserAge_from, *otherUserAge_to;
BOOL isFromView;
BOOL doUploadPicture, allowToLoad_set;
NSManagedObjectContext *context_settings;
NSManagedObject *managedDataObject_settings;
AppDelegate *delegate_settings;
NSFetchRequest *userDataRequest_settings;
NSArray *userDataArray_settings;
NSString *entityId_settings;



@implementation userSettings
@synthesize name, email, gender, interests, saveButton, myGenderButton_male, myGenderButton_female, myAgeLab, myAgeSlider, otherUserAgeLab_to, otherUserOrient_gay, otherUserAgeLab_from, otherUserGender_male, otherUserAgeSlider_to, otherUserOrient_trans, otherUserGender_female, otherUserAgeSlider_from, otherUserOrient_lesbian, otherUserOrient_straight, myOrientButton_gay, myOrientButton_trans, myOrientButton_lesbian, myOrientButton_straight, userProfileScroll;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    allowToLoad_set= NO;
    
    saveButton.layer.cornerRadius= 18;
    saveButton.clipsToBounds= YES;
    
    //Check if the call is from sign up, if so, then hide the cancel button
    if([_fromView isEqualToString:@"signUp"]||[_fromView isEqualToString:@"facebook"]){
        _cancelButton.hidden= YES;
    }
    else{
        _cancelButton.hidden= NO;
        _cancelButton.layer.cornerRadius= 18;
        _cancelButton.clipsToBounds= YES;
    }

    //Get entityId and username from key chain
    entityId_settings = [SAMKeychain passwordForService:@"entityId" account:@"haloStatus-entityId"];
    //Initialise Core data object
    NSError *error;
    context_settings = [self managedObjectContext];
    userDataRequest_settings= [[NSFetchRequest alloc]initWithEntityName:@"User"];
    userDataRequest_settings.predicate = [NSPredicate predicateWithFormat:@"entityId == %@", entityId_settings];
    userDataArray_settings = [context_settings executeFetchRequest:userDataRequest_settings error:&error];
    
    if([userDataArray_settings count]==0){
        //Nothing
        allowToLoad_set= NO;
    }
    else{
        managedDataObject_settings= [userDataArray_settings objectAtIndex:0];
        [self initialise];
        allowToLoad_set= YES;
    }

    
}

-(void)uploadDP{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uploadCompletedFB:) name:@"uploadCompletedFB" object:nil];
    //Resize image to two different sizes
    //SMALL IMAGE
    //[servBrdge uploadSmallImage:[self resizeImage:profilePicture.image scaleToSize:CGSizeMake(100, 100)] entityId:entityId_settings notiName:@"" name:name.text];
    //[servBrdge uploadLargeImage:[self resizeImage:profilePicture.image scaleToSize:CGSizeMake(400, 400)] entityId:entityId_settings notiName:@"uploadCompletedFB" name:name.text];
    
    [servBrdge uploadSmallImage:[self resizeImage:profilePicture.image reqWidth:100.0 reqHeight:120.0] entityId:entityId_settings notiName:@"uploadCompletedFB" name:name.text];
    
    [servBrdge uploadLargeImage:[self resizeImage:profilePicture.image reqWidth:380.0 reqHeight:400.0] entityId:entityId_settings notiName:@"uploadCompletedFB" name:name.text];
}

-(UIImage *)resizeImage:(UIImage *)image reqWidth:(float)reqWidth reqHeight:(float)reqHeight{
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = reqWidth/reqHeight;
    
    if(imgRatio!=maxRatio){
        if(imgRatio < maxRatio){
            imgRatio = reqHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = reqHeight;
        }
        else{
            imgRatio = reqWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = reqWidth;
        }
    }
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
    
    
    /*UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0);
     [image drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
     UIImage *newImage= UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     return newImage;*/
}

-(void)uploadCompletedFB:(NSNotification*)notificaton{
 
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"uploadCompletedFB" object:nil];
    
    NSString *response= [[notificaton userInfo]objectForKey:@"profilePicConf"];
    
    if([response isEqualToString:@"notSaved"]){
        //Re-upload
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uploadCompletedFB:) name:@"uploadCompletedFB" object:nil];
        [self uploadDP];
    }
    else{
        //Nothing
    }
 }

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    UIImage* userDP= [UIImage imageWithData:[managedDataObject_settings valueForKey:@"userDP"]];
    
    if(userDP == [NSNull null]){
        //Nothing
    }
    else{
        profilePicture.image= userDP;
        profilePicture.layer.cornerRadius= 50;
        profilePicture.clipsToBounds= YES;
        
        //Check if from Facebook, then start uploading images here
        if([_fromView isEqualToString:@"facebook"]){
            if(profilePicture.image){
                //Upload
                [self uploadDP];
            }
            else{
                //Nothing
            }
        }

    }
    
    //Create a notification to know when data is saved
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dataSaved:) name:@"dataSaved" object:nil];
}

-(void)initialise{
    
    //Load information from user class if available
    isFromView= NO;
    
    email.text= [managedDataObject_settings valueForKey:@"email"];
    myAge= [[managedDataObject_settings valueForKey:@"userAge"]intValue];
    otherUserAge_from= [managedDataObject_settings valueForKey:@"otherUserAgeFrom"];
    otherUserAge_to= [managedDataObject_settings valueForKey:@"otherUserAgeTo"];
    
    [_otherOritAnySwitch setOn:NO];
    
    //******************************MY NAME*********************************************************
    if([managedDataObject_settings valueForKey:@"name"]){
        name.text= [managedDataObject_settings valueForKey:@"name"];
    }
    else{
        name.text= @"";
    }
    
    //*******************************MY GENDER**********************************************************
    if([[managedDataObject_settings valueForKey:@"gender"]isEqualToString:@"Female"]){
        [myGenderButton_female setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
        [myGenderButton_male setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        myGender= @"Female";
    }
    else{
        [myGenderButton_female setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [myGenderButton_male setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
        myGender= @"Male";
    }
    
    //*******************************MY ORIENTATION*****************************************************
    if([[managedDataObject_settings valueForKey:@"orientation"]isEqualToString:@"Gay"]){
        //Set this to on
        [myOrientButton_gay setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
        //Set all other to off
        [myOrientButton_lesbian setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [myOrientButton_straight setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [myOrientButton_trans setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        myOrientation= @"Gay";
    }
    else if([[managedDataObject_settings valueForKey:@"orientation"]isEqualToString:@"Lesbian"]){
        //Set this to on
        [myOrientButton_lesbian setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
        //Set all other to off
        [myOrientButton_straight setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [myOrientButton_gay setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [myOrientButton_trans setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        myOrientation= @"Lesbian";
    }
    else if([[managedDataObject_settings valueForKey:@"orientation"]isEqualToString:@"Transgender"]){
        //Set this to on
        [myOrientButton_trans setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
        //Set all other to off
        [myOrientButton_lesbian setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [myOrientButton_gay setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [myOrientButton_straight setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        myOrientation= @"Transgender";
    }
    else if([[managedDataObject_settings valueForKey:@"orientation"]isEqualToString:@"Straight"]){
        //Set this to on
        [myOrientButton_straight setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
        //Set all other to off
        [myOrientButton_lesbian setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [myOrientButton_gay setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [myOrientButton_trans setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        myOrientation= @"Straight";
    }
    else{
        //Undisclosed
        
        myOrientation= @"Not Disclosed";
        
        [_orientationDiscloseSwitch setOn:NO];
        [self orientationDiscloseAction:_orientationDiscloseSwitch];
        
    }
    
    
    //*******************************OTHER USER GENDER************************************************
    if([[managedDataObject_settings valueForKey:@"otherUserGender"]isEqualToString:@"Female"]){
        [otherUserGender_female setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
        [otherUserGender_male setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        otherUserGender= @"Female";
    }
    else{
        [otherUserGender_male setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
        [otherUserGender_female setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        otherUserGender= @"Male";
    }
    
    //*******************************OTHER USER ORIENTATION******************************************
    if([[managedDataObject_settings valueForKey:@"otherUserOrientation"]isEqualToString:@"Gay"]){
        //Set this to on
        [otherUserOrient_gay setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
        //Set all other to off
        [otherUserOrient_straight setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [otherUserOrient_lesbian setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [otherUserOrient_trans setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        otherUserOrientation= @"Gay";
    }
    else if([[managedDataObject_settings valueForKey:@"otherUserOrientation"]isEqualToString:@"Lesbian"]){
        [otherUserOrient_lesbian setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
        //Set all other to off
        [otherUserOrient_straight setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [otherUserOrient_gay setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [otherUserOrient_trans setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        otherUserOrientation= @"Lesbian";
    }
    else if([[managedDataObject_settings valueForKey:@"otherUserOrientation"]isEqualToString:@"Transgender"]){
        [otherUserOrient_trans setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
        //Set all other to off
        [otherUserOrient_straight setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [otherUserOrient_lesbian setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [otherUserOrient_gay setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        otherUserOrientation= @"Transgender";
    }
    else if([[managedDataObject_settings valueForKey:@"otherUserOrientation"]isEqualToString:@"Straight"]){
        [otherUserOrient_straight setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
        //Set all other to off
        [otherUserOrient_gay setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [otherUserOrient_lesbian setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        [otherUserOrient_trans setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
        otherUserOrientation= @"Straight";
    }
    else{
        otherUserOrientation= @"Any";
        
        [_otherOritAnySwitch setOn:YES];
        [self otherOrientationSwitch:_otherOritAnySwitch];
        
        
        
    }
    
    interests.text= [managedDataObject_settings valueForKey:@"interest"];
    
    
    //*******************************MY AGE*********************************************************
    if([managedDataObject_settings valueForKey:@"userAge"]==nil){
        myAgeSlider.value= 18.0;
        myAgeLab.text= @"18";
        myAge= 18;
    }
    else{
        float age= [[managedDataObject_settings valueForKey:@"userAge"]floatValue];
        if(age<18){
            age = 18;
        }
        [myAgeSlider setValue:age animated:YES];
        myAgeLab.text= [NSString stringWithFormat:@"%d",(int)age];
        [myAgeSlider setNeedsDisplay];
        myAge= age;
    }
    
    //*******************************OTHERUSERAGE_FROM***********************************************
    if([[managedDataObject_settings valueForKey:@"otherUserAgeFrom"]isEqualToString:@""] || [managedDataObject_settings valueForKey:@"otherUserAgeFrom"]==nil){
        otherUserAgeSlider_from.value= 18.0;
        otherUserAgeLab_from.text= @"18";
        otherUserAge_from= @"18";
        [managedDataObject_settings setValue:@"18" forKey:@"otherUserAgeFrom"];
    }
    else{
        float age= [[managedDataObject_settings valueForKey:@"otherUserAgeFrom"]floatValue];
        [otherUserAgeSlider_from setValue:age animated:YES];
        otherUserAgeLab_from.text= [managedDataObject_settings valueForKey:@"otherUserAgeFrom"];
        [otherUserAgeSlider_from setNeedsDisplay];
        otherUserAge_from= [NSString stringWithFormat:@"%d",(int)age];
        [managedDataObject_settings setValue:otherUserAge_from forKey:@"otherUserAgeFrom"];
    }
    
    //*******************************OTHERUSERAGE_TO***********************************************
    if([[managedDataObject_settings valueForKey:@"otherUserAgeTo"]isEqualToString:@""] || [managedDataObject_settings valueForKey:@"otherUserAgeTo"]==nil){
        otherUserAgeSlider_to.value= 100.0;
        otherUserAgeLab_to.text= @"100";
        otherUserAge_to = @"100";
        [managedDataObject_settings setValue:@"100" forKey:@"otherUserAgeTo"];
    }
    else{
        float age= [[managedDataObject_settings valueForKey:@"otherUserAgeTo"]floatValue];
        [otherUserAgeSlider_to setValue:age animated:YES];
        otherUserAgeLab_to.text= [managedDataObject_settings valueForKey:@"otherUserAgeTo"];
        [otherUserAgeSlider_to setNeedsDisplay];
        otherUserAge_to= [NSString stringWithFormat:@"%d",(int)age];
        [managedDataObject_settings setValue:otherUserAge_to forKey:@"otherUserAgeTo"];
    }
    
    
    
    servBrdge= [[ServerQueryBridge alloc]init];
    [servBrdge initialiseBackend];
    [servBrdge initialiseCoreData:entityId_settings];
    
    //Initalise other variables and functions
    
    //Tap gesture to remove keyboard
    UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeKeyboard:)];
    tap.cancelsTouchesInView= NO;
    [self.view addGestureRecognizer:tap];
    
    [userProfileScroll setScrollEnabled:YES];
    [userProfileScroll setContentSize:CGSizeMake(300, 1700)];
    
    //Set navigation text colour
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];

    
    //Set save button titles
    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
    saveButton.enabled= YES;
    
    
    myAgeSlider.minimumValue=18;
    myAgeSlider.maximumValue= 100;
    otherUserAgeSlider_from.minimumValue=18;
    otherUserAgeSlider_from.maximumValue= 100;
    otherUserAgeSlider_to.minimumValue=18;
    otherUserAgeSlider_to.maximumValue= 100;
}


- (IBAction)otherUserAgeSliderAction_from:(id)sender {
    int value= roundf(otherUserAgeSlider_from.value * 1.0) * 1;
    otherUserAgeLab_from.text= [NSString stringWithFormat:@"%d",value];
    
    otherUserAge_from= [NSString stringWithFormat:@"%d",value];
}

- (IBAction)otherUserAgeSliderAction_to:(id)sender {
    
    int value= roundf(otherUserAgeSlider_to.value * 1.0) * 1;
    otherUserAgeLab_to.text= [NSString stringWithFormat:@"%d",value];

    otherUserAge_to= [NSString stringWithFormat:@"%d",value];
}

- (IBAction)myAgeSliderAction:(id)sender {
    int value= roundf(myAgeSlider.value * 1.0) * 1;
    myAgeLab.text= [NSString stringWithFormat:@"%d",value];
    myAge= value;
}

- (IBAction)myGenderAction_female:(id)sender {
    NSLog(@"Clicked female button");
    
    //Set my image to checked
    [myGenderButton_female setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
    //Set the male image to unchecked
    [myGenderButton_male setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    
    myGender= @"Female";
}

- (IBAction)myGenderAction_male:(id)sender {
    
    //Set my image to checked
    [myGenderButton_male setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
    //Set the female image to unchecked
    [myGenderButton_female setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    
    myGender= @"Male";
    
}

- (IBAction)myOrientAction_straight:(id)sender {
    
    //Set this to on
    [myOrientButton_straight setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
    //Set all other to off
    [myOrientButton_lesbian setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [myOrientButton_gay setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [myOrientButton_trans setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    
    myOrientation= @"Straight";

}

- (IBAction)myOrientAction_gay:(id)sender {
    
    //Set this to on
    [myOrientButton_gay setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
    //Set all other to off
    [myOrientButton_lesbian setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [myOrientButton_straight setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [myOrientButton_trans setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    
    myOrientation= @"Gay";
}

- (IBAction)myOrientAction_lesbian:(id)sender {
    //Set this to on
    [myOrientButton_lesbian setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
    //Set all other to off
    [myOrientButton_straight setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [myOrientButton_gay setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [myOrientButton_trans setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    
    myOrientation= @"Lesbian";
}

- (IBAction)myOrientAction_trans:(id)sender {
    //Set this to on
    [myOrientButton_trans setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
    //Set all other to off
    [myOrientButton_lesbian setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [myOrientButton_gay setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [myOrientButton_straight setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    
    myOrientation= @"Transgender";
}

- (IBAction)otherUserGenderAction_male:(id)sender {
    
    //Set this to on
    [otherUserGender_male setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
    //Set all other to off
    [otherUserGender_female setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    
    otherUserGender= @"Male";
}

- (IBAction)otherUserGenderAction_female:(id)sender {
    
    //Set this to on
    [otherUserGender_female setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
    //Set all other to off
    [otherUserGender_male setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    otherUserGender= @"Female";
}

- (IBAction)otherUserOrientAction_straight:(id)sender {
    //Set this to on
    [otherUserOrient_straight setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
    //Set all other to off
    [otherUserOrient_lesbian setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [otherUserOrient_gay setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [otherUserOrient_trans setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    
    otherUserOrientation= @"Straight";
    
}

- (IBAction)otherUserOrientAction_gay:(id)sender {
    //Set this to on
    [otherUserOrient_gay setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
    //Set all other to off
    [otherUserOrient_lesbian setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [otherUserOrient_straight setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [otherUserOrient_trans setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    
    otherUserOrientation= @"Gay";
}

- (IBAction)otherUserOrientAction_lesbian:(id)sender {
    //Set this to on
    [otherUserOrient_lesbian setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
    //Set all other to off
    [otherUserOrient_straight setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [otherUserOrient_gay setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [otherUserOrient_trans setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    
    otherUserOrientation= @"Lesbian";
}

- (IBAction)otherUserOrientAction_trans:(id)sender {
    //Set this to on
    [otherUserOrient_trans setImage: [UIImage imageNamed:@"radioButton_checked.png"]forState:UIControlStateNormal];
    //Set all other to off
    [otherUserOrient_lesbian setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [otherUserOrient_gay setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    [otherUserOrient_straight setImage: [UIImage imageNamed:@"radioButton_unchecked.png"]forState:UIControlStateNormal];
    
    otherUserOrientation= @"Transgender";
}

-(void)dataSaved:(NSNotification*)notification{
    saveButton.enabled= YES;

    //Post notitifcation for Sign up, which will only get picked up if this page was started from there
    [[NSNotificationCenter defaultCenter] postNotificationName:@"signUpCompleted" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"fbAccessSignUpCompleted" object:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



-(void)loadDP{
    
    //Check if image exists, if not, then display default image
    imageData= [managedDataObject_settings valueForKey:@"userDP"];
    UIImage *image= [UIImage imageWithData:imageData];
    
    if(image){
        [profilePicture setImage:image];
    }
    else{
        //Get pic from server
        //Create a notification for server to come back to
        //[[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(profilePictureLoaded:) name:@"profilePictureLoaded" object:nil];
        //[servBrdge downloadProfilePicture:[[userInfo getEntityId]stringByAppendingString:@"Large.png"]];
        
    }

}


-(void)profilePictureLoaded:(NSNotification*)notification{
    
    UIImage* image= [[notification userInfo]objectForKey:@"profilePicture"];
    
    if(image){
        [profilePicture setImage:image];
        [managedDataObject_settings setValue:UIImagePNGRepresentation(image) forKey:@"userDP"];
        [self saveData];
    }
    else{
        //Display default
        UIImage *imageDefault= [UIImage imageNamed:@"no_image_profile.png"];
        [profilePicture setImage:imageDefault];
    }
    
}



//****************************************************************************************

-(void) closeKeyboard: (UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
}

- (IBAction)logoutAction:(id)sender {
    [servBrdge logOutUser];
    
    AppDelegate *appDelegateTemp= (AppDelegate*)[[UIApplication sharedApplication]delegate];
    UIViewController* rootController= [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]]instantiateViewControllerWithIdentifier:@"loginController"];
    UINavigationController* navigation= [[UINavigationController alloc]initWithRootViewController:rootController];
    appDelegateTemp.window.rootViewController= navigation;
    
    //Notify user profile that user has logged out
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logoutStopAction" object:self];
}
  

- (IBAction)cancelMyProfileAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)orientationDiscloseAction:(id)sender {
    
    UISwitch *mySwitch = (UISwitch *)sender;
    if(mySwitch.tag == 1){
        if ([mySwitch isOn]) {
            _myOrientationTitle.text= @"Orientation (Disclosed)";
            
            //Reset to Straight
            [self myOrientAction_straight:0];
            [myOrientButton_straight setEnabled:YES];
            [myOrientButton_gay setEnabled:YES];
            [myOrientButton_lesbian setEnabled:YES];
            [myOrientButton_trans setEnabled:YES];
            
        } else {
            _myOrientationTitle.text= @"Orientation (Undisclosed)";
            
            //Set all boxes to unchecked
            [myOrientButton_straight setImage:[UIImage imageNamed:@"radioButton_unchecked.png"] forState:UIControlStateNormal];
            [myOrientButton_gay setImage:[UIImage imageNamed:@"radioButton_unchecked.png"] forState:UIControlStateNormal];
            [myOrientButton_lesbian setImage:[UIImage imageNamed:@"radioButton_unchecked.png"] forState:UIControlStateNormal];
            [myOrientButton_trans setImage:[UIImage imageNamed:@"radioButton_unchecked.png"] forState:UIControlStateNormal];
            [myOrientButton_straight setEnabled:NO];
            [myOrientButton_gay setEnabled:NO];
            [myOrientButton_lesbian setEnabled:NO];
            [myOrientButton_trans setEnabled:NO];
            
            myOrientation= @"Not Disclosed";
        }
    }
    else{
        //Nothing
    }
    
}

- (IBAction)otherOrientationSwitch:(id)sender {
    
    UISwitch *mySwitch = (UISwitch *)sender;
    
    if(mySwitch.tag == 2){
        if ([mySwitch isOn]) {
            
            //Set all boxes to unchecked
            [otherUserOrient_straight setImage:[UIImage imageNamed:@"radioButton_unchecked.png"] forState:UIControlStateNormal];
            [otherUserOrient_gay setImage:[UIImage imageNamed:@"radioButton_unchecked.png"] forState:UIControlStateNormal];
            [otherUserOrient_lesbian setImage:[UIImage imageNamed:@"radioButton_unchecked.png"] forState:UIControlStateNormal];
            [otherUserOrient_trans setImage:[UIImage imageNamed:@"radioButton_unchecked.png"] forState:UIControlStateNormal];
            [otherUserOrient_straight setEnabled:NO];
            [otherUserOrient_gay setEnabled:NO];
            [otherUserOrient_lesbian setEnabled:NO];
            [otherUserOrient_trans setEnabled:NO];
            
            otherUserOrientation = @"Any";
            
        } else {
            
            //Reset to Straight
            [self otherUserOrientAction_straight:0];
            [otherUserOrient_straight setEnabled:YES];
            [otherUserOrient_gay setEnabled:YES];
            [otherUserOrient_lesbian setEnabled:YES];
            [otherUserOrient_trans setEnabled:YES];
        }

    }
    else{
        //Nothing
    }
    
    
}

  
-(void)saveData{
    NSError *error;
    if(![context_settings save:&error]){
        NSLog(@"FAILED TO SAVE NEW USER:%@",error);
    }
    else{
        NSLog(@"SAVED TO DEVICE");
    }
}

  
- (IBAction)saveProfile:(id)sender{
    
    if([name.text isEqualToString:@""]|| [myGender isEqualToString:@""] || [myOrientation isEqualToString:@""]||[interests.text isEqualToString:@""]||[otherUserGender isEqualToString:@""]||[otherUserOrientation isEqualToString:@""]){
        //Error
        [self errorAlert:@"No fields can be left empty, please fill all the fields."];
    }
    else{
        
        if(profilePicture.image){
            if(myAge < 18){
                myAge= 18;
            }
            
            //Save all fields
            [managedDataObject_settings setValue:name.text forKey:@"name"];
            [managedDataObject_settings setValue:[NSNumber numberWithInt:myAge] forKey:@"userAge"];
            [managedDataObject_settings setValue:myGender forKey:@"gender"];
            [managedDataObject_settings setValue:myOrientation forKey:@"orientation"];
            [managedDataObject_settings setValue:interests.text forKey:@"interest"];
            [managedDataObject_settings setValue:otherUserGender forKey:@"otherUserGender"];
            [managedDataObject_settings setValue:otherUserOrientation forKey:@"otherUserOrientation"];
            [managedDataObject_settings setValue:otherUserAge_from forKey:@"otherUserAgeFrom"];
            [managedDataObject_settings setValue:otherUserAge_to forKey:@"otherUserAgeTo"];
            
            //Save to device
            [self saveData];
            //Save to server
            [servBrdge saveData:@"dataSaved" entityId:entityId_settings];

        }
        else{
            [self errorAlert:@"Display picture cannot be empty, please select a display picture"];
        }
        
    }
    
}
  
-(void)errorAlert:(NSString*)message{
    UIAlertController *alert= [UIAlertController alertControllerWithTitle:@"ERROR" message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* okButton= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];

}


@end
