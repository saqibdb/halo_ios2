//
//  myStatCell.h
//  LookUp
//
//  Created by Divey Punj on 30/5/17.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface myStatCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *friendNameLab;
@property (strong, nonatomic) IBOutlet UILabel *venueLab;
@property (strong, nonatomic) IBOutlet UILabel *friendStatusLab;
@property (strong, nonatomic) IBOutlet UILabel *connectDateLab;
@property (strong, nonatomic) IBOutlet UIImageView *friendStatusImg;


-(void)setCell: (NSString*)name status:(NSString*)status venueName:(NSString*)venueName timeStamp:(NSString*)timeStamp;

@end
