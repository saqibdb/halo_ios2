//
//  chatMessageHistory.h
//  LookUp
//
//  Created by Divey Punj on 17/04/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Sinch/Sinch.h>

@interface chatMessageHistory : NSObject <SINMessage>

@property (nonatomic, strong)NSString* messageId;
@property (nonatomic, strong)NSArray* recipientIds;
@property (nonatomic, strong)NSString* senderId;
@property (nonatomic, strong)NSString* text;
@property (nonatomic, strong)NSDictionary* headers;
@property (nonatomic, strong)NSDate* timestamp;



@end
