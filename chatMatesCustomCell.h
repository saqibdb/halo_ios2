//
//  chatMatesCustomCell.h
//  LookUp
//
//  Created by Divey Punj on 15/04/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface chatMatesCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *chatMateNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *chatMatePhoto;
@property (strong, nonatomic) IBOutlet UILabel *messageContactBorder;
@property (strong, nonatomic) IBOutlet UILabel *messagePreviewLab;
@property (strong, nonatomic) IBOutlet UIButton *removeChatMateButton;
@property (strong, nonatomic) IBOutlet UIImageView *messNewInd;
@property (strong, nonatomic) IBOutlet UIButton *acceptBut;
@property (strong, nonatomic) IBOutlet UIButton *rejectBut;



- (IBAction)removeChatMateAction:(id)sender;
- (IBAction)acceptAction:(id)sender;
- (IBAction)rejectAction:(id)sender;


- (void) setCell : (NSString *)chatMateName imageUrl: (NSString*)imageUrl entityId:(NSString*)eID prevMessage: (NSString*)prevMessage showRemove:(BOOL)showRemove indexRow:(NSInteger)indexRow isNewMessage:(BOOL)isNewMessage mateStatus:(NSString*)mateStatus;

@end
