//
//  barcodeView.h
//  LookUp
//
//  Created by Divey Punj on 12/05/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZXingObjC/ZXingObjC.h>
#import "camOverlayController.h"
#import "AppDelegate.h"
#import "ServerQueryBridge.h"

@interface barcodeView : UIViewController <UIImagePickerControllerDelegate>

{
    UIImagePickerController *barCodePhotoTake;
}

@property (strong, nonatomic) IBOutlet UIImageView *barcodeImg;
@property (strong, nonatomic) IBOutlet UILabel *bcInfoLab;
@property (strong, nonatomic) IBOutlet UIButton *addFriendBut;
@property (strong, nonatomic) IBOutlet UIView *progressView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;
@property (strong, nonatomic) IBOutlet UILabel *progressLab;
@property (strong, nonatomic) NSString *scannedStr;

- (IBAction)addedMeAction:(id)sender;
- (IBAction)addFriendAction:(id)sender;


@end
